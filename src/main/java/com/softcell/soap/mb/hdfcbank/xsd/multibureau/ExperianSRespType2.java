/**
 * ExperianSRespType2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ExperianSRespType2 bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExperianSRespType2 implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ExperianSRespType2
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for CAPSREPORTTIME
     */
    protected java.lang.String localCAPSREPORTTIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSREPORTTIMETracker = false;

    /**
     * field for CAPSREPORTNUMBER
     */
    protected java.lang.String localCAPSREPORTNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSREPORTNUMBERTracker = false;

    /**
     * field for ENQUIRY_REASON_CODE
     */
    protected java.lang.String localENQUIRY_REASON_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_REASON_CODETracker = false;

    /**
     * field for CAPSENQUIRYREASON
     */
    protected java.lang.String localCAPSENQUIRYREASON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSENQUIRYREASONTracker = false;

    /**
     * field for FINANCE_PURPOSE_CODE
     */
    protected java.lang.String localFINANCE_PURPOSE_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFINANCE_PURPOSE_CODETracker = false;

    /**
     * field for CAPSFINANCEPURPOSE
     */
    protected java.lang.String localCAPSFINANCEPURPOSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSFINANCEPURPOSETracker = false;

    /**
     * field for CAPSAMOUNTFINANCED
     */
    protected java.lang.String localCAPSAMOUNTFINANCED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAMOUNTFINANCEDTracker = false;

    /**
     * field for CAPSDURATIONOFAGREEMENT
     */
    protected java.lang.String localCAPSDURATIONOFAGREEMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSDURATIONOFAGREEMENTTracker = false;

    /**
     * field for CAPSAPPLICANTLASTNAME
     */
    protected java.lang.String localCAPSAPPLICANTLASTNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTLASTNAMETracker = false;

    /**
     * field for CAPSAPPLICANTFIRSTNAME
     */
    protected java.lang.String localCAPSAPPLICANTFIRSTNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTFIRSTNAMETracker = false;

    /**
     * field for CAPSAPPLICANTMIDDLENAME1
     */
    protected java.lang.String localCAPSAPPLICANTMIDDLENAME1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTMIDDLENAME1Tracker = false;

    /**
     * field for CAPSAPPLICANTMIDDLENAME2
     */
    protected java.lang.String localCAPSAPPLICANTMIDDLENAME2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTMIDDLENAME2Tracker = false;

    /**
     * field for CAPSAPPLICANTMIDDLENAME3
     */
    protected java.lang.String localCAPSAPPLICANTMIDDLENAME3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTMIDDLENAME3Tracker = false;

    /**
     * field for CAPSAPPLICANTGENDERCODE
     */
    protected java.lang.String localCAPSAPPLICANTGENDERCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTGENDERCODETracker = false;

    /**
     * field for CAPSAPPLICANTGENDER
     */
    protected java.lang.String localCAPSAPPLICANTGENDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTGENDERTracker = false;

    /**
     * field for CAPSINCOME_TAX_PAN
     */
    protected java.lang.String localCAPSINCOME_TAX_PAN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSINCOME_TAX_PANTracker = false;

    /**
     * field for CAPSPAN_ISSUE_DATE
     */
    protected java.lang.String localCAPSPAN_ISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSPAN_ISSUE_DATETracker = false;

    /**
     * field for CAPSPAN_EXP_DATE
     */
    protected java.lang.String localCAPSPAN_EXP_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSPAN_EXP_DATETracker = false;

    /**
     * field for CAPSPASSPORT_NUMBER
     */
    protected java.lang.String localCAPSPASSPORT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSPASSPORT_NUMBERTracker = false;

    /**
     * field for CAPSPASSPORT_ISSUE_DATE
     */
    protected java.lang.String localCAPSPASSPORT_ISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSPASSPORT_ISSUE_DATETracker = false;

    /**
     * field for CAPSPASSPORT_EXP_DATE
     */
    protected java.lang.String localCAPSPASSPORT_EXP_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSPASSPORT_EXP_DATETracker = false;

    /**
     * field for CAPSVOTER_IDENTITY_CARD
     */
    protected java.lang.String localCAPSVOTER_IDENTITY_CARD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSVOTER_IDENTITY_CARDTracker = false;

    /**
     * field for CAPSVOTER_ID_ISSUE_DATE
     */
    protected java.lang.String localCAPSVOTER_ID_ISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSVOTER_ID_ISSUE_DATETracker = false;

    /**
     * field for CAPSVOTER_ID_EXP_DATE
     */
    protected java.lang.String localCAPSVOTER_ID_EXP_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSVOTER_ID_EXP_DATETracker = false;

    /**
     * field for CAPSDRIVER_LICENSE_NUMBER
     */
    protected java.lang.String localCAPSDRIVER_LICENSE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSDRIVER_LICENSE_NUMBERTracker = false;

    /**
     * field for CAPSDRIVER_LICENSE_ISSUE_DATE
     */
    protected java.lang.String localCAPSDRIVER_LICENSE_ISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSDRIVER_LICENSE_ISSUE_DATETracker = false;

    /**
     * field for CAPSDRIVER_LICENSE_EXP_DATE
     */
    protected java.lang.String localCAPSDRIVER_LICENSE_EXP_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSDRIVER_LICENSE_EXP_DATETracker = false;

    /**
     * field for CAPSRATION_CARD_NUMBER
     */
    protected java.lang.String localCAPSRATION_CARD_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSRATION_CARD_NUMBERTracker = false;

    /**
     * field for CAPSRATION_CARD_ISSUE_DATE
     */
    protected java.lang.String localCAPSRATION_CARD_ISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSRATION_CARD_ISSUE_DATETracker = false;

    /**
     * field for CAPSRATION_CARD_EXP_DATE
     */
    protected java.lang.String localCAPSRATION_CARD_EXP_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSRATION_CARD_EXP_DATETracker = false;

    /**
     * field for CAPSUNIVERSAL_ID_NUMBER
     */
    protected java.lang.String localCAPSUNIVERSAL_ID_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSUNIVERSAL_ID_NUMBERTracker = false;

    /**
     * field for CAPSUNIVERSAL_ID_ISSUE_DATE
     */
    protected java.lang.String localCAPSUNIVERSAL_ID_ISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSUNIVERSAL_ID_ISSUE_DATETracker = false;

    /**
     * field for CAPSUNIVERSAL_ID_EXP_DATE
     */
    protected java.lang.String localCAPSUNIVERSAL_ID_EXP_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSUNIVERSAL_ID_EXP_DATETracker = false;

    /**
     * field for CAPSAPPLICANTDOBAPPLICANT
     */
    protected java.lang.String localCAPSAPPLICANTDOBAPPLICANT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTDOBAPPLICANTTracker = false;

    /**
     * field for CAPSAPPLICANTTELNOAPPLICANT1ST
     */
    protected java.lang.String localCAPSAPPLICANTTELNOAPPLICANT1ST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTTELNOAPPLICANT1STTracker = false;

    /**
     * field for CAPSAPPLICANTTELEXT
     */
    protected java.lang.String localCAPSAPPLICANTTELEXT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTTELEXTTracker = false;

    /**
     * field for CAPSAPPLICANTTELTYPE
     */
    protected java.lang.String localCAPSAPPLICANTTELTYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTTELTYPETracker = false;

    /**
     * field for CAPSAPPLICANTMOBILEPHONENUMBER
     */
    protected java.lang.String localCAPSAPPLICANTMOBILEPHONENUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTMOBILEPHONENUMBERTracker = false;

    /**
     * field for CAPSAPPLICANTEMAILID
     */
    protected java.lang.String localCAPSAPPLICANTEMAILID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTEMAILIDTracker = false;

    /**
     * field for CAPSAPPLICANTINCOME
     */
    protected java.lang.String localCAPSAPPLICANTINCOME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTINCOMETracker = false;

    /**
     * field for APPLICANT_MARITALSTATUSCODE
     */
    protected java.lang.String localAPPLICANT_MARITALSTATUSCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAPPLICANT_MARITALSTATUSCODETracker = false;

    /**
     * field for CAPSAPPLICANTMARITALSTATUS
     */
    protected java.lang.String localCAPSAPPLICANTMARITALSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTMARITALSTATUSTracker = false;

    /**
     * field for CAPSAPPLICANTEMPLMTSTATUSCODE
     */
    protected java.lang.String localCAPSAPPLICANTEMPLMTSTATUSCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTEMPLMTSTATUSCODETracker = false;

    /**
     * field for CAPSAPPLICANTEMPLOYMENTSTATUS
     */
    protected java.lang.String localCAPSAPPLICANTEMPLOYMENTSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTEMPLOYMENTSTATUSTracker = false;

    /**
     * field for CAPSAPPLICANTDATEWITHEMPLOYER
     */
    protected java.lang.String localCAPSAPPLICANTDATEWITHEMPLOYER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTDATEWITHEMPLOYERTracker = false;

    /**
     * field for CAPSAPPLTNOMAJORCRDTCARDHELD
     */
    protected java.lang.String localCAPSAPPLTNOMAJORCRDTCARDHELD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLTNOMAJORCRDTCARDHELDTracker = false;

    /**
     * field for CAPSAPPLTFLATNOPLOTNOHOUSENO
     */
    protected java.lang.String localCAPSAPPLTFLATNOPLOTNOHOUSENO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLTFLATNOPLOTNOHOUSENOTracker = false;

    /**
     * field for CAPSAPPLICANTBLDGNOSOCIETYNAME
     */
    protected java.lang.String localCAPSAPPLICANTBLDGNOSOCIETYNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTBLDGNOSOCIETYNAMETracker = false;

    /**
     * field for CAPSAPPLTRDNONAMEAREALOCALITY
     */
    protected java.lang.String localCAPSAPPLTRDNONAMEAREALOCALITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLTRDNONAMEAREALOCALITYTracker = false;

    /**
     * field for CAPSAPPLICANTCITY
     */
    protected java.lang.String localCAPSAPPLICANTCITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTCITYTracker = false;

    /**
     * field for CAPSAPPLICANTLANDMARK
     */
    protected java.lang.String localCAPSAPPLICANTLANDMARK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTLANDMARKTracker = false;

    /**
     * field for CAPSAPPLICANTSTATECODE
     */
    protected java.lang.String localCAPSAPPLICANTSTATECODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTSTATECODETracker = false;

    /**
     * field for CAPSAPPLICANTSTATE
     */
    protected java.lang.String localCAPSAPPLICANTSTATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTSTATETracker = false;

    /**
     * field for CAPSAPPLICANTPINCODE
     */
    protected java.lang.String localCAPSAPPLICANTPINCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTPINCODETracker = false;

    /**
     * field for CAPSAPPLICANTCOUNTRYCODE
     */
    protected java.lang.String localCAPSAPPLICANTCOUNTRYCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSAPPLICANTCOUNTRYCODETracker = false;

    /**
     * field for BUREAUSCORE
     */
    protected java.lang.String localBUREAUSCORE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBUREAUSCORETracker = false;

    /**
     * field for BUREAUSCORECONFIDLEVEL
     */
    protected java.lang.String localBUREAUSCORECONFIDLEVEL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBUREAUSCORECONFIDLEVELTracker = false;

    /**
     * field for CREDITRATING
     */
    protected java.lang.String localCREDITRATING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITRATINGTracker = false;

    /**
     * field for TNOFBFHLCADEXHL
     */
    protected java.lang.String localTNOFBFHLCADEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFBFHLCADEXHLTracker = false;

    /**
     * field for TOTVALOFBFHLCAD
     */
    protected java.lang.String localTOTVALOFBFHLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFBFHLCADTracker = false;

    /**
     * field for MNTSMRBFHLCAD
     */
    protected java.lang.String localMNTSMRBFHLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRBFHLCADTracker = false;

    /**
     * field for TNOFHLCAD
     */
    protected java.lang.String localTNOFHLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFHLCADTracker = false;

    /**
     * field for TOTVALOFHLCAD
     */
    protected java.lang.String localTOTVALOFHLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFHLCADTracker = false;

    /**
     * field for MNTSMRHLCAD
     */
    protected java.lang.String localMNTSMRHLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRHLCADTracker = false;

    /**
     * field for TNOFTELCOSCAD
     */
    protected java.lang.String localTNOFTELCOSCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFTELCOSCADTracker = false;

    /**
     * field for TOTVALOFTELCOSCAD
     */
    protected java.lang.String localTOTVALOFTELCOSCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFTELCOSCADTracker = false;

    /**
     * field for MNTSMRTELCOSCAD
     */
    protected java.lang.String localMNTSMRTELCOSCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRTELCOSCADTracker = false;

    /**
     * field for TNOFMFCAD
     */
    protected java.lang.String localTNOFMFCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFMFCADTracker = false;

    /**
     * field for TOTVALOFMFCAD
     */
    protected java.lang.String localTOTVALOFMFCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFMFCADTracker = false;

    /**
     * field for MNTSMRMFCAD
     */
    protected java.lang.String localMNTSMRMFCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRMFCADTracker = false;

    /**
     * field for TNOFRETAILCAD
     */
    protected java.lang.String localTNOFRETAILCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFRETAILCADTracker = false;

    /**
     * field for TOTVALOFRETAILCAD
     */
    protected java.lang.String localTOTVALOFRETAILCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFRETAILCADTracker = false;

    /**
     * field for MNTSMRRETAILCAD
     */
    protected java.lang.String localMNTSMRRETAILCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRRETAILCADTracker = false;

    /**
     * field for TNOFALLCAD
     */
    protected java.lang.String localTNOFALLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFALLCADTracker = false;

    /**
     * field for TOTVALOFALLCAD
     */
    protected java.lang.String localTOTVALOFALLCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFALLCADTracker = false;

    /**
     * field for MNTSMRCADALL
     */
    protected java.lang.String localMNTSMRCADALL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRCADALLTracker = false;

    /**
     * field for TNOFBFHLACAEXHL
     */
    protected java.lang.String localTNOFBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFBFHLACAEXHLTracker = false;

    /**
     * field for BALBFHLACAEXHL
     */
    protected java.lang.String localBALBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBALBFHLACAEXHLTracker = false;

    /**
     * field for WCDSTBFHLACAEXHL
     */
    protected java.lang.String localWCDSTBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTBFHLACAEXHLTracker = false;

    /**
     * field for WDSPR6MNTBFHLACAEXHL
     */
    protected java.lang.String localWDSPR6MNTBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR6MNTBFHLACAEXHLTracker = false;

    /**
     * field for WDSPR712MNTBFHLACAEXHL
     */
    protected java.lang.String localWDSPR712MNTBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR712MNTBFHLACAEXHLTracker = false;

    /**
     * field for AGEOFOLDESTBFHLACAEXHL
     */
    protected java.lang.String localAGEOFOLDESTBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTBFHLACAEXHLTracker = false;

    /**
     * field for HCBPERREVACCBFHLACAEXHL
     */
    protected java.lang.String localHCBPERREVACCBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHCBPERREVACCBFHLACAEXHLTracker = false;

    /**
     * field for TCBPERREVACCBFHLACAEXHL
     */
    protected java.lang.String localTCBPERREVACCBFHLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTCBPERREVACCBFHLACAEXHLTracker = false;

    /**
     * field for TNOFHLACA
     */
    protected java.lang.String localTNOFHLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFHLACATracker = false;

    /**
     * field for BALHLACA
     */
    protected java.lang.String localBALHLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBALHLACATracker = false;

    /**
     * field for WCDSTHLACA
     */
    protected java.lang.String localWCDSTHLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTHLACATracker = false;

    /**
     * field for WDSPR6MNTHLACA
     */
    protected java.lang.String localWDSPR6MNTHLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR6MNTHLACATracker = false;

    /**
     * field for WDSPR712MNTHLACA
     */
    protected java.lang.String localWDSPR712MNTHLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR712MNTHLACATracker = false;

    /**
     * field for AGEOFOLDESTHLACA
     */
    protected java.lang.String localAGEOFOLDESTHLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTHLACATracker = false;

    /**
     * field for TNOFMFACA
     */
    protected java.lang.String localTNOFMFACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFMFACATracker = false;

    /**
     * field for TOTALBALMFACA
     */
    protected java.lang.String localTOTALBALMFACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALBALMFACATracker = false;

    /**
     * field for WCDSTMFACA
     */
    protected java.lang.String localWCDSTMFACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTMFACATracker = false;

    /**
     * field for WDSPR6MNTMFACA
     */
    protected java.lang.String localWDSPR6MNTMFACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR6MNTMFACATracker = false;

    /**
     * field for WDSPR712MNTMFACA
     */
    protected java.lang.String localWDSPR712MNTMFACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR712MNTMFACATracker = false;

    /**
     * field for AGEOFOLDESTMFACA
     */
    protected java.lang.String localAGEOFOLDESTMFACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTMFACATracker = false;

    /**
     * field for TNOFTELCOSACA
     */
    protected java.lang.String localTNOFTELCOSACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFTELCOSACATracker = false;

    /**
     * field for TOTALBALTELCOSACA
     */
    protected java.lang.String localTOTALBALTELCOSACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALBALTELCOSACATracker = false;

    /**
     * field for WCDSTTELCOSACA
     */
    protected java.lang.String localWCDSTTELCOSACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTTELCOSACATracker = false;

    /**
     * field for WDSPR6MNTTELCOSACA
     */
    protected java.lang.String localWDSPR6MNTTELCOSACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR6MNTTELCOSACATracker = false;

    /**
     * field for WDSPR712MNTTELCOSACA
     */
    protected java.lang.String localWDSPR712MNTTELCOSACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR712MNTTELCOSACATracker = false;

    /**
     * field for AGEOFOLDESTTELCOSACA
     */
    protected java.lang.String localAGEOFOLDESTTELCOSACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTTELCOSACATracker = false;

    /**
     * field for TNOFRETAILACA
     */
    protected java.lang.String localTNOFRETAILACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFRETAILACATracker = false;

    /**
     * field for TOTALBALRETAILACA
     */
    protected java.lang.String localTOTALBALRETAILACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALBALRETAILACATracker = false;

    /**
     * field for WCDSTRETAILACA
     */
    protected java.lang.String localWCDSTRETAILACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTRETAILACATracker = false;

    /**
     * field for WDSPR6MNTRETAILACA
     */
    protected java.lang.String localWDSPR6MNTRETAILACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR6MNTRETAILACATracker = false;

    /**
     * field for WDSPR712MNTRETAILACA
     */
    protected java.lang.String localWDSPR712MNTRETAILACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR712MNTRETAILACATracker = false;

    /**
     * field for AGEOFOLDESTRETAILACA
     */
    protected java.lang.String localAGEOFOLDESTRETAILACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTRETAILACATracker = false;

    /**
     * field for HCBLMPERREVACCRET
     */
    protected java.lang.String localHCBLMPERREVACCRET;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHCBLMPERREVACCRETTracker = false;

    /**
     * field for TOTCURBALLMPERREVACCRET
     */
    protected java.lang.String localTOTCURBALLMPERREVACCRET;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTCURBALLMPERREVACCRETTracker = false;

    /**
     * field for TNOFALLACA
     */
    protected java.lang.String localTNOFALLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFALLACATracker = false;

    /**
     * field for BALALLACAEXHL
     */
    protected java.lang.String localBALALLACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBALALLACAEXHLTracker = false;

    /**
     * field for WCDSTALLACA
     */
    protected java.lang.String localWCDSTALLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTALLACATracker = false;

    /**
     * field for WDSPR6MNTALLACA
     */
    protected java.lang.String localWDSPR6MNTALLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR6MNTALLACATracker = false;

    /**
     * field for WDSPR712MNTALLACA
     */
    protected java.lang.String localWDSPR712MNTALLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWDSPR712MNTALLACATracker = false;

    /**
     * field for AGEOFOLDESTALLACA
     */
    protected java.lang.String localAGEOFOLDESTALLACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTALLACATracker = false;

    /**
     * field for TNOFNDELBFHLINACAEXHL
     */
    protected java.lang.String localTNOFNDELBFHLINACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFNDELBFHLINACAEXHLTracker = false;

    /**
     * field for TNOFDELBFHLINACAEXHL
     */
    protected java.lang.String localTNOFDELBFHLINACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFDELBFHLINACAEXHLTracker = false;

    /**
     * field for TNOFNDELHLINACA
     */
    protected java.lang.String localTNOFNDELHLINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFNDELHLINACATracker = false;

    /**
     * field for TNOFDELHLINACA
     */
    protected java.lang.String localTNOFDELHLINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFDELHLINACATracker = false;

    /**
     * field for TNOFNDELMFINACA
     */
    protected java.lang.String localTNOFNDELMFINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFNDELMFINACATracker = false;

    /**
     * field for TNOFDELMFINACA
     */
    protected java.lang.String localTNOFDELMFINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFDELMFINACATracker = false;

    /**
     * field for TNOFNDELTELCOSINACA
     */
    protected java.lang.String localTNOFNDELTELCOSINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFNDELTELCOSINACATracker = false;

    /**
     * field for TNOFDELTELCOSINACA
     */
    protected java.lang.String localTNOFDELTELCOSINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFDELTELCOSINACATracker = false;

    /**
     * field for TNOFNDELRETAILINACA
     */
    protected java.lang.String localTNOFNDELRETAILINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFNDELRETAILINACATracker = false;

    /**
     * field for TNOFDELRETAILINACA
     */
    protected java.lang.String localTNOFDELRETAILINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFDELRETAILINACATracker = false;

    /**
     * field for BFHLCAPSLAST90DAYS
     */
    protected java.lang.String localBFHLCAPSLAST90DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBFHLCAPSLAST90DAYSTracker = false;

    /**
     * field for MFCAPSLAST90DAYS
     */
    protected java.lang.String localMFCAPSLAST90DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMFCAPSLAST90DAYSTracker = false;

    /**
     * field for TELCOSCAPSLAST90DAYS
     */
    protected java.lang.String localTELCOSCAPSLAST90DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELCOSCAPSLAST90DAYSTracker = false;

    /**
     * field for RETAILCAPSLAST90DAYS
     */
    protected java.lang.String localRETAILCAPSLAST90DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRETAILCAPSLAST90DAYSTracker = false;

    /**
     * field for TNOFOCOMCAD
     */
    protected java.lang.String localTNOFOCOMCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFOCOMCADTracker = false;

    /**
     * field for TOTVALOFOCOMCAD
     */
    protected java.lang.String localTOTVALOFOCOMCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTVALOFOCOMCADTracker = false;

    /**
     * field for MNTSMROCOMCAD
     */
    protected java.lang.String localMNTSMROCOMCAD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMROCOMCADTracker = false;

    /**
     * field for TNOFOCOMACA
     */
    protected java.lang.String localTNOFOCOMACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFOCOMACATracker = false;

    /**
     * field for BALOCOMACAEXHL
     */
    protected java.lang.String localBALOCOMACAEXHL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBALOCOMACAEXHLTracker = false;

    /**
     * field for BALOCOMACAHLONLY
     */
    protected java.lang.String localBALOCOMACAHLONLY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBALOCOMACAHLONLYTracker = false;

    /**
     * field for WCDSTOCOMACA
     */
    protected java.lang.String localWCDSTOCOMACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWCDSTOCOMACATracker = false;

    /**
     * field for HCBLMPERREVOCOMACA
     */
    protected java.lang.String localHCBLMPERREVOCOMACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHCBLMPERREVOCOMACATracker = false;

    /**
     * field for TNOFNDELOCOMINACA
     */
    protected java.lang.String localTNOFNDELOCOMINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFNDELOCOMINACATracker = false;

    /**
     * field for TNOFDELOCOMINACA
     */
    protected java.lang.String localTNOFDELOCOMINACA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFDELOCOMINACATracker = false;

    /**
     * field for TNOFOCOMCAPSLAST90DAYS
     */
    protected java.lang.String localTNOFOCOMCAPSLAST90DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFOCOMCAPSLAST90DAYSTracker = false;

    /**
     * field for ANYRELCBDATADISYN
     */
    protected java.lang.String localANYRELCBDATADISYN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localANYRELCBDATADISYNTracker = false;

    /**
     * field for OTHRELCBDFCPOSMATYN
     */
    protected java.lang.String localOTHRELCBDFCPOSMATYN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOTHRELCBDFCPOSMATYNTracker = false;

    /**
     * field for TNOFCADCLASSEDASSFWDWO
     */
    protected java.lang.String localTNOFCADCLASSEDASSFWDWO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTNOFCADCLASSEDASSFWDWOTracker = false;

    /**
     * field for MNTSMRCADCLASSEDASSFWDWO
     */
    protected java.lang.String localMNTSMRCADCLASSEDASSFWDWO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTSMRCADCLASSEDASSFWDWOTracker = false;

    /**
     * field for NUMOFCADSFWDWOLAST24MNT
     */
    protected java.lang.String localNUMOFCADSFWDWOLAST24MNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNUMOFCADSFWDWOLAST24MNTTracker = false;

    /**
     * field for TOTCURBALLIVESACC
     */
    protected java.lang.String localTOTCURBALLIVESACC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTCURBALLIVESACCTracker = false;

    /**
     * field for TOTCURBALLIVEUACC
     */
    protected java.lang.String localTOTCURBALLIVEUACC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTCURBALLIVEUACCTracker = false;

    /**
     * field for TOTCURBALMAXBALLIVESACC
     */
    protected java.lang.String localTOTCURBALMAXBALLIVESACC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTCURBALMAXBALLIVESACCTracker = false;

    /**
     * field for TOTCURBALMAXBALLIVEUACC
     */
    protected java.lang.String localTOTCURBALMAXBALLIVEUACC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTCURBALMAXBALLIVEUACCTracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_DATE
     */
    protected java.lang.String localOUTPUT_WRITE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_DATETracker = false;

    /**
     * field for OUTPUT_READ_DATE
     */
    protected java.lang.String localOUTPUT_READ_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_DATETracker = false;

    /**
     * field for DATEOFADDITION
     */
    protected java.lang.String localDATEOFADDITION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOFADDITIONTracker = false;

    /**
     * field for ACCOUNT_KEY
     */
    protected java.lang.String localACCOUNT_KEY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_KEYTracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isCAPSREPORTTIMESpecified() {
        return localCAPSREPORTTIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSREPORTTIME() {
        return localCAPSREPORTTIME;
    }

    /**
     * Auto generated setter method
     * @param param CAPSREPORTTIME
     */
    public void setCAPSREPORTTIME(java.lang.String param) {
        localCAPSREPORTTIMETracker = param != null;

        this.localCAPSREPORTTIME = param;
    }

    public boolean isCAPSREPORTNUMBERSpecified() {
        return localCAPSREPORTNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSREPORTNUMBER() {
        return localCAPSREPORTNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSREPORTNUMBER
     */
    public void setCAPSREPORTNUMBER(java.lang.String param) {
        localCAPSREPORTNUMBERTracker = param != null;

        this.localCAPSREPORTNUMBER = param;
    }

    public boolean isENQUIRY_REASON_CODESpecified() {
        return localENQUIRY_REASON_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_REASON_CODE() {
        return localENQUIRY_REASON_CODE;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_REASON_CODE
     */
    public void setENQUIRY_REASON_CODE(java.lang.String param) {
        localENQUIRY_REASON_CODETracker = param != null;

        this.localENQUIRY_REASON_CODE = param;
    }

    public boolean isCAPSENQUIRYREASONSpecified() {
        return localCAPSENQUIRYREASONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSENQUIRYREASON() {
        return localCAPSENQUIRYREASON;
    }

    /**
     * Auto generated setter method
     * @param param CAPSENQUIRYREASON
     */
    public void setCAPSENQUIRYREASON(java.lang.String param) {
        localCAPSENQUIRYREASONTracker = param != null;

        this.localCAPSENQUIRYREASON = param;
    }

    public boolean isFINANCE_PURPOSE_CODESpecified() {
        return localFINANCE_PURPOSE_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFINANCE_PURPOSE_CODE() {
        return localFINANCE_PURPOSE_CODE;
    }

    /**
     * Auto generated setter method
     * @param param FINANCE_PURPOSE_CODE
     */
    public void setFINANCE_PURPOSE_CODE(java.lang.String param) {
        localFINANCE_PURPOSE_CODETracker = param != null;

        this.localFINANCE_PURPOSE_CODE = param;
    }

    public boolean isCAPSFINANCEPURPOSESpecified() {
        return localCAPSFINANCEPURPOSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSFINANCEPURPOSE() {
        return localCAPSFINANCEPURPOSE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSFINANCEPURPOSE
     */
    public void setCAPSFINANCEPURPOSE(java.lang.String param) {
        localCAPSFINANCEPURPOSETracker = param != null;

        this.localCAPSFINANCEPURPOSE = param;
    }

    public boolean isCAPSAMOUNTFINANCEDSpecified() {
        return localCAPSAMOUNTFINANCEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAMOUNTFINANCED() {
        return localCAPSAMOUNTFINANCED;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAMOUNTFINANCED
     */
    public void setCAPSAMOUNTFINANCED(java.lang.String param) {
        localCAPSAMOUNTFINANCEDTracker = param != null;

        this.localCAPSAMOUNTFINANCED = param;
    }

    public boolean isCAPSDURATIONOFAGREEMENTSpecified() {
        return localCAPSDURATIONOFAGREEMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSDURATIONOFAGREEMENT() {
        return localCAPSDURATIONOFAGREEMENT;
    }

    /**
     * Auto generated setter method
     * @param param CAPSDURATIONOFAGREEMENT
     */
    public void setCAPSDURATIONOFAGREEMENT(java.lang.String param) {
        localCAPSDURATIONOFAGREEMENTTracker = param != null;

        this.localCAPSDURATIONOFAGREEMENT = param;
    }

    public boolean isCAPSAPPLICANTLASTNAMESpecified() {
        return localCAPSAPPLICANTLASTNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTLASTNAME() {
        return localCAPSAPPLICANTLASTNAME;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTLASTNAME
     */
    public void setCAPSAPPLICANTLASTNAME(java.lang.String param) {
        localCAPSAPPLICANTLASTNAMETracker = param != null;

        this.localCAPSAPPLICANTLASTNAME = param;
    }

    public boolean isCAPSAPPLICANTFIRSTNAMESpecified() {
        return localCAPSAPPLICANTFIRSTNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTFIRSTNAME() {
        return localCAPSAPPLICANTFIRSTNAME;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTFIRSTNAME
     */
    public void setCAPSAPPLICANTFIRSTNAME(java.lang.String param) {
        localCAPSAPPLICANTFIRSTNAMETracker = param != null;

        this.localCAPSAPPLICANTFIRSTNAME = param;
    }

    public boolean isCAPSAPPLICANTMIDDLENAME1Specified() {
        return localCAPSAPPLICANTMIDDLENAME1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTMIDDLENAME1() {
        return localCAPSAPPLICANTMIDDLENAME1;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTMIDDLENAME1
     */
    public void setCAPSAPPLICANTMIDDLENAME1(java.lang.String param) {
        localCAPSAPPLICANTMIDDLENAME1Tracker = param != null;

        this.localCAPSAPPLICANTMIDDLENAME1 = param;
    }

    public boolean isCAPSAPPLICANTMIDDLENAME2Specified() {
        return localCAPSAPPLICANTMIDDLENAME2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTMIDDLENAME2() {
        return localCAPSAPPLICANTMIDDLENAME2;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTMIDDLENAME2
     */
    public void setCAPSAPPLICANTMIDDLENAME2(java.lang.String param) {
        localCAPSAPPLICANTMIDDLENAME2Tracker = param != null;

        this.localCAPSAPPLICANTMIDDLENAME2 = param;
    }

    public boolean isCAPSAPPLICANTMIDDLENAME3Specified() {
        return localCAPSAPPLICANTMIDDLENAME3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTMIDDLENAME3() {
        return localCAPSAPPLICANTMIDDLENAME3;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTMIDDLENAME3
     */
    public void setCAPSAPPLICANTMIDDLENAME3(java.lang.String param) {
        localCAPSAPPLICANTMIDDLENAME3Tracker = param != null;

        this.localCAPSAPPLICANTMIDDLENAME3 = param;
    }

    public boolean isCAPSAPPLICANTGENDERCODESpecified() {
        return localCAPSAPPLICANTGENDERCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTGENDERCODE() {
        return localCAPSAPPLICANTGENDERCODE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTGENDERCODE
     */
    public void setCAPSAPPLICANTGENDERCODE(java.lang.String param) {
        localCAPSAPPLICANTGENDERCODETracker = param != null;

        this.localCAPSAPPLICANTGENDERCODE = param;
    }

    public boolean isCAPSAPPLICANTGENDERSpecified() {
        return localCAPSAPPLICANTGENDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTGENDER() {
        return localCAPSAPPLICANTGENDER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTGENDER
     */
    public void setCAPSAPPLICANTGENDER(java.lang.String param) {
        localCAPSAPPLICANTGENDERTracker = param != null;

        this.localCAPSAPPLICANTGENDER = param;
    }

    public boolean isCAPSINCOME_TAX_PANSpecified() {
        return localCAPSINCOME_TAX_PANTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSINCOME_TAX_PAN() {
        return localCAPSINCOME_TAX_PAN;
    }

    /**
     * Auto generated setter method
     * @param param CAPSINCOME_TAX_PAN
     */
    public void setCAPSINCOME_TAX_PAN(java.lang.String param) {
        localCAPSINCOME_TAX_PANTracker = param != null;

        this.localCAPSINCOME_TAX_PAN = param;
    }

    public boolean isCAPSPAN_ISSUE_DATESpecified() {
        return localCAPSPAN_ISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSPAN_ISSUE_DATE() {
        return localCAPSPAN_ISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSPAN_ISSUE_DATE
     */
    public void setCAPSPAN_ISSUE_DATE(java.lang.String param) {
        localCAPSPAN_ISSUE_DATETracker = param != null;

        this.localCAPSPAN_ISSUE_DATE = param;
    }

    public boolean isCAPSPAN_EXP_DATESpecified() {
        return localCAPSPAN_EXP_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSPAN_EXP_DATE() {
        return localCAPSPAN_EXP_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSPAN_EXP_DATE
     */
    public void setCAPSPAN_EXP_DATE(java.lang.String param) {
        localCAPSPAN_EXP_DATETracker = param != null;

        this.localCAPSPAN_EXP_DATE = param;
    }

    public boolean isCAPSPASSPORT_NUMBERSpecified() {
        return localCAPSPASSPORT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSPASSPORT_NUMBER() {
        return localCAPSPASSPORT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSPASSPORT_NUMBER
     */
    public void setCAPSPASSPORT_NUMBER(java.lang.String param) {
        localCAPSPASSPORT_NUMBERTracker = param != null;

        this.localCAPSPASSPORT_NUMBER = param;
    }

    public boolean isCAPSPASSPORT_ISSUE_DATESpecified() {
        return localCAPSPASSPORT_ISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSPASSPORT_ISSUE_DATE() {
        return localCAPSPASSPORT_ISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSPASSPORT_ISSUE_DATE
     */
    public void setCAPSPASSPORT_ISSUE_DATE(java.lang.String param) {
        localCAPSPASSPORT_ISSUE_DATETracker = param != null;

        this.localCAPSPASSPORT_ISSUE_DATE = param;
    }

    public boolean isCAPSPASSPORT_EXP_DATESpecified() {
        return localCAPSPASSPORT_EXP_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSPASSPORT_EXP_DATE() {
        return localCAPSPASSPORT_EXP_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSPASSPORT_EXP_DATE
     */
    public void setCAPSPASSPORT_EXP_DATE(java.lang.String param) {
        localCAPSPASSPORT_EXP_DATETracker = param != null;

        this.localCAPSPASSPORT_EXP_DATE = param;
    }

    public boolean isCAPSVOTER_IDENTITY_CARDSpecified() {
        return localCAPSVOTER_IDENTITY_CARDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSVOTER_IDENTITY_CARD() {
        return localCAPSVOTER_IDENTITY_CARD;
    }

    /**
     * Auto generated setter method
     * @param param CAPSVOTER_IDENTITY_CARD
     */
    public void setCAPSVOTER_IDENTITY_CARD(java.lang.String param) {
        localCAPSVOTER_IDENTITY_CARDTracker = param != null;

        this.localCAPSVOTER_IDENTITY_CARD = param;
    }

    public boolean isCAPSVOTER_ID_ISSUE_DATESpecified() {
        return localCAPSVOTER_ID_ISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSVOTER_ID_ISSUE_DATE() {
        return localCAPSVOTER_ID_ISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSVOTER_ID_ISSUE_DATE
     */
    public void setCAPSVOTER_ID_ISSUE_DATE(java.lang.String param) {
        localCAPSVOTER_ID_ISSUE_DATETracker = param != null;

        this.localCAPSVOTER_ID_ISSUE_DATE = param;
    }

    public boolean isCAPSVOTER_ID_EXP_DATESpecified() {
        return localCAPSVOTER_ID_EXP_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSVOTER_ID_EXP_DATE() {
        return localCAPSVOTER_ID_EXP_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSVOTER_ID_EXP_DATE
     */
    public void setCAPSVOTER_ID_EXP_DATE(java.lang.String param) {
        localCAPSVOTER_ID_EXP_DATETracker = param != null;

        this.localCAPSVOTER_ID_EXP_DATE = param;
    }

    public boolean isCAPSDRIVER_LICENSE_NUMBERSpecified() {
        return localCAPSDRIVER_LICENSE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSDRIVER_LICENSE_NUMBER() {
        return localCAPSDRIVER_LICENSE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSDRIVER_LICENSE_NUMBER
     */
    public void setCAPSDRIVER_LICENSE_NUMBER(java.lang.String param) {
        localCAPSDRIVER_LICENSE_NUMBERTracker = param != null;

        this.localCAPSDRIVER_LICENSE_NUMBER = param;
    }

    public boolean isCAPSDRIVER_LICENSE_ISSUE_DATESpecified() {
        return localCAPSDRIVER_LICENSE_ISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSDRIVER_LICENSE_ISSUE_DATE() {
        return localCAPSDRIVER_LICENSE_ISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSDRIVER_LICENSE_ISSUE_DATE
     */
    public void setCAPSDRIVER_LICENSE_ISSUE_DATE(java.lang.String param) {
        localCAPSDRIVER_LICENSE_ISSUE_DATETracker = param != null;

        this.localCAPSDRIVER_LICENSE_ISSUE_DATE = param;
    }

    public boolean isCAPSDRIVER_LICENSE_EXP_DATESpecified() {
        return localCAPSDRIVER_LICENSE_EXP_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSDRIVER_LICENSE_EXP_DATE() {
        return localCAPSDRIVER_LICENSE_EXP_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSDRIVER_LICENSE_EXP_DATE
     */
    public void setCAPSDRIVER_LICENSE_EXP_DATE(java.lang.String param) {
        localCAPSDRIVER_LICENSE_EXP_DATETracker = param != null;

        this.localCAPSDRIVER_LICENSE_EXP_DATE = param;
    }

    public boolean isCAPSRATION_CARD_NUMBERSpecified() {
        return localCAPSRATION_CARD_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSRATION_CARD_NUMBER() {
        return localCAPSRATION_CARD_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSRATION_CARD_NUMBER
     */
    public void setCAPSRATION_CARD_NUMBER(java.lang.String param) {
        localCAPSRATION_CARD_NUMBERTracker = param != null;

        this.localCAPSRATION_CARD_NUMBER = param;
    }

    public boolean isCAPSRATION_CARD_ISSUE_DATESpecified() {
        return localCAPSRATION_CARD_ISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSRATION_CARD_ISSUE_DATE() {
        return localCAPSRATION_CARD_ISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSRATION_CARD_ISSUE_DATE
     */
    public void setCAPSRATION_CARD_ISSUE_DATE(java.lang.String param) {
        localCAPSRATION_CARD_ISSUE_DATETracker = param != null;

        this.localCAPSRATION_CARD_ISSUE_DATE = param;
    }

    public boolean isCAPSRATION_CARD_EXP_DATESpecified() {
        return localCAPSRATION_CARD_EXP_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSRATION_CARD_EXP_DATE() {
        return localCAPSRATION_CARD_EXP_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSRATION_CARD_EXP_DATE
     */
    public void setCAPSRATION_CARD_EXP_DATE(java.lang.String param) {
        localCAPSRATION_CARD_EXP_DATETracker = param != null;

        this.localCAPSRATION_CARD_EXP_DATE = param;
    }

    public boolean isCAPSUNIVERSAL_ID_NUMBERSpecified() {
        return localCAPSUNIVERSAL_ID_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSUNIVERSAL_ID_NUMBER() {
        return localCAPSUNIVERSAL_ID_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSUNIVERSAL_ID_NUMBER
     */
    public void setCAPSUNIVERSAL_ID_NUMBER(java.lang.String param) {
        localCAPSUNIVERSAL_ID_NUMBERTracker = param != null;

        this.localCAPSUNIVERSAL_ID_NUMBER = param;
    }

    public boolean isCAPSUNIVERSAL_ID_ISSUE_DATESpecified() {
        return localCAPSUNIVERSAL_ID_ISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSUNIVERSAL_ID_ISSUE_DATE() {
        return localCAPSUNIVERSAL_ID_ISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSUNIVERSAL_ID_ISSUE_DATE
     */
    public void setCAPSUNIVERSAL_ID_ISSUE_DATE(java.lang.String param) {
        localCAPSUNIVERSAL_ID_ISSUE_DATETracker = param != null;

        this.localCAPSUNIVERSAL_ID_ISSUE_DATE = param;
    }

    public boolean isCAPSUNIVERSAL_ID_EXP_DATESpecified() {
        return localCAPSUNIVERSAL_ID_EXP_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSUNIVERSAL_ID_EXP_DATE() {
        return localCAPSUNIVERSAL_ID_EXP_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSUNIVERSAL_ID_EXP_DATE
     */
    public void setCAPSUNIVERSAL_ID_EXP_DATE(java.lang.String param) {
        localCAPSUNIVERSAL_ID_EXP_DATETracker = param != null;

        this.localCAPSUNIVERSAL_ID_EXP_DATE = param;
    }

    public boolean isCAPSAPPLICANTDOBAPPLICANTSpecified() {
        return localCAPSAPPLICANTDOBAPPLICANTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTDOBAPPLICANT() {
        return localCAPSAPPLICANTDOBAPPLICANT;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTDOBAPPLICANT
     */
    public void setCAPSAPPLICANTDOBAPPLICANT(java.lang.String param) {
        localCAPSAPPLICANTDOBAPPLICANTTracker = param != null;

        this.localCAPSAPPLICANTDOBAPPLICANT = param;
    }

    public boolean isCAPSAPPLICANTTELNOAPPLICANT1STSpecified() {
        return localCAPSAPPLICANTTELNOAPPLICANT1STTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTTELNOAPPLICANT1ST() {
        return localCAPSAPPLICANTTELNOAPPLICANT1ST;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTTELNOAPPLICANT1ST
     */
    public void setCAPSAPPLICANTTELNOAPPLICANT1ST(java.lang.String param) {
        localCAPSAPPLICANTTELNOAPPLICANT1STTracker = param != null;

        this.localCAPSAPPLICANTTELNOAPPLICANT1ST = param;
    }

    public boolean isCAPSAPPLICANTTELEXTSpecified() {
        return localCAPSAPPLICANTTELEXTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTTELEXT() {
        return localCAPSAPPLICANTTELEXT;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTTELEXT
     */
    public void setCAPSAPPLICANTTELEXT(java.lang.String param) {
        localCAPSAPPLICANTTELEXTTracker = param != null;

        this.localCAPSAPPLICANTTELEXT = param;
    }

    public boolean isCAPSAPPLICANTTELTYPESpecified() {
        return localCAPSAPPLICANTTELTYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTTELTYPE() {
        return localCAPSAPPLICANTTELTYPE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTTELTYPE
     */
    public void setCAPSAPPLICANTTELTYPE(java.lang.String param) {
        localCAPSAPPLICANTTELTYPETracker = param != null;

        this.localCAPSAPPLICANTTELTYPE = param;
    }

    public boolean isCAPSAPPLICANTMOBILEPHONENUMBERSpecified() {
        return localCAPSAPPLICANTMOBILEPHONENUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTMOBILEPHONENUMBER() {
        return localCAPSAPPLICANTMOBILEPHONENUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTMOBILEPHONENUMBER
     */
    public void setCAPSAPPLICANTMOBILEPHONENUMBER(java.lang.String param) {
        localCAPSAPPLICANTMOBILEPHONENUMBERTracker = param != null;

        this.localCAPSAPPLICANTMOBILEPHONENUMBER = param;
    }

    public boolean isCAPSAPPLICANTEMAILIDSpecified() {
        return localCAPSAPPLICANTEMAILIDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTEMAILID() {
        return localCAPSAPPLICANTEMAILID;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTEMAILID
     */
    public void setCAPSAPPLICANTEMAILID(java.lang.String param) {
        localCAPSAPPLICANTEMAILIDTracker = param != null;

        this.localCAPSAPPLICANTEMAILID = param;
    }

    public boolean isCAPSAPPLICANTINCOMESpecified() {
        return localCAPSAPPLICANTINCOMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTINCOME() {
        return localCAPSAPPLICANTINCOME;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTINCOME
     */
    public void setCAPSAPPLICANTINCOME(java.lang.String param) {
        localCAPSAPPLICANTINCOMETracker = param != null;

        this.localCAPSAPPLICANTINCOME = param;
    }

    public boolean isAPPLICANT_MARITALSTATUSCODESpecified() {
        return localAPPLICANT_MARITALSTATUSCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAPPLICANT_MARITALSTATUSCODE() {
        return localAPPLICANT_MARITALSTATUSCODE;
    }

    /**
     * Auto generated setter method
     * @param param APPLICANT_MARITALSTATUSCODE
     */
    public void setAPPLICANT_MARITALSTATUSCODE(java.lang.String param) {
        localAPPLICANT_MARITALSTATUSCODETracker = param != null;

        this.localAPPLICANT_MARITALSTATUSCODE = param;
    }

    public boolean isCAPSAPPLICANTMARITALSTATUSSpecified() {
        return localCAPSAPPLICANTMARITALSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTMARITALSTATUS() {
        return localCAPSAPPLICANTMARITALSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTMARITALSTATUS
     */
    public void setCAPSAPPLICANTMARITALSTATUS(java.lang.String param) {
        localCAPSAPPLICANTMARITALSTATUSTracker = param != null;

        this.localCAPSAPPLICANTMARITALSTATUS = param;
    }

    public boolean isCAPSAPPLICANTEMPLMTSTATUSCODESpecified() {
        return localCAPSAPPLICANTEMPLMTSTATUSCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTEMPLMTSTATUSCODE() {
        return localCAPSAPPLICANTEMPLMTSTATUSCODE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTEMPLMTSTATUSCODE
     */
    public void setCAPSAPPLICANTEMPLMTSTATUSCODE(java.lang.String param) {
        localCAPSAPPLICANTEMPLMTSTATUSCODETracker = param != null;

        this.localCAPSAPPLICANTEMPLMTSTATUSCODE = param;
    }

    public boolean isCAPSAPPLICANTEMPLOYMENTSTATUSSpecified() {
        return localCAPSAPPLICANTEMPLOYMENTSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTEMPLOYMENTSTATUS() {
        return localCAPSAPPLICANTEMPLOYMENTSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTEMPLOYMENTSTATUS
     */
    public void setCAPSAPPLICANTEMPLOYMENTSTATUS(java.lang.String param) {
        localCAPSAPPLICANTEMPLOYMENTSTATUSTracker = param != null;

        this.localCAPSAPPLICANTEMPLOYMENTSTATUS = param;
    }

    public boolean isCAPSAPPLICANTDATEWITHEMPLOYERSpecified() {
        return localCAPSAPPLICANTDATEWITHEMPLOYERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTDATEWITHEMPLOYER() {
        return localCAPSAPPLICANTDATEWITHEMPLOYER;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTDATEWITHEMPLOYER
     */
    public void setCAPSAPPLICANTDATEWITHEMPLOYER(java.lang.String param) {
        localCAPSAPPLICANTDATEWITHEMPLOYERTracker = param != null;

        this.localCAPSAPPLICANTDATEWITHEMPLOYER = param;
    }

    public boolean isCAPSAPPLTNOMAJORCRDTCARDHELDSpecified() {
        return localCAPSAPPLTNOMAJORCRDTCARDHELDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLTNOMAJORCRDTCARDHELD() {
        return localCAPSAPPLTNOMAJORCRDTCARDHELD;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLTNOMAJORCRDTCARDHELD
     */
    public void setCAPSAPPLTNOMAJORCRDTCARDHELD(java.lang.String param) {
        localCAPSAPPLTNOMAJORCRDTCARDHELDTracker = param != null;

        this.localCAPSAPPLTNOMAJORCRDTCARDHELD = param;
    }

    public boolean isCAPSAPPLTFLATNOPLOTNOHOUSENOSpecified() {
        return localCAPSAPPLTFLATNOPLOTNOHOUSENOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLTFLATNOPLOTNOHOUSENO() {
        return localCAPSAPPLTFLATNOPLOTNOHOUSENO;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLTFLATNOPLOTNOHOUSENO
     */
    public void setCAPSAPPLTFLATNOPLOTNOHOUSENO(java.lang.String param) {
        localCAPSAPPLTFLATNOPLOTNOHOUSENOTracker = param != null;

        this.localCAPSAPPLTFLATNOPLOTNOHOUSENO = param;
    }

    public boolean isCAPSAPPLICANTBLDGNOSOCIETYNAMESpecified() {
        return localCAPSAPPLICANTBLDGNOSOCIETYNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTBLDGNOSOCIETYNAME() {
        return localCAPSAPPLICANTBLDGNOSOCIETYNAME;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTBLDGNOSOCIETYNAME
     */
    public void setCAPSAPPLICANTBLDGNOSOCIETYNAME(java.lang.String param) {
        localCAPSAPPLICANTBLDGNOSOCIETYNAMETracker = param != null;

        this.localCAPSAPPLICANTBLDGNOSOCIETYNAME = param;
    }

    public boolean isCAPSAPPLTRDNONAMEAREALOCALITYSpecified() {
        return localCAPSAPPLTRDNONAMEAREALOCALITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLTRDNONAMEAREALOCALITY() {
        return localCAPSAPPLTRDNONAMEAREALOCALITY;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLTRDNONAMEAREALOCALITY
     */
    public void setCAPSAPPLTRDNONAMEAREALOCALITY(java.lang.String param) {
        localCAPSAPPLTRDNONAMEAREALOCALITYTracker = param != null;

        this.localCAPSAPPLTRDNONAMEAREALOCALITY = param;
    }

    public boolean isCAPSAPPLICANTCITYSpecified() {
        return localCAPSAPPLICANTCITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTCITY() {
        return localCAPSAPPLICANTCITY;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTCITY
     */
    public void setCAPSAPPLICANTCITY(java.lang.String param) {
        localCAPSAPPLICANTCITYTracker = param != null;

        this.localCAPSAPPLICANTCITY = param;
    }

    public boolean isCAPSAPPLICANTLANDMARKSpecified() {
        return localCAPSAPPLICANTLANDMARKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTLANDMARK() {
        return localCAPSAPPLICANTLANDMARK;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTLANDMARK
     */
    public void setCAPSAPPLICANTLANDMARK(java.lang.String param) {
        localCAPSAPPLICANTLANDMARKTracker = param != null;

        this.localCAPSAPPLICANTLANDMARK = param;
    }

    public boolean isCAPSAPPLICANTSTATECODESpecified() {
        return localCAPSAPPLICANTSTATECODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTSTATECODE() {
        return localCAPSAPPLICANTSTATECODE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTSTATECODE
     */
    public void setCAPSAPPLICANTSTATECODE(java.lang.String param) {
        localCAPSAPPLICANTSTATECODETracker = param != null;

        this.localCAPSAPPLICANTSTATECODE = param;
    }

    public boolean isCAPSAPPLICANTSTATESpecified() {
        return localCAPSAPPLICANTSTATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTSTATE() {
        return localCAPSAPPLICANTSTATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTSTATE
     */
    public void setCAPSAPPLICANTSTATE(java.lang.String param) {
        localCAPSAPPLICANTSTATETracker = param != null;

        this.localCAPSAPPLICANTSTATE = param;
    }

    public boolean isCAPSAPPLICANTPINCODESpecified() {
        return localCAPSAPPLICANTPINCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTPINCODE() {
        return localCAPSAPPLICANTPINCODE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTPINCODE
     */
    public void setCAPSAPPLICANTPINCODE(java.lang.String param) {
        localCAPSAPPLICANTPINCODETracker = param != null;

        this.localCAPSAPPLICANTPINCODE = param;
    }

    public boolean isCAPSAPPLICANTCOUNTRYCODESpecified() {
        return localCAPSAPPLICANTCOUNTRYCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSAPPLICANTCOUNTRYCODE() {
        return localCAPSAPPLICANTCOUNTRYCODE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSAPPLICANTCOUNTRYCODE
     */
    public void setCAPSAPPLICANTCOUNTRYCODE(java.lang.String param) {
        localCAPSAPPLICANTCOUNTRYCODETracker = param != null;

        this.localCAPSAPPLICANTCOUNTRYCODE = param;
    }

    public boolean isBUREAUSCORESpecified() {
        return localBUREAUSCORETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBUREAUSCORE() {
        return localBUREAUSCORE;
    }

    /**
     * Auto generated setter method
     * @param param BUREAUSCORE
     */
    public void setBUREAUSCORE(java.lang.String param) {
        localBUREAUSCORETracker = param != null;

        this.localBUREAUSCORE = param;
    }

    public boolean isBUREAUSCORECONFIDLEVELSpecified() {
        return localBUREAUSCORECONFIDLEVELTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBUREAUSCORECONFIDLEVEL() {
        return localBUREAUSCORECONFIDLEVEL;
    }

    /**
     * Auto generated setter method
     * @param param BUREAUSCORECONFIDLEVEL
     */
    public void setBUREAUSCORECONFIDLEVEL(java.lang.String param) {
        localBUREAUSCORECONFIDLEVELTracker = param != null;

        this.localBUREAUSCORECONFIDLEVEL = param;
    }

    public boolean isCREDITRATINGSpecified() {
        return localCREDITRATINGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITRATING() {
        return localCREDITRATING;
    }

    /**
     * Auto generated setter method
     * @param param CREDITRATING
     */
    public void setCREDITRATING(java.lang.String param) {
        localCREDITRATINGTracker = param != null;

        this.localCREDITRATING = param;
    }

    public boolean isTNOFBFHLCADEXHLSpecified() {
        return localTNOFBFHLCADEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFBFHLCADEXHL() {
        return localTNOFBFHLCADEXHL;
    }

    /**
     * Auto generated setter method
     * @param param TNOFBFHLCADEXHL
     */
    public void setTNOFBFHLCADEXHL(java.lang.String param) {
        localTNOFBFHLCADEXHLTracker = param != null;

        this.localTNOFBFHLCADEXHL = param;
    }

    public boolean isTOTVALOFBFHLCADSpecified() {
        return localTOTVALOFBFHLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFBFHLCAD() {
        return localTOTVALOFBFHLCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFBFHLCAD
     */
    public void setTOTVALOFBFHLCAD(java.lang.String param) {
        localTOTVALOFBFHLCADTracker = param != null;

        this.localTOTVALOFBFHLCAD = param;
    }

    public boolean isMNTSMRBFHLCADSpecified() {
        return localMNTSMRBFHLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRBFHLCAD() {
        return localMNTSMRBFHLCAD;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRBFHLCAD
     */
    public void setMNTSMRBFHLCAD(java.lang.String param) {
        localMNTSMRBFHLCADTracker = param != null;

        this.localMNTSMRBFHLCAD = param;
    }

    public boolean isTNOFHLCADSpecified() {
        return localTNOFHLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFHLCAD() {
        return localTNOFHLCAD;
    }

    /**
     * Auto generated setter method
     * @param param TNOFHLCAD
     */
    public void setTNOFHLCAD(java.lang.String param) {
        localTNOFHLCADTracker = param != null;

        this.localTNOFHLCAD = param;
    }

    public boolean isTOTVALOFHLCADSpecified() {
        return localTOTVALOFHLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFHLCAD() {
        return localTOTVALOFHLCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFHLCAD
     */
    public void setTOTVALOFHLCAD(java.lang.String param) {
        localTOTVALOFHLCADTracker = param != null;

        this.localTOTVALOFHLCAD = param;
    }

    public boolean isMNTSMRHLCADSpecified() {
        return localMNTSMRHLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRHLCAD() {
        return localMNTSMRHLCAD;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRHLCAD
     */
    public void setMNTSMRHLCAD(java.lang.String param) {
        localMNTSMRHLCADTracker = param != null;

        this.localMNTSMRHLCAD = param;
    }

    public boolean isTNOFTELCOSCADSpecified() {
        return localTNOFTELCOSCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFTELCOSCAD() {
        return localTNOFTELCOSCAD;
    }

    /**
     * Auto generated setter method
     * @param param TNOFTELCOSCAD
     */
    public void setTNOFTELCOSCAD(java.lang.String param) {
        localTNOFTELCOSCADTracker = param != null;

        this.localTNOFTELCOSCAD = param;
    }

    public boolean isTOTVALOFTELCOSCADSpecified() {
        return localTOTVALOFTELCOSCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFTELCOSCAD() {
        return localTOTVALOFTELCOSCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFTELCOSCAD
     */
    public void setTOTVALOFTELCOSCAD(java.lang.String param) {
        localTOTVALOFTELCOSCADTracker = param != null;

        this.localTOTVALOFTELCOSCAD = param;
    }

    public boolean isMNTSMRTELCOSCADSpecified() {
        return localMNTSMRTELCOSCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRTELCOSCAD() {
        return localMNTSMRTELCOSCAD;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRTELCOSCAD
     */
    public void setMNTSMRTELCOSCAD(java.lang.String param) {
        localMNTSMRTELCOSCADTracker = param != null;

        this.localMNTSMRTELCOSCAD = param;
    }

    public boolean isTNOFMFCADSpecified() {
        return localTNOFMFCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFMFCAD() {
        return localTNOFMFCAD;
    }

    /**
     * Auto generated setter method
     * @param param TNOFMFCAD
     */
    public void setTNOFMFCAD(java.lang.String param) {
        localTNOFMFCADTracker = param != null;

        this.localTNOFMFCAD = param;
    }

    public boolean isTOTVALOFMFCADSpecified() {
        return localTOTVALOFMFCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFMFCAD() {
        return localTOTVALOFMFCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFMFCAD
     */
    public void setTOTVALOFMFCAD(java.lang.String param) {
        localTOTVALOFMFCADTracker = param != null;

        this.localTOTVALOFMFCAD = param;
    }

    public boolean isMNTSMRMFCADSpecified() {
        return localMNTSMRMFCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRMFCAD() {
        return localMNTSMRMFCAD;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRMFCAD
     */
    public void setMNTSMRMFCAD(java.lang.String param) {
        localMNTSMRMFCADTracker = param != null;

        this.localMNTSMRMFCAD = param;
    }

    public boolean isTNOFRETAILCADSpecified() {
        return localTNOFRETAILCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFRETAILCAD() {
        return localTNOFRETAILCAD;
    }

    /**
     * Auto generated setter method
     * @param param TNOFRETAILCAD
     */
    public void setTNOFRETAILCAD(java.lang.String param) {
        localTNOFRETAILCADTracker = param != null;

        this.localTNOFRETAILCAD = param;
    }

    public boolean isTOTVALOFRETAILCADSpecified() {
        return localTOTVALOFRETAILCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFRETAILCAD() {
        return localTOTVALOFRETAILCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFRETAILCAD
     */
    public void setTOTVALOFRETAILCAD(java.lang.String param) {
        localTOTVALOFRETAILCADTracker = param != null;

        this.localTOTVALOFRETAILCAD = param;
    }

    public boolean isMNTSMRRETAILCADSpecified() {
        return localMNTSMRRETAILCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRRETAILCAD() {
        return localMNTSMRRETAILCAD;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRRETAILCAD
     */
    public void setMNTSMRRETAILCAD(java.lang.String param) {
        localMNTSMRRETAILCADTracker = param != null;

        this.localMNTSMRRETAILCAD = param;
    }

    public boolean isTNOFALLCADSpecified() {
        return localTNOFALLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFALLCAD() {
        return localTNOFALLCAD;
    }

    /**
     * Auto generated setter method
     * @param param TNOFALLCAD
     */
    public void setTNOFALLCAD(java.lang.String param) {
        localTNOFALLCADTracker = param != null;

        this.localTNOFALLCAD = param;
    }

    public boolean isTOTVALOFALLCADSpecified() {
        return localTOTVALOFALLCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFALLCAD() {
        return localTOTVALOFALLCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFALLCAD
     */
    public void setTOTVALOFALLCAD(java.lang.String param) {
        localTOTVALOFALLCADTracker = param != null;

        this.localTOTVALOFALLCAD = param;
    }

    public boolean isMNTSMRCADALLSpecified() {
        return localMNTSMRCADALLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRCADALL() {
        return localMNTSMRCADALL;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRCADALL
     */
    public void setMNTSMRCADALL(java.lang.String param) {
        localMNTSMRCADALLTracker = param != null;

        this.localMNTSMRCADALL = param;
    }

    public boolean isTNOFBFHLACAEXHLSpecified() {
        return localTNOFBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFBFHLACAEXHL() {
        return localTNOFBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param TNOFBFHLACAEXHL
     */
    public void setTNOFBFHLACAEXHL(java.lang.String param) {
        localTNOFBFHLACAEXHLTracker = param != null;

        this.localTNOFBFHLACAEXHL = param;
    }

    public boolean isBALBFHLACAEXHLSpecified() {
        return localBALBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBALBFHLACAEXHL() {
        return localBALBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param BALBFHLACAEXHL
     */
    public void setBALBFHLACAEXHL(java.lang.String param) {
        localBALBFHLACAEXHLTracker = param != null;

        this.localBALBFHLACAEXHL = param;
    }

    public boolean isWCDSTBFHLACAEXHLSpecified() {
        return localWCDSTBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTBFHLACAEXHL() {
        return localWCDSTBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTBFHLACAEXHL
     */
    public void setWCDSTBFHLACAEXHL(java.lang.String param) {
        localWCDSTBFHLACAEXHLTracker = param != null;

        this.localWCDSTBFHLACAEXHL = param;
    }

    public boolean isWDSPR6MNTBFHLACAEXHLSpecified() {
        return localWDSPR6MNTBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR6MNTBFHLACAEXHL() {
        return localWDSPR6MNTBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR6MNTBFHLACAEXHL
     */
    public void setWDSPR6MNTBFHLACAEXHL(java.lang.String param) {
        localWDSPR6MNTBFHLACAEXHLTracker = param != null;

        this.localWDSPR6MNTBFHLACAEXHL = param;
    }

    public boolean isWDSPR712MNTBFHLACAEXHLSpecified() {
        return localWDSPR712MNTBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR712MNTBFHLACAEXHL() {
        return localWDSPR712MNTBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR712MNTBFHLACAEXHL
     */
    public void setWDSPR712MNTBFHLACAEXHL(java.lang.String param) {
        localWDSPR712MNTBFHLACAEXHLTracker = param != null;

        this.localWDSPR712MNTBFHLACAEXHL = param;
    }

    public boolean isAGEOFOLDESTBFHLACAEXHLSpecified() {
        return localAGEOFOLDESTBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTBFHLACAEXHL() {
        return localAGEOFOLDESTBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTBFHLACAEXHL
     */
    public void setAGEOFOLDESTBFHLACAEXHL(java.lang.String param) {
        localAGEOFOLDESTBFHLACAEXHLTracker = param != null;

        this.localAGEOFOLDESTBFHLACAEXHL = param;
    }

    public boolean isHCBPERREVACCBFHLACAEXHLSpecified() {
        return localHCBPERREVACCBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHCBPERREVACCBFHLACAEXHL() {
        return localHCBPERREVACCBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param HCBPERREVACCBFHLACAEXHL
     */
    public void setHCBPERREVACCBFHLACAEXHL(java.lang.String param) {
        localHCBPERREVACCBFHLACAEXHLTracker = param != null;

        this.localHCBPERREVACCBFHLACAEXHL = param;
    }

    public boolean isTCBPERREVACCBFHLACAEXHLSpecified() {
        return localTCBPERREVACCBFHLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTCBPERREVACCBFHLACAEXHL() {
        return localTCBPERREVACCBFHLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param TCBPERREVACCBFHLACAEXHL
     */
    public void setTCBPERREVACCBFHLACAEXHL(java.lang.String param) {
        localTCBPERREVACCBFHLACAEXHLTracker = param != null;

        this.localTCBPERREVACCBFHLACAEXHL = param;
    }

    public boolean isTNOFHLACASpecified() {
        return localTNOFHLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFHLACA() {
        return localTNOFHLACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFHLACA
     */
    public void setTNOFHLACA(java.lang.String param) {
        localTNOFHLACATracker = param != null;

        this.localTNOFHLACA = param;
    }

    public boolean isBALHLACASpecified() {
        return localBALHLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBALHLACA() {
        return localBALHLACA;
    }

    /**
     * Auto generated setter method
     * @param param BALHLACA
     */
    public void setBALHLACA(java.lang.String param) {
        localBALHLACATracker = param != null;

        this.localBALHLACA = param;
    }

    public boolean isWCDSTHLACASpecified() {
        return localWCDSTHLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTHLACA() {
        return localWCDSTHLACA;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTHLACA
     */
    public void setWCDSTHLACA(java.lang.String param) {
        localWCDSTHLACATracker = param != null;

        this.localWCDSTHLACA = param;
    }

    public boolean isWDSPR6MNTHLACASpecified() {
        return localWDSPR6MNTHLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR6MNTHLACA() {
        return localWDSPR6MNTHLACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR6MNTHLACA
     */
    public void setWDSPR6MNTHLACA(java.lang.String param) {
        localWDSPR6MNTHLACATracker = param != null;

        this.localWDSPR6MNTHLACA = param;
    }

    public boolean isWDSPR712MNTHLACASpecified() {
        return localWDSPR712MNTHLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR712MNTHLACA() {
        return localWDSPR712MNTHLACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR712MNTHLACA
     */
    public void setWDSPR712MNTHLACA(java.lang.String param) {
        localWDSPR712MNTHLACATracker = param != null;

        this.localWDSPR712MNTHLACA = param;
    }

    public boolean isAGEOFOLDESTHLACASpecified() {
        return localAGEOFOLDESTHLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTHLACA() {
        return localAGEOFOLDESTHLACA;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTHLACA
     */
    public void setAGEOFOLDESTHLACA(java.lang.String param) {
        localAGEOFOLDESTHLACATracker = param != null;

        this.localAGEOFOLDESTHLACA = param;
    }

    public boolean isTNOFMFACASpecified() {
        return localTNOFMFACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFMFACA() {
        return localTNOFMFACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFMFACA
     */
    public void setTNOFMFACA(java.lang.String param) {
        localTNOFMFACATracker = param != null;

        this.localTNOFMFACA = param;
    }

    public boolean isTOTALBALMFACASpecified() {
        return localTOTALBALMFACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALBALMFACA() {
        return localTOTALBALMFACA;
    }

    /**
     * Auto generated setter method
     * @param param TOTALBALMFACA
     */
    public void setTOTALBALMFACA(java.lang.String param) {
        localTOTALBALMFACATracker = param != null;

        this.localTOTALBALMFACA = param;
    }

    public boolean isWCDSTMFACASpecified() {
        return localWCDSTMFACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTMFACA() {
        return localWCDSTMFACA;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTMFACA
     */
    public void setWCDSTMFACA(java.lang.String param) {
        localWCDSTMFACATracker = param != null;

        this.localWCDSTMFACA = param;
    }

    public boolean isWDSPR6MNTMFACASpecified() {
        return localWDSPR6MNTMFACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR6MNTMFACA() {
        return localWDSPR6MNTMFACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR6MNTMFACA
     */
    public void setWDSPR6MNTMFACA(java.lang.String param) {
        localWDSPR6MNTMFACATracker = param != null;

        this.localWDSPR6MNTMFACA = param;
    }

    public boolean isWDSPR712MNTMFACASpecified() {
        return localWDSPR712MNTMFACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR712MNTMFACA() {
        return localWDSPR712MNTMFACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR712MNTMFACA
     */
    public void setWDSPR712MNTMFACA(java.lang.String param) {
        localWDSPR712MNTMFACATracker = param != null;

        this.localWDSPR712MNTMFACA = param;
    }

    public boolean isAGEOFOLDESTMFACASpecified() {
        return localAGEOFOLDESTMFACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTMFACA() {
        return localAGEOFOLDESTMFACA;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTMFACA
     */
    public void setAGEOFOLDESTMFACA(java.lang.String param) {
        localAGEOFOLDESTMFACATracker = param != null;

        this.localAGEOFOLDESTMFACA = param;
    }

    public boolean isTNOFTELCOSACASpecified() {
        return localTNOFTELCOSACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFTELCOSACA() {
        return localTNOFTELCOSACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFTELCOSACA
     */
    public void setTNOFTELCOSACA(java.lang.String param) {
        localTNOFTELCOSACATracker = param != null;

        this.localTNOFTELCOSACA = param;
    }

    public boolean isTOTALBALTELCOSACASpecified() {
        return localTOTALBALTELCOSACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALBALTELCOSACA() {
        return localTOTALBALTELCOSACA;
    }

    /**
     * Auto generated setter method
     * @param param TOTALBALTELCOSACA
     */
    public void setTOTALBALTELCOSACA(java.lang.String param) {
        localTOTALBALTELCOSACATracker = param != null;

        this.localTOTALBALTELCOSACA = param;
    }

    public boolean isWCDSTTELCOSACASpecified() {
        return localWCDSTTELCOSACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTTELCOSACA() {
        return localWCDSTTELCOSACA;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTTELCOSACA
     */
    public void setWCDSTTELCOSACA(java.lang.String param) {
        localWCDSTTELCOSACATracker = param != null;

        this.localWCDSTTELCOSACA = param;
    }

    public boolean isWDSPR6MNTTELCOSACASpecified() {
        return localWDSPR6MNTTELCOSACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR6MNTTELCOSACA() {
        return localWDSPR6MNTTELCOSACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR6MNTTELCOSACA
     */
    public void setWDSPR6MNTTELCOSACA(java.lang.String param) {
        localWDSPR6MNTTELCOSACATracker = param != null;

        this.localWDSPR6MNTTELCOSACA = param;
    }

    public boolean isWDSPR712MNTTELCOSACASpecified() {
        return localWDSPR712MNTTELCOSACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR712MNTTELCOSACA() {
        return localWDSPR712MNTTELCOSACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR712MNTTELCOSACA
     */
    public void setWDSPR712MNTTELCOSACA(java.lang.String param) {
        localWDSPR712MNTTELCOSACATracker = param != null;

        this.localWDSPR712MNTTELCOSACA = param;
    }

    public boolean isAGEOFOLDESTTELCOSACASpecified() {
        return localAGEOFOLDESTTELCOSACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTTELCOSACA() {
        return localAGEOFOLDESTTELCOSACA;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTTELCOSACA
     */
    public void setAGEOFOLDESTTELCOSACA(java.lang.String param) {
        localAGEOFOLDESTTELCOSACATracker = param != null;

        this.localAGEOFOLDESTTELCOSACA = param;
    }

    public boolean isTNOFRETAILACASpecified() {
        return localTNOFRETAILACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFRETAILACA() {
        return localTNOFRETAILACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFRETAILACA
     */
    public void setTNOFRETAILACA(java.lang.String param) {
        localTNOFRETAILACATracker = param != null;

        this.localTNOFRETAILACA = param;
    }

    public boolean isTOTALBALRETAILACASpecified() {
        return localTOTALBALRETAILACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALBALRETAILACA() {
        return localTOTALBALRETAILACA;
    }

    /**
     * Auto generated setter method
     * @param param TOTALBALRETAILACA
     */
    public void setTOTALBALRETAILACA(java.lang.String param) {
        localTOTALBALRETAILACATracker = param != null;

        this.localTOTALBALRETAILACA = param;
    }

    public boolean isWCDSTRETAILACASpecified() {
        return localWCDSTRETAILACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTRETAILACA() {
        return localWCDSTRETAILACA;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTRETAILACA
     */
    public void setWCDSTRETAILACA(java.lang.String param) {
        localWCDSTRETAILACATracker = param != null;

        this.localWCDSTRETAILACA = param;
    }

    public boolean isWDSPR6MNTRETAILACASpecified() {
        return localWDSPR6MNTRETAILACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR6MNTRETAILACA() {
        return localWDSPR6MNTRETAILACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR6MNTRETAILACA
     */
    public void setWDSPR6MNTRETAILACA(java.lang.String param) {
        localWDSPR6MNTRETAILACATracker = param != null;

        this.localWDSPR6MNTRETAILACA = param;
    }

    public boolean isWDSPR712MNTRETAILACASpecified() {
        return localWDSPR712MNTRETAILACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR712MNTRETAILACA() {
        return localWDSPR712MNTRETAILACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR712MNTRETAILACA
     */
    public void setWDSPR712MNTRETAILACA(java.lang.String param) {
        localWDSPR712MNTRETAILACATracker = param != null;

        this.localWDSPR712MNTRETAILACA = param;
    }

    public boolean isAGEOFOLDESTRETAILACASpecified() {
        return localAGEOFOLDESTRETAILACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTRETAILACA() {
        return localAGEOFOLDESTRETAILACA;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTRETAILACA
     */
    public void setAGEOFOLDESTRETAILACA(java.lang.String param) {
        localAGEOFOLDESTRETAILACATracker = param != null;

        this.localAGEOFOLDESTRETAILACA = param;
    }

    public boolean isHCBLMPERREVACCRETSpecified() {
        return localHCBLMPERREVACCRETTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHCBLMPERREVACCRET() {
        return localHCBLMPERREVACCRET;
    }

    /**
     * Auto generated setter method
     * @param param HCBLMPERREVACCRET
     */
    public void setHCBLMPERREVACCRET(java.lang.String param) {
        localHCBLMPERREVACCRETTracker = param != null;

        this.localHCBLMPERREVACCRET = param;
    }

    public boolean isTOTCURBALLMPERREVACCRETSpecified() {
        return localTOTCURBALLMPERREVACCRETTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTCURBALLMPERREVACCRET() {
        return localTOTCURBALLMPERREVACCRET;
    }

    /**
     * Auto generated setter method
     * @param param TOTCURBALLMPERREVACCRET
     */
    public void setTOTCURBALLMPERREVACCRET(java.lang.String param) {
        localTOTCURBALLMPERREVACCRETTracker = param != null;

        this.localTOTCURBALLMPERREVACCRET = param;
    }

    public boolean isTNOFALLACASpecified() {
        return localTNOFALLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFALLACA() {
        return localTNOFALLACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFALLACA
     */
    public void setTNOFALLACA(java.lang.String param) {
        localTNOFALLACATracker = param != null;

        this.localTNOFALLACA = param;
    }

    public boolean isBALALLACAEXHLSpecified() {
        return localBALALLACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBALALLACAEXHL() {
        return localBALALLACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param BALALLACAEXHL
     */
    public void setBALALLACAEXHL(java.lang.String param) {
        localBALALLACAEXHLTracker = param != null;

        this.localBALALLACAEXHL = param;
    }

    public boolean isWCDSTALLACASpecified() {
        return localWCDSTALLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTALLACA() {
        return localWCDSTALLACA;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTALLACA
     */
    public void setWCDSTALLACA(java.lang.String param) {
        localWCDSTALLACATracker = param != null;

        this.localWCDSTALLACA = param;
    }

    public boolean isWDSPR6MNTALLACASpecified() {
        return localWDSPR6MNTALLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR6MNTALLACA() {
        return localWDSPR6MNTALLACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR6MNTALLACA
     */
    public void setWDSPR6MNTALLACA(java.lang.String param) {
        localWDSPR6MNTALLACATracker = param != null;

        this.localWDSPR6MNTALLACA = param;
    }

    public boolean isWDSPR712MNTALLACASpecified() {
        return localWDSPR712MNTALLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWDSPR712MNTALLACA() {
        return localWDSPR712MNTALLACA;
    }

    /**
     * Auto generated setter method
     * @param param WDSPR712MNTALLACA
     */
    public void setWDSPR712MNTALLACA(java.lang.String param) {
        localWDSPR712MNTALLACATracker = param != null;

        this.localWDSPR712MNTALLACA = param;
    }

    public boolean isAGEOFOLDESTALLACASpecified() {
        return localAGEOFOLDESTALLACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTALLACA() {
        return localAGEOFOLDESTALLACA;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTALLACA
     */
    public void setAGEOFOLDESTALLACA(java.lang.String param) {
        localAGEOFOLDESTALLACATracker = param != null;

        this.localAGEOFOLDESTALLACA = param;
    }

    public boolean isTNOFNDELBFHLINACAEXHLSpecified() {
        return localTNOFNDELBFHLINACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFNDELBFHLINACAEXHL() {
        return localTNOFNDELBFHLINACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param TNOFNDELBFHLINACAEXHL
     */
    public void setTNOFNDELBFHLINACAEXHL(java.lang.String param) {
        localTNOFNDELBFHLINACAEXHLTracker = param != null;

        this.localTNOFNDELBFHLINACAEXHL = param;
    }

    public boolean isTNOFDELBFHLINACAEXHLSpecified() {
        return localTNOFDELBFHLINACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFDELBFHLINACAEXHL() {
        return localTNOFDELBFHLINACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param TNOFDELBFHLINACAEXHL
     */
    public void setTNOFDELBFHLINACAEXHL(java.lang.String param) {
        localTNOFDELBFHLINACAEXHLTracker = param != null;

        this.localTNOFDELBFHLINACAEXHL = param;
    }

    public boolean isTNOFNDELHLINACASpecified() {
        return localTNOFNDELHLINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFNDELHLINACA() {
        return localTNOFNDELHLINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFNDELHLINACA
     */
    public void setTNOFNDELHLINACA(java.lang.String param) {
        localTNOFNDELHLINACATracker = param != null;

        this.localTNOFNDELHLINACA = param;
    }

    public boolean isTNOFDELHLINACASpecified() {
        return localTNOFDELHLINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFDELHLINACA() {
        return localTNOFDELHLINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFDELHLINACA
     */
    public void setTNOFDELHLINACA(java.lang.String param) {
        localTNOFDELHLINACATracker = param != null;

        this.localTNOFDELHLINACA = param;
    }

    public boolean isTNOFNDELMFINACASpecified() {
        return localTNOFNDELMFINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFNDELMFINACA() {
        return localTNOFNDELMFINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFNDELMFINACA
     */
    public void setTNOFNDELMFINACA(java.lang.String param) {
        localTNOFNDELMFINACATracker = param != null;

        this.localTNOFNDELMFINACA = param;
    }

    public boolean isTNOFDELMFINACASpecified() {
        return localTNOFDELMFINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFDELMFINACA() {
        return localTNOFDELMFINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFDELMFINACA
     */
    public void setTNOFDELMFINACA(java.lang.String param) {
        localTNOFDELMFINACATracker = param != null;

        this.localTNOFDELMFINACA = param;
    }

    public boolean isTNOFNDELTELCOSINACASpecified() {
        return localTNOFNDELTELCOSINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFNDELTELCOSINACA() {
        return localTNOFNDELTELCOSINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFNDELTELCOSINACA
     */
    public void setTNOFNDELTELCOSINACA(java.lang.String param) {
        localTNOFNDELTELCOSINACATracker = param != null;

        this.localTNOFNDELTELCOSINACA = param;
    }

    public boolean isTNOFDELTELCOSINACASpecified() {
        return localTNOFDELTELCOSINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFDELTELCOSINACA() {
        return localTNOFDELTELCOSINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFDELTELCOSINACA
     */
    public void setTNOFDELTELCOSINACA(java.lang.String param) {
        localTNOFDELTELCOSINACATracker = param != null;

        this.localTNOFDELTELCOSINACA = param;
    }

    public boolean isTNOFNDELRETAILINACASpecified() {
        return localTNOFNDELRETAILINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFNDELRETAILINACA() {
        return localTNOFNDELRETAILINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFNDELRETAILINACA
     */
    public void setTNOFNDELRETAILINACA(java.lang.String param) {
        localTNOFNDELRETAILINACATracker = param != null;

        this.localTNOFNDELRETAILINACA = param;
    }

    public boolean isTNOFDELRETAILINACASpecified() {
        return localTNOFDELRETAILINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFDELRETAILINACA() {
        return localTNOFDELRETAILINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFDELRETAILINACA
     */
    public void setTNOFDELRETAILINACA(java.lang.String param) {
        localTNOFDELRETAILINACATracker = param != null;

        this.localTNOFDELRETAILINACA = param;
    }

    public boolean isBFHLCAPSLAST90DAYSSpecified() {
        return localBFHLCAPSLAST90DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBFHLCAPSLAST90DAYS() {
        return localBFHLCAPSLAST90DAYS;
    }

    /**
     * Auto generated setter method
     * @param param BFHLCAPSLAST90DAYS
     */
    public void setBFHLCAPSLAST90DAYS(java.lang.String param) {
        localBFHLCAPSLAST90DAYSTracker = param != null;

        this.localBFHLCAPSLAST90DAYS = param;
    }

    public boolean isMFCAPSLAST90DAYSSpecified() {
        return localMFCAPSLAST90DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMFCAPSLAST90DAYS() {
        return localMFCAPSLAST90DAYS;
    }

    /**
     * Auto generated setter method
     * @param param MFCAPSLAST90DAYS
     */
    public void setMFCAPSLAST90DAYS(java.lang.String param) {
        localMFCAPSLAST90DAYSTracker = param != null;

        this.localMFCAPSLAST90DAYS = param;
    }

    public boolean isTELCOSCAPSLAST90DAYSSpecified() {
        return localTELCOSCAPSLAST90DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELCOSCAPSLAST90DAYS() {
        return localTELCOSCAPSLAST90DAYS;
    }

    /**
     * Auto generated setter method
     * @param param TELCOSCAPSLAST90DAYS
     */
    public void setTELCOSCAPSLAST90DAYS(java.lang.String param) {
        localTELCOSCAPSLAST90DAYSTracker = param != null;

        this.localTELCOSCAPSLAST90DAYS = param;
    }

    public boolean isRETAILCAPSLAST90DAYSSpecified() {
        return localRETAILCAPSLAST90DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRETAILCAPSLAST90DAYS() {
        return localRETAILCAPSLAST90DAYS;
    }

    /**
     * Auto generated setter method
     * @param param RETAILCAPSLAST90DAYS
     */
    public void setRETAILCAPSLAST90DAYS(java.lang.String param) {
        localRETAILCAPSLAST90DAYSTracker = param != null;

        this.localRETAILCAPSLAST90DAYS = param;
    }

    public boolean isTNOFOCOMCADSpecified() {
        return localTNOFOCOMCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFOCOMCAD() {
        return localTNOFOCOMCAD;
    }

    /**
     * Auto generated setter method
     * @param param TNOFOCOMCAD
     */
    public void setTNOFOCOMCAD(java.lang.String param) {
        localTNOFOCOMCADTracker = param != null;

        this.localTNOFOCOMCAD = param;
    }

    public boolean isTOTVALOFOCOMCADSpecified() {
        return localTOTVALOFOCOMCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTVALOFOCOMCAD() {
        return localTOTVALOFOCOMCAD;
    }

    /**
     * Auto generated setter method
     * @param param TOTVALOFOCOMCAD
     */
    public void setTOTVALOFOCOMCAD(java.lang.String param) {
        localTOTVALOFOCOMCADTracker = param != null;

        this.localTOTVALOFOCOMCAD = param;
    }

    public boolean isMNTSMROCOMCADSpecified() {
        return localMNTSMROCOMCADTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMROCOMCAD() {
        return localMNTSMROCOMCAD;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMROCOMCAD
     */
    public void setMNTSMROCOMCAD(java.lang.String param) {
        localMNTSMROCOMCADTracker = param != null;

        this.localMNTSMROCOMCAD = param;
    }

    public boolean isTNOFOCOMACASpecified() {
        return localTNOFOCOMACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFOCOMACA() {
        return localTNOFOCOMACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFOCOMACA
     */
    public void setTNOFOCOMACA(java.lang.String param) {
        localTNOFOCOMACATracker = param != null;

        this.localTNOFOCOMACA = param;
    }

    public boolean isBALOCOMACAEXHLSpecified() {
        return localBALOCOMACAEXHLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBALOCOMACAEXHL() {
        return localBALOCOMACAEXHL;
    }

    /**
     * Auto generated setter method
     * @param param BALOCOMACAEXHL
     */
    public void setBALOCOMACAEXHL(java.lang.String param) {
        localBALOCOMACAEXHLTracker = param != null;

        this.localBALOCOMACAEXHL = param;
    }

    public boolean isBALOCOMACAHLONLYSpecified() {
        return localBALOCOMACAHLONLYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBALOCOMACAHLONLY() {
        return localBALOCOMACAHLONLY;
    }

    /**
     * Auto generated setter method
     * @param param BALOCOMACAHLONLY
     */
    public void setBALOCOMACAHLONLY(java.lang.String param) {
        localBALOCOMACAHLONLYTracker = param != null;

        this.localBALOCOMACAHLONLY = param;
    }

    public boolean isWCDSTOCOMACASpecified() {
        return localWCDSTOCOMACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWCDSTOCOMACA() {
        return localWCDSTOCOMACA;
    }

    /**
     * Auto generated setter method
     * @param param WCDSTOCOMACA
     */
    public void setWCDSTOCOMACA(java.lang.String param) {
        localWCDSTOCOMACATracker = param != null;

        this.localWCDSTOCOMACA = param;
    }

    public boolean isHCBLMPERREVOCOMACASpecified() {
        return localHCBLMPERREVOCOMACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHCBLMPERREVOCOMACA() {
        return localHCBLMPERREVOCOMACA;
    }

    /**
     * Auto generated setter method
     * @param param HCBLMPERREVOCOMACA
     */
    public void setHCBLMPERREVOCOMACA(java.lang.String param) {
        localHCBLMPERREVOCOMACATracker = param != null;

        this.localHCBLMPERREVOCOMACA = param;
    }

    public boolean isTNOFNDELOCOMINACASpecified() {
        return localTNOFNDELOCOMINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFNDELOCOMINACA() {
        return localTNOFNDELOCOMINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFNDELOCOMINACA
     */
    public void setTNOFNDELOCOMINACA(java.lang.String param) {
        localTNOFNDELOCOMINACATracker = param != null;

        this.localTNOFNDELOCOMINACA = param;
    }

    public boolean isTNOFDELOCOMINACASpecified() {
        return localTNOFDELOCOMINACATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFDELOCOMINACA() {
        return localTNOFDELOCOMINACA;
    }

    /**
     * Auto generated setter method
     * @param param TNOFDELOCOMINACA
     */
    public void setTNOFDELOCOMINACA(java.lang.String param) {
        localTNOFDELOCOMINACATracker = param != null;

        this.localTNOFDELOCOMINACA = param;
    }

    public boolean isTNOFOCOMCAPSLAST90DAYSSpecified() {
        return localTNOFOCOMCAPSLAST90DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFOCOMCAPSLAST90DAYS() {
        return localTNOFOCOMCAPSLAST90DAYS;
    }

    /**
     * Auto generated setter method
     * @param param TNOFOCOMCAPSLAST90DAYS
     */
    public void setTNOFOCOMCAPSLAST90DAYS(java.lang.String param) {
        localTNOFOCOMCAPSLAST90DAYSTracker = param != null;

        this.localTNOFOCOMCAPSLAST90DAYS = param;
    }

    public boolean isANYRELCBDATADISYNSpecified() {
        return localANYRELCBDATADISYNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getANYRELCBDATADISYN() {
        return localANYRELCBDATADISYN;
    }

    /**
     * Auto generated setter method
     * @param param ANYRELCBDATADISYN
     */
    public void setANYRELCBDATADISYN(java.lang.String param) {
        localANYRELCBDATADISYNTracker = param != null;

        this.localANYRELCBDATADISYN = param;
    }

    public boolean isOTHRELCBDFCPOSMATYNSpecified() {
        return localOTHRELCBDFCPOSMATYNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOTHRELCBDFCPOSMATYN() {
        return localOTHRELCBDFCPOSMATYN;
    }

    /**
     * Auto generated setter method
     * @param param OTHRELCBDFCPOSMATYN
     */
    public void setOTHRELCBDFCPOSMATYN(java.lang.String param) {
        localOTHRELCBDFCPOSMATYNTracker = param != null;

        this.localOTHRELCBDFCPOSMATYN = param;
    }

    public boolean isTNOFCADCLASSEDASSFWDWOSpecified() {
        return localTNOFCADCLASSEDASSFWDWOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTNOFCADCLASSEDASSFWDWO() {
        return localTNOFCADCLASSEDASSFWDWO;
    }

    /**
     * Auto generated setter method
     * @param param TNOFCADCLASSEDASSFWDWO
     */
    public void setTNOFCADCLASSEDASSFWDWO(java.lang.String param) {
        localTNOFCADCLASSEDASSFWDWOTracker = param != null;

        this.localTNOFCADCLASSEDASSFWDWO = param;
    }

    public boolean isMNTSMRCADCLASSEDASSFWDWOSpecified() {
        return localMNTSMRCADCLASSEDASSFWDWOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTSMRCADCLASSEDASSFWDWO() {
        return localMNTSMRCADCLASSEDASSFWDWO;
    }

    /**
     * Auto generated setter method
     * @param param MNTSMRCADCLASSEDASSFWDWO
     */
    public void setMNTSMRCADCLASSEDASSFWDWO(java.lang.String param) {
        localMNTSMRCADCLASSEDASSFWDWOTracker = param != null;

        this.localMNTSMRCADCLASSEDASSFWDWO = param;
    }

    public boolean isNUMOFCADSFWDWOLAST24MNTSpecified() {
        return localNUMOFCADSFWDWOLAST24MNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNUMOFCADSFWDWOLAST24MNT() {
        return localNUMOFCADSFWDWOLAST24MNT;
    }

    /**
     * Auto generated setter method
     * @param param NUMOFCADSFWDWOLAST24MNT
     */
    public void setNUMOFCADSFWDWOLAST24MNT(java.lang.String param) {
        localNUMOFCADSFWDWOLAST24MNTTracker = param != null;

        this.localNUMOFCADSFWDWOLAST24MNT = param;
    }

    public boolean isTOTCURBALLIVESACCSpecified() {
        return localTOTCURBALLIVESACCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTCURBALLIVESACC() {
        return localTOTCURBALLIVESACC;
    }

    /**
     * Auto generated setter method
     * @param param TOTCURBALLIVESACC
     */
    public void setTOTCURBALLIVESACC(java.lang.String param) {
        localTOTCURBALLIVESACCTracker = param != null;

        this.localTOTCURBALLIVESACC = param;
    }

    public boolean isTOTCURBALLIVEUACCSpecified() {
        return localTOTCURBALLIVEUACCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTCURBALLIVEUACC() {
        return localTOTCURBALLIVEUACC;
    }

    /**
     * Auto generated setter method
     * @param param TOTCURBALLIVEUACC
     */
    public void setTOTCURBALLIVEUACC(java.lang.String param) {
        localTOTCURBALLIVEUACCTracker = param != null;

        this.localTOTCURBALLIVEUACC = param;
    }

    public boolean isTOTCURBALMAXBALLIVESACCSpecified() {
        return localTOTCURBALMAXBALLIVESACCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTCURBALMAXBALLIVESACC() {
        return localTOTCURBALMAXBALLIVESACC;
    }

    /**
     * Auto generated setter method
     * @param param TOTCURBALMAXBALLIVESACC
     */
    public void setTOTCURBALMAXBALLIVESACC(java.lang.String param) {
        localTOTCURBALMAXBALLIVESACCTracker = param != null;

        this.localTOTCURBALMAXBALLIVESACC = param;
    }

    public boolean isTOTCURBALMAXBALLIVEUACCSpecified() {
        return localTOTCURBALMAXBALLIVEUACCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTCURBALMAXBALLIVEUACC() {
        return localTOTCURBALMAXBALLIVEUACC;
    }

    /**
     * Auto generated setter method
     * @param param TOTCURBALMAXBALLIVEUACC
     */
    public void setTOTCURBALMAXBALLIVEUACC(java.lang.String param) {
        localTOTCURBALMAXBALLIVEUACCTracker = param != null;

        this.localTOTCURBALMAXBALLIVEUACC = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_DATESpecified() {
        return localOUTPUT_WRITE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_DATE() {
        return localOUTPUT_WRITE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_DATE
     */
    public void setOUTPUT_WRITE_DATE(java.lang.String param) {
        localOUTPUT_WRITE_DATETracker = param != null;

        this.localOUTPUT_WRITE_DATE = param;
    }

    public boolean isOUTPUT_READ_DATESpecified() {
        return localOUTPUT_READ_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_DATE() {
        return localOUTPUT_READ_DATE;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_DATE
     */
    public void setOUTPUT_READ_DATE(java.lang.String param) {
        localOUTPUT_READ_DATETracker = param != null;

        this.localOUTPUT_READ_DATE = param;
    }

    public boolean isDATEOFADDITIONSpecified() {
        return localDATEOFADDITIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOFADDITION() {
        return localDATEOFADDITION;
    }

    /**
     * Auto generated setter method
     * @param param DATEOFADDITION
     */
    public void setDATEOFADDITION(java.lang.String param) {
        localDATEOFADDITIONTracker = param != null;

        this.localDATEOFADDITION = param;
    }

    public boolean isACCOUNT_KEYSpecified() {
        return localACCOUNT_KEYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_KEY() {
        return localACCOUNT_KEY;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_KEY
     */
    public void setACCOUNT_KEY(java.lang.String param) {
        localACCOUNT_KEYTracker = param != null;

        this.localACCOUNT_KEY = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ExperianSRespType2", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ExperianSRespType2", xmlWriter);
            }
        }

        if (localCAPSREPORTTIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSREPORTTIME", xmlWriter);

            if (localCAPSREPORTTIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSREPORTTIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSREPORTTIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSREPORTNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSREPORTNUMBER", xmlWriter);

            if (localCAPSREPORTNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSREPORTNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSREPORTNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_REASON_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_REASON_CODE", xmlWriter);

            if (localENQUIRY_REASON_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_REASON_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_REASON_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSENQUIRYREASONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSENQUIRYREASON", xmlWriter);

            if (localCAPSENQUIRYREASON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSENQUIRYREASON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSENQUIRYREASON);
            }

            xmlWriter.writeEndElement();
        }

        if (localFINANCE_PURPOSE_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "FINANCE_PURPOSE_CODE", xmlWriter);

            if (localFINANCE_PURPOSE_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FINANCE_PURPOSE_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFINANCE_PURPOSE_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSFINANCEPURPOSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSFINANCEPURPOSE", xmlWriter);

            if (localCAPSFINANCEPURPOSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSFINANCEPURPOSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSFINANCEPURPOSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAMOUNTFINANCEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAMOUNTFINANCED", xmlWriter);

            if (localCAPSAMOUNTFINANCED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAMOUNTFINANCED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAMOUNTFINANCED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSDURATIONOFAGREEMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSDURATIONOFAGREEMENT",
                xmlWriter);

            if (localCAPSDURATIONOFAGREEMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSDURATIONOFAGREEMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSDURATIONOFAGREEMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTLASTNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTLASTNAME",
                xmlWriter);

            if (localCAPSAPPLICANTLASTNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTLASTNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTLASTNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTFIRSTNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTFIRSTNAME",
                xmlWriter);

            if (localCAPSAPPLICANTFIRSTNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTFIRSTNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTFIRSTNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTMIDDLENAME1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTMIDDLENAME1",
                xmlWriter);

            if (localCAPSAPPLICANTMIDDLENAME1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTMIDDLENAME1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTMIDDLENAME1);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTMIDDLENAME2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTMIDDLENAME2",
                xmlWriter);

            if (localCAPSAPPLICANTMIDDLENAME2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTMIDDLENAME2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTMIDDLENAME2);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTMIDDLENAME3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTMIDDLENAME3",
                xmlWriter);

            if (localCAPSAPPLICANTMIDDLENAME3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTMIDDLENAME3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTMIDDLENAME3);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTGENDERCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTGENDERCODE",
                xmlWriter);

            if (localCAPSAPPLICANTGENDERCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTGENDERCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTGENDERCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTGENDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTGENDER", xmlWriter);

            if (localCAPSAPPLICANTGENDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTGENDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTGENDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSINCOME_TAX_PANTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSINCOME_TAX_PAN", xmlWriter);

            if (localCAPSINCOME_TAX_PAN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSINCOME_TAX_PAN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSINCOME_TAX_PAN);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSPAN_ISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSPAN_ISSUE_DATE", xmlWriter);

            if (localCAPSPAN_ISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSPAN_ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSPAN_ISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSPAN_EXP_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSPAN_EXP_DATE", xmlWriter);

            if (localCAPSPAN_EXP_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSPAN_EXP_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSPAN_EXP_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSPASSPORT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSPASSPORT_NUMBER", xmlWriter);

            if (localCAPSPASSPORT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSPASSPORT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSPASSPORT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSPASSPORT_ISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSPASSPORT_ISSUE_DATE",
                xmlWriter);

            if (localCAPSPASSPORT_ISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSPASSPORT_ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSPASSPORT_ISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSPASSPORT_EXP_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSPASSPORT_EXP_DATE",
                xmlWriter);

            if (localCAPSPASSPORT_EXP_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSPASSPORT_EXP_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSPASSPORT_EXP_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSVOTER_IDENTITY_CARDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSVOTER_IDENTITY_CARD",
                xmlWriter);

            if (localCAPSVOTER_IDENTITY_CARD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSVOTER_IDENTITY_CARD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSVOTER_IDENTITY_CARD);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSVOTER_ID_ISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSVOTER_ID_ISSUE_DATE",
                xmlWriter);

            if (localCAPSVOTER_ID_ISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSVOTER_ID_ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSVOTER_ID_ISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSVOTER_ID_EXP_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSVOTER_ID_EXP_DATE",
                xmlWriter);

            if (localCAPSVOTER_ID_EXP_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSVOTER_ID_EXP_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSVOTER_ID_EXP_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSDRIVER_LICENSE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSDRIVER_LICENSE_NUMBER",
                xmlWriter);

            if (localCAPSDRIVER_LICENSE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSDRIVER_LICENSE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSDRIVER_LICENSE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSDRIVER_LICENSE_ISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSDRIVER_LICENSE_ISSUE_DATE",
                xmlWriter);

            if (localCAPSDRIVER_LICENSE_ISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSDRIVER_LICENSE_ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSDRIVER_LICENSE_ISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSDRIVER_LICENSE_EXP_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSDRIVER_LICENSE_EXP_DATE",
                xmlWriter);

            if (localCAPSDRIVER_LICENSE_EXP_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSDRIVER_LICENSE_EXP_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSDRIVER_LICENSE_EXP_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSRATION_CARD_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSRATION_CARD_NUMBER",
                xmlWriter);

            if (localCAPSRATION_CARD_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSRATION_CARD_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSRATION_CARD_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSRATION_CARD_ISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSRATION_CARD_ISSUE_DATE",
                xmlWriter);

            if (localCAPSRATION_CARD_ISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSRATION_CARD_ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSRATION_CARD_ISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSRATION_CARD_EXP_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSRATION_CARD_EXP_DATE",
                xmlWriter);

            if (localCAPSRATION_CARD_EXP_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSRATION_CARD_EXP_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSRATION_CARD_EXP_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSUNIVERSAL_ID_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSUNIVERSAL_ID_NUMBER",
                xmlWriter);

            if (localCAPSUNIVERSAL_ID_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSUNIVERSAL_ID_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSUNIVERSAL_ID_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSUNIVERSAL_ID_ISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSUNIVERSAL_ID_ISSUE_DATE",
                xmlWriter);

            if (localCAPSUNIVERSAL_ID_ISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSUNIVERSAL_ID_ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSUNIVERSAL_ID_ISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSUNIVERSAL_ID_EXP_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSUNIVERSAL_ID_EXP_DATE",
                xmlWriter);

            if (localCAPSUNIVERSAL_ID_EXP_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSUNIVERSAL_ID_EXP_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSUNIVERSAL_ID_EXP_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTDOBAPPLICANTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTDOBAPPLICANT",
                xmlWriter);

            if (localCAPSAPPLICANTDOBAPPLICANT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTDOBAPPLICANT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTDOBAPPLICANT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTTELNOAPPLICANT1STTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "CAPSAPPLICANTTELNOAPPLICANT1ST", xmlWriter);

            if (localCAPSAPPLICANTTELNOAPPLICANT1ST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTTELNOAPPLICANT1ST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTTELNOAPPLICANT1ST);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTTELEXTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTTELEXT", xmlWriter);

            if (localCAPSAPPLICANTTELEXT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTTELEXT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTTELEXT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTTELTYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTTELTYPE", xmlWriter);

            if (localCAPSAPPLICANTTELTYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTTELTYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTTELTYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTMOBILEPHONENUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "CAPSAPPLICANTMOBILEPHONENUMBER", xmlWriter);

            if (localCAPSAPPLICANTMOBILEPHONENUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTMOBILEPHONENUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTMOBILEPHONENUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTEMAILIDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTEMAILID", xmlWriter);

            if (localCAPSAPPLICANTEMAILID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTEMAILID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTEMAILID);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTINCOMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTINCOME", xmlWriter);

            if (localCAPSAPPLICANTINCOME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTINCOME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTINCOME);
            }

            xmlWriter.writeEndElement();
        }

        if (localAPPLICANT_MARITALSTATUSCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "APPLICANT_MARITALSTATUSCODE",
                xmlWriter);

            if (localAPPLICANT_MARITALSTATUSCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "APPLICANT_MARITALSTATUSCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAPPLICANT_MARITALSTATUSCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTMARITALSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTMARITALSTATUS",
                xmlWriter);

            if (localCAPSAPPLICANTMARITALSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTMARITALSTATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTMARITALSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTEMPLMTSTATUSCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTEMPLMTSTATUSCODE",
                xmlWriter);

            if (localCAPSAPPLICANTEMPLMTSTATUSCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTEMPLMTSTATUSCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTEMPLMTSTATUSCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTEMPLOYMENTSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTEMPLOYMENTSTATUS",
                xmlWriter);

            if (localCAPSAPPLICANTEMPLOYMENTSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTEMPLOYMENTSTATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTEMPLOYMENTSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTDATEWITHEMPLOYERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTDATEWITHEMPLOYER",
                xmlWriter);

            if (localCAPSAPPLICANTDATEWITHEMPLOYER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTDATEWITHEMPLOYER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTDATEWITHEMPLOYER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLTNOMAJORCRDTCARDHELDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLTNOMAJORCRDTCARDHELD",
                xmlWriter);

            if (localCAPSAPPLTNOMAJORCRDTCARDHELD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLTNOMAJORCRDTCARDHELD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLTNOMAJORCRDTCARDHELD);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLTFLATNOPLOTNOHOUSENOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLTFLATNOPLOTNOHOUSENO",
                xmlWriter);

            if (localCAPSAPPLTFLATNOPLOTNOHOUSENO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLTFLATNOPLOTNOHOUSENO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLTFLATNOPLOTNOHOUSENO);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTBLDGNOSOCIETYNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "CAPSAPPLICANTBLDGNOSOCIETYNAME", xmlWriter);

            if (localCAPSAPPLICANTBLDGNOSOCIETYNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTBLDGNOSOCIETYNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTBLDGNOSOCIETYNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLTRDNONAMEAREALOCALITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLTRDNONAMEAREALOCALITY",
                xmlWriter);

            if (localCAPSAPPLTRDNONAMEAREALOCALITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLTRDNONAMEAREALOCALITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLTRDNONAMEAREALOCALITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTCITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTCITY", xmlWriter);

            if (localCAPSAPPLICANTCITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTCITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTCITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTLANDMARKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTLANDMARK",
                xmlWriter);

            if (localCAPSAPPLICANTLANDMARK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTLANDMARK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTLANDMARK);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTSTATECODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTSTATECODE",
                xmlWriter);

            if (localCAPSAPPLICANTSTATECODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTSTATECODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTSTATECODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTSTATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTSTATE", xmlWriter);

            if (localCAPSAPPLICANTSTATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTSTATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTSTATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTPINCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTPINCODE", xmlWriter);

            if (localCAPSAPPLICANTPINCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTPINCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTPINCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSAPPLICANTCOUNTRYCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSAPPLICANTCOUNTRYCODE",
                xmlWriter);

            if (localCAPSAPPLICANTCOUNTRYCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSAPPLICANTCOUNTRYCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSAPPLICANTCOUNTRYCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localBUREAUSCORETracker) {
            namespace = "";
            writeStartElement(null, namespace, "BUREAUSCORE", xmlWriter);

            if (localBUREAUSCORE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BUREAUSCORE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBUREAUSCORE);
            }

            xmlWriter.writeEndElement();
        }

        if (localBUREAUSCORECONFIDLEVELTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BUREAUSCORECONFIDLEVEL",
                xmlWriter);

            if (localBUREAUSCORECONFIDLEVEL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BUREAUSCORECONFIDLEVEL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBUREAUSCORECONFIDLEVEL);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITRATINGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITRATING", xmlWriter);

            if (localCREDITRATING == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITRATING cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITRATING);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFBFHLCADEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFBFHLCADEXHL", xmlWriter);

            if (localTNOFBFHLCADEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFBFHLCADEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFBFHLCADEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFBFHLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFBFHLCAD", xmlWriter);

            if (localTOTVALOFBFHLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFBFHLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFBFHLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRBFHLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRBFHLCAD", xmlWriter);

            if (localMNTSMRBFHLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRBFHLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRBFHLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFHLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFHLCAD", xmlWriter);

            if (localTNOFHLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFHLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFHLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFHLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFHLCAD", xmlWriter);

            if (localTOTVALOFHLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFHLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFHLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRHLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRHLCAD", xmlWriter);

            if (localMNTSMRHLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRHLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRHLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFTELCOSCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFTELCOSCAD", xmlWriter);

            if (localTNOFTELCOSCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFTELCOSCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFTELCOSCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFTELCOSCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFTELCOSCAD", xmlWriter);

            if (localTOTVALOFTELCOSCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFTELCOSCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFTELCOSCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRTELCOSCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRTELCOSCAD", xmlWriter);

            if (localMNTSMRTELCOSCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRTELCOSCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRTELCOSCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFMFCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFMFCAD", xmlWriter);

            if (localTNOFMFCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFMFCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFMFCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFMFCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFMFCAD", xmlWriter);

            if (localTOTVALOFMFCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFMFCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFMFCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRMFCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRMFCAD", xmlWriter);

            if (localMNTSMRMFCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRMFCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRMFCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFRETAILCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFRETAILCAD", xmlWriter);

            if (localTNOFRETAILCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFRETAILCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFRETAILCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFRETAILCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFRETAILCAD", xmlWriter);

            if (localTOTVALOFRETAILCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFRETAILCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFRETAILCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRRETAILCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRRETAILCAD", xmlWriter);

            if (localMNTSMRRETAILCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRRETAILCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRRETAILCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFALLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFALLCAD", xmlWriter);

            if (localTNOFALLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFALLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFALLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFALLCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFALLCAD", xmlWriter);

            if (localTOTVALOFALLCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFALLCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFALLCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRCADALLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRCADALL", xmlWriter);

            if (localMNTSMRCADALL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRCADALL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRCADALL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFBFHLACAEXHL", xmlWriter);

            if (localTNOFBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localBALBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BALBFHLACAEXHL", xmlWriter);

            if (localBALBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BALBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBALBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTBFHLACAEXHL", xmlWriter);

            if (localWCDSTBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR6MNTBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR6MNTBFHLACAEXHL", xmlWriter);

            if (localWDSPR6MNTBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR6MNTBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR6MNTBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR712MNTBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR712MNTBFHLACAEXHL",
                xmlWriter);

            if (localWDSPR712MNTBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR712MNTBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR712MNTBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTBFHLACAEXHL",
                xmlWriter);

            if (localAGEOFOLDESTBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localHCBPERREVACCBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "HCBPERREVACCBFHLACAEXHL",
                xmlWriter);

            if (localHCBPERREVACCBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HCBPERREVACCBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHCBPERREVACCBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTCBPERREVACCBFHLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TCBPERREVACCBFHLACAEXHL",
                xmlWriter);

            if (localTCBPERREVACCBFHLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TCBPERREVACCBFHLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTCBPERREVACCBFHLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFHLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFHLACA", xmlWriter);

            if (localTNOFHLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFHLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFHLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localBALHLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "BALHLACA", xmlWriter);

            if (localBALHLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BALHLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBALHLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTHLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTHLACA", xmlWriter);

            if (localWCDSTHLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTHLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTHLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR6MNTHLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR6MNTHLACA", xmlWriter);

            if (localWDSPR6MNTHLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR6MNTHLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR6MNTHLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR712MNTHLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR712MNTHLACA", xmlWriter);

            if (localWDSPR712MNTHLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR712MNTHLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR712MNTHLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTHLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTHLACA", xmlWriter);

            if (localAGEOFOLDESTHLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTHLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTHLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFMFACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFMFACA", xmlWriter);

            if (localTNOFMFACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFMFACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFMFACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALBALMFACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALBALMFACA", xmlWriter);

            if (localTOTALBALMFACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALBALMFACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALBALMFACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTMFACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTMFACA", xmlWriter);

            if (localWCDSTMFACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTMFACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTMFACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR6MNTMFACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR6MNTMFACA", xmlWriter);

            if (localWDSPR6MNTMFACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR6MNTMFACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR6MNTMFACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR712MNTMFACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR712MNTMFACA", xmlWriter);

            if (localWDSPR712MNTMFACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR712MNTMFACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR712MNTMFACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTMFACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTMFACA", xmlWriter);

            if (localAGEOFOLDESTMFACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTMFACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTMFACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFTELCOSACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFTELCOSACA", xmlWriter);

            if (localTNOFTELCOSACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFTELCOSACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFTELCOSACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALBALTELCOSACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALBALTELCOSACA", xmlWriter);

            if (localTOTALBALTELCOSACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALBALTELCOSACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALBALTELCOSACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTTELCOSACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTTELCOSACA", xmlWriter);

            if (localWCDSTTELCOSACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTTELCOSACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTTELCOSACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR6MNTTELCOSACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR6MNTTELCOSACA", xmlWriter);

            if (localWDSPR6MNTTELCOSACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR6MNTTELCOSACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR6MNTTELCOSACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR712MNTTELCOSACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR712MNTTELCOSACA", xmlWriter);

            if (localWDSPR712MNTTELCOSACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR712MNTTELCOSACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR712MNTTELCOSACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTTELCOSACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTTELCOSACA", xmlWriter);

            if (localAGEOFOLDESTTELCOSACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTTELCOSACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTTELCOSACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFRETAILACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFRETAILACA", xmlWriter);

            if (localTNOFRETAILACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFRETAILACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFRETAILACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALBALRETAILACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALBALRETAILACA", xmlWriter);

            if (localTOTALBALRETAILACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALBALRETAILACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALBALRETAILACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTRETAILACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTRETAILACA", xmlWriter);

            if (localWCDSTRETAILACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTRETAILACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTRETAILACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR6MNTRETAILACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR6MNTRETAILACA", xmlWriter);

            if (localWDSPR6MNTRETAILACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR6MNTRETAILACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR6MNTRETAILACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR712MNTRETAILACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR712MNTRETAILACA", xmlWriter);

            if (localWDSPR712MNTRETAILACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR712MNTRETAILACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR712MNTRETAILACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTRETAILACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTRETAILACA", xmlWriter);

            if (localAGEOFOLDESTRETAILACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTRETAILACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTRETAILACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localHCBLMPERREVACCRETTracker) {
            namespace = "";
            writeStartElement(null, namespace, "HCBLMPERREVACCRET", xmlWriter);

            if (localHCBLMPERREVACCRET == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HCBLMPERREVACCRET cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHCBLMPERREVACCRET);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTCURBALLMPERREVACCRETTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTCURBALLMPERREVACCRET",
                xmlWriter);

            if (localTOTCURBALLMPERREVACCRET == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTCURBALLMPERREVACCRET cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTCURBALLMPERREVACCRET);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFALLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFALLACA", xmlWriter);

            if (localTNOFALLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFALLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFALLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localBALALLACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BALALLACAEXHL", xmlWriter);

            if (localBALALLACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BALALLACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBALALLACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTALLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTALLACA", xmlWriter);

            if (localWCDSTALLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTALLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTALLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR6MNTALLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR6MNTALLACA", xmlWriter);

            if (localWDSPR6MNTALLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR6MNTALLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR6MNTALLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localWDSPR712MNTALLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WDSPR712MNTALLACA", xmlWriter);

            if (localWDSPR712MNTALLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WDSPR712MNTALLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWDSPR712MNTALLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTALLACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTALLACA", xmlWriter);

            if (localAGEOFOLDESTALLACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTALLACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTALLACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFNDELBFHLINACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFNDELBFHLINACAEXHL",
                xmlWriter);

            if (localTNOFNDELBFHLINACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFNDELBFHLINACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFNDELBFHLINACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFDELBFHLINACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFDELBFHLINACAEXHL", xmlWriter);

            if (localTNOFDELBFHLINACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFDELBFHLINACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFDELBFHLINACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFNDELHLINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFNDELHLINACA", xmlWriter);

            if (localTNOFNDELHLINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFNDELHLINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFNDELHLINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFDELHLINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFDELHLINACA", xmlWriter);

            if (localTNOFDELHLINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFDELHLINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFDELHLINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFNDELMFINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFNDELMFINACA", xmlWriter);

            if (localTNOFNDELMFINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFNDELMFINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFNDELMFINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFDELMFINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFDELMFINACA", xmlWriter);

            if (localTNOFDELMFINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFDELMFINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFDELMFINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFNDELTELCOSINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFNDELTELCOSINACA", xmlWriter);

            if (localTNOFNDELTELCOSINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFNDELTELCOSINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFNDELTELCOSINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFDELTELCOSINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFDELTELCOSINACA", xmlWriter);

            if (localTNOFDELTELCOSINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFDELTELCOSINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFDELTELCOSINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFNDELRETAILINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFNDELRETAILINACA", xmlWriter);

            if (localTNOFNDELRETAILINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFNDELRETAILINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFNDELRETAILINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFDELRETAILINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFDELRETAILINACA", xmlWriter);

            if (localTNOFDELRETAILINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFDELRETAILINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFDELRETAILINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localBFHLCAPSLAST90DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BFHLCAPSLAST90DAYS", xmlWriter);

            if (localBFHLCAPSLAST90DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BFHLCAPSLAST90DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBFHLCAPSLAST90DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localMFCAPSLAST90DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MFCAPSLAST90DAYS", xmlWriter);

            if (localMFCAPSLAST90DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MFCAPSLAST90DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMFCAPSLAST90DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELCOSCAPSLAST90DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELCOSCAPSLAST90DAYS", xmlWriter);

            if (localTELCOSCAPSLAST90DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELCOSCAPSLAST90DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELCOSCAPSLAST90DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localRETAILCAPSLAST90DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RETAILCAPSLAST90DAYS", xmlWriter);

            if (localRETAILCAPSLAST90DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RETAILCAPSLAST90DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRETAILCAPSLAST90DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFOCOMCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFOCOMCAD", xmlWriter);

            if (localTNOFOCOMCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFOCOMCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFOCOMCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTVALOFOCOMCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTVALOFOCOMCAD", xmlWriter);

            if (localTOTVALOFOCOMCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTVALOFOCOMCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTVALOFOCOMCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMROCOMCADTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMROCOMCAD", xmlWriter);

            if (localMNTSMROCOMCAD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMROCOMCAD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMROCOMCAD);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFOCOMACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFOCOMACA", xmlWriter);

            if (localTNOFOCOMACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFOCOMACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFOCOMACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localBALOCOMACAEXHLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BALOCOMACAEXHL", xmlWriter);

            if (localBALOCOMACAEXHL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BALOCOMACAEXHL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBALOCOMACAEXHL);
            }

            xmlWriter.writeEndElement();
        }

        if (localBALOCOMACAHLONLYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BALOCOMACAHLONLY", xmlWriter);

            if (localBALOCOMACAHLONLY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BALOCOMACAHLONLY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBALOCOMACAHLONLY);
            }

            xmlWriter.writeEndElement();
        }

        if (localWCDSTOCOMACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "WCDSTOCOMACA", xmlWriter);

            if (localWCDSTOCOMACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WCDSTOCOMACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWCDSTOCOMACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localHCBLMPERREVOCOMACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "HCBLMPERREVOCOMACA", xmlWriter);

            if (localHCBLMPERREVOCOMACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HCBLMPERREVOCOMACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHCBLMPERREVOCOMACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFNDELOCOMINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFNDELOCOMINACA", xmlWriter);

            if (localTNOFNDELOCOMINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFNDELOCOMINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFNDELOCOMINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFDELOCOMINACATracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFDELOCOMINACA", xmlWriter);

            if (localTNOFDELOCOMINACA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFDELOCOMINACA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFDELOCOMINACA);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFOCOMCAPSLAST90DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFOCOMCAPSLAST90DAYS",
                xmlWriter);

            if (localTNOFOCOMCAPSLAST90DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFOCOMCAPSLAST90DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFOCOMCAPSLAST90DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localANYRELCBDATADISYNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ANYRELCBDATADISYN", xmlWriter);

            if (localANYRELCBDATADISYN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ANYRELCBDATADISYN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localANYRELCBDATADISYN);
            }

            xmlWriter.writeEndElement();
        }

        if (localOTHRELCBDFCPOSMATYNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OTHRELCBDFCPOSMATYN", xmlWriter);

            if (localOTHRELCBDFCPOSMATYN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OTHRELCBDFCPOSMATYN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOTHRELCBDFCPOSMATYN);
            }

            xmlWriter.writeEndElement();
        }

        if (localTNOFCADCLASSEDASSFWDWOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TNOFCADCLASSEDASSFWDWO",
                xmlWriter);

            if (localTNOFCADCLASSEDASSFWDWO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TNOFCADCLASSEDASSFWDWO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTNOFCADCLASSEDASSFWDWO);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTSMRCADCLASSEDASSFWDWOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTSMRCADCLASSEDASSFWDWO",
                xmlWriter);

            if (localMNTSMRCADCLASSEDASSFWDWO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTSMRCADCLASSEDASSFWDWO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTSMRCADCLASSEDASSFWDWO);
            }

            xmlWriter.writeEndElement();
        }

        if (localNUMOFCADSFWDWOLAST24MNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NUMOFCADSFWDWOLAST24MNT",
                xmlWriter);

            if (localNUMOFCADSFWDWOLAST24MNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NUMOFCADSFWDWOLAST24MNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNUMOFCADSFWDWOLAST24MNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTCURBALLIVESACCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTCURBALLIVESACC", xmlWriter);

            if (localTOTCURBALLIVESACC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTCURBALLIVESACC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTCURBALLIVESACC);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTCURBALLIVEUACCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTCURBALLIVEUACC", xmlWriter);

            if (localTOTCURBALLIVEUACC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTCURBALLIVEUACC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTCURBALLIVEUACC);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTCURBALMAXBALLIVESACCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTCURBALMAXBALLIVESACC",
                xmlWriter);

            if (localTOTCURBALMAXBALLIVESACC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTCURBALMAXBALLIVESACC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTCURBALMAXBALLIVESACC);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTCURBALMAXBALLIVEUACCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTCURBALMAXBALLIVEUACC",
                xmlWriter);

            if (localTOTCURBALMAXBALLIVEUACC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTCURBALMAXBALLIVEUACC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTCURBALMAXBALLIVEUACC);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_DATE", xmlWriter);

            if (localOUTPUT_WRITE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_DATE", xmlWriter);

            if (localOUTPUT_READ_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOFADDITIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOFADDITION", xmlWriter);

            if (localDATEOFADDITION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOFADDITION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOFADDITION);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_KEYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_KEY", xmlWriter);

            if (localACCOUNT_KEY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_KEY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_KEY);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ExperianSRespType2 parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ExperianSRespType2 object = new ExperianSRespType2();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ExperianSRespType2".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ExperianSRespType2) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSREPORTTIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSREPORTTIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSREPORTTIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSREPORTTIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSREPORTNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSREPORTNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSREPORTNUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSREPORTNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRY_REASON_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRY_REASON_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_REASON_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_REASON_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSENQUIRYREASON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSENQUIRYREASON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSENQUIRYREASON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSENQUIRYREASON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FINANCE_PURPOSE_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FINANCE_PURPOSE_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FINANCE_PURPOSE_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFINANCE_PURPOSE_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSFINANCEPURPOSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSFINANCEPURPOSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSFINANCEPURPOSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSFINANCEPURPOSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAMOUNTFINANCED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAMOUNTFINANCED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAMOUNTFINANCED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAMOUNTFINANCED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSDURATIONOFAGREEMENT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSDURATIONOFAGREEMENT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSDURATIONOFAGREEMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSDURATIONOFAGREEMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTLASTNAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTLASTNAME").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTLASTNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTLASTNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTFIRSTNAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTFIRSTNAME").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTFIRSTNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTFIRSTNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMIDDLENAME1").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMIDDLENAME1").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTMIDDLENAME1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTMIDDLENAME1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMIDDLENAME2").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMIDDLENAME2").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTMIDDLENAME2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTMIDDLENAME2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMIDDLENAME3").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMIDDLENAME3").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTMIDDLENAME3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTMIDDLENAME3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTGENDERCODE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTGENDERCODE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTGENDERCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTGENDERCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTGENDER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTGENDER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTGENDER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTGENDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSINCOME_TAX_PAN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSINCOME_TAX_PAN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSINCOME_TAX_PAN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSINCOME_TAX_PAN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSPAN_ISSUE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSPAN_ISSUE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSPAN_ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSPAN_ISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSPAN_EXP_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSPAN_EXP_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSPAN_EXP_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSPAN_EXP_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSPASSPORT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSPASSPORT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSPASSPORT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSPASSPORT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSPASSPORT_ISSUE_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSPASSPORT_ISSUE_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSPASSPORT_ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSPASSPORT_ISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSPASSPORT_EXP_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSPASSPORT_EXP_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSPASSPORT_EXP_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSPASSPORT_EXP_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSVOTER_IDENTITY_CARD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSVOTER_IDENTITY_CARD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSVOTER_IDENTITY_CARD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSVOTER_IDENTITY_CARD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSVOTER_ID_ISSUE_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSVOTER_ID_ISSUE_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSVOTER_ID_ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSVOTER_ID_ISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSVOTER_ID_EXP_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSVOTER_ID_EXP_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSVOTER_ID_EXP_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSVOTER_ID_EXP_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSDRIVER_LICENSE_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSDRIVER_LICENSE_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSDRIVER_LICENSE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSDRIVER_LICENSE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSDRIVER_LICENSE_ISSUE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSDRIVER_LICENSE_ISSUE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSDRIVER_LICENSE_ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSDRIVER_LICENSE_ISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSDRIVER_LICENSE_EXP_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSDRIVER_LICENSE_EXP_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSDRIVER_LICENSE_EXP_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSDRIVER_LICENSE_EXP_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSRATION_CARD_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSRATION_CARD_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSRATION_CARD_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSRATION_CARD_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSRATION_CARD_ISSUE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSRATION_CARD_ISSUE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSRATION_CARD_ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSRATION_CARD_ISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSRATION_CARD_EXP_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSRATION_CARD_EXP_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSRATION_CARD_EXP_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSRATION_CARD_EXP_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSUNIVERSAL_ID_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSUNIVERSAL_ID_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSUNIVERSAL_ID_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSUNIVERSAL_ID_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSUNIVERSAL_ID_ISSUE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSUNIVERSAL_ID_ISSUE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSUNIVERSAL_ID_ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSUNIVERSAL_ID_ISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSUNIVERSAL_ID_EXP_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSUNIVERSAL_ID_EXP_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSUNIVERSAL_ID_EXP_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSUNIVERSAL_ID_EXP_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTDOBAPPLICANT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTDOBAPPLICANT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTDOBAPPLICANT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTDOBAPPLICANT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTTELNOAPPLICANT1ST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTTELNOAPPLICANT1ST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTTELNOAPPLICANT1ST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTTELNOAPPLICANT1ST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTTELEXT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTTELEXT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTTELEXT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTTELEXT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTTELTYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTTELTYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTTELTYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTTELTYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMOBILEPHONENUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMOBILEPHONENUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTMOBILEPHONENUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTMOBILEPHONENUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTEMAILID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTEMAILID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTEMAILID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTEMAILID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTINCOME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTINCOME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTINCOME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTINCOME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "APPLICANT_MARITALSTATUSCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "APPLICANT_MARITALSTATUSCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "APPLICANT_MARITALSTATUSCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAPPLICANT_MARITALSTATUSCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMARITALSTATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTMARITALSTATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTMARITALSTATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTMARITALSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTEMPLMTSTATUSCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTEMPLMTSTATUSCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTEMPLMTSTATUSCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTEMPLMTSTATUSCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTEMPLOYMENTSTATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTEMPLOYMENTSTATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTEMPLOYMENTSTATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTEMPLOYMENTSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTDATEWITHEMPLOYER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTDATEWITHEMPLOYER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTDATEWITHEMPLOYER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTDATEWITHEMPLOYER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLTNOMAJORCRDTCARDHELD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLTNOMAJORCRDTCARDHELD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLTNOMAJORCRDTCARDHELD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLTNOMAJORCRDTCARDHELD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLTFLATNOPLOTNOHOUSENO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLTFLATNOPLOTNOHOUSENO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLTFLATNOPLOTNOHOUSENO" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLTFLATNOPLOTNOHOUSENO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTBLDGNOSOCIETYNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTBLDGNOSOCIETYNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTBLDGNOSOCIETYNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTBLDGNOSOCIETYNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLTRDNONAMEAREALOCALITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLTRDNONAMEAREALOCALITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLTRDNONAMEAREALOCALITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLTRDNONAMEAREALOCALITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTCITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTCITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTCITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTCITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTLANDMARK").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTLANDMARK").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTLANDMARK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTLANDMARK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTSTATECODE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTSTATECODE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTSTATECODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTSTATECODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTSTATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTSTATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTSTATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTSTATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTPINCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSAPPLICANTPINCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTPINCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTPINCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTCOUNTRYCODE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAPSAPPLICANTCOUNTRYCODE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSAPPLICANTCOUNTRYCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSAPPLICANTCOUNTRYCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BUREAUSCORE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BUREAUSCORE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BUREAUSCORE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBUREAUSCORE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "BUREAUSCORECONFIDLEVEL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "BUREAUSCORECONFIDLEVEL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BUREAUSCORECONFIDLEVEL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBUREAUSCORECONFIDLEVEL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITRATING").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITRATING").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITRATING" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITRATING(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFBFHLCADEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFBFHLCADEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFBFHLCADEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFBFHLCADEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFBFHLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFBFHLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFBFHLCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFBFHLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMRBFHLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMRBFHLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRBFHLCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRBFHLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFHLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFHLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFHLCAD" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFHLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFHLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFHLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFHLCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFHLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMRHLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMRHLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRHLCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRHLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFTELCOSCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFTELCOSCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFTELCOSCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFTELCOSCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFTELCOSCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFTELCOSCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFTELCOSCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFTELCOSCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMRTELCOSCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMRTELCOSCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRTELCOSCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRTELCOSCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFMFCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFMFCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFMFCAD" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFMFCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFMFCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFMFCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFMFCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFMFCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMRMFCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMRMFCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRMFCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRMFCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFRETAILCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFRETAILCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFRETAILCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFRETAILCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFRETAILCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFRETAILCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFRETAILCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFRETAILCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMRRETAILCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMRRETAILCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRRETAILCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRRETAILCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFALLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFALLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFALLCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFALLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFALLCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFALLCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFALLCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFALLCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMRCADALL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMRCADALL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRCADALL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRCADALL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFBFHLACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFBFHLACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BALBFHLACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BALBFHLACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BALBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBALBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTBFHLACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTBFHLACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR6MNTBFHLACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR6MNTBFHLACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR6MNTBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR6MNTBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "WDSPR712MNTBFHLACAEXHL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "WDSPR712MNTBFHLACAEXHL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR712MNTBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR712MNTBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "AGEOFOLDESTBFHLACAEXHL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "AGEOFOLDESTBFHLACAEXHL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "HCBPERREVACCBFHLACAEXHL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "HCBPERREVACCBFHLACAEXHL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HCBPERREVACCBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHCBPERREVACCBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TCBPERREVACCBFHLACAEXHL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TCBPERREVACCBFHLACAEXHL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TCBPERREVACCBFHLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTCBPERREVACCBFHLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFHLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFHLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFHLACA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFHLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BALHLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BALHLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BALHLACA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBALHLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTHLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTHLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTHLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTHLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR6MNTHLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR6MNTHLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR6MNTHLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR6MNTHLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR712MNTHLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR712MNTHLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR712MNTHLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR712MNTHLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGEOFOLDESTHLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGEOFOLDESTHLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTHLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTHLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFMFACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFMFACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFMFACA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFMFACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTALBALMFACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTALBALMFACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALBALMFACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALBALMFACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTMFACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTMFACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTMFACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTMFACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR6MNTMFACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR6MNTMFACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR6MNTMFACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR6MNTMFACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR712MNTMFACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR712MNTMFACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR712MNTMFACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR712MNTMFACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGEOFOLDESTMFACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGEOFOLDESTMFACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTMFACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTMFACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFTELCOSACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFTELCOSACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFTELCOSACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFTELCOSACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTALBALTELCOSACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTALBALTELCOSACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALBALTELCOSACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALBALTELCOSACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTTELCOSACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTTELCOSACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTTELCOSACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTTELCOSACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR6MNTTELCOSACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR6MNTTELCOSACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR6MNTTELCOSACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR6MNTTELCOSACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR712MNTTELCOSACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR712MNTTELCOSACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR712MNTTELCOSACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR712MNTTELCOSACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGEOFOLDESTTELCOSACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGEOFOLDESTTELCOSACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTTELCOSACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTTELCOSACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFRETAILACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFRETAILACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFRETAILACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFRETAILACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTALBALRETAILACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTALBALRETAILACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALBALRETAILACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALBALRETAILACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTRETAILACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTRETAILACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTRETAILACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTRETAILACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR6MNTRETAILACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR6MNTRETAILACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR6MNTRETAILACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR6MNTRETAILACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR712MNTRETAILACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR712MNTRETAILACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR712MNTRETAILACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR712MNTRETAILACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGEOFOLDESTRETAILACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGEOFOLDESTRETAILACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTRETAILACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTRETAILACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HCBLMPERREVACCRET").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "HCBLMPERREVACCRET").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HCBLMPERREVACCRET" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHCBLMPERREVACCRET(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTCURBALLMPERREVACCRET").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTCURBALLMPERREVACCRET").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTCURBALLMPERREVACCRET" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTCURBALLMPERREVACCRET(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFALLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFALLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFALLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFALLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BALALLACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BALALLACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BALALLACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBALALLACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTALLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTALLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTALLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTALLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR6MNTALLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR6MNTALLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR6MNTALLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR6MNTALLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WDSPR712MNTALLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WDSPR712MNTALLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WDSPR712MNTALLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWDSPR712MNTALLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGEOFOLDESTALLACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGEOFOLDESTALLACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTALLACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTALLACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TNOFNDELBFHLINACAEXHL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TNOFNDELBFHLINACAEXHL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFNDELBFHLINACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFNDELBFHLINACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFDELBFHLINACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFDELBFHLINACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFDELBFHLINACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFDELBFHLINACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFNDELHLINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFNDELHLINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFNDELHLINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFNDELHLINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFDELHLINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFDELHLINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFDELHLINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFDELHLINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFNDELMFINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFNDELMFINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFNDELMFINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFNDELMFINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFDELMFINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFDELMFINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFDELMFINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFDELMFINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFNDELTELCOSINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFNDELTELCOSINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFNDELTELCOSINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFNDELTELCOSINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFDELTELCOSINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFDELTELCOSINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFDELTELCOSINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFDELTELCOSINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFNDELRETAILINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFNDELRETAILINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFNDELRETAILINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFNDELRETAILINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFDELRETAILINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFDELRETAILINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFDELRETAILINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFDELRETAILINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BFHLCAPSLAST90DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BFHLCAPSLAST90DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BFHLCAPSLAST90DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBFHLCAPSLAST90DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MFCAPSLAST90DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MFCAPSLAST90DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MFCAPSLAST90DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMFCAPSLAST90DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELCOSCAPSLAST90DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELCOSCAPSLAST90DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELCOSCAPSLAST90DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELCOSCAPSLAST90DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RETAILCAPSLAST90DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RETAILCAPSLAST90DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RETAILCAPSLAST90DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRETAILCAPSLAST90DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFOCOMCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFOCOMCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFOCOMCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFOCOMCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTVALOFOCOMCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTVALOFOCOMCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTVALOFOCOMCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTVALOFOCOMCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MNTSMROCOMCAD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MNTSMROCOMCAD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMROCOMCAD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMROCOMCAD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFOCOMACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFOCOMACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFOCOMACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFOCOMACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BALOCOMACAEXHL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BALOCOMACAEXHL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BALOCOMACAEXHL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBALOCOMACAEXHL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BALOCOMACAHLONLY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BALOCOMACAHLONLY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BALOCOMACAHLONLY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBALOCOMACAHLONLY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WCDSTOCOMACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WCDSTOCOMACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WCDSTOCOMACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWCDSTOCOMACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HCBLMPERREVOCOMACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "HCBLMPERREVOCOMACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HCBLMPERREVOCOMACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHCBLMPERREVOCOMACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFNDELOCOMINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFNDELOCOMINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFNDELOCOMINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFNDELOCOMINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TNOFDELOCOMINACA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TNOFDELOCOMINACA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFDELOCOMINACA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFDELOCOMINACA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TNOFOCOMCAPSLAST90DAYS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TNOFOCOMCAPSLAST90DAYS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFOCOMCAPSLAST90DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFOCOMCAPSLAST90DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ANYRELCBDATADISYN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ANYRELCBDATADISYN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ANYRELCBDATADISYN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setANYRELCBDATADISYN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OTHRELCBDFCPOSMATYN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OTHRELCBDFCPOSMATYN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OTHRELCBDFCPOSMATYN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOTHRELCBDFCPOSMATYN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TNOFCADCLASSEDASSFWDWO").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TNOFCADCLASSEDASSFWDWO").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TNOFCADCLASSEDASSFWDWO" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTNOFCADCLASSEDASSFWDWO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MNTSMRCADCLASSEDASSFWDWO").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MNTSMRCADCLASSEDASSFWDWO").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTSMRCADCLASSEDASSFWDWO" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTSMRCADCLASSEDASSFWDWO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NUMOFCADSFWDWOLAST24MNT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NUMOFCADSFWDWOLAST24MNT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUMOFCADSFWDWOLAST24MNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUMOFCADSFWDWOLAST24MNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTCURBALLIVESACC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTCURBALLIVESACC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTCURBALLIVESACC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTCURBALLIVESACC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTCURBALLIVEUACC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTCURBALLIVEUACC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTCURBALLIVEUACC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTCURBALLIVEUACC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTCURBALMAXBALLIVESACC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTCURBALMAXBALLIVESACC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTCURBALMAXBALLIVESACC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTCURBALMAXBALLIVESACC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTCURBALMAXBALLIVEUACC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTCURBALMAXBALLIVEUACC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTCURBALMAXBALLIVEUACC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTCURBALMAXBALLIVEUACC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEOFADDITION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEOFADDITION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOFADDITION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOFADDITION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_KEY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_KEY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_KEY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_KEY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
