/**
 * MBEoTRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  MBEoTRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class MBEoTRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = MBEoTRequestType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for HEADER
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.EotHeaderType localHEADER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHEADERTracker = false;

    /**
     * field for ACKNOWLEDGEMENTID
     */
    protected java.lang.String localACKNOWLEDGEMENTID;

    /**
     * field for STATUS
     */
    protected java.lang.String localSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUSTracker = false;

    /**
     * field for SENTTOCIBIL
     */
    protected java.lang.String localSENTTOCIBIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENTTOCIBILTracker = false;

    /**
     * field for SENTTOEQUIFAX
     */
    protected java.lang.String localSENTTOEQUIFAX;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENTTOEQUIFAXTracker = false;

    /**
     * field for SENTTOEXPERIAN
     */
    protected java.lang.String localSENTTOEXPERIAN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENTTOEXPERIANTracker = false;

    /**
     * field for SENTTOCHM
     */
    protected java.lang.String localSENTTOCHM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENTTOCHMTracker = false;

    /**
     * field for SoaFillers
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaFillers localSoaFillers;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSoaFillersTracker = false;

    /**
     * field for SoaStandard
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaStandard localSoaStandard;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSoaStandardTracker = false;

    public boolean isHEADERSpecified() {
        return localHEADERTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.EotHeaderType
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.EotHeaderType getHEADER() {
        return localHEADER;
    }

    /**
     * Auto generated setter method
     * @param param HEADER
     */
    public void setHEADER(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EotHeaderType param) {
        localHEADERTracker = param != null;

        this.localHEADER = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACKNOWLEDGEMENTID() {
        return localACKNOWLEDGEMENTID;
    }

    /**
     * Auto generated setter method
     * @param param ACKNOWLEDGEMENTID
     */
    public void setACKNOWLEDGEMENTID(java.lang.String param) {
        this.localACKNOWLEDGEMENTID = param;
    }

    public boolean isSTATUSSpecified() {
        return localSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS() {
        return localSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param STATUS
     */
    public void setSTATUS(java.lang.String param) {
        localSTATUSTracker = param != null;

        this.localSTATUS = param;
    }

    public boolean isSENTTOCIBILSpecified() {
        return localSENTTOCIBILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENTTOCIBIL() {
        return localSENTTOCIBIL;
    }

    /**
     * Auto generated setter method
     * @param param SENTTOCIBIL
     */
    public void setSENTTOCIBIL(java.lang.String param) {
        localSENTTOCIBILTracker = param != null;

        this.localSENTTOCIBIL = param;
    }

    public boolean isSENTTOEQUIFAXSpecified() {
        return localSENTTOEQUIFAXTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENTTOEQUIFAX() {
        return localSENTTOEQUIFAX;
    }

    /**
     * Auto generated setter method
     * @param param SENTTOEQUIFAX
     */
    public void setSENTTOEQUIFAX(java.lang.String param) {
        localSENTTOEQUIFAXTracker = param != null;

        this.localSENTTOEQUIFAX = param;
    }

    public boolean isSENTTOEXPERIANSpecified() {
        return localSENTTOEXPERIANTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENTTOEXPERIAN() {
        return localSENTTOEXPERIAN;
    }

    /**
     * Auto generated setter method
     * @param param SENTTOEXPERIAN
     */
    public void setSENTTOEXPERIAN(java.lang.String param) {
        localSENTTOEXPERIANTracker = param != null;

        this.localSENTTOEXPERIAN = param;
    }

    public boolean isSENTTOCHMSpecified() {
        return localSENTTOCHMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENTTOCHM() {
        return localSENTTOCHM;
    }

    /**
     * Auto generated setter method
     * @param param SENTTOCHM
     */
    public void setSENTTOCHM(java.lang.String param) {
        localSENTTOCHMTracker = param != null;

        this.localSENTTOCHM = param;
    }

    public boolean isSoaFillersSpecified() {
        return localSoaFillersTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaFillers
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaFillers getSoaFillers() {
        return localSoaFillers;
    }

    /**
     * Auto generated setter method
     * @param param SoaFillers
     */
    public void setSoaFillers(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaFillers param) {
        localSoaFillersTracker = param != null;

        this.localSoaFillers = param;
    }

    public boolean isSoaStandardSpecified() {
        return localSoaStandardTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaStandard
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaStandard getSoaStandard() {
        return localSoaStandard;
    }

    /**
     * Auto generated setter method
     * @param param SoaStandard
     */
    public void setSoaStandard(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaStandard param) {
        localSoaStandardTracker = param != null;

        this.localSoaStandard = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":MBEoTRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "MBEoTRequestType", xmlWriter);
            }
        }

        if (localHEADERTracker) {
            if (localHEADER == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "HEADER cannot be null!!");
            }

            localHEADER.serialize(new javax.xml.namespace.QName("", "HEADER"),
                xmlWriter);
        }

        namespace = "";
        writeStartElement(null, namespace, "ACKNOWLEDGEMENT-ID", xmlWriter);

        if (localACKNOWLEDGEMENTID == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "ACKNOWLEDGEMENT-ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localACKNOWLEDGEMENTID);
        }

        xmlWriter.writeEndElement();

        if (localSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS", xmlWriter);

            if (localSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENTTOCIBILTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SENT-TO-CIBIL", xmlWriter);

            if (localSENTTOCIBIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT-TO-CIBIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENTTOCIBIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENTTOEQUIFAXTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SENT-TO-EQUIFAX", xmlWriter);

            if (localSENTTOEQUIFAX == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT-TO-EQUIFAX cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENTTOEQUIFAX);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENTTOEXPERIANTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SENT-TO-EXPERIAN", xmlWriter);

            if (localSENTTOEXPERIAN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT-TO-EXPERIAN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENTTOEXPERIAN);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENTTOCHMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SENT-TO-CHM", xmlWriter);

            if (localSENTTOCHM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT-TO-CHM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENTTOCHM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSoaFillersTracker) {
            if (localSoaFillers == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "soaFillers cannot be null!!");
            }

            localSoaFillers.serialize(new javax.xml.namespace.QName("",
                    "soaFillers"), xmlWriter);
        }

        if (localSoaStandardTracker) {
            if (localSoaStandard == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "soaStandard cannot be null!!");
            }

            localSoaStandard.serialize(new javax.xml.namespace.QName("",
                    "soaStandard"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static MBEoTRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            MBEoTRequestType object = new MBEoTRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"MBEoTRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (MBEoTRequestType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HEADER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "HEADER").equals(
                            reader.getName())) {
                    object.setHEADER(com.softcell.soap.mb.hdfcbank.xsd.multibureau.EotHeaderType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACKNOWLEDGEMENT-ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACKNOWLEDGEMENT-ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACKNOWLEDGEMENT-ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACKNOWLEDGEMENTID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SENT-TO-CIBIL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT-TO-CIBIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT-TO-CIBIL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENTTOCIBIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SENT-TO-EQUIFAX").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT-TO-EQUIFAX").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT-TO-EQUIFAX" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENTTOEQUIFAX(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SENT-TO-EXPERIAN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT-TO-EXPERIAN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT-TO-EXPERIAN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENTTOEXPERIAN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SENT-TO-CHM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT-TO-CHM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT-TO-CHM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENTTOCHM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "soaFillers").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "soaFillers").equals(
                            reader.getName())) {
                    object.setSoaFillers(com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaFillers.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "soaStandard").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "soaStandard").equals(
                            reader.getName())) {
                    object.setSoaStandard(com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaStandard.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
