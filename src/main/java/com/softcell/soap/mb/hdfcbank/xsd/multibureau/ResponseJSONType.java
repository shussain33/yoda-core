/**
 * ResponseJSONType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ResponseJSONType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ResponseJSONType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ResponseJSONType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for CIBIL_SROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[] localCIBIL_SROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCIBIL_SROP_DOMAIN_LISTTracker = false;

    /**
     * field for CIBIL_EROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[] localCIBIL_EROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCIBIL_EROP_DOMAIN_LISTTracker = false;

    /**
     * field for EQUIFAX_SROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[] localEQUIFAX_SROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEQUIFAX_SROP_DOMAIN_LISTTracker = false;

    /**
     * field for EQUIFAX_EROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[] localEQUIFAX_EROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEQUIFAX_EROP_DOMAIN_LISTTracker = false;

    /**
     * field for EXPERIAN_SROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[] localEXPERIAN_SROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPERIAN_SROP_DOMAIN_LISTTracker = false;

    /**
     * field for EXPERIAN_EROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[] localEXPERIAN_EROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPERIAN_EROP_DOMAIN_LISTTracker = false;

    /**
     * field for CHM_BASE_SROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[] localCHM_BASE_SROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCHM_BASE_SROP_DOMAIN_LISTTracker = false;

    /**
     * field for CHM_BASE_EROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[] localCHM_BASE_EROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCHM_BASE_EROP_DOMAIN_LISTTracker = false;

    /**
     * field for CHM_AOR_SROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[] localCHM_AOR_SROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCHM_AOR_SROP_DOMAIN_LISTTracker = false;

    /**
     * field for CHM_AOR_EROP_DOMAIN_LIST
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[] localCHM_AOR_EROP_DOMAIN_LIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCHM_AOR_EROP_DOMAIN_LISTTracker = false;

    public boolean isCIBIL_SROP_DOMAIN_LISTSpecified() {
        return localCIBIL_SROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[] getCIBIL_SROP_DOMAIN_LIST() {
        return localCIBIL_SROP_DOMAIN_LIST;
    }

    /**
     * validate the array for CIBIL_SROP_DOMAIN_LIST
     */
    protected void validateCIBIL_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param CIBIL_SROP_DOMAIN_LIST
     */
    public void setCIBIL_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[] param) {
        validateCIBIL_SROP_DOMAIN_LIST(param);

        localCIBIL_SROP_DOMAIN_LISTTracker = param != null;

        this.localCIBIL_SROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType
     */
    public void addCIBIL_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType param) {
        if (localCIBIL_SROP_DOMAIN_LIST == null) {
            localCIBIL_SROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[] {
                    
                };
        }

        //update the setting tracker
        localCIBIL_SROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCIBIL_SROP_DOMAIN_LIST);
        list.add(param);
        this.localCIBIL_SROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[list.size()]);
    }

    public boolean isCIBIL_EROP_DOMAIN_LISTSpecified() {
        return localCIBIL_EROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[] getCIBIL_EROP_DOMAIN_LIST() {
        return localCIBIL_EROP_DOMAIN_LIST;
    }

    /**
     * validate the array for CIBIL_EROP_DOMAIN_LIST
     */
    protected void validateCIBIL_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param CIBIL_EROP_DOMAIN_LIST
     */
    public void setCIBIL_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[] param) {
        validateCIBIL_EROP_DOMAIN_LIST(param);

        localCIBIL_EROP_DOMAIN_LISTTracker = param != null;

        this.localCIBIL_EROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType
     */
    public void addCIBIL_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType param) {
        if (localCIBIL_EROP_DOMAIN_LIST == null) {
            localCIBIL_EROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[] {
                    
                };
        }

        //update the setting tracker
        localCIBIL_EROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCIBIL_EROP_DOMAIN_LIST);
        list.add(param);
        this.localCIBIL_EROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[list.size()]);
    }

    public boolean isEQUIFAX_SROP_DOMAIN_LISTSpecified() {
        return localEQUIFAX_SROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[] getEQUIFAX_SROP_DOMAIN_LIST() {
        return localEQUIFAX_SROP_DOMAIN_LIST;
    }

    /**
     * validate the array for EQUIFAX_SROP_DOMAIN_LIST
     */
    protected void validateEQUIFAX_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param EQUIFAX_SROP_DOMAIN_LIST
     */
    public void setEQUIFAX_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[] param) {
        validateEQUIFAX_SROP_DOMAIN_LIST(param);

        localEQUIFAX_SROP_DOMAIN_LISTTracker = param != null;

        this.localEQUIFAX_SROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType
     */
    public void addEQUIFAX_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType param) {
        if (localEQUIFAX_SROP_DOMAIN_LIST == null) {
            localEQUIFAX_SROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[] {
                    
                };
        }

        //update the setting tracker
        localEQUIFAX_SROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localEQUIFAX_SROP_DOMAIN_LIST);
        list.add(param);
        this.localEQUIFAX_SROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[list.size()]);
    }

    public boolean isEQUIFAX_EROP_DOMAIN_LISTSpecified() {
        return localEQUIFAX_EROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[] getEQUIFAX_EROP_DOMAIN_LIST() {
        return localEQUIFAX_EROP_DOMAIN_LIST;
    }

    /**
     * validate the array for EQUIFAX_EROP_DOMAIN_LIST
     */
    protected void validateEQUIFAX_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param EQUIFAX_EROP_DOMAIN_LIST
     */
    public void setEQUIFAX_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[] param) {
        validateEQUIFAX_EROP_DOMAIN_LIST(param);

        localEQUIFAX_EROP_DOMAIN_LISTTracker = param != null;

        this.localEQUIFAX_EROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType
     */
    public void addEQUIFAX_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType param) {
        if (localEQUIFAX_EROP_DOMAIN_LIST == null) {
            localEQUIFAX_EROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[] {
                    
                };
        }

        //update the setting tracker
        localEQUIFAX_EROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localEQUIFAX_EROP_DOMAIN_LIST);
        list.add(param);
        this.localEQUIFAX_EROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[list.size()]);
    }

    public boolean isEXPERIAN_SROP_DOMAIN_LISTSpecified() {
        return localEXPERIAN_SROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[] getEXPERIAN_SROP_DOMAIN_LIST() {
        return localEXPERIAN_SROP_DOMAIN_LIST;
    }

    /**
     * validate the array for EXPERIAN_SROP_DOMAIN_LIST
     */
    protected void validateEXPERIAN_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param EXPERIAN_SROP_DOMAIN_LIST
     */
    public void setEXPERIAN_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[] param) {
        validateEXPERIAN_SROP_DOMAIN_LIST(param);

        localEXPERIAN_SROP_DOMAIN_LISTTracker = param != null;

        this.localEXPERIAN_SROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType
     */
    public void addEXPERIAN_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType param) {
        if (localEXPERIAN_SROP_DOMAIN_LIST == null) {
            localEXPERIAN_SROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[] {
                    
                };
        }

        //update the setting tracker
        localEXPERIAN_SROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localEXPERIAN_SROP_DOMAIN_LIST);
        list.add(param);
        this.localEXPERIAN_SROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[list.size()]);
    }

    public boolean isEXPERIAN_EROP_DOMAIN_LISTSpecified() {
        return localEXPERIAN_EROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[] getEXPERIAN_EROP_DOMAIN_LIST() {
        return localEXPERIAN_EROP_DOMAIN_LIST;
    }

    /**
     * validate the array for EXPERIAN_EROP_DOMAIN_LIST
     */
    protected void validateEXPERIAN_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param EXPERIAN_EROP_DOMAIN_LIST
     */
    public void setEXPERIAN_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[] param) {
        validateEXPERIAN_EROP_DOMAIN_LIST(param);

        localEXPERIAN_EROP_DOMAIN_LISTTracker = param != null;

        this.localEXPERIAN_EROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType
     */
    public void addEXPERIAN_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType param) {
        if (localEXPERIAN_EROP_DOMAIN_LIST == null) {
            localEXPERIAN_EROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[] {
                    
                };
        }

        //update the setting tracker
        localEXPERIAN_EROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localEXPERIAN_EROP_DOMAIN_LIST);
        list.add(param);
        this.localEXPERIAN_EROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[list.size()]);
    }

    public boolean isCHM_BASE_SROP_DOMAIN_LISTSpecified() {
        return localCHM_BASE_SROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[] getCHM_BASE_SROP_DOMAIN_LIST() {
        return localCHM_BASE_SROP_DOMAIN_LIST;
    }

    /**
     * validate the array for CHM_BASE_SROP_DOMAIN_LIST
     */
    protected void validateCHM_BASE_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param CHM_BASE_SROP_DOMAIN_LIST
     */
    public void setCHM_BASE_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[] param) {
        validateCHM_BASE_SROP_DOMAIN_LIST(param);

        localCHM_BASE_SROP_DOMAIN_LISTTracker = param != null;

        this.localCHM_BASE_SROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType
     */
    public void addCHM_BASE_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType param) {
        if (localCHM_BASE_SROP_DOMAIN_LIST == null) {
            localCHM_BASE_SROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[] {
                    
                };
        }

        //update the setting tracker
        localCHM_BASE_SROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCHM_BASE_SROP_DOMAIN_LIST);
        list.add(param);
        this.localCHM_BASE_SROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[list.size()]);
    }

    public boolean isCHM_BASE_EROP_DOMAIN_LISTSpecified() {
        return localCHM_BASE_EROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[] getCHM_BASE_EROP_DOMAIN_LIST() {
        return localCHM_BASE_EROP_DOMAIN_LIST;
    }

    /**
     * validate the array for CHM_BASE_EROP_DOMAIN_LIST
     */
    protected void validateCHM_BASE_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param CHM_BASE_EROP_DOMAIN_LIST
     */
    public void setCHM_BASE_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[] param) {
        validateCHM_BASE_EROP_DOMAIN_LIST(param);

        localCHM_BASE_EROP_DOMAIN_LISTTracker = param != null;

        this.localCHM_BASE_EROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType
     */
    public void addCHM_BASE_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType param) {
        if (localCHM_BASE_EROP_DOMAIN_LIST == null) {
            localCHM_BASE_EROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[] {
                    
                };
        }

        //update the setting tracker
        localCHM_BASE_EROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCHM_BASE_EROP_DOMAIN_LIST);
        list.add(param);
        this.localCHM_BASE_EROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[list.size()]);
    }

    public boolean isCHM_AOR_SROP_DOMAIN_LISTSpecified() {
        return localCHM_AOR_SROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[] getCHM_AOR_SROP_DOMAIN_LIST() {
        return localCHM_AOR_SROP_DOMAIN_LIST;
    }

    /**
     * validate the array for CHM_AOR_SROP_DOMAIN_LIST
     */
    protected void validateCHM_AOR_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param CHM_AOR_SROP_DOMAIN_LIST
     */
    public void setCHM_AOR_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[] param) {
        validateCHM_AOR_SROP_DOMAIN_LIST(param);

        localCHM_AOR_SROP_DOMAIN_LISTTracker = param != null;

        this.localCHM_AOR_SROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType
     */
    public void addCHM_AOR_SROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType param) {
        if (localCHM_AOR_SROP_DOMAIN_LIST == null) {
            localCHM_AOR_SROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[] {
                    
                };
        }

        //update the setting tracker
        localCHM_AOR_SROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCHM_AOR_SROP_DOMAIN_LIST);
        list.add(param);
        this.localCHM_AOR_SROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[list.size()]);
    }

    public boolean isCHM_AOR_EROP_DOMAIN_LISTSpecified() {
        return localCHM_AOR_EROP_DOMAIN_LISTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[] getCHM_AOR_EROP_DOMAIN_LIST() {
        return localCHM_AOR_EROP_DOMAIN_LIST;
    }

    /**
     * validate the array for CHM_AOR_EROP_DOMAIN_LIST
     */
    protected void validateCHM_AOR_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param CHM_AOR_EROP_DOMAIN_LIST
     */
    public void setCHM_AOR_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[] param) {
        validateCHM_AOR_EROP_DOMAIN_LIST(param);

        localCHM_AOR_EROP_DOMAIN_LISTTracker = param != null;

        this.localCHM_AOR_EROP_DOMAIN_LIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType
     */
    public void addCHM_AOR_EROP_DOMAIN_LIST(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType param) {
        if (localCHM_AOR_EROP_DOMAIN_LIST == null) {
            localCHM_AOR_EROP_DOMAIN_LIST = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[] {
                    
                };
        }

        //update the setting tracker
        localCHM_AOR_EROP_DOMAIN_LISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCHM_AOR_EROP_DOMAIN_LIST);
        list.add(param);
        this.localCHM_AOR_EROP_DOMAIN_LIST = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[list.size()]);
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ResponseJSONType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ResponseJSONType", xmlWriter);
            }
        }

        if (localCIBIL_SROP_DOMAIN_LISTTracker) {
            if (localCIBIL_SROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localCIBIL_SROP_DOMAIN_LIST.length; i++) {
                    if (localCIBIL_SROP_DOMAIN_LIST[i] != null) {
                        localCIBIL_SROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "CIBIL_SROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "CIBIL_SROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localCIBIL_EROP_DOMAIN_LISTTracker) {
            if (localCIBIL_EROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localCIBIL_EROP_DOMAIN_LIST.length; i++) {
                    if (localCIBIL_EROP_DOMAIN_LIST[i] != null) {
                        localCIBIL_EROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "CIBIL_EROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "CIBIL_EROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localEQUIFAX_SROP_DOMAIN_LISTTracker) {
            if (localEQUIFAX_SROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localEQUIFAX_SROP_DOMAIN_LIST.length;
                        i++) {
                    if (localEQUIFAX_SROP_DOMAIN_LIST[i] != null) {
                        localEQUIFAX_SROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "EQUIFAX_SROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "EQUIFAX_SROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localEQUIFAX_EROP_DOMAIN_LISTTracker) {
            if (localEQUIFAX_EROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localEQUIFAX_EROP_DOMAIN_LIST.length;
                        i++) {
                    if (localEQUIFAX_EROP_DOMAIN_LIST[i] != null) {
                        localEQUIFAX_EROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "EQUIFAX_EROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "EQUIFAX_EROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localEXPERIAN_SROP_DOMAIN_LISTTracker) {
            if (localEXPERIAN_SROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localEXPERIAN_SROP_DOMAIN_LIST.length;
                        i++) {
                    if (localEXPERIAN_SROP_DOMAIN_LIST[i] != null) {
                        localEXPERIAN_SROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "EXPERIAN_SROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPERIAN_SROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localEXPERIAN_EROP_DOMAIN_LISTTracker) {
            if (localEXPERIAN_EROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localEXPERIAN_EROP_DOMAIN_LIST.length;
                        i++) {
                    if (localEXPERIAN_EROP_DOMAIN_LIST[i] != null) {
                        localEXPERIAN_EROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "EXPERIAN_EROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPERIAN_EROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localCHM_BASE_SROP_DOMAIN_LISTTracker) {
            if (localCHM_BASE_SROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localCHM_BASE_SROP_DOMAIN_LIST.length;
                        i++) {
                    if (localCHM_BASE_SROP_DOMAIN_LIST[i] != null) {
                        localCHM_BASE_SROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "CHM_BASE_SROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "CHM_BASE_SROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localCHM_BASE_EROP_DOMAIN_LISTTracker) {
            if (localCHM_BASE_EROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localCHM_BASE_EROP_DOMAIN_LIST.length;
                        i++) {
                    if (localCHM_BASE_EROP_DOMAIN_LIST[i] != null) {
                        localCHM_BASE_EROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "CHM_BASE_EROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "CHM_BASE_EROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localCHM_AOR_SROP_DOMAIN_LISTTracker) {
            if (localCHM_AOR_SROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localCHM_AOR_SROP_DOMAIN_LIST.length;
                        i++) {
                    if (localCHM_AOR_SROP_DOMAIN_LIST[i] != null) {
                        localCHM_AOR_SROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "CHM_AOR_SROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "CHM_AOR_SROP_DOMAIN_LIST cannot be null!!");
            }
        }

        if (localCHM_AOR_EROP_DOMAIN_LISTTracker) {
            if (localCHM_AOR_EROP_DOMAIN_LIST != null) {
                for (int i = 0; i < localCHM_AOR_EROP_DOMAIN_LIST.length;
                        i++) {
                    if (localCHM_AOR_EROP_DOMAIN_LIST[i] != null) {
                        localCHM_AOR_EROP_DOMAIN_LIST[i].serialize(new javax.xml.namespace.QName(
                                "", "CHM_AOR_EROP_DOMAIN_LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "CHM_AOR_EROP_DOMAIN_LIST cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ResponseJSONType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ResponseJSONType object = new ResponseJSONType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ResponseJSONType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ResponseJSONType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list1 = new java.util.ArrayList();

                java.util.ArrayList list2 = new java.util.ArrayList();

                java.util.ArrayList list3 = new java.util.ArrayList();

                java.util.ArrayList list4 = new java.util.ArrayList();

                java.util.ArrayList list5 = new java.util.ArrayList();

                java.util.ArrayList list6 = new java.util.ArrayList();

                java.util.ArrayList list7 = new java.util.ArrayList();

                java.util.ArrayList list8 = new java.util.ArrayList();

                java.util.ArrayList list9 = new java.util.ArrayList();

                java.util.ArrayList list10 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CIBIL_SROP_DOMAIN_LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CIBIL_SROP_DOMAIN_LIST").equals(reader.getName())) {
                    // Process the array and step past its final element's end.
                    list1.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone1 = false;

                    while (!loopDone1) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone1 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "CIBIL_SROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list1.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone1 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCIBIL_SROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType.class,
                            list1));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CIBIL_EROP_DOMAIN_LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CIBIL_EROP_DOMAIN_LIST").equals(reader.getName())) {
                    // Process the array and step past its final element's end.
                    list2.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone2 = false;

                    while (!loopDone2) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone2 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "CIBIL_EROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list2.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone2 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCIBIL_EROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType.class,
                            list2));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EQUIFAX_SROP_DOMAIN_LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EQUIFAX_SROP_DOMAIN_LIST").equals(reader.getName())) {
                    // Process the array and step past its final element's end.
                    list3.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone3 = false;

                    while (!loopDone3) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone3 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "EQUIFAX_SROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list3.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone3 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setEQUIFAX_SROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType.class,
                            list3));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EQUIFAX_EROP_DOMAIN_LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EQUIFAX_EROP_DOMAIN_LIST").equals(reader.getName())) {
                    // Process the array and step past its final element's end.
                    list4.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone4 = false;

                    while (!loopDone4) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone4 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "EQUIFAX_EROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list4.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone4 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setEQUIFAX_EROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType.class,
                            list4));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EXPERIAN_SROP_DOMAIN_LIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EXPERIAN_SROP_DOMAIN_LIST").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list5.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone5 = false;

                    while (!loopDone5) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone5 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "EXPERIAN_SROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list5.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone5 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setEXPERIAN_SROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType.class,
                            list5));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EXPERIAN_EROP_DOMAIN_LIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EXPERIAN_EROP_DOMAIN_LIST").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list6.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone6 = false;

                    while (!loopDone6) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone6 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "EXPERIAN_EROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list6.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone6 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setEXPERIAN_EROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType.class,
                            list6));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CHM_BASE_SROP_DOMAIN_LIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CHM_BASE_SROP_DOMAIN_LIST").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list7.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone7 = false;

                    while (!loopDone7) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone7 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "CHM_BASE_SROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list7.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone7 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCHM_BASE_SROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType.class,
                            list7));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CHM_BASE_EROP_DOMAIN_LIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CHM_BASE_EROP_DOMAIN_LIST").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list8.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone8 = false;

                    while (!loopDone8) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone8 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "CHM_BASE_EROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list8.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone8 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCHM_BASE_EROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType.class,
                            list8));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CHM_AOR_SROP_DOMAIN_LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CHM_AOR_SROP_DOMAIN_LIST").equals(reader.getName())) {
                    // Process the array and step past its final element's end.
                    list9.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone9 = false;

                    while (!loopDone9) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone9 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "CHM_AOR_SROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list9.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone9 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCHM_AOR_SROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType.class,
                            list9));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CHM_AOR_EROP_DOMAIN_LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CHM_AOR_EROP_DOMAIN_LIST").equals(reader.getName())) {
                    // Process the array and step past its final element's end.
                    list10.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone10 = false;

                    while (!loopDone10) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone10 = true;
                        } else {
                            if (new javax.xml.namespace.QName("",
                                        "CHM_AOR_EROP_DOMAIN_LIST").equals(
                                        reader.getName())) {
                                list10.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType.Factory.parse(
                                        reader));
                            } else {
                                loopDone10 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCHM_AOR_EROP_DOMAIN_LIST((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType.class,
                            list10));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
