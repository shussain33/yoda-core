/**
 * CibilERespType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  CibilERespType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class CibilERespType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = CibilERespType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for ENQUIRY_DATE
     */
    protected java.lang.String localENQUIRY_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_DATETracker = false;

    /**
     * field for SEGMENT_TAG
     */
    protected java.lang.String localSEGMENT_TAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEGMENT_TAGTracker = false;

    /**
     * field for TYPE_OF_ERROR
     */
    protected java.lang.String localTYPE_OF_ERROR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTYPE_OF_ERRORTracker = false;

    /**
     * field for DATE_PROCESSED
     */
    protected java.lang.String localDATE_PROCESSED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_PROCESSEDTracker = false;

    /**
     * field for TIME_PROCESSED
     */
    protected java.lang.String localTIME_PROCESSED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTIME_PROCESSEDTracker = false;

    /**
     * field for ENQUIRY_MEMBER_CODE_ID
     */
    protected java.lang.String localENQUIRY_MEMBER_CODE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_MEMBER_CODE_IDTracker = false;

    /**
     * field for ERROR_TYPE_CODE
     */
    protected java.lang.String localERROR_TYPE_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERROR_TYPE_CODETracker = false;

    /**
     * field for ERROR_TYPE
     */
    protected java.lang.String localERROR_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERROR_TYPETracker = false;

    /**
     * field for DATA
     */
    protected java.lang.String localDATA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATATracker = false;

    /**
     * field for SEGMENT
     */
    protected java.lang.String localSEGMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEGMENTTracker = false;

    /**
     * field for SPECIFIED
     */
    protected java.lang.String localSPECIFIED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSPECIFIEDTracker = false;

    /**
     * field for EXPECTED
     */
    protected java.lang.String localEXPECTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPECTEDTracker = false;

    /**
     * field for INVALID_VERSION
     */
    protected java.lang.String localINVALID_VERSION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_VERSIONTracker = false;

    /**
     * field for INVALID_FIELD_LENGTH
     */
    protected java.lang.String localINVALID_FIELD_LENGTH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_FIELD_LENGTHTracker = false;

    /**
     * field for INVALID_TOTAL_LENGTH
     */
    protected java.lang.String localINVALID_TOTAL_LENGTH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_TOTAL_LENGTHTracker = false;

    /**
     * field for INVALID_ENQUIRY_PURPOSE
     */
    protected java.lang.String localINVALID_ENQUIRY_PURPOSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_ENQUIRY_PURPOSETracker = false;

    /**
     * field for INVALID_ENQUIRY_AMOUNT
     */
    protected java.lang.String localINVALID_ENQUIRY_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_ENQUIRY_AMOUNTTracker = false;

    /**
     * field for INVALID_ENQUIRY_MEMBER_USER_ID
     */
    protected java.lang.String localINVALID_ENQUIRY_MEMBER_USER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_ENQUIRY_MEMBER_USER_IDTracker = false;

    /**
     * field for REQUIRED_ENQ_SEG_MISSING
     */
    protected java.lang.String localREQUIRED_ENQ_SEG_MISSING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREQUIRED_ENQ_SEG_MISSINGTracker = false;

    /**
     * field for INVALID_ENQUIRY_DATA
     */
    protected java.lang.String localINVALID_ENQUIRY_DATA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_ENQUIRY_DATATracker = false;

    /**
     * field for CIBIL_SYSTEM_ERROR
     */
    protected java.lang.String localCIBIL_SYSTEM_ERROR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCIBIL_SYSTEM_ERRORTracker = false;

    /**
     * field for INVALID_SEGMENT_TAG
     */
    protected java.lang.String localINVALID_SEGMENT_TAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_SEGMENT_TAGTracker = false;

    /**
     * field for INVALID_SEGMENT_ORDER
     */
    protected java.lang.String localINVALID_SEGMENT_ORDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_SEGMENT_ORDERTracker = false;

    /**
     * field for INVALID_FIELD_TAG_ORDER
     */
    protected java.lang.String localINVALID_FIELD_TAG_ORDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_FIELD_TAG_ORDERTracker = false;

    /**
     * field for MISSING_REQUIRED_FIELD
     */
    protected java.lang.String localMISSING_REQUIRED_FIELD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMISSING_REQUIRED_FIELDTracker = false;

    /**
     * field for REQUESTED_RESP_SIZE_EXCEEDED
     */
    protected java.lang.String localREQUESTED_RESP_SIZE_EXCEEDED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREQUESTED_RESP_SIZE_EXCEEDEDTracker = false;

    /**
     * field for INVALID_INPUT_OUTPUT_MEDIA
     */
    protected java.lang.String localINVALID_INPUT_OUTPUT_MEDIA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINVALID_INPUT_OUTPUT_MEDIATracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_TIME
     */
    protected java.lang.String localOUTPUT_WRITE_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_TIMETracker = false;

    /**
     * field for OUTPUT_READ_TIME
     */
    protected java.lang.String localOUTPUT_READ_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_TIMETracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isENQUIRY_DATESpecified() {
        return localENQUIRY_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_DATE() {
        return localENQUIRY_DATE;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_DATE
     */
    public void setENQUIRY_DATE(java.lang.String param) {
        localENQUIRY_DATETracker = param != null;

        this.localENQUIRY_DATE = param;
    }

    public boolean isSEGMENT_TAGSpecified() {
        return localSEGMENT_TAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSEGMENT_TAG() {
        return localSEGMENT_TAG;
    }

    /**
     * Auto generated setter method
     * @param param SEGMENT_TAG
     */
    public void setSEGMENT_TAG(java.lang.String param) {
        localSEGMENT_TAGTracker = param != null;

        this.localSEGMENT_TAG = param;
    }

    public boolean isTYPE_OF_ERRORSpecified() {
        return localTYPE_OF_ERRORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTYPE_OF_ERROR() {
        return localTYPE_OF_ERROR;
    }

    /**
     * Auto generated setter method
     * @param param TYPE_OF_ERROR
     */
    public void setTYPE_OF_ERROR(java.lang.String param) {
        localTYPE_OF_ERRORTracker = param != null;

        this.localTYPE_OF_ERROR = param;
    }

    public boolean isDATE_PROCESSEDSpecified() {
        return localDATE_PROCESSEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_PROCESSED() {
        return localDATE_PROCESSED;
    }

    /**
     * Auto generated setter method
     * @param param DATE_PROCESSED
     */
    public void setDATE_PROCESSED(java.lang.String param) {
        localDATE_PROCESSEDTracker = param != null;

        this.localDATE_PROCESSED = param;
    }

    public boolean isTIME_PROCESSEDSpecified() {
        return localTIME_PROCESSEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTIME_PROCESSED() {
        return localTIME_PROCESSED;
    }

    /**
     * Auto generated setter method
     * @param param TIME_PROCESSED
     */
    public void setTIME_PROCESSED(java.lang.String param) {
        localTIME_PROCESSEDTracker = param != null;

        this.localTIME_PROCESSED = param;
    }

    public boolean isENQUIRY_MEMBER_CODE_IDSpecified() {
        return localENQUIRY_MEMBER_CODE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_MEMBER_CODE_ID() {
        return localENQUIRY_MEMBER_CODE_ID;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_MEMBER_CODE_ID
     */
    public void setENQUIRY_MEMBER_CODE_ID(java.lang.String param) {
        localENQUIRY_MEMBER_CODE_IDTracker = param != null;

        this.localENQUIRY_MEMBER_CODE_ID = param;
    }

    public boolean isERROR_TYPE_CODESpecified() {
        return localERROR_TYPE_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERROR_TYPE_CODE() {
        return localERROR_TYPE_CODE;
    }

    /**
     * Auto generated setter method
     * @param param ERROR_TYPE_CODE
     */
    public void setERROR_TYPE_CODE(java.lang.String param) {
        localERROR_TYPE_CODETracker = param != null;

        this.localERROR_TYPE_CODE = param;
    }

    public boolean isERROR_TYPESpecified() {
        return localERROR_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERROR_TYPE() {
        return localERROR_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param ERROR_TYPE
     */
    public void setERROR_TYPE(java.lang.String param) {
        localERROR_TYPETracker = param != null;

        this.localERROR_TYPE = param;
    }

    public boolean isDATASpecified() {
        return localDATATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATA() {
        return localDATA;
    }

    /**
     * Auto generated setter method
     * @param param DATA
     */
    public void setDATA(java.lang.String param) {
        localDATATracker = param != null;

        this.localDATA = param;
    }

    public boolean isSEGMENTSpecified() {
        return localSEGMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSEGMENT() {
        return localSEGMENT;
    }

    /**
     * Auto generated setter method
     * @param param SEGMENT
     */
    public void setSEGMENT(java.lang.String param) {
        localSEGMENTTracker = param != null;

        this.localSEGMENT = param;
    }

    public boolean isSPECIFIEDSpecified() {
        return localSPECIFIEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSPECIFIED() {
        return localSPECIFIED;
    }

    /**
     * Auto generated setter method
     * @param param SPECIFIED
     */
    public void setSPECIFIED(java.lang.String param) {
        localSPECIFIEDTracker = param != null;

        this.localSPECIFIED = param;
    }

    public boolean isEXPECTEDSpecified() {
        return localEXPECTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXPECTED() {
        return localEXPECTED;
    }

    /**
     * Auto generated setter method
     * @param param EXPECTED
     */
    public void setEXPECTED(java.lang.String param) {
        localEXPECTEDTracker = param != null;

        this.localEXPECTED = param;
    }

    public boolean isINVALID_VERSIONSpecified() {
        return localINVALID_VERSIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_VERSION() {
        return localINVALID_VERSION;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_VERSION
     */
    public void setINVALID_VERSION(java.lang.String param) {
        localINVALID_VERSIONTracker = param != null;

        this.localINVALID_VERSION = param;
    }

    public boolean isINVALID_FIELD_LENGTHSpecified() {
        return localINVALID_FIELD_LENGTHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_FIELD_LENGTH() {
        return localINVALID_FIELD_LENGTH;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_FIELD_LENGTH
     */
    public void setINVALID_FIELD_LENGTH(java.lang.String param) {
        localINVALID_FIELD_LENGTHTracker = param != null;

        this.localINVALID_FIELD_LENGTH = param;
    }

    public boolean isINVALID_TOTAL_LENGTHSpecified() {
        return localINVALID_TOTAL_LENGTHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_TOTAL_LENGTH() {
        return localINVALID_TOTAL_LENGTH;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_TOTAL_LENGTH
     */
    public void setINVALID_TOTAL_LENGTH(java.lang.String param) {
        localINVALID_TOTAL_LENGTHTracker = param != null;

        this.localINVALID_TOTAL_LENGTH = param;
    }

    public boolean isINVALID_ENQUIRY_PURPOSESpecified() {
        return localINVALID_ENQUIRY_PURPOSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_ENQUIRY_PURPOSE() {
        return localINVALID_ENQUIRY_PURPOSE;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_ENQUIRY_PURPOSE
     */
    public void setINVALID_ENQUIRY_PURPOSE(java.lang.String param) {
        localINVALID_ENQUIRY_PURPOSETracker = param != null;

        this.localINVALID_ENQUIRY_PURPOSE = param;
    }

    public boolean isINVALID_ENQUIRY_AMOUNTSpecified() {
        return localINVALID_ENQUIRY_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_ENQUIRY_AMOUNT() {
        return localINVALID_ENQUIRY_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_ENQUIRY_AMOUNT
     */
    public void setINVALID_ENQUIRY_AMOUNT(java.lang.String param) {
        localINVALID_ENQUIRY_AMOUNTTracker = param != null;

        this.localINVALID_ENQUIRY_AMOUNT = param;
    }

    public boolean isINVALID_ENQUIRY_MEMBER_USER_IDSpecified() {
        return localINVALID_ENQUIRY_MEMBER_USER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_ENQUIRY_MEMBER_USER_ID() {
        return localINVALID_ENQUIRY_MEMBER_USER_ID;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_ENQUIRY_MEMBER_USER_ID
     */
    public void setINVALID_ENQUIRY_MEMBER_USER_ID(java.lang.String param) {
        localINVALID_ENQUIRY_MEMBER_USER_IDTracker = param != null;

        this.localINVALID_ENQUIRY_MEMBER_USER_ID = param;
    }

    public boolean isREQUIRED_ENQ_SEG_MISSINGSpecified() {
        return localREQUIRED_ENQ_SEG_MISSINGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREQUIRED_ENQ_SEG_MISSING() {
        return localREQUIRED_ENQ_SEG_MISSING;
    }

    /**
     * Auto generated setter method
     * @param param REQUIRED_ENQ_SEG_MISSING
     */
    public void setREQUIRED_ENQ_SEG_MISSING(java.lang.String param) {
        localREQUIRED_ENQ_SEG_MISSINGTracker = param != null;

        this.localREQUIRED_ENQ_SEG_MISSING = param;
    }

    public boolean isINVALID_ENQUIRY_DATASpecified() {
        return localINVALID_ENQUIRY_DATATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_ENQUIRY_DATA() {
        return localINVALID_ENQUIRY_DATA;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_ENQUIRY_DATA
     */
    public void setINVALID_ENQUIRY_DATA(java.lang.String param) {
        localINVALID_ENQUIRY_DATATracker = param != null;

        this.localINVALID_ENQUIRY_DATA = param;
    }

    public boolean isCIBIL_SYSTEM_ERRORSpecified() {
        return localCIBIL_SYSTEM_ERRORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCIBIL_SYSTEM_ERROR() {
        return localCIBIL_SYSTEM_ERROR;
    }

    /**
     * Auto generated setter method
     * @param param CIBIL_SYSTEM_ERROR
     */
    public void setCIBIL_SYSTEM_ERROR(java.lang.String param) {
        localCIBIL_SYSTEM_ERRORTracker = param != null;

        this.localCIBIL_SYSTEM_ERROR = param;
    }

    public boolean isINVALID_SEGMENT_TAGSpecified() {
        return localINVALID_SEGMENT_TAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_SEGMENT_TAG() {
        return localINVALID_SEGMENT_TAG;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_SEGMENT_TAG
     */
    public void setINVALID_SEGMENT_TAG(java.lang.String param) {
        localINVALID_SEGMENT_TAGTracker = param != null;

        this.localINVALID_SEGMENT_TAG = param;
    }

    public boolean isINVALID_SEGMENT_ORDERSpecified() {
        return localINVALID_SEGMENT_ORDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_SEGMENT_ORDER() {
        return localINVALID_SEGMENT_ORDER;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_SEGMENT_ORDER
     */
    public void setINVALID_SEGMENT_ORDER(java.lang.String param) {
        localINVALID_SEGMENT_ORDERTracker = param != null;

        this.localINVALID_SEGMENT_ORDER = param;
    }

    public boolean isINVALID_FIELD_TAG_ORDERSpecified() {
        return localINVALID_FIELD_TAG_ORDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_FIELD_TAG_ORDER() {
        return localINVALID_FIELD_TAG_ORDER;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_FIELD_TAG_ORDER
     */
    public void setINVALID_FIELD_TAG_ORDER(java.lang.String param) {
        localINVALID_FIELD_TAG_ORDERTracker = param != null;

        this.localINVALID_FIELD_TAG_ORDER = param;
    }

    public boolean isMISSING_REQUIRED_FIELDSpecified() {
        return localMISSING_REQUIRED_FIELDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMISSING_REQUIRED_FIELD() {
        return localMISSING_REQUIRED_FIELD;
    }

    /**
     * Auto generated setter method
     * @param param MISSING_REQUIRED_FIELD
     */
    public void setMISSING_REQUIRED_FIELD(java.lang.String param) {
        localMISSING_REQUIRED_FIELDTracker = param != null;

        this.localMISSING_REQUIRED_FIELD = param;
    }

    public boolean isREQUESTED_RESP_SIZE_EXCEEDEDSpecified() {
        return localREQUESTED_RESP_SIZE_EXCEEDEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREQUESTED_RESP_SIZE_EXCEEDED() {
        return localREQUESTED_RESP_SIZE_EXCEEDED;
    }

    /**
     * Auto generated setter method
     * @param param REQUESTED_RESP_SIZE_EXCEEDED
     */
    public void setREQUESTED_RESP_SIZE_EXCEEDED(java.lang.String param) {
        localREQUESTED_RESP_SIZE_EXCEEDEDTracker = param != null;

        this.localREQUESTED_RESP_SIZE_EXCEEDED = param;
    }

    public boolean isINVALID_INPUT_OUTPUT_MEDIASpecified() {
        return localINVALID_INPUT_OUTPUT_MEDIATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINVALID_INPUT_OUTPUT_MEDIA() {
        return localINVALID_INPUT_OUTPUT_MEDIA;
    }

    /**
     * Auto generated setter method
     * @param param INVALID_INPUT_OUTPUT_MEDIA
     */
    public void setINVALID_INPUT_OUTPUT_MEDIA(java.lang.String param) {
        localINVALID_INPUT_OUTPUT_MEDIATracker = param != null;

        this.localINVALID_INPUT_OUTPUT_MEDIA = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_TIMESpecified() {
        return localOUTPUT_WRITE_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_TIME() {
        return localOUTPUT_WRITE_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_TIME
     */
    public void setOUTPUT_WRITE_TIME(java.lang.String param) {
        localOUTPUT_WRITE_TIMETracker = param != null;

        this.localOUTPUT_WRITE_TIME = param;
    }

    public boolean isOUTPUT_READ_TIMESpecified() {
        return localOUTPUT_READ_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_TIME() {
        return localOUTPUT_READ_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_TIME
     */
    public void setOUTPUT_READ_TIME(java.lang.String param) {
        localOUTPUT_READ_TIMETracker = param != null;

        this.localOUTPUT_READ_TIME = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":CibilERespType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "CibilERespType", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_DATE", xmlWriter);

            if (localENQUIRY_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSEGMENT_TAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SEGMENT_TAG", xmlWriter);

            if (localSEGMENT_TAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SEGMENT_TAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSEGMENT_TAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localTYPE_OF_ERRORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TYPE_OF_ERROR", xmlWriter);

            if (localTYPE_OF_ERROR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TYPE_OF_ERROR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTYPE_OF_ERROR);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_PROCESSEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_PROCESSED", xmlWriter);

            if (localDATE_PROCESSED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_PROCESSED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_PROCESSED);
            }

            xmlWriter.writeEndElement();
        }

        if (localTIME_PROCESSEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TIME_PROCESSED", xmlWriter);

            if (localTIME_PROCESSED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TIME_PROCESSED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTIME_PROCESSED);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_MEMBER_CODE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_MEMBER_CODE_ID",
                xmlWriter);

            if (localENQUIRY_MEMBER_CODE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_MEMBER_CODE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_MEMBER_CODE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localERROR_TYPE_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERROR_TYPE_CODE", xmlWriter);

            if (localERROR_TYPE_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERROR_TYPE_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERROR_TYPE_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localERROR_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERROR_TYPE", xmlWriter);

            if (localERROR_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERROR_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERROR_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATATracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATA", xmlWriter);

            if (localDATA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSEGMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SEGMENT", xmlWriter);

            if (localSEGMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SEGMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSEGMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSPECIFIEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SPECIFIED", xmlWriter);

            if (localSPECIFIED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SPECIFIED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSPECIFIED);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXPECTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXPECTED", xmlWriter);

            if (localEXPECTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPECTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXPECTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_VERSIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_VERSION", xmlWriter);

            if (localINVALID_VERSION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_VERSION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_VERSION);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_FIELD_LENGTHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_FIELD_LENGTH", xmlWriter);

            if (localINVALID_FIELD_LENGTH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_FIELD_LENGTH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_FIELD_LENGTH);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_TOTAL_LENGTHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_TOTAL_LENGTH", xmlWriter);

            if (localINVALID_TOTAL_LENGTH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_TOTAL_LENGTH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_TOTAL_LENGTH);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_ENQUIRY_PURPOSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_ENQUIRY_PURPOSE",
                xmlWriter);

            if (localINVALID_ENQUIRY_PURPOSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_ENQUIRY_PURPOSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_ENQUIRY_PURPOSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_ENQUIRY_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_ENQUIRY_AMOUNT",
                xmlWriter);

            if (localINVALID_ENQUIRY_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_ENQUIRY_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_ENQUIRY_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_ENQUIRY_MEMBER_USER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "INVALID_ENQUIRY_MEMBER_USER_ID", xmlWriter);

            if (localINVALID_ENQUIRY_MEMBER_USER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_ENQUIRY_MEMBER_USER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_ENQUIRY_MEMBER_USER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localREQUIRED_ENQ_SEG_MISSINGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REQUIRED_ENQ_SEG_MISSING",
                xmlWriter);

            if (localREQUIRED_ENQ_SEG_MISSING == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REQUIRED_ENQ_SEG_MISSING cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREQUIRED_ENQ_SEG_MISSING);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_ENQUIRY_DATATracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_ENQUIRY_DATA", xmlWriter);

            if (localINVALID_ENQUIRY_DATA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_ENQUIRY_DATA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_ENQUIRY_DATA);
            }

            xmlWriter.writeEndElement();
        }

        if (localCIBIL_SYSTEM_ERRORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CIBIL_SYSTEM_ERROR", xmlWriter);

            if (localCIBIL_SYSTEM_ERROR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CIBIL_SYSTEM_ERROR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCIBIL_SYSTEM_ERROR);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_SEGMENT_TAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_SEGMENT_TAG", xmlWriter);

            if (localINVALID_SEGMENT_TAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_SEGMENT_TAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_SEGMENT_TAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_SEGMENT_ORDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_SEGMENT_ORDER",
                xmlWriter);

            if (localINVALID_SEGMENT_ORDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_SEGMENT_ORDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_SEGMENT_ORDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_FIELD_TAG_ORDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_FIELD_TAG_ORDER",
                xmlWriter);

            if (localINVALID_FIELD_TAG_ORDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_FIELD_TAG_ORDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_FIELD_TAG_ORDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localMISSING_REQUIRED_FIELDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MISSING_REQUIRED_FIELD",
                xmlWriter);

            if (localMISSING_REQUIRED_FIELD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MISSING_REQUIRED_FIELD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMISSING_REQUIRED_FIELD);
            }

            xmlWriter.writeEndElement();
        }

        if (localREQUESTED_RESP_SIZE_EXCEEDEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REQUESTED_RESP_SIZE_EXCEEDED",
                xmlWriter);

            if (localREQUESTED_RESP_SIZE_EXCEEDED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REQUESTED_RESP_SIZE_EXCEEDED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREQUESTED_RESP_SIZE_EXCEEDED);
            }

            xmlWriter.writeEndElement();
        }

        if (localINVALID_INPUT_OUTPUT_MEDIATracker) {
            namespace = "";
            writeStartElement(null, namespace, "INVALID_INPUT_OUTPUT_MEDIA",
                xmlWriter);

            if (localINVALID_INPUT_OUTPUT_MEDIA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INVALID_INPUT_OUTPUT_MEDIA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINVALID_INPUT_OUTPUT_MEDIA);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_TIME", xmlWriter);

            if (localOUTPUT_WRITE_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_TIME", xmlWriter);

            if (localOUTPUT_READ_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static CibilERespType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            CibilERespType object = new CibilERespType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"CibilERespType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (CibilERespType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRY_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRY_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SEGMENT_TAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEGMENT_TAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SEGMENT_TAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSEGMENT_TAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TYPE_OF_ERROR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPE_OF_ERROR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TYPE_OF_ERROR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTYPE_OF_ERROR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_PROCESSED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_PROCESSED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_PROCESSED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_PROCESSED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TIME_PROCESSED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TIME_PROCESSED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TIME_PROCESSED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTIME_PROCESSED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ENQUIRY_MEMBER_CODE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ENQUIRY_MEMBER_CODE_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_MEMBER_CODE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_MEMBER_CODE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERROR_TYPE_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERROR_TYPE_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERROR_TYPE_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERROR_TYPE_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERROR_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERROR_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERROR_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERROR_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SEGMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEGMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SEGMENT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSEGMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SPECIFIED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SPECIFIED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SPECIFIED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSPECIFIED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EXPECTED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPECTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXPECTED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXPECTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INVALID_VERSION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INVALID_VERSION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_VERSION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_VERSION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INVALID_FIELD_LENGTH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INVALID_FIELD_LENGTH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_FIELD_LENGTH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_FIELD_LENGTH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INVALID_TOTAL_LENGTH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INVALID_TOTAL_LENGTH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_TOTAL_LENGTH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_TOTAL_LENGTH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INVALID_ENQUIRY_PURPOSE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INVALID_ENQUIRY_PURPOSE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_ENQUIRY_PURPOSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_ENQUIRY_PURPOSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INVALID_ENQUIRY_AMOUNT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INVALID_ENQUIRY_AMOUNT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_ENQUIRY_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_ENQUIRY_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INVALID_ENQUIRY_MEMBER_USER_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INVALID_ENQUIRY_MEMBER_USER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_ENQUIRY_MEMBER_USER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_ENQUIRY_MEMBER_USER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REQUIRED_ENQ_SEG_MISSING").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REQUIRED_ENQ_SEG_MISSING").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REQUIRED_ENQ_SEG_MISSING" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREQUIRED_ENQ_SEG_MISSING(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INVALID_ENQUIRY_DATA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INVALID_ENQUIRY_DATA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_ENQUIRY_DATA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_ENQUIRY_DATA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CIBIL_SYSTEM_ERROR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CIBIL_SYSTEM_ERROR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CIBIL_SYSTEM_ERROR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCIBIL_SYSTEM_ERROR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INVALID_SEGMENT_TAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INVALID_SEGMENT_TAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_SEGMENT_TAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_SEGMENT_TAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INVALID_SEGMENT_ORDER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INVALID_SEGMENT_ORDER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_SEGMENT_ORDER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_SEGMENT_ORDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INVALID_FIELD_TAG_ORDER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INVALID_FIELD_TAG_ORDER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_FIELD_TAG_ORDER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_FIELD_TAG_ORDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MISSING_REQUIRED_FIELD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MISSING_REQUIRED_FIELD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MISSING_REQUIRED_FIELD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMISSING_REQUIRED_FIELD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REQUESTED_RESP_SIZE_EXCEEDED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REQUESTED_RESP_SIZE_EXCEEDED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REQUESTED_RESP_SIZE_EXCEEDED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREQUESTED_RESP_SIZE_EXCEEDED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INVALID_INPUT_OUTPUT_MEDIA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INVALID_INPUT_OUTPUT_MEDIA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INVALID_INPUT_OUTPUT_MEDIA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINVALID_INPUT_OUTPUT_MEDIA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
