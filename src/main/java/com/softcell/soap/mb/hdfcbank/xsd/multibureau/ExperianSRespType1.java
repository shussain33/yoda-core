/**
 * ExperianSRespType1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ExperianSRespType1 bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExperianSRespType1 implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ExperianSRespType1
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for ENQUIRY_DATE
     */
    protected java.lang.String localENQUIRY_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_DATETracker = false;

    /**
     * field for SYSTEMCODE_HEADER
     */
    protected java.lang.String localSYSTEMCODE_HEADER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSYSTEMCODE_HEADERTracker = false;

    /**
     * field for MESSAGETEXT_HEADER
     */
    protected java.lang.String localMESSAGETEXT_HEADER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMESSAGETEXT_HEADERTracker = false;

    /**
     * field for REPORTDATE
     */
    protected java.lang.String localREPORTDATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTDATETracker = false;

    /**
     * field for REPORTIME_HEADER
     */
    protected java.lang.String localREPORTIME_HEADER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTIME_HEADERTracker = false;

    /**
     * field for USERMESSAGETEXT
     */
    protected java.lang.String localUSERMESSAGETEXT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUSERMESSAGETEXTTracker = false;

    /**
     * field for ENQUIRYUSERNAME
     */
    protected java.lang.String localENQUIRYUSERNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRYUSERNAMETracker = false;

    /**
     * field for CREDIT_PROFILE_REPORT_DATE
     */
    protected java.lang.String localCREDIT_PROFILE_REPORT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_PROFILE_REPORT_DATETracker = false;

    /**
     * field for CREDIT_PROFILE_REPORT_TIME
     */
    protected java.lang.String localCREDIT_PROFILE_REPORT_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_PROFILE_REPORT_TIMETracker = false;

    /**
     * field for VERSION
     */
    protected java.lang.String localVERSION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVERSIONTracker = false;

    /**
     * field for REPORTNUMBER
     */
    protected java.lang.String localREPORTNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTNUMBERTracker = false;

    /**
     * field for SUBSCRIBER
     */
    protected java.lang.String localSUBSCRIBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBSCRIBERTracker = false;

    /**
     * field for SUBSCRIBERNAME
     */
    protected java.lang.String localSUBSCRIBERNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBSCRIBERNAMETracker = false;

    /**
     * field for ENQUIRYREASON
     */
    protected java.lang.String localENQUIRYREASON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRYREASONTracker = false;

    /**
     * field for FINANCEPURPOSE
     */
    protected java.lang.String localFINANCEPURPOSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFINANCEPURPOSETracker = false;

    /**
     * field for AMOUNTFINANCED
     */
    protected java.lang.String localAMOUNTFINANCED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNTFINANCEDTracker = false;

    /**
     * field for DURATIONOFAGREEMENT
     */
    protected java.lang.String localDURATIONOFAGREEMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDURATIONOFAGREEMENTTracker = false;

    /**
     * field for LASTNAME
     */
    protected java.lang.String localLASTNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLASTNAMETracker = false;

    /**
     * field for FIRSTNAME
     */
    protected java.lang.String localFIRSTNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFIRSTNAMETracker = false;

    /**
     * field for MIDDLENAME1
     */
    protected java.lang.String localMIDDLENAME1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDDLENAME1Tracker = false;

    /**
     * field for MIDDLENAME2
     */
    protected java.lang.String localMIDDLENAME2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDDLENAME2Tracker = false;

    /**
     * field for MIDDLENAME3
     */
    protected java.lang.String localMIDDLENAME3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDDLENAME3Tracker = false;

    /**
     * field for GENDERCODE
     */
    protected java.lang.String localGENDERCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDERCODETracker = false;

    /**
     * field for GENDER
     */
    protected java.lang.String localGENDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDERTracker = false;

    /**
     * field for INCOME_TAX_PAN_APP
     */
    protected java.lang.String localINCOME_TAX_PAN_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCOME_TAX_PAN_APPTracker = false;

    /**
     * field for PAN_ISSUE_DATE_APP
     */
    protected java.lang.String localPAN_ISSUE_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_ISSUE_DATE_APPTracker = false;

    /**
     * field for PAN_EXP_DATE_APP
     */
    protected java.lang.String localPAN_EXP_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_EXP_DATE_APPTracker = false;

    /**
     * field for PASSPORT_NUMBER_APP
     */
    protected java.lang.String localPASSPORT_NUMBER_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_NUMBER_APPTracker = false;

    /**
     * field for PASSPORT_ISSUE_DATE_APP
     */
    protected java.lang.String localPASSPORT_ISSUE_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_ISSUE_DATE_APPTracker = false;

    /**
     * field for PASSPORT_EXP_DATE_APP
     */
    protected java.lang.String localPASSPORT_EXP_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_EXP_DATE_APPTracker = false;

    /**
     * field for VOTER_IDENTITY_CARD_APP
     */
    protected java.lang.String localVOTER_IDENTITY_CARD_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_IDENTITY_CARD_APPTracker = false;

    /**
     * field for VOTER_ID_ISSUE_DATE_APP
     */
    protected java.lang.String localVOTER_ID_ISSUE_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_ID_ISSUE_DATE_APPTracker = false;

    /**
     * field for VOTER_ID_EXP_DATE_APP
     */
    protected java.lang.String localVOTER_ID_EXP_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_ID_EXP_DATE_APPTracker = false;

    /**
     * field for DRIVER_LICENSE_NUMBER_APP
     */
    protected java.lang.String localDRIVER_LICENSE_NUMBER_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVER_LICENSE_NUMBER_APPTracker = false;

    /**
     * field for DRIVER_LICENSE_ISSUE_DATE_APP
     */
    protected java.lang.String localDRIVER_LICENSE_ISSUE_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVER_LICENSE_ISSUE_DATE_APPTracker = false;

    /**
     * field for DRIVER_LICENSE_EXP_DATE_APP
     */
    protected java.lang.String localDRIVER_LICENSE_EXP_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVER_LICENSE_EXP_DATE_APPTracker = false;

    /**
     * field for RATION_CARD_NUMBER_APP
     */
    protected java.lang.String localRATION_CARD_NUMBER_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_NUMBER_APPTracker = false;

    /**
     * field for RATION_CARD_ISSUE_DATE_APP
     */
    protected java.lang.String localRATION_CARD_ISSUE_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_ISSUE_DATE_APPTracker = false;

    /**
     * field for RATION_CARD_EXP_DATE_APP
     */
    protected java.lang.String localRATION_CARD_EXP_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_EXP_DATE_APPTracker = false;

    /**
     * field for UNIVERSAL_ID_NUMBER_APP
     */
    protected java.lang.String localUNIVERSAL_ID_NUMBER_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUNIVERSAL_ID_NUMBER_APPTracker = false;

    /**
     * field for UNIVERSAL_ID_ISSUE_DATE_APP
     */
    protected java.lang.String localUNIVERSAL_ID_ISSUE_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUNIVERSAL_ID_ISSUE_DATE_APPTracker = false;

    /**
     * field for UNIVERSAL_ID_EXP_DATE_APP
     */
    protected java.lang.String localUNIVERSAL_ID_EXP_DATE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUNIVERSAL_ID_EXP_DATE_APPTracker = false;

    /**
     * field for DATEOFBIRTHAPPLICANT
     */
    protected java.lang.String localDATEOFBIRTHAPPLICANT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOFBIRTHAPPLICANTTracker = false;

    /**
     * field for TELEPHONENUMBERAPPLICANT1ST
     */
    protected java.lang.String localTELEPHONENUMBERAPPLICANT1ST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEPHONENUMBERAPPLICANT1STTracker = false;

    /**
     * field for TELEEXTENSION_APP
     */
    protected java.lang.String localTELEEXTENSION_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEEXTENSION_APPTracker = false;

    /**
     * field for TELETYPE_APP
     */
    protected java.lang.String localTELETYPE_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELETYPE_APPTracker = false;

    /**
     * field for MOBILEPHONENUMBER
     */
    protected java.lang.String localMOBILEPHONENUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOBILEPHONENUMBERTracker = false;

    /**
     * field for EMAILID_APP
     */
    protected java.lang.String localEMAILID_APP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILID_APPTracker = false;

    /**
     * field for INCOME
     */
    protected java.lang.String localINCOME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCOMETracker = false;

    /**
     * field for MARITALSTATUSCODE
     */
    protected java.lang.String localMARITALSTATUSCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMARITALSTATUSCODETracker = false;

    /**
     * field for MARITALSTATUS
     */
    protected java.lang.String localMARITALSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMARITALSTATUSTracker = false;

    /**
     * field for EMPLOYMENTSTATUS
     */
    protected java.lang.String localEMPLOYMENTSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMPLOYMENTSTATUSTracker = false;

    /**
     * field for DATEWITHEMPLOYER
     */
    protected java.lang.String localDATEWITHEMPLOYER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEWITHEMPLOYERTracker = false;

    /**
     * field for NUMBEROFMAJORCREDITCARDHELD
     */
    protected java.lang.String localNUMBEROFMAJORCREDITCARDHELD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNUMBEROFMAJORCREDITCARDHELDTracker = false;

    /**
     * field for FLATNOPLOTNOHOUSENO
     */
    protected java.lang.String localFLATNOPLOTNOHOUSENO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFLATNOPLOTNOHOUSENOTracker = false;

    /**
     * field for BLDGNOSOCIETYNAME
     */
    protected java.lang.String localBLDGNOSOCIETYNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBLDGNOSOCIETYNAMETracker = false;

    /**
     * field for ROADNONAMEAREALOCALITY
     */
    protected java.lang.String localROADNONAMEAREALOCALITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localROADNONAMEAREALOCALITYTracker = false;

    /**
     * field for CITY
     */
    protected java.lang.String localCITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCITYTracker = false;

    /**
     * field for LANDMARK
     */
    protected java.lang.String localLANDMARK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLANDMARKTracker = false;

    /**
     * field for STATE
     */
    protected java.lang.String localSTATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATETracker = false;

    /**
     * field for PINCODE
     */
    protected java.lang.String localPINCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPINCODETracker = false;

    /**
     * field for COUNTRY_CODE
     */
    protected java.lang.String localCOUNTRY_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOUNTRY_CODETracker = false;

    /**
     * field for ADD_FLATNOPLOTNOHOUSENO
     */
    protected java.lang.String localADD_FLATNOPLOTNOHOUSENO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_FLATNOPLOTNOHOUSENOTracker = false;

    /**
     * field for ADD_BLDGNOSOCIETYNAME
     */
    protected java.lang.String localADD_BLDGNOSOCIETYNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_BLDGNOSOCIETYNAMETracker = false;

    /**
     * field for ADD_ROADNONAMEAREALOCALITY
     */
    protected java.lang.String localADD_ROADNONAMEAREALOCALITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_ROADNONAMEAREALOCALITYTracker = false;

    /**
     * field for ADD_CITY
     */
    protected java.lang.String localADD_CITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_CITYTracker = false;

    /**
     * field for ADD_LANDMARK
     */
    protected java.lang.String localADD_LANDMARK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_LANDMARKTracker = false;

    /**
     * field for ADD_STATE
     */
    protected java.lang.String localADD_STATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_STATETracker = false;

    /**
     * field for ADD_PINCODE
     */
    protected java.lang.String localADD_PINCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_PINCODETracker = false;

    /**
     * field for ADD_COUNTRYCODE
     */
    protected java.lang.String localADD_COUNTRYCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_COUNTRYCODETracker = false;

    /**
     * field for CREDITACCOUNTTOTAL
     */
    protected java.lang.String localCREDITACCOUNTTOTAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITACCOUNTTOTALTracker = false;

    /**
     * field for CREDITACCOUNTACTIVE
     */
    protected java.lang.String localCREDITACCOUNTACTIVE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITACCOUNTACTIVETracker = false;

    /**
     * field for CREDITACCOUNTDEFAULT
     */
    protected java.lang.String localCREDITACCOUNTDEFAULT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITACCOUNTDEFAULTTracker = false;

    /**
     * field for CREDITACCOUNTCLOSED
     */
    protected java.lang.String localCREDITACCOUNTCLOSED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITACCOUNTCLOSEDTracker = false;

    /**
     * field for CADSUITFILEDCURRENTBALANCE
     */
    protected java.lang.String localCADSUITFILEDCURRENTBALANCE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCADSUITFILEDCURRENTBALANCETracker = false;

    /**
     * field for OUTSTANDINGBALANCESECURED
     */
    protected java.lang.String localOUTSTANDINGBALANCESECURED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTSTANDINGBALANCESECUREDTracker = false;

    /**
     * field for OUTSTANDINGBALSECUREDPER
     */
    protected java.lang.String localOUTSTANDINGBALSECUREDPER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTSTANDINGBALSECUREDPERTracker = false;

    /**
     * field for OUTSTANDINGBALANCEUNSECURED
     */
    protected java.lang.String localOUTSTANDINGBALANCEUNSECURED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTSTANDINGBALANCEUNSECUREDTracker = false;

    /**
     * field for OUTSTANDINGBALUNSECPER
     */
    protected java.lang.String localOUTSTANDINGBALUNSECPER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTSTANDINGBALUNSECPERTracker = false;

    /**
     * field for OUTSTANDINGBALANCEALL
     */
    protected java.lang.String localOUTSTANDINGBALANCEALL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTSTANDINGBALANCEALLTracker = false;

    /**
     * field for IDENTIFICATIONNUMBER
     */
    protected java.lang.String localIDENTIFICATIONNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIDENTIFICATIONNUMBERTracker = false;

    /**
     * field for CAISSUBSCRIBERNAME
     */
    protected java.lang.String localCAISSUBSCRIBERNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAISSUBSCRIBERNAMETracker = false;

    /**
     * field for ACCOUNTNUMBER
     */
    protected java.lang.String localACCOUNTNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTNUMBERTracker = false;

    /**
     * field for PORTFOLIOTYPE
     */
    protected java.lang.String localPORTFOLIOTYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPORTFOLIOTYPETracker = false;

    /**
     * field for ACCOUNTTYPE_CODE
     */
    protected java.lang.String localACCOUNTTYPE_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTTYPE_CODETracker = false;

    /**
     * field for ACCOUNTTYPE
     */
    protected java.lang.String localACCOUNTTYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTTYPETracker = false;

    /**
     * field for OPENDATE
     */
    protected java.lang.String localOPENDATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPENDATETracker = false;

    /**
     * field for HIGHESTCREDITORORGNLOANAMT
     */
    protected java.lang.String localHIGHESTCREDITORORGNLOANAMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHIGHESTCREDITORORGNLOANAMTTracker = false;

    /**
     * field for TERMSDURATION
     */
    protected java.lang.String localTERMSDURATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTERMSDURATIONTracker = false;

    /**
     * field for TERMSFREQUENCY
     */
    protected java.lang.String localTERMSFREQUENCY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTERMSFREQUENCYTracker = false;

    /**
     * field for SCHEDULEDMONTHLYPAYAMT
     */
    protected java.lang.String localSCHEDULEDMONTHLYPAYAMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCHEDULEDMONTHLYPAYAMTTracker = false;

    /**
     * field for ACCOUNTSTATUS_CODE
     */
    protected java.lang.String localACCOUNTSTATUS_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTSTATUS_CODETracker = false;

    /**
     * field for ACCOUNTSTATUS
     */
    protected java.lang.String localACCOUNTSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTSTATUSTracker = false;

    /**
     * field for PAYMENTRATING
     */
    protected java.lang.String localPAYMENTRATING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENTRATINGTracker = false;

    /**
     * field for PAYMENTHISTORYPROFILE
     */
    protected java.lang.String localPAYMENTHISTORYPROFILE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENTHISTORYPROFILETracker = false;

    /**
     * field for SPECIALCOMMENT
     */
    protected java.lang.String localSPECIALCOMMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSPECIALCOMMENTTracker = false;

    /**
     * field for CURRENTBALANCE_TL
     */
    protected java.lang.String localCURRENTBALANCE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENTBALANCE_TLTracker = false;

    /**
     * field for AMOUNTPASTDUE_TL
     */
    protected java.lang.String localAMOUNTPASTDUE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNTPASTDUE_TLTracker = false;

    /**
     * field for ORIGINALCHARGEOFFAMOUNT
     */
    protected java.lang.String localORIGINALCHARGEOFFAMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localORIGINALCHARGEOFFAMOUNTTracker = false;

    /**
     * field for DATEREPORTED
     */
    protected java.lang.String localDATEREPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEREPORTEDTracker = false;

    /**
     * field for DATEOFFIRSTDELINQUENCY
     */
    protected java.lang.String localDATEOFFIRSTDELINQUENCY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOFFIRSTDELINQUENCYTracker = false;

    /**
     * field for DATECLOSED
     */
    protected java.lang.String localDATECLOSED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATECLOSEDTracker = false;

    /**
     * field for DATEOFLASTPAYMENT
     */
    protected java.lang.String localDATEOFLASTPAYMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOFLASTPAYMENTTracker = false;

    /**
     * field for SUITFWILFULDFTWRITTENOFFSTS
     */
    protected java.lang.String localSUITFWILFULDFTWRITTENOFFSTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUITFWILFULDFTWRITTENOFFSTSTracker = false;

    /**
     * field for SUITFILEDWILFULDEF
     */
    protected java.lang.String localSUITFILEDWILFULDEF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUITFILEDWILFULDEFTracker = false;

    /**
     * field for WRITTENOFFSETTLEDSTATUS
     */
    protected java.lang.String localWRITTENOFFSETTLEDSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITTENOFFSETTLEDSTATUSTracker = false;

    /**
     * field for VALUEOFCREDITSLASTMONTH
     */
    protected java.lang.String localVALUEOFCREDITSLASTMONTH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVALUEOFCREDITSLASTMONTHTracker = false;

    /**
     * field for OCCUPATIONCODE
     */
    protected java.lang.String localOCCUPATIONCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOCCUPATIONCODETracker = false;

    /**
     * field for SATTLEMENTAMOUNT
     */
    protected java.lang.String localSATTLEMENTAMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSATTLEMENTAMOUNTTracker = false;

    /**
     * field for VALUEOFCOLATERAL
     */
    protected java.lang.String localVALUEOFCOLATERAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVALUEOFCOLATERALTracker = false;

    /**
     * field for TYPEOFCOLATERAL
     */
    protected java.lang.String localTYPEOFCOLATERAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTYPEOFCOLATERALTracker = false;

    /**
     * field for WRITTENOFFAMTTOTAL
     */
    protected java.lang.String localWRITTENOFFAMTTOTAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITTENOFFAMTTOTALTracker = false;

    /**
     * field for WRITTENOFFAMTPRINCIPAL
     */
    protected java.lang.String localWRITTENOFFAMTPRINCIPAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITTENOFFAMTPRINCIPALTracker = false;

    /**
     * field for RATEOFINTEREST
     */
    protected java.lang.String localRATEOFINTEREST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATEOFINTERESTTracker = false;

    /**
     * field for REPAYMENTTENURE
     */
    protected java.lang.String localREPAYMENTTENURE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPAYMENTTENURETracker = false;

    /**
     * field for PROMOTIONALRATEFLAG
     */
    protected java.lang.String localPROMOTIONALRATEFLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROMOTIONALRATEFLAGTracker = false;

    /**
     * field for CAISINCOME
     */
    protected java.lang.String localCAISINCOME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAISINCOMETracker = false;

    /**
     * field for INCOMEINDICATOR
     */
    protected java.lang.String localINCOMEINDICATOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCOMEINDICATORTracker = false;

    /**
     * field for INCOMEFREQUENCYINDICATOR
     */
    protected java.lang.String localINCOMEFREQUENCYINDICATOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCOMEFREQUENCYINDICATORTracker = false;

    /**
     * field for DEFAULTSTATUSDATE
     */
    protected java.lang.String localDEFAULTSTATUSDATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDEFAULTSTATUSDATETracker = false;

    /**
     * field for LITIGATIONSTATUSDATE
     */
    protected java.lang.String localLITIGATIONSTATUSDATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLITIGATIONSTATUSDATETracker = false;

    /**
     * field for WRITEOFFSTATUSDATE
     */
    protected java.lang.String localWRITEOFFSTATUSDATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITEOFFSTATUSDATETracker = false;

    /**
     * field for CURRENCYCODE
     */
    protected java.lang.String localCURRENCYCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENCYCODETracker = false;

    /**
     * field for SUBSCRIBERCOMMENTS
     */
    protected java.lang.String localSUBSCRIBERCOMMENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBSCRIBERCOMMENTSTracker = false;

    /**
     * field for CONSUMERCOMMENTS
     */
    protected java.lang.String localCONSUMERCOMMENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONSUMERCOMMENTSTracker = false;

    /**
     * field for ACCOUNTHOLDERTYPECODE
     */
    protected java.lang.String localACCOUNTHOLDERTYPECODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTHOLDERTYPECODETracker = false;

    /**
     * field for ACCOUNTHOLDER_TYPENAME
     */
    protected java.lang.String localACCOUNTHOLDER_TYPENAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTHOLDER_TYPENAMETracker = false;

    /**
     * field for YEAR_HIST
     */
    protected java.lang.String localYEAR_HIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localYEAR_HISTTracker = false;

    /**
     * field for MONTH_HIST
     */
    protected java.lang.String localMONTH_HIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMONTH_HISTTracker = false;

    /**
     * field for DAYSPASTDUE
     */
    protected java.lang.String localDAYSPASTDUE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDAYSPASTDUETracker = false;

    /**
     * field for ASSETCLASSIFICATION
     */
    protected java.lang.String localASSETCLASSIFICATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localASSETCLASSIFICATIONTracker = false;

    /**
     * field for YEAR_ADVHIST
     */
    protected java.lang.String localYEAR_ADVHIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localYEAR_ADVHISTTracker = false;

    /**
     * field for MONTH_ADVHIST
     */
    protected java.lang.String localMONTH_ADVHIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMONTH_ADVHISTTracker = false;

    /**
     * field for CASHLIMIT
     */
    protected java.lang.String localCASHLIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCASHLIMITTracker = false;

    /**
     * field for CREDITLIMITAMT
     */
    protected java.lang.String localCREDITLIMITAMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITLIMITAMTTracker = false;

    /**
     * field for ACTUALPAYMENTAMT
     */
    protected java.lang.String localACTUALPAYMENTAMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACTUALPAYMENTAMTTracker = false;

    /**
     * field for EMIAMT
     */
    protected java.lang.String localEMIAMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMIAMTTracker = false;

    /**
     * field for CURRENTBALANCE_ADVHIST
     */
    protected java.lang.String localCURRENTBALANCE_ADVHIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENTBALANCE_ADVHISTTracker = false;

    /**
     * field for AMOUNTPASTDUE_ADVHIST
     */
    protected java.lang.String localAMOUNTPASTDUE_ADVHIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNTPASTDUE_ADVHISTTracker = false;

    /**
     * field for SURNAMENONNORMALIZED
     */
    protected java.lang.String localSURNAMENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSURNAMENONNORMALIZEDTracker = false;

    /**
     * field for FIRSTNAMENONNORMALIZED
     */
    protected java.lang.String localFIRSTNAMENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFIRSTNAMENONNORMALIZEDTracker = false;

    /**
     * field for MIDDLENAME1NONNORMALIZED
     */
    protected java.lang.String localMIDDLENAME1NONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDDLENAME1NONNORMALIZEDTracker = false;

    /**
     * field for MIDDLENAME2NONNORMALIZED
     */
    protected java.lang.String localMIDDLENAME2NONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDDLENAME2NONNORMALIZEDTracker = false;

    /**
     * field for MIDDLENAME3NONNORMALIZED
     */
    protected java.lang.String localMIDDLENAME3NONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDDLENAME3NONNORMALIZEDTracker = false;

    /**
     * field for ALIAS
     */
    protected java.lang.String localALIAS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALIASTracker = false;

    /**
     * field for CAISGENDERCODE
     */
    protected java.lang.String localCAISGENDERCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAISGENDERCODETracker = false;

    /**
     * field for CAISGENDER
     */
    protected java.lang.String localCAISGENDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAISGENDERTracker = false;

    /**
     * field for CAISINCOMETAXPAN
     */
    protected java.lang.String localCAISINCOMETAXPAN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAISINCOMETAXPANTracker = false;

    /**
     * field for CAISPASSPORTNUMBER
     */
    protected java.lang.String localCAISPASSPORTNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAISPASSPORTNUMBERTracker = false;

    /**
     * field for VOTERIDNUMBER
     */
    protected java.lang.String localVOTERIDNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTERIDNUMBERTracker = false;

    /**
     * field for DATEOFBIRTH
     */
    protected java.lang.String localDATEOFBIRTH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOFBIRTHTracker = false;

    /**
     * field for FIRSTLINEOFADDNONNORMALIZED
     */
    protected java.lang.String localFIRSTLINEOFADDNONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFIRSTLINEOFADDNONNORMALIZEDTracker = false;

    /**
     * field for SECONDLINEOFADDNONNORMALIZED
     */
    protected java.lang.String localSECONDLINEOFADDNONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECONDLINEOFADDNONNORMALIZEDTracker = false;

    /**
     * field for THIRDLINEOFADDNONNORMALIZED
     */
    protected java.lang.String localTHIRDLINEOFADDNONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTHIRDLINEOFADDNONNORMALIZEDTracker = false;

    /**
     * field for CITYNONNORMALIZED
     */
    protected java.lang.String localCITYNONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCITYNONNORMALIZEDTracker = false;

    /**
     * field for FIFTHLINEOFADDNONNORMALIZED
     */
    protected java.lang.String localFIFTHLINEOFADDNONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFIFTHLINEOFADDNONNORMALIZEDTracker = false;

    /**
     * field for STATECODENONNORMALIZED
     */
    protected java.lang.String localSTATECODENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATECODENONNORMALIZEDTracker = false;

    /**
     * field for STATENONNORMALIZED
     */
    protected java.lang.String localSTATENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATENONNORMALIZEDTracker = false;

    /**
     * field for ZIPPOSTALCODENONNORMALIZED
     */
    protected java.lang.String localZIPPOSTALCODENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localZIPPOSTALCODENONNORMALIZEDTracker = false;

    /**
     * field for COUNTRYCODENONNORMALIZED
     */
    protected java.lang.String localCOUNTRYCODENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOUNTRYCODENONNORMALIZEDTracker = false;

    /**
     * field for ADDINDICATORNONNORMALIZED
     */
    protected java.lang.String localADDINDICATORNONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDINDICATORNONNORMALIZEDTracker = false;

    /**
     * field for RESIDENCECODENONNORMALIZED
     */
    protected java.lang.String localRESIDENCECODENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESIDENCECODENONNORMALIZEDTracker = false;

    /**
     * field for RESIDENCENONNORMALIZED
     */
    protected java.lang.String localRESIDENCENONNORMALIZED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESIDENCENONNORMALIZEDTracker = false;

    /**
     * field for TELEPHONENUMBER
     */
    protected java.lang.String localTELEPHONENUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEPHONENUMBERTracker = false;

    /**
     * field for TELETYPE_PHONE
     */
    protected java.lang.String localTELETYPE_PHONE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELETYPE_PHONETracker = false;

    /**
     * field for TELEEXTENSION_PHONE
     */
    protected java.lang.String localTELEEXTENSION_PHONE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEEXTENSION_PHONETracker = false;

    /**
     * field for MOBILETELEPHONENUMBER
     */
    protected java.lang.String localMOBILETELEPHONENUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOBILETELEPHONENUMBERTracker = false;

    /**
     * field for FAXNUMBER
     */
    protected java.lang.String localFAXNUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFAXNUMBERTracker = false;

    /**
     * field for EMAILID_PHONE
     */
    protected java.lang.String localEMAILID_PHONE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILID_PHONETracker = false;

    /**
     * field for INCOME_TAX_PAN_ID
     */
    protected java.lang.String localINCOME_TAX_PAN_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCOME_TAX_PAN_IDTracker = false;

    /**
     * field for PAN_ISSUE_DATE_ID
     */
    protected java.lang.String localPAN_ISSUE_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_ISSUE_DATE_IDTracker = false;

    /**
     * field for PAN_EXP_DATE_ID
     */
    protected java.lang.String localPAN_EXP_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_EXP_DATE_IDTracker = false;

    /**
     * field for PASSPORT_NUMBER_ID
     */
    protected java.lang.String localPASSPORT_NUMBER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_NUMBER_IDTracker = false;

    /**
     * field for PASSPORT_ISSUE_DATE_ID
     */
    protected java.lang.String localPASSPORT_ISSUE_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_ISSUE_DATE_IDTracker = false;

    /**
     * field for PASSPORT_EXP_DATE_ID
     */
    protected java.lang.String localPASSPORT_EXP_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_EXP_DATE_IDTracker = false;

    /**
     * field for VOTER_IDENTITY_CARD_ID
     */
    protected java.lang.String localVOTER_IDENTITY_CARD_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_IDENTITY_CARD_IDTracker = false;

    /**
     * field for VOTER_ID_ISSUE_DATE_ID
     */
    protected java.lang.String localVOTER_ID_ISSUE_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_ID_ISSUE_DATE_IDTracker = false;

    /**
     * field for VOTER_ID_EXP_DATE_ID
     */
    protected java.lang.String localVOTER_ID_EXP_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_ID_EXP_DATE_IDTracker = false;

    /**
     * field for DRIVER_LICENSE_NUMBER_ID
     */
    protected java.lang.String localDRIVER_LICENSE_NUMBER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVER_LICENSE_NUMBER_IDTracker = false;

    /**
     * field for DRIVER_LICENSE_ISSUE_DATE_ID
     */
    protected java.lang.String localDRIVER_LICENSE_ISSUE_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVER_LICENSE_ISSUE_DATE_IDTracker = false;

    /**
     * field for DRIVER_LICENSE_EXP_DATE_ID
     */
    protected java.lang.String localDRIVER_LICENSE_EXP_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVER_LICENSE_EXP_DATE_IDTracker = false;

    /**
     * field for RATION_CARD_NUMBER_ID
     */
    protected java.lang.String localRATION_CARD_NUMBER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_NUMBER_IDTracker = false;

    /**
     * field for RATION_CARD_ISSUE_DATE_ID
     */
    protected java.lang.String localRATION_CARD_ISSUE_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_ISSUE_DATE_IDTracker = false;

    /**
     * field for RATION_CARD_EXP_DATE_ID
     */
    protected java.lang.String localRATION_CARD_EXP_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_EXP_DATE_IDTracker = false;

    /**
     * field for UNIVERSAL_ID_NUMBER_ID
     */
    protected java.lang.String localUNIVERSAL_ID_NUMBER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUNIVERSAL_ID_NUMBER_IDTracker = false;

    /**
     * field for UNIVERSAL_ID_ISSUE_DATE_ID
     */
    protected java.lang.String localUNIVERSAL_ID_ISSUE_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUNIVERSAL_ID_ISSUE_DATE_IDTracker = false;

    /**
     * field for UNIVERSAL_ID_EXP_DATE_ID
     */
    protected java.lang.String localUNIVERSAL_ID_EXP_DATE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUNIVERSAL_ID_EXP_DATE_IDTracker = false;

    /**
     * field for EMAILID_ID
     */
    protected java.lang.String localEMAILID_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILID_IDTracker = false;

    /**
     * field for EXACTMATCH
     */
    protected java.lang.String localEXACTMATCH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXACTMATCHTracker = false;

    /**
     * field for CAPSLAST7DAYS
     */
    protected java.lang.String localCAPSLAST7DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSLAST7DAYSTracker = false;

    /**
     * field for CAPSLAST30DAYS
     */
    protected java.lang.String localCAPSLAST30DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSLAST30DAYSTracker = false;

    /**
     * field for CAPSLAST90DAYS
     */
    protected java.lang.String localCAPSLAST90DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSLAST90DAYSTracker = false;

    /**
     * field for CAPSLAST180DAYS
     */
    protected java.lang.String localCAPSLAST180DAYS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSLAST180DAYSTracker = false;

    /**
     * field for CAPSSUBSCRIBERCODE
     */
    protected java.lang.String localCAPSSUBSCRIBERCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSSUBSCRIBERCODETracker = false;

    /**
     * field for CAPSSUBSCRIBERNAME
     */
    protected java.lang.String localCAPSSUBSCRIBERNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSSUBSCRIBERNAMETracker = false;

    /**
     * field for CAPSDATEOFREQUEST
     */
    protected java.lang.String localCAPSDATEOFREQUEST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSDATEOFREQUESTTracker = false;

    /**
     * field for CAPSREPORTDATE
     */
    protected java.lang.String localCAPSREPORTDATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAPSREPORTDATETracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isENQUIRY_DATESpecified() {
        return localENQUIRY_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_DATE() {
        return localENQUIRY_DATE;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_DATE
     */
    public void setENQUIRY_DATE(java.lang.String param) {
        localENQUIRY_DATETracker = param != null;

        this.localENQUIRY_DATE = param;
    }

    public boolean isSYSTEMCODE_HEADERSpecified() {
        return localSYSTEMCODE_HEADERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSYSTEMCODE_HEADER() {
        return localSYSTEMCODE_HEADER;
    }

    /**
     * Auto generated setter method
     * @param param SYSTEMCODE_HEADER
     */
    public void setSYSTEMCODE_HEADER(java.lang.String param) {
        localSYSTEMCODE_HEADERTracker = param != null;

        this.localSYSTEMCODE_HEADER = param;
    }

    public boolean isMESSAGETEXT_HEADERSpecified() {
        return localMESSAGETEXT_HEADERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMESSAGETEXT_HEADER() {
        return localMESSAGETEXT_HEADER;
    }

    /**
     * Auto generated setter method
     * @param param MESSAGETEXT_HEADER
     */
    public void setMESSAGETEXT_HEADER(java.lang.String param) {
        localMESSAGETEXT_HEADERTracker = param != null;

        this.localMESSAGETEXT_HEADER = param;
    }

    public boolean isREPORTDATESpecified() {
        return localREPORTDATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTDATE() {
        return localREPORTDATE;
    }

    /**
     * Auto generated setter method
     * @param param REPORTDATE
     */
    public void setREPORTDATE(java.lang.String param) {
        localREPORTDATETracker = param != null;

        this.localREPORTDATE = param;
    }

    public boolean isREPORTIME_HEADERSpecified() {
        return localREPORTIME_HEADERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTIME_HEADER() {
        return localREPORTIME_HEADER;
    }

    /**
     * Auto generated setter method
     * @param param REPORTIME_HEADER
     */
    public void setREPORTIME_HEADER(java.lang.String param) {
        localREPORTIME_HEADERTracker = param != null;

        this.localREPORTIME_HEADER = param;
    }

    public boolean isUSERMESSAGETEXTSpecified() {
        return localUSERMESSAGETEXTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUSERMESSAGETEXT() {
        return localUSERMESSAGETEXT;
    }

    /**
     * Auto generated setter method
     * @param param USERMESSAGETEXT
     */
    public void setUSERMESSAGETEXT(java.lang.String param) {
        localUSERMESSAGETEXTTracker = param != null;

        this.localUSERMESSAGETEXT = param;
    }

    public boolean isENQUIRYUSERNAMESpecified() {
        return localENQUIRYUSERNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRYUSERNAME() {
        return localENQUIRYUSERNAME;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRYUSERNAME
     */
    public void setENQUIRYUSERNAME(java.lang.String param) {
        localENQUIRYUSERNAMETracker = param != null;

        this.localENQUIRYUSERNAME = param;
    }

    public boolean isCREDIT_PROFILE_REPORT_DATESpecified() {
        return localCREDIT_PROFILE_REPORT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_PROFILE_REPORT_DATE() {
        return localCREDIT_PROFILE_REPORT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_PROFILE_REPORT_DATE
     */
    public void setCREDIT_PROFILE_REPORT_DATE(java.lang.String param) {
        localCREDIT_PROFILE_REPORT_DATETracker = param != null;

        this.localCREDIT_PROFILE_REPORT_DATE = param;
    }

    public boolean isCREDIT_PROFILE_REPORT_TIMESpecified() {
        return localCREDIT_PROFILE_REPORT_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_PROFILE_REPORT_TIME() {
        return localCREDIT_PROFILE_REPORT_TIME;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_PROFILE_REPORT_TIME
     */
    public void setCREDIT_PROFILE_REPORT_TIME(java.lang.String param) {
        localCREDIT_PROFILE_REPORT_TIMETracker = param != null;

        this.localCREDIT_PROFILE_REPORT_TIME = param;
    }

    public boolean isVERSIONSpecified() {
        return localVERSIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVERSION() {
        return localVERSION;
    }

    /**
     * Auto generated setter method
     * @param param VERSION
     */
    public void setVERSION(java.lang.String param) {
        localVERSIONTracker = param != null;

        this.localVERSION = param;
    }

    public boolean isREPORTNUMBERSpecified() {
        return localREPORTNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTNUMBER() {
        return localREPORTNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param REPORTNUMBER
     */
    public void setREPORTNUMBER(java.lang.String param) {
        localREPORTNUMBERTracker = param != null;

        this.localREPORTNUMBER = param;
    }

    public boolean isSUBSCRIBERSpecified() {
        return localSUBSCRIBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUBSCRIBER() {
        return localSUBSCRIBER;
    }

    /**
     * Auto generated setter method
     * @param param SUBSCRIBER
     */
    public void setSUBSCRIBER(java.lang.String param) {
        localSUBSCRIBERTracker = param != null;

        this.localSUBSCRIBER = param;
    }

    public boolean isSUBSCRIBERNAMESpecified() {
        return localSUBSCRIBERNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUBSCRIBERNAME() {
        return localSUBSCRIBERNAME;
    }

    /**
     * Auto generated setter method
     * @param param SUBSCRIBERNAME
     */
    public void setSUBSCRIBERNAME(java.lang.String param) {
        localSUBSCRIBERNAMETracker = param != null;

        this.localSUBSCRIBERNAME = param;
    }

    public boolean isENQUIRYREASONSpecified() {
        return localENQUIRYREASONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRYREASON() {
        return localENQUIRYREASON;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRYREASON
     */
    public void setENQUIRYREASON(java.lang.String param) {
        localENQUIRYREASONTracker = param != null;

        this.localENQUIRYREASON = param;
    }

    public boolean isFINANCEPURPOSESpecified() {
        return localFINANCEPURPOSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFINANCEPURPOSE() {
        return localFINANCEPURPOSE;
    }

    /**
     * Auto generated setter method
     * @param param FINANCEPURPOSE
     */
    public void setFINANCEPURPOSE(java.lang.String param) {
        localFINANCEPURPOSETracker = param != null;

        this.localFINANCEPURPOSE = param;
    }

    public boolean isAMOUNTFINANCEDSpecified() {
        return localAMOUNTFINANCEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNTFINANCED() {
        return localAMOUNTFINANCED;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNTFINANCED
     */
    public void setAMOUNTFINANCED(java.lang.String param) {
        localAMOUNTFINANCEDTracker = param != null;

        this.localAMOUNTFINANCED = param;
    }

    public boolean isDURATIONOFAGREEMENTSpecified() {
        return localDURATIONOFAGREEMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDURATIONOFAGREEMENT() {
        return localDURATIONOFAGREEMENT;
    }

    /**
     * Auto generated setter method
     * @param param DURATIONOFAGREEMENT
     */
    public void setDURATIONOFAGREEMENT(java.lang.String param) {
        localDURATIONOFAGREEMENTTracker = param != null;

        this.localDURATIONOFAGREEMENT = param;
    }

    public boolean isLASTNAMESpecified() {
        return localLASTNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLASTNAME() {
        return localLASTNAME;
    }

    /**
     * Auto generated setter method
     * @param param LASTNAME
     */
    public void setLASTNAME(java.lang.String param) {
        localLASTNAMETracker = param != null;

        this.localLASTNAME = param;
    }

    public boolean isFIRSTNAMESpecified() {
        return localFIRSTNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFIRSTNAME() {
        return localFIRSTNAME;
    }

    /**
     * Auto generated setter method
     * @param param FIRSTNAME
     */
    public void setFIRSTNAME(java.lang.String param) {
        localFIRSTNAMETracker = param != null;

        this.localFIRSTNAME = param;
    }

    public boolean isMIDDLENAME1Specified() {
        return localMIDDLENAME1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDDLENAME1() {
        return localMIDDLENAME1;
    }

    /**
     * Auto generated setter method
     * @param param MIDDLENAME1
     */
    public void setMIDDLENAME1(java.lang.String param) {
        localMIDDLENAME1Tracker = param != null;

        this.localMIDDLENAME1 = param;
    }

    public boolean isMIDDLENAME2Specified() {
        return localMIDDLENAME2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDDLENAME2() {
        return localMIDDLENAME2;
    }

    /**
     * Auto generated setter method
     * @param param MIDDLENAME2
     */
    public void setMIDDLENAME2(java.lang.String param) {
        localMIDDLENAME2Tracker = param != null;

        this.localMIDDLENAME2 = param;
    }

    public boolean isMIDDLENAME3Specified() {
        return localMIDDLENAME3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDDLENAME3() {
        return localMIDDLENAME3;
    }

    /**
     * Auto generated setter method
     * @param param MIDDLENAME3
     */
    public void setMIDDLENAME3(java.lang.String param) {
        localMIDDLENAME3Tracker = param != null;

        this.localMIDDLENAME3 = param;
    }

    public boolean isGENDERCODESpecified() {
        return localGENDERCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDERCODE() {
        return localGENDERCODE;
    }

    /**
     * Auto generated setter method
     * @param param GENDERCODE
     */
    public void setGENDERCODE(java.lang.String param) {
        localGENDERCODETracker = param != null;

        this.localGENDERCODE = param;
    }

    public boolean isGENDERSpecified() {
        return localGENDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDER() {
        return localGENDER;
    }

    /**
     * Auto generated setter method
     * @param param GENDER
     */
    public void setGENDER(java.lang.String param) {
        localGENDERTracker = param != null;

        this.localGENDER = param;
    }

    public boolean isINCOME_TAX_PAN_APPSpecified() {
        return localINCOME_TAX_PAN_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCOME_TAX_PAN_APP() {
        return localINCOME_TAX_PAN_APP;
    }

    /**
     * Auto generated setter method
     * @param param INCOME_TAX_PAN_APP
     */
    public void setINCOME_TAX_PAN_APP(java.lang.String param) {
        localINCOME_TAX_PAN_APPTracker = param != null;

        this.localINCOME_TAX_PAN_APP = param;
    }

    public boolean isPAN_ISSUE_DATE_APPSpecified() {
        return localPAN_ISSUE_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_ISSUE_DATE_APP() {
        return localPAN_ISSUE_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param PAN_ISSUE_DATE_APP
     */
    public void setPAN_ISSUE_DATE_APP(java.lang.String param) {
        localPAN_ISSUE_DATE_APPTracker = param != null;

        this.localPAN_ISSUE_DATE_APP = param;
    }

    public boolean isPAN_EXP_DATE_APPSpecified() {
        return localPAN_EXP_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_EXP_DATE_APP() {
        return localPAN_EXP_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param PAN_EXP_DATE_APP
     */
    public void setPAN_EXP_DATE_APP(java.lang.String param) {
        localPAN_EXP_DATE_APPTracker = param != null;

        this.localPAN_EXP_DATE_APP = param;
    }

    public boolean isPASSPORT_NUMBER_APPSpecified() {
        return localPASSPORT_NUMBER_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_NUMBER_APP() {
        return localPASSPORT_NUMBER_APP;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_NUMBER_APP
     */
    public void setPASSPORT_NUMBER_APP(java.lang.String param) {
        localPASSPORT_NUMBER_APPTracker = param != null;

        this.localPASSPORT_NUMBER_APP = param;
    }

    public boolean isPASSPORT_ISSUE_DATE_APPSpecified() {
        return localPASSPORT_ISSUE_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_ISSUE_DATE_APP() {
        return localPASSPORT_ISSUE_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_ISSUE_DATE_APP
     */
    public void setPASSPORT_ISSUE_DATE_APP(java.lang.String param) {
        localPASSPORT_ISSUE_DATE_APPTracker = param != null;

        this.localPASSPORT_ISSUE_DATE_APP = param;
    }

    public boolean isPASSPORT_EXP_DATE_APPSpecified() {
        return localPASSPORT_EXP_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_EXP_DATE_APP() {
        return localPASSPORT_EXP_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_EXP_DATE_APP
     */
    public void setPASSPORT_EXP_DATE_APP(java.lang.String param) {
        localPASSPORT_EXP_DATE_APPTracker = param != null;

        this.localPASSPORT_EXP_DATE_APP = param;
    }

    public boolean isVOTER_IDENTITY_CARD_APPSpecified() {
        return localVOTER_IDENTITY_CARD_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_IDENTITY_CARD_APP() {
        return localVOTER_IDENTITY_CARD_APP;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_IDENTITY_CARD_APP
     */
    public void setVOTER_IDENTITY_CARD_APP(java.lang.String param) {
        localVOTER_IDENTITY_CARD_APPTracker = param != null;

        this.localVOTER_IDENTITY_CARD_APP = param;
    }

    public boolean isVOTER_ID_ISSUE_DATE_APPSpecified() {
        return localVOTER_ID_ISSUE_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID_ISSUE_DATE_APP() {
        return localVOTER_ID_ISSUE_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID_ISSUE_DATE_APP
     */
    public void setVOTER_ID_ISSUE_DATE_APP(java.lang.String param) {
        localVOTER_ID_ISSUE_DATE_APPTracker = param != null;

        this.localVOTER_ID_ISSUE_DATE_APP = param;
    }

    public boolean isVOTER_ID_EXP_DATE_APPSpecified() {
        return localVOTER_ID_EXP_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID_EXP_DATE_APP() {
        return localVOTER_ID_EXP_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID_EXP_DATE_APP
     */
    public void setVOTER_ID_EXP_DATE_APP(java.lang.String param) {
        localVOTER_ID_EXP_DATE_APPTracker = param != null;

        this.localVOTER_ID_EXP_DATE_APP = param;
    }

    public boolean isDRIVER_LICENSE_NUMBER_APPSpecified() {
        return localDRIVER_LICENSE_NUMBER_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVER_LICENSE_NUMBER_APP() {
        return localDRIVER_LICENSE_NUMBER_APP;
    }

    /**
     * Auto generated setter method
     * @param param DRIVER_LICENSE_NUMBER_APP
     */
    public void setDRIVER_LICENSE_NUMBER_APP(java.lang.String param) {
        localDRIVER_LICENSE_NUMBER_APPTracker = param != null;

        this.localDRIVER_LICENSE_NUMBER_APP = param;
    }

    public boolean isDRIVER_LICENSE_ISSUE_DATE_APPSpecified() {
        return localDRIVER_LICENSE_ISSUE_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVER_LICENSE_ISSUE_DATE_APP() {
        return localDRIVER_LICENSE_ISSUE_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param DRIVER_LICENSE_ISSUE_DATE_APP
     */
    public void setDRIVER_LICENSE_ISSUE_DATE_APP(java.lang.String param) {
        localDRIVER_LICENSE_ISSUE_DATE_APPTracker = param != null;

        this.localDRIVER_LICENSE_ISSUE_DATE_APP = param;
    }

    public boolean isDRIVER_LICENSE_EXP_DATE_APPSpecified() {
        return localDRIVER_LICENSE_EXP_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVER_LICENSE_EXP_DATE_APP() {
        return localDRIVER_LICENSE_EXP_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param DRIVER_LICENSE_EXP_DATE_APP
     */
    public void setDRIVER_LICENSE_EXP_DATE_APP(java.lang.String param) {
        localDRIVER_LICENSE_EXP_DATE_APPTracker = param != null;

        this.localDRIVER_LICENSE_EXP_DATE_APP = param;
    }

    public boolean isRATION_CARD_NUMBER_APPSpecified() {
        return localRATION_CARD_NUMBER_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_NUMBER_APP() {
        return localRATION_CARD_NUMBER_APP;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_NUMBER_APP
     */
    public void setRATION_CARD_NUMBER_APP(java.lang.String param) {
        localRATION_CARD_NUMBER_APPTracker = param != null;

        this.localRATION_CARD_NUMBER_APP = param;
    }

    public boolean isRATION_CARD_ISSUE_DATE_APPSpecified() {
        return localRATION_CARD_ISSUE_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_ISSUE_DATE_APP() {
        return localRATION_CARD_ISSUE_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_ISSUE_DATE_APP
     */
    public void setRATION_CARD_ISSUE_DATE_APP(java.lang.String param) {
        localRATION_CARD_ISSUE_DATE_APPTracker = param != null;

        this.localRATION_CARD_ISSUE_DATE_APP = param;
    }

    public boolean isRATION_CARD_EXP_DATE_APPSpecified() {
        return localRATION_CARD_EXP_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_EXP_DATE_APP() {
        return localRATION_CARD_EXP_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_EXP_DATE_APP
     */
    public void setRATION_CARD_EXP_DATE_APP(java.lang.String param) {
        localRATION_CARD_EXP_DATE_APPTracker = param != null;

        this.localRATION_CARD_EXP_DATE_APP = param;
    }

    public boolean isUNIVERSAL_ID_NUMBER_APPSpecified() {
        return localUNIVERSAL_ID_NUMBER_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUNIVERSAL_ID_NUMBER_APP() {
        return localUNIVERSAL_ID_NUMBER_APP;
    }

    /**
     * Auto generated setter method
     * @param param UNIVERSAL_ID_NUMBER_APP
     */
    public void setUNIVERSAL_ID_NUMBER_APP(java.lang.String param) {
        localUNIVERSAL_ID_NUMBER_APPTracker = param != null;

        this.localUNIVERSAL_ID_NUMBER_APP = param;
    }

    public boolean isUNIVERSAL_ID_ISSUE_DATE_APPSpecified() {
        return localUNIVERSAL_ID_ISSUE_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUNIVERSAL_ID_ISSUE_DATE_APP() {
        return localUNIVERSAL_ID_ISSUE_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param UNIVERSAL_ID_ISSUE_DATE_APP
     */
    public void setUNIVERSAL_ID_ISSUE_DATE_APP(java.lang.String param) {
        localUNIVERSAL_ID_ISSUE_DATE_APPTracker = param != null;

        this.localUNIVERSAL_ID_ISSUE_DATE_APP = param;
    }

    public boolean isUNIVERSAL_ID_EXP_DATE_APPSpecified() {
        return localUNIVERSAL_ID_EXP_DATE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUNIVERSAL_ID_EXP_DATE_APP() {
        return localUNIVERSAL_ID_EXP_DATE_APP;
    }

    /**
     * Auto generated setter method
     * @param param UNIVERSAL_ID_EXP_DATE_APP
     */
    public void setUNIVERSAL_ID_EXP_DATE_APP(java.lang.String param) {
        localUNIVERSAL_ID_EXP_DATE_APPTracker = param != null;

        this.localUNIVERSAL_ID_EXP_DATE_APP = param;
    }

    public boolean isDATEOFBIRTHAPPLICANTSpecified() {
        return localDATEOFBIRTHAPPLICANTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOFBIRTHAPPLICANT() {
        return localDATEOFBIRTHAPPLICANT;
    }

    /**
     * Auto generated setter method
     * @param param DATEOFBIRTHAPPLICANT
     */
    public void setDATEOFBIRTHAPPLICANT(java.lang.String param) {
        localDATEOFBIRTHAPPLICANTTracker = param != null;

        this.localDATEOFBIRTHAPPLICANT = param;
    }

    public boolean isTELEPHONENUMBERAPPLICANT1STSpecified() {
        return localTELEPHONENUMBERAPPLICANT1STTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEPHONENUMBERAPPLICANT1ST() {
        return localTELEPHONENUMBERAPPLICANT1ST;
    }

    /**
     * Auto generated setter method
     * @param param TELEPHONENUMBERAPPLICANT1ST
     */
    public void setTELEPHONENUMBERAPPLICANT1ST(java.lang.String param) {
        localTELEPHONENUMBERAPPLICANT1STTracker = param != null;

        this.localTELEPHONENUMBERAPPLICANT1ST = param;
    }

    public boolean isTELEEXTENSION_APPSpecified() {
        return localTELEEXTENSION_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEEXTENSION_APP() {
        return localTELEEXTENSION_APP;
    }

    /**
     * Auto generated setter method
     * @param param TELEEXTENSION_APP
     */
    public void setTELEEXTENSION_APP(java.lang.String param) {
        localTELEEXTENSION_APPTracker = param != null;

        this.localTELEEXTENSION_APP = param;
    }

    public boolean isTELETYPE_APPSpecified() {
        return localTELETYPE_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELETYPE_APP() {
        return localTELETYPE_APP;
    }

    /**
     * Auto generated setter method
     * @param param TELETYPE_APP
     */
    public void setTELETYPE_APP(java.lang.String param) {
        localTELETYPE_APPTracker = param != null;

        this.localTELETYPE_APP = param;
    }

    public boolean isMOBILEPHONENUMBERSpecified() {
        return localMOBILEPHONENUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOBILEPHONENUMBER() {
        return localMOBILEPHONENUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MOBILEPHONENUMBER
     */
    public void setMOBILEPHONENUMBER(java.lang.String param) {
        localMOBILEPHONENUMBERTracker = param != null;

        this.localMOBILEPHONENUMBER = param;
    }

    public boolean isEMAILID_APPSpecified() {
        return localEMAILID_APPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAILID_APP() {
        return localEMAILID_APP;
    }

    /**
     * Auto generated setter method
     * @param param EMAILID_APP
     */
    public void setEMAILID_APP(java.lang.String param) {
        localEMAILID_APPTracker = param != null;

        this.localEMAILID_APP = param;
    }

    public boolean isINCOMESpecified() {
        return localINCOMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCOME() {
        return localINCOME;
    }

    /**
     * Auto generated setter method
     * @param param INCOME
     */
    public void setINCOME(java.lang.String param) {
        localINCOMETracker = param != null;

        this.localINCOME = param;
    }

    public boolean isMARITALSTATUSCODESpecified() {
        return localMARITALSTATUSCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMARITALSTATUSCODE() {
        return localMARITALSTATUSCODE;
    }

    /**
     * Auto generated setter method
     * @param param MARITALSTATUSCODE
     */
    public void setMARITALSTATUSCODE(java.lang.String param) {
        localMARITALSTATUSCODETracker = param != null;

        this.localMARITALSTATUSCODE = param;
    }

    public boolean isMARITALSTATUSSpecified() {
        return localMARITALSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMARITALSTATUS() {
        return localMARITALSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param MARITALSTATUS
     */
    public void setMARITALSTATUS(java.lang.String param) {
        localMARITALSTATUSTracker = param != null;

        this.localMARITALSTATUS = param;
    }

    public boolean isEMPLOYMENTSTATUSSpecified() {
        return localEMPLOYMENTSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMPLOYMENTSTATUS() {
        return localEMPLOYMENTSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param EMPLOYMENTSTATUS
     */
    public void setEMPLOYMENTSTATUS(java.lang.String param) {
        localEMPLOYMENTSTATUSTracker = param != null;

        this.localEMPLOYMENTSTATUS = param;
    }

    public boolean isDATEWITHEMPLOYERSpecified() {
        return localDATEWITHEMPLOYERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEWITHEMPLOYER() {
        return localDATEWITHEMPLOYER;
    }

    /**
     * Auto generated setter method
     * @param param DATEWITHEMPLOYER
     */
    public void setDATEWITHEMPLOYER(java.lang.String param) {
        localDATEWITHEMPLOYERTracker = param != null;

        this.localDATEWITHEMPLOYER = param;
    }

    public boolean isNUMBEROFMAJORCREDITCARDHELDSpecified() {
        return localNUMBEROFMAJORCREDITCARDHELDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNUMBEROFMAJORCREDITCARDHELD() {
        return localNUMBEROFMAJORCREDITCARDHELD;
    }

    /**
     * Auto generated setter method
     * @param param NUMBEROFMAJORCREDITCARDHELD
     */
    public void setNUMBEROFMAJORCREDITCARDHELD(java.lang.String param) {
        localNUMBEROFMAJORCREDITCARDHELDTracker = param != null;

        this.localNUMBEROFMAJORCREDITCARDHELD = param;
    }

    public boolean isFLATNOPLOTNOHOUSENOSpecified() {
        return localFLATNOPLOTNOHOUSENOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFLATNOPLOTNOHOUSENO() {
        return localFLATNOPLOTNOHOUSENO;
    }

    /**
     * Auto generated setter method
     * @param param FLATNOPLOTNOHOUSENO
     */
    public void setFLATNOPLOTNOHOUSENO(java.lang.String param) {
        localFLATNOPLOTNOHOUSENOTracker = param != null;

        this.localFLATNOPLOTNOHOUSENO = param;
    }

    public boolean isBLDGNOSOCIETYNAMESpecified() {
        return localBLDGNOSOCIETYNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBLDGNOSOCIETYNAME() {
        return localBLDGNOSOCIETYNAME;
    }

    /**
     * Auto generated setter method
     * @param param BLDGNOSOCIETYNAME
     */
    public void setBLDGNOSOCIETYNAME(java.lang.String param) {
        localBLDGNOSOCIETYNAMETracker = param != null;

        this.localBLDGNOSOCIETYNAME = param;
    }

    public boolean isROADNONAMEAREALOCALITYSpecified() {
        return localROADNONAMEAREALOCALITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getROADNONAMEAREALOCALITY() {
        return localROADNONAMEAREALOCALITY;
    }

    /**
     * Auto generated setter method
     * @param param ROADNONAMEAREALOCALITY
     */
    public void setROADNONAMEAREALOCALITY(java.lang.String param) {
        localROADNONAMEAREALOCALITYTracker = param != null;

        this.localROADNONAMEAREALOCALITY = param;
    }

    public boolean isCITYSpecified() {
        return localCITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCITY() {
        return localCITY;
    }

    /**
     * Auto generated setter method
     * @param param CITY
     */
    public void setCITY(java.lang.String param) {
        localCITYTracker = param != null;

        this.localCITY = param;
    }

    public boolean isLANDMARKSpecified() {
        return localLANDMARKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLANDMARK() {
        return localLANDMARK;
    }

    /**
     * Auto generated setter method
     * @param param LANDMARK
     */
    public void setLANDMARK(java.lang.String param) {
        localLANDMARKTracker = param != null;

        this.localLANDMARK = param;
    }

    public boolean isSTATESpecified() {
        return localSTATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATE() {
        return localSTATE;
    }

    /**
     * Auto generated setter method
     * @param param STATE
     */
    public void setSTATE(java.lang.String param) {
        localSTATETracker = param != null;

        this.localSTATE = param;
    }

    public boolean isPINCODESpecified() {
        return localPINCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPINCODE() {
        return localPINCODE;
    }

    /**
     * Auto generated setter method
     * @param param PINCODE
     */
    public void setPINCODE(java.lang.String param) {
        localPINCODETracker = param != null;

        this.localPINCODE = param;
    }

    public boolean isCOUNTRY_CODESpecified() {
        return localCOUNTRY_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOUNTRY_CODE() {
        return localCOUNTRY_CODE;
    }

    /**
     * Auto generated setter method
     * @param param COUNTRY_CODE
     */
    public void setCOUNTRY_CODE(java.lang.String param) {
        localCOUNTRY_CODETracker = param != null;

        this.localCOUNTRY_CODE = param;
    }

    public boolean isADD_FLATNOPLOTNOHOUSENOSpecified() {
        return localADD_FLATNOPLOTNOHOUSENOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_FLATNOPLOTNOHOUSENO() {
        return localADD_FLATNOPLOTNOHOUSENO;
    }

    /**
     * Auto generated setter method
     * @param param ADD_FLATNOPLOTNOHOUSENO
     */
    public void setADD_FLATNOPLOTNOHOUSENO(java.lang.String param) {
        localADD_FLATNOPLOTNOHOUSENOTracker = param != null;

        this.localADD_FLATNOPLOTNOHOUSENO = param;
    }

    public boolean isADD_BLDGNOSOCIETYNAMESpecified() {
        return localADD_BLDGNOSOCIETYNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_BLDGNOSOCIETYNAME() {
        return localADD_BLDGNOSOCIETYNAME;
    }

    /**
     * Auto generated setter method
     * @param param ADD_BLDGNOSOCIETYNAME
     */
    public void setADD_BLDGNOSOCIETYNAME(java.lang.String param) {
        localADD_BLDGNOSOCIETYNAMETracker = param != null;

        this.localADD_BLDGNOSOCIETYNAME = param;
    }

    public boolean isADD_ROADNONAMEAREALOCALITYSpecified() {
        return localADD_ROADNONAMEAREALOCALITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_ROADNONAMEAREALOCALITY() {
        return localADD_ROADNONAMEAREALOCALITY;
    }

    /**
     * Auto generated setter method
     * @param param ADD_ROADNONAMEAREALOCALITY
     */
    public void setADD_ROADNONAMEAREALOCALITY(java.lang.String param) {
        localADD_ROADNONAMEAREALOCALITYTracker = param != null;

        this.localADD_ROADNONAMEAREALOCALITY = param;
    }

    public boolean isADD_CITYSpecified() {
        return localADD_CITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_CITY() {
        return localADD_CITY;
    }

    /**
     * Auto generated setter method
     * @param param ADD_CITY
     */
    public void setADD_CITY(java.lang.String param) {
        localADD_CITYTracker = param != null;

        this.localADD_CITY = param;
    }

    public boolean isADD_LANDMARKSpecified() {
        return localADD_LANDMARKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_LANDMARK() {
        return localADD_LANDMARK;
    }

    /**
     * Auto generated setter method
     * @param param ADD_LANDMARK
     */
    public void setADD_LANDMARK(java.lang.String param) {
        localADD_LANDMARKTracker = param != null;

        this.localADD_LANDMARK = param;
    }

    public boolean isADD_STATESpecified() {
        return localADD_STATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_STATE() {
        return localADD_STATE;
    }

    /**
     * Auto generated setter method
     * @param param ADD_STATE
     */
    public void setADD_STATE(java.lang.String param) {
        localADD_STATETracker = param != null;

        this.localADD_STATE = param;
    }

    public boolean isADD_PINCODESpecified() {
        return localADD_PINCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_PINCODE() {
        return localADD_PINCODE;
    }

    /**
     * Auto generated setter method
     * @param param ADD_PINCODE
     */
    public void setADD_PINCODE(java.lang.String param) {
        localADD_PINCODETracker = param != null;

        this.localADD_PINCODE = param;
    }

    public boolean isADD_COUNTRYCODESpecified() {
        return localADD_COUNTRYCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_COUNTRYCODE() {
        return localADD_COUNTRYCODE;
    }

    /**
     * Auto generated setter method
     * @param param ADD_COUNTRYCODE
     */
    public void setADD_COUNTRYCODE(java.lang.String param) {
        localADD_COUNTRYCODETracker = param != null;

        this.localADD_COUNTRYCODE = param;
    }

    public boolean isCREDITACCOUNTTOTALSpecified() {
        return localCREDITACCOUNTTOTALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITACCOUNTTOTAL() {
        return localCREDITACCOUNTTOTAL;
    }

    /**
     * Auto generated setter method
     * @param param CREDITACCOUNTTOTAL
     */
    public void setCREDITACCOUNTTOTAL(java.lang.String param) {
        localCREDITACCOUNTTOTALTracker = param != null;

        this.localCREDITACCOUNTTOTAL = param;
    }

    public boolean isCREDITACCOUNTACTIVESpecified() {
        return localCREDITACCOUNTACTIVETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITACCOUNTACTIVE() {
        return localCREDITACCOUNTACTIVE;
    }

    /**
     * Auto generated setter method
     * @param param CREDITACCOUNTACTIVE
     */
    public void setCREDITACCOUNTACTIVE(java.lang.String param) {
        localCREDITACCOUNTACTIVETracker = param != null;

        this.localCREDITACCOUNTACTIVE = param;
    }

    public boolean isCREDITACCOUNTDEFAULTSpecified() {
        return localCREDITACCOUNTDEFAULTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITACCOUNTDEFAULT() {
        return localCREDITACCOUNTDEFAULT;
    }

    /**
     * Auto generated setter method
     * @param param CREDITACCOUNTDEFAULT
     */
    public void setCREDITACCOUNTDEFAULT(java.lang.String param) {
        localCREDITACCOUNTDEFAULTTracker = param != null;

        this.localCREDITACCOUNTDEFAULT = param;
    }

    public boolean isCREDITACCOUNTCLOSEDSpecified() {
        return localCREDITACCOUNTCLOSEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITACCOUNTCLOSED() {
        return localCREDITACCOUNTCLOSED;
    }

    /**
     * Auto generated setter method
     * @param param CREDITACCOUNTCLOSED
     */
    public void setCREDITACCOUNTCLOSED(java.lang.String param) {
        localCREDITACCOUNTCLOSEDTracker = param != null;

        this.localCREDITACCOUNTCLOSED = param;
    }

    public boolean isCADSUITFILEDCURRENTBALANCESpecified() {
        return localCADSUITFILEDCURRENTBALANCETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCADSUITFILEDCURRENTBALANCE() {
        return localCADSUITFILEDCURRENTBALANCE;
    }

    /**
     * Auto generated setter method
     * @param param CADSUITFILEDCURRENTBALANCE
     */
    public void setCADSUITFILEDCURRENTBALANCE(java.lang.String param) {
        localCADSUITFILEDCURRENTBALANCETracker = param != null;

        this.localCADSUITFILEDCURRENTBALANCE = param;
    }

    public boolean isOUTSTANDINGBALANCESECUREDSpecified() {
        return localOUTSTANDINGBALANCESECUREDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTSTANDINGBALANCESECURED() {
        return localOUTSTANDINGBALANCESECURED;
    }

    /**
     * Auto generated setter method
     * @param param OUTSTANDINGBALANCESECURED
     */
    public void setOUTSTANDINGBALANCESECURED(java.lang.String param) {
        localOUTSTANDINGBALANCESECUREDTracker = param != null;

        this.localOUTSTANDINGBALANCESECURED = param;
    }

    public boolean isOUTSTANDINGBALSECUREDPERSpecified() {
        return localOUTSTANDINGBALSECUREDPERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTSTANDINGBALSECUREDPER() {
        return localOUTSTANDINGBALSECUREDPER;
    }

    /**
     * Auto generated setter method
     * @param param OUTSTANDINGBALSECUREDPER
     */
    public void setOUTSTANDINGBALSECUREDPER(java.lang.String param) {
        localOUTSTANDINGBALSECUREDPERTracker = param != null;

        this.localOUTSTANDINGBALSECUREDPER = param;
    }

    public boolean isOUTSTANDINGBALANCEUNSECUREDSpecified() {
        return localOUTSTANDINGBALANCEUNSECUREDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTSTANDINGBALANCEUNSECURED() {
        return localOUTSTANDINGBALANCEUNSECURED;
    }

    /**
     * Auto generated setter method
     * @param param OUTSTANDINGBALANCEUNSECURED
     */
    public void setOUTSTANDINGBALANCEUNSECURED(java.lang.String param) {
        localOUTSTANDINGBALANCEUNSECUREDTracker = param != null;

        this.localOUTSTANDINGBALANCEUNSECURED = param;
    }

    public boolean isOUTSTANDINGBALUNSECPERSpecified() {
        return localOUTSTANDINGBALUNSECPERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTSTANDINGBALUNSECPER() {
        return localOUTSTANDINGBALUNSECPER;
    }

    /**
     * Auto generated setter method
     * @param param OUTSTANDINGBALUNSECPER
     */
    public void setOUTSTANDINGBALUNSECPER(java.lang.String param) {
        localOUTSTANDINGBALUNSECPERTracker = param != null;

        this.localOUTSTANDINGBALUNSECPER = param;
    }

    public boolean isOUTSTANDINGBALANCEALLSpecified() {
        return localOUTSTANDINGBALANCEALLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTSTANDINGBALANCEALL() {
        return localOUTSTANDINGBALANCEALL;
    }

    /**
     * Auto generated setter method
     * @param param OUTSTANDINGBALANCEALL
     */
    public void setOUTSTANDINGBALANCEALL(java.lang.String param) {
        localOUTSTANDINGBALANCEALLTracker = param != null;

        this.localOUTSTANDINGBALANCEALL = param;
    }

    public boolean isIDENTIFICATIONNUMBERSpecified() {
        return localIDENTIFICATIONNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIDENTIFICATIONNUMBER() {
        return localIDENTIFICATIONNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param IDENTIFICATIONNUMBER
     */
    public void setIDENTIFICATIONNUMBER(java.lang.String param) {
        localIDENTIFICATIONNUMBERTracker = param != null;

        this.localIDENTIFICATIONNUMBER = param;
    }

    public boolean isCAISSUBSCRIBERNAMESpecified() {
        return localCAISSUBSCRIBERNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAISSUBSCRIBERNAME() {
        return localCAISSUBSCRIBERNAME;
    }

    /**
     * Auto generated setter method
     * @param param CAISSUBSCRIBERNAME
     */
    public void setCAISSUBSCRIBERNAME(java.lang.String param) {
        localCAISSUBSCRIBERNAMETracker = param != null;

        this.localCAISSUBSCRIBERNAME = param;
    }

    public boolean isACCOUNTNUMBERSpecified() {
        return localACCOUNTNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTNUMBER() {
        return localACCOUNTNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTNUMBER
     */
    public void setACCOUNTNUMBER(java.lang.String param) {
        localACCOUNTNUMBERTracker = param != null;

        this.localACCOUNTNUMBER = param;
    }

    public boolean isPORTFOLIOTYPESpecified() {
        return localPORTFOLIOTYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPORTFOLIOTYPE() {
        return localPORTFOLIOTYPE;
    }

    /**
     * Auto generated setter method
     * @param param PORTFOLIOTYPE
     */
    public void setPORTFOLIOTYPE(java.lang.String param) {
        localPORTFOLIOTYPETracker = param != null;

        this.localPORTFOLIOTYPE = param;
    }

    public boolean isACCOUNTTYPE_CODESpecified() {
        return localACCOUNTTYPE_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTTYPE_CODE() {
        return localACCOUNTTYPE_CODE;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTTYPE_CODE
     */
    public void setACCOUNTTYPE_CODE(java.lang.String param) {
        localACCOUNTTYPE_CODETracker = param != null;

        this.localACCOUNTTYPE_CODE = param;
    }

    public boolean isACCOUNTTYPESpecified() {
        return localACCOUNTTYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTTYPE() {
        return localACCOUNTTYPE;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTTYPE
     */
    public void setACCOUNTTYPE(java.lang.String param) {
        localACCOUNTTYPETracker = param != null;

        this.localACCOUNTTYPE = param;
    }

    public boolean isOPENDATESpecified() {
        return localOPENDATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOPENDATE() {
        return localOPENDATE;
    }

    /**
     * Auto generated setter method
     * @param param OPENDATE
     */
    public void setOPENDATE(java.lang.String param) {
        localOPENDATETracker = param != null;

        this.localOPENDATE = param;
    }

    public boolean isHIGHESTCREDITORORGNLOANAMTSpecified() {
        return localHIGHESTCREDITORORGNLOANAMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHIGHESTCREDITORORGNLOANAMT() {
        return localHIGHESTCREDITORORGNLOANAMT;
    }

    /**
     * Auto generated setter method
     * @param param HIGHESTCREDITORORGNLOANAMT
     */
    public void setHIGHESTCREDITORORGNLOANAMT(java.lang.String param) {
        localHIGHESTCREDITORORGNLOANAMTTracker = param != null;

        this.localHIGHESTCREDITORORGNLOANAMT = param;
    }

    public boolean isTERMSDURATIONSpecified() {
        return localTERMSDURATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTERMSDURATION() {
        return localTERMSDURATION;
    }

    /**
     * Auto generated setter method
     * @param param TERMSDURATION
     */
    public void setTERMSDURATION(java.lang.String param) {
        localTERMSDURATIONTracker = param != null;

        this.localTERMSDURATION = param;
    }

    public boolean isTERMSFREQUENCYSpecified() {
        return localTERMSFREQUENCYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTERMSFREQUENCY() {
        return localTERMSFREQUENCY;
    }

    /**
     * Auto generated setter method
     * @param param TERMSFREQUENCY
     */
    public void setTERMSFREQUENCY(java.lang.String param) {
        localTERMSFREQUENCYTracker = param != null;

        this.localTERMSFREQUENCY = param;
    }

    public boolean isSCHEDULEDMONTHLYPAYAMTSpecified() {
        return localSCHEDULEDMONTHLYPAYAMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCHEDULEDMONTHLYPAYAMT() {
        return localSCHEDULEDMONTHLYPAYAMT;
    }

    /**
     * Auto generated setter method
     * @param param SCHEDULEDMONTHLYPAYAMT
     */
    public void setSCHEDULEDMONTHLYPAYAMT(java.lang.String param) {
        localSCHEDULEDMONTHLYPAYAMTTracker = param != null;

        this.localSCHEDULEDMONTHLYPAYAMT = param;
    }

    public boolean isACCOUNTSTATUS_CODESpecified() {
        return localACCOUNTSTATUS_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTSTATUS_CODE() {
        return localACCOUNTSTATUS_CODE;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTSTATUS_CODE
     */
    public void setACCOUNTSTATUS_CODE(java.lang.String param) {
        localACCOUNTSTATUS_CODETracker = param != null;

        this.localACCOUNTSTATUS_CODE = param;
    }

    public boolean isACCOUNTSTATUSSpecified() {
        return localACCOUNTSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTSTATUS() {
        return localACCOUNTSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTSTATUS
     */
    public void setACCOUNTSTATUS(java.lang.String param) {
        localACCOUNTSTATUSTracker = param != null;

        this.localACCOUNTSTATUS = param;
    }

    public boolean isPAYMENTRATINGSpecified() {
        return localPAYMENTRATINGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENTRATING() {
        return localPAYMENTRATING;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENTRATING
     */
    public void setPAYMENTRATING(java.lang.String param) {
        localPAYMENTRATINGTracker = param != null;

        this.localPAYMENTRATING = param;
    }

    public boolean isPAYMENTHISTORYPROFILESpecified() {
        return localPAYMENTHISTORYPROFILETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENTHISTORYPROFILE() {
        return localPAYMENTHISTORYPROFILE;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENTHISTORYPROFILE
     */
    public void setPAYMENTHISTORYPROFILE(java.lang.String param) {
        localPAYMENTHISTORYPROFILETracker = param != null;

        this.localPAYMENTHISTORYPROFILE = param;
    }

    public boolean isSPECIALCOMMENTSpecified() {
        return localSPECIALCOMMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSPECIALCOMMENT() {
        return localSPECIALCOMMENT;
    }

    /**
     * Auto generated setter method
     * @param param SPECIALCOMMENT
     */
    public void setSPECIALCOMMENT(java.lang.String param) {
        localSPECIALCOMMENTTracker = param != null;

        this.localSPECIALCOMMENT = param;
    }

    public boolean isCURRENTBALANCE_TLSpecified() {
        return localCURRENTBALANCE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENTBALANCE_TL() {
        return localCURRENTBALANCE_TL;
    }

    /**
     * Auto generated setter method
     * @param param CURRENTBALANCE_TL
     */
    public void setCURRENTBALANCE_TL(java.lang.String param) {
        localCURRENTBALANCE_TLTracker = param != null;

        this.localCURRENTBALANCE_TL = param;
    }

    public boolean isAMOUNTPASTDUE_TLSpecified() {
        return localAMOUNTPASTDUE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNTPASTDUE_TL() {
        return localAMOUNTPASTDUE_TL;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNTPASTDUE_TL
     */
    public void setAMOUNTPASTDUE_TL(java.lang.String param) {
        localAMOUNTPASTDUE_TLTracker = param != null;

        this.localAMOUNTPASTDUE_TL = param;
    }

    public boolean isORIGINALCHARGEOFFAMOUNTSpecified() {
        return localORIGINALCHARGEOFFAMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getORIGINALCHARGEOFFAMOUNT() {
        return localORIGINALCHARGEOFFAMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param ORIGINALCHARGEOFFAMOUNT
     */
    public void setORIGINALCHARGEOFFAMOUNT(java.lang.String param) {
        localORIGINALCHARGEOFFAMOUNTTracker = param != null;

        this.localORIGINALCHARGEOFFAMOUNT = param;
    }

    public boolean isDATEREPORTEDSpecified() {
        return localDATEREPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEREPORTED() {
        return localDATEREPORTED;
    }

    /**
     * Auto generated setter method
     * @param param DATEREPORTED
     */
    public void setDATEREPORTED(java.lang.String param) {
        localDATEREPORTEDTracker = param != null;

        this.localDATEREPORTED = param;
    }

    public boolean isDATEOFFIRSTDELINQUENCYSpecified() {
        return localDATEOFFIRSTDELINQUENCYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOFFIRSTDELINQUENCY() {
        return localDATEOFFIRSTDELINQUENCY;
    }

    /**
     * Auto generated setter method
     * @param param DATEOFFIRSTDELINQUENCY
     */
    public void setDATEOFFIRSTDELINQUENCY(java.lang.String param) {
        localDATEOFFIRSTDELINQUENCYTracker = param != null;

        this.localDATEOFFIRSTDELINQUENCY = param;
    }

    public boolean isDATECLOSEDSpecified() {
        return localDATECLOSEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATECLOSED() {
        return localDATECLOSED;
    }

    /**
     * Auto generated setter method
     * @param param DATECLOSED
     */
    public void setDATECLOSED(java.lang.String param) {
        localDATECLOSEDTracker = param != null;

        this.localDATECLOSED = param;
    }

    public boolean isDATEOFLASTPAYMENTSpecified() {
        return localDATEOFLASTPAYMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOFLASTPAYMENT() {
        return localDATEOFLASTPAYMENT;
    }

    /**
     * Auto generated setter method
     * @param param DATEOFLASTPAYMENT
     */
    public void setDATEOFLASTPAYMENT(java.lang.String param) {
        localDATEOFLASTPAYMENTTracker = param != null;

        this.localDATEOFLASTPAYMENT = param;
    }

    public boolean isSUITFWILFULDFTWRITTENOFFSTSSpecified() {
        return localSUITFWILFULDFTWRITTENOFFSTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUITFWILFULDFTWRITTENOFFSTS() {
        return localSUITFWILFULDFTWRITTENOFFSTS;
    }

    /**
     * Auto generated setter method
     * @param param SUITFWILFULDFTWRITTENOFFSTS
     */
    public void setSUITFWILFULDFTWRITTENOFFSTS(java.lang.String param) {
        localSUITFWILFULDFTWRITTENOFFSTSTracker = param != null;

        this.localSUITFWILFULDFTWRITTENOFFSTS = param;
    }

    public boolean isSUITFILEDWILFULDEFSpecified() {
        return localSUITFILEDWILFULDEFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUITFILEDWILFULDEF() {
        return localSUITFILEDWILFULDEF;
    }

    /**
     * Auto generated setter method
     * @param param SUITFILEDWILFULDEF
     */
    public void setSUITFILEDWILFULDEF(java.lang.String param) {
        localSUITFILEDWILFULDEFTracker = param != null;

        this.localSUITFILEDWILFULDEF = param;
    }

    public boolean isWRITTENOFFSETTLEDSTATUSSpecified() {
        return localWRITTENOFFSETTLEDSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITTENOFFSETTLEDSTATUS() {
        return localWRITTENOFFSETTLEDSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param WRITTENOFFSETTLEDSTATUS
     */
    public void setWRITTENOFFSETTLEDSTATUS(java.lang.String param) {
        localWRITTENOFFSETTLEDSTATUSTracker = param != null;

        this.localWRITTENOFFSETTLEDSTATUS = param;
    }

    public boolean isVALUEOFCREDITSLASTMONTHSpecified() {
        return localVALUEOFCREDITSLASTMONTHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVALUEOFCREDITSLASTMONTH() {
        return localVALUEOFCREDITSLASTMONTH;
    }

    /**
     * Auto generated setter method
     * @param param VALUEOFCREDITSLASTMONTH
     */
    public void setVALUEOFCREDITSLASTMONTH(java.lang.String param) {
        localVALUEOFCREDITSLASTMONTHTracker = param != null;

        this.localVALUEOFCREDITSLASTMONTH = param;
    }

    public boolean isOCCUPATIONCODESpecified() {
        return localOCCUPATIONCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOCCUPATIONCODE() {
        return localOCCUPATIONCODE;
    }

    /**
     * Auto generated setter method
     * @param param OCCUPATIONCODE
     */
    public void setOCCUPATIONCODE(java.lang.String param) {
        localOCCUPATIONCODETracker = param != null;

        this.localOCCUPATIONCODE = param;
    }

    public boolean isSATTLEMENTAMOUNTSpecified() {
        return localSATTLEMENTAMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSATTLEMENTAMOUNT() {
        return localSATTLEMENTAMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param SATTLEMENTAMOUNT
     */
    public void setSATTLEMENTAMOUNT(java.lang.String param) {
        localSATTLEMENTAMOUNTTracker = param != null;

        this.localSATTLEMENTAMOUNT = param;
    }

    public boolean isVALUEOFCOLATERALSpecified() {
        return localVALUEOFCOLATERALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVALUEOFCOLATERAL() {
        return localVALUEOFCOLATERAL;
    }

    /**
     * Auto generated setter method
     * @param param VALUEOFCOLATERAL
     */
    public void setVALUEOFCOLATERAL(java.lang.String param) {
        localVALUEOFCOLATERALTracker = param != null;

        this.localVALUEOFCOLATERAL = param;
    }

    public boolean isTYPEOFCOLATERALSpecified() {
        return localTYPEOFCOLATERALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTYPEOFCOLATERAL() {
        return localTYPEOFCOLATERAL;
    }

    /**
     * Auto generated setter method
     * @param param TYPEOFCOLATERAL
     */
    public void setTYPEOFCOLATERAL(java.lang.String param) {
        localTYPEOFCOLATERALTracker = param != null;

        this.localTYPEOFCOLATERAL = param;
    }

    public boolean isWRITTENOFFAMTTOTALSpecified() {
        return localWRITTENOFFAMTTOTALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITTENOFFAMTTOTAL() {
        return localWRITTENOFFAMTTOTAL;
    }

    /**
     * Auto generated setter method
     * @param param WRITTENOFFAMTTOTAL
     */
    public void setWRITTENOFFAMTTOTAL(java.lang.String param) {
        localWRITTENOFFAMTTOTALTracker = param != null;

        this.localWRITTENOFFAMTTOTAL = param;
    }

    public boolean isWRITTENOFFAMTPRINCIPALSpecified() {
        return localWRITTENOFFAMTPRINCIPALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITTENOFFAMTPRINCIPAL() {
        return localWRITTENOFFAMTPRINCIPAL;
    }

    /**
     * Auto generated setter method
     * @param param WRITTENOFFAMTPRINCIPAL
     */
    public void setWRITTENOFFAMTPRINCIPAL(java.lang.String param) {
        localWRITTENOFFAMTPRINCIPALTracker = param != null;

        this.localWRITTENOFFAMTPRINCIPAL = param;
    }

    public boolean isRATEOFINTERESTSpecified() {
        return localRATEOFINTERESTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATEOFINTEREST() {
        return localRATEOFINTEREST;
    }

    /**
     * Auto generated setter method
     * @param param RATEOFINTEREST
     */
    public void setRATEOFINTEREST(java.lang.String param) {
        localRATEOFINTERESTTracker = param != null;

        this.localRATEOFINTEREST = param;
    }

    public boolean isREPAYMENTTENURESpecified() {
        return localREPAYMENTTENURETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPAYMENTTENURE() {
        return localREPAYMENTTENURE;
    }

    /**
     * Auto generated setter method
     * @param param REPAYMENTTENURE
     */
    public void setREPAYMENTTENURE(java.lang.String param) {
        localREPAYMENTTENURETracker = param != null;

        this.localREPAYMENTTENURE = param;
    }

    public boolean isPROMOTIONALRATEFLAGSpecified() {
        return localPROMOTIONALRATEFLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPROMOTIONALRATEFLAG() {
        return localPROMOTIONALRATEFLAG;
    }

    /**
     * Auto generated setter method
     * @param param PROMOTIONALRATEFLAG
     */
    public void setPROMOTIONALRATEFLAG(java.lang.String param) {
        localPROMOTIONALRATEFLAGTracker = param != null;

        this.localPROMOTIONALRATEFLAG = param;
    }

    public boolean isCAISINCOMESpecified() {
        return localCAISINCOMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAISINCOME() {
        return localCAISINCOME;
    }

    /**
     * Auto generated setter method
     * @param param CAISINCOME
     */
    public void setCAISINCOME(java.lang.String param) {
        localCAISINCOMETracker = param != null;

        this.localCAISINCOME = param;
    }

    public boolean isINCOMEINDICATORSpecified() {
        return localINCOMEINDICATORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCOMEINDICATOR() {
        return localINCOMEINDICATOR;
    }

    /**
     * Auto generated setter method
     * @param param INCOMEINDICATOR
     */
    public void setINCOMEINDICATOR(java.lang.String param) {
        localINCOMEINDICATORTracker = param != null;

        this.localINCOMEINDICATOR = param;
    }

    public boolean isINCOMEFREQUENCYINDICATORSpecified() {
        return localINCOMEFREQUENCYINDICATORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCOMEFREQUENCYINDICATOR() {
        return localINCOMEFREQUENCYINDICATOR;
    }

    /**
     * Auto generated setter method
     * @param param INCOMEFREQUENCYINDICATOR
     */
    public void setINCOMEFREQUENCYINDICATOR(java.lang.String param) {
        localINCOMEFREQUENCYINDICATORTracker = param != null;

        this.localINCOMEFREQUENCYINDICATOR = param;
    }

    public boolean isDEFAULTSTATUSDATESpecified() {
        return localDEFAULTSTATUSDATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDEFAULTSTATUSDATE() {
        return localDEFAULTSTATUSDATE;
    }

    /**
     * Auto generated setter method
     * @param param DEFAULTSTATUSDATE
     */
    public void setDEFAULTSTATUSDATE(java.lang.String param) {
        localDEFAULTSTATUSDATETracker = param != null;

        this.localDEFAULTSTATUSDATE = param;
    }

    public boolean isLITIGATIONSTATUSDATESpecified() {
        return localLITIGATIONSTATUSDATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLITIGATIONSTATUSDATE() {
        return localLITIGATIONSTATUSDATE;
    }

    /**
     * Auto generated setter method
     * @param param LITIGATIONSTATUSDATE
     */
    public void setLITIGATIONSTATUSDATE(java.lang.String param) {
        localLITIGATIONSTATUSDATETracker = param != null;

        this.localLITIGATIONSTATUSDATE = param;
    }

    public boolean isWRITEOFFSTATUSDATESpecified() {
        return localWRITEOFFSTATUSDATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITEOFFSTATUSDATE() {
        return localWRITEOFFSTATUSDATE;
    }

    /**
     * Auto generated setter method
     * @param param WRITEOFFSTATUSDATE
     */
    public void setWRITEOFFSTATUSDATE(java.lang.String param) {
        localWRITEOFFSTATUSDATETracker = param != null;

        this.localWRITEOFFSTATUSDATE = param;
    }

    public boolean isCURRENCYCODESpecified() {
        return localCURRENCYCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENCYCODE() {
        return localCURRENCYCODE;
    }

    /**
     * Auto generated setter method
     * @param param CURRENCYCODE
     */
    public void setCURRENCYCODE(java.lang.String param) {
        localCURRENCYCODETracker = param != null;

        this.localCURRENCYCODE = param;
    }

    public boolean isSUBSCRIBERCOMMENTSSpecified() {
        return localSUBSCRIBERCOMMENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUBSCRIBERCOMMENTS() {
        return localSUBSCRIBERCOMMENTS;
    }

    /**
     * Auto generated setter method
     * @param param SUBSCRIBERCOMMENTS
     */
    public void setSUBSCRIBERCOMMENTS(java.lang.String param) {
        localSUBSCRIBERCOMMENTSTracker = param != null;

        this.localSUBSCRIBERCOMMENTS = param;
    }

    public boolean isCONSUMERCOMMENTSSpecified() {
        return localCONSUMERCOMMENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONSUMERCOMMENTS() {
        return localCONSUMERCOMMENTS;
    }

    /**
     * Auto generated setter method
     * @param param CONSUMERCOMMENTS
     */
    public void setCONSUMERCOMMENTS(java.lang.String param) {
        localCONSUMERCOMMENTSTracker = param != null;

        this.localCONSUMERCOMMENTS = param;
    }

    public boolean isACCOUNTHOLDERTYPECODESpecified() {
        return localACCOUNTHOLDERTYPECODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTHOLDERTYPECODE() {
        return localACCOUNTHOLDERTYPECODE;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTHOLDERTYPECODE
     */
    public void setACCOUNTHOLDERTYPECODE(java.lang.String param) {
        localACCOUNTHOLDERTYPECODETracker = param != null;

        this.localACCOUNTHOLDERTYPECODE = param;
    }

    public boolean isACCOUNTHOLDER_TYPENAMESpecified() {
        return localACCOUNTHOLDER_TYPENAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTHOLDER_TYPENAME() {
        return localACCOUNTHOLDER_TYPENAME;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTHOLDER_TYPENAME
     */
    public void setACCOUNTHOLDER_TYPENAME(java.lang.String param) {
        localACCOUNTHOLDER_TYPENAMETracker = param != null;

        this.localACCOUNTHOLDER_TYPENAME = param;
    }

    public boolean isYEAR_HISTSpecified() {
        return localYEAR_HISTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getYEAR_HIST() {
        return localYEAR_HIST;
    }

    /**
     * Auto generated setter method
     * @param param YEAR_HIST
     */
    public void setYEAR_HIST(java.lang.String param) {
        localYEAR_HISTTracker = param != null;

        this.localYEAR_HIST = param;
    }

    public boolean isMONTH_HISTSpecified() {
        return localMONTH_HISTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMONTH_HIST() {
        return localMONTH_HIST;
    }

    /**
     * Auto generated setter method
     * @param param MONTH_HIST
     */
    public void setMONTH_HIST(java.lang.String param) {
        localMONTH_HISTTracker = param != null;

        this.localMONTH_HIST = param;
    }

    public boolean isDAYSPASTDUESpecified() {
        return localDAYSPASTDUETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDAYSPASTDUE() {
        return localDAYSPASTDUE;
    }

    /**
     * Auto generated setter method
     * @param param DAYSPASTDUE
     */
    public void setDAYSPASTDUE(java.lang.String param) {
        localDAYSPASTDUETracker = param != null;

        this.localDAYSPASTDUE = param;
    }

    public boolean isASSETCLASSIFICATIONSpecified() {
        return localASSETCLASSIFICATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getASSETCLASSIFICATION() {
        return localASSETCLASSIFICATION;
    }

    /**
     * Auto generated setter method
     * @param param ASSETCLASSIFICATION
     */
    public void setASSETCLASSIFICATION(java.lang.String param) {
        localASSETCLASSIFICATIONTracker = param != null;

        this.localASSETCLASSIFICATION = param;
    }

    public boolean isYEAR_ADVHISTSpecified() {
        return localYEAR_ADVHISTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getYEAR_ADVHIST() {
        return localYEAR_ADVHIST;
    }

    /**
     * Auto generated setter method
     * @param param YEAR_ADVHIST
     */
    public void setYEAR_ADVHIST(java.lang.String param) {
        localYEAR_ADVHISTTracker = param != null;

        this.localYEAR_ADVHIST = param;
    }

    public boolean isMONTH_ADVHISTSpecified() {
        return localMONTH_ADVHISTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMONTH_ADVHIST() {
        return localMONTH_ADVHIST;
    }

    /**
     * Auto generated setter method
     * @param param MONTH_ADVHIST
     */
    public void setMONTH_ADVHIST(java.lang.String param) {
        localMONTH_ADVHISTTracker = param != null;

        this.localMONTH_ADVHIST = param;
    }

    public boolean isCASHLIMITSpecified() {
        return localCASHLIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCASHLIMIT() {
        return localCASHLIMIT;
    }

    /**
     * Auto generated setter method
     * @param param CASHLIMIT
     */
    public void setCASHLIMIT(java.lang.String param) {
        localCASHLIMITTracker = param != null;

        this.localCASHLIMIT = param;
    }

    public boolean isCREDITLIMITAMTSpecified() {
        return localCREDITLIMITAMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITLIMITAMT() {
        return localCREDITLIMITAMT;
    }

    /**
     * Auto generated setter method
     * @param param CREDITLIMITAMT
     */
    public void setCREDITLIMITAMT(java.lang.String param) {
        localCREDITLIMITAMTTracker = param != null;

        this.localCREDITLIMITAMT = param;
    }

    public boolean isACTUALPAYMENTAMTSpecified() {
        return localACTUALPAYMENTAMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACTUALPAYMENTAMT() {
        return localACTUALPAYMENTAMT;
    }

    /**
     * Auto generated setter method
     * @param param ACTUALPAYMENTAMT
     */
    public void setACTUALPAYMENTAMT(java.lang.String param) {
        localACTUALPAYMENTAMTTracker = param != null;

        this.localACTUALPAYMENTAMT = param;
    }

    public boolean isEMIAMTSpecified() {
        return localEMIAMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMIAMT() {
        return localEMIAMT;
    }

    /**
     * Auto generated setter method
     * @param param EMIAMT
     */
    public void setEMIAMT(java.lang.String param) {
        localEMIAMTTracker = param != null;

        this.localEMIAMT = param;
    }

    public boolean isCURRENTBALANCE_ADVHISTSpecified() {
        return localCURRENTBALANCE_ADVHISTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENTBALANCE_ADVHIST() {
        return localCURRENTBALANCE_ADVHIST;
    }

    /**
     * Auto generated setter method
     * @param param CURRENTBALANCE_ADVHIST
     */
    public void setCURRENTBALANCE_ADVHIST(java.lang.String param) {
        localCURRENTBALANCE_ADVHISTTracker = param != null;

        this.localCURRENTBALANCE_ADVHIST = param;
    }

    public boolean isAMOUNTPASTDUE_ADVHISTSpecified() {
        return localAMOUNTPASTDUE_ADVHISTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNTPASTDUE_ADVHIST() {
        return localAMOUNTPASTDUE_ADVHIST;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNTPASTDUE_ADVHIST
     */
    public void setAMOUNTPASTDUE_ADVHIST(java.lang.String param) {
        localAMOUNTPASTDUE_ADVHISTTracker = param != null;

        this.localAMOUNTPASTDUE_ADVHIST = param;
    }

    public boolean isSURNAMENONNORMALIZEDSpecified() {
        return localSURNAMENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSURNAMENONNORMALIZED() {
        return localSURNAMENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param SURNAMENONNORMALIZED
     */
    public void setSURNAMENONNORMALIZED(java.lang.String param) {
        localSURNAMENONNORMALIZEDTracker = param != null;

        this.localSURNAMENONNORMALIZED = param;
    }

    public boolean isFIRSTNAMENONNORMALIZEDSpecified() {
        return localFIRSTNAMENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFIRSTNAMENONNORMALIZED() {
        return localFIRSTNAMENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param FIRSTNAMENONNORMALIZED
     */
    public void setFIRSTNAMENONNORMALIZED(java.lang.String param) {
        localFIRSTNAMENONNORMALIZEDTracker = param != null;

        this.localFIRSTNAMENONNORMALIZED = param;
    }

    public boolean isMIDDLENAME1NONNORMALIZEDSpecified() {
        return localMIDDLENAME1NONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDDLENAME1NONNORMALIZED() {
        return localMIDDLENAME1NONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param MIDDLENAME1NONNORMALIZED
     */
    public void setMIDDLENAME1NONNORMALIZED(java.lang.String param) {
        localMIDDLENAME1NONNORMALIZEDTracker = param != null;

        this.localMIDDLENAME1NONNORMALIZED = param;
    }

    public boolean isMIDDLENAME2NONNORMALIZEDSpecified() {
        return localMIDDLENAME2NONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDDLENAME2NONNORMALIZED() {
        return localMIDDLENAME2NONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param MIDDLENAME2NONNORMALIZED
     */
    public void setMIDDLENAME2NONNORMALIZED(java.lang.String param) {
        localMIDDLENAME2NONNORMALIZEDTracker = param != null;

        this.localMIDDLENAME2NONNORMALIZED = param;
    }

    public boolean isMIDDLENAME3NONNORMALIZEDSpecified() {
        return localMIDDLENAME3NONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDDLENAME3NONNORMALIZED() {
        return localMIDDLENAME3NONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param MIDDLENAME3NONNORMALIZED
     */
    public void setMIDDLENAME3NONNORMALIZED(java.lang.String param) {
        localMIDDLENAME3NONNORMALIZEDTracker = param != null;

        this.localMIDDLENAME3NONNORMALIZED = param;
    }

    public boolean isALIASSpecified() {
        return localALIASTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALIAS() {
        return localALIAS;
    }

    /**
     * Auto generated setter method
     * @param param ALIAS
     */
    public void setALIAS(java.lang.String param) {
        localALIASTracker = param != null;

        this.localALIAS = param;
    }

    public boolean isCAISGENDERCODESpecified() {
        return localCAISGENDERCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAISGENDERCODE() {
        return localCAISGENDERCODE;
    }

    /**
     * Auto generated setter method
     * @param param CAISGENDERCODE
     */
    public void setCAISGENDERCODE(java.lang.String param) {
        localCAISGENDERCODETracker = param != null;

        this.localCAISGENDERCODE = param;
    }

    public boolean isCAISGENDERSpecified() {
        return localCAISGENDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAISGENDER() {
        return localCAISGENDER;
    }

    /**
     * Auto generated setter method
     * @param param CAISGENDER
     */
    public void setCAISGENDER(java.lang.String param) {
        localCAISGENDERTracker = param != null;

        this.localCAISGENDER = param;
    }

    public boolean isCAISINCOMETAXPANSpecified() {
        return localCAISINCOMETAXPANTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAISINCOMETAXPAN() {
        return localCAISINCOMETAXPAN;
    }

    /**
     * Auto generated setter method
     * @param param CAISINCOMETAXPAN
     */
    public void setCAISINCOMETAXPAN(java.lang.String param) {
        localCAISINCOMETAXPANTracker = param != null;

        this.localCAISINCOMETAXPAN = param;
    }

    public boolean isCAISPASSPORTNUMBERSpecified() {
        return localCAISPASSPORTNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAISPASSPORTNUMBER() {
        return localCAISPASSPORTNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param CAISPASSPORTNUMBER
     */
    public void setCAISPASSPORTNUMBER(java.lang.String param) {
        localCAISPASSPORTNUMBERTracker = param != null;

        this.localCAISPASSPORTNUMBER = param;
    }

    public boolean isVOTERIDNUMBERSpecified() {
        return localVOTERIDNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTERIDNUMBER() {
        return localVOTERIDNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param VOTERIDNUMBER
     */
    public void setVOTERIDNUMBER(java.lang.String param) {
        localVOTERIDNUMBERTracker = param != null;

        this.localVOTERIDNUMBER = param;
    }

    public boolean isDATEOFBIRTHSpecified() {
        return localDATEOFBIRTHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOFBIRTH() {
        return localDATEOFBIRTH;
    }

    /**
     * Auto generated setter method
     * @param param DATEOFBIRTH
     */
    public void setDATEOFBIRTH(java.lang.String param) {
        localDATEOFBIRTHTracker = param != null;

        this.localDATEOFBIRTH = param;
    }

    public boolean isFIRSTLINEOFADDNONNORMALIZEDSpecified() {
        return localFIRSTLINEOFADDNONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFIRSTLINEOFADDNONNORMALIZED() {
        return localFIRSTLINEOFADDNONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param FIRSTLINEOFADDNONNORMALIZED
     */
    public void setFIRSTLINEOFADDNONNORMALIZED(java.lang.String param) {
        localFIRSTLINEOFADDNONNORMALIZEDTracker = param != null;

        this.localFIRSTLINEOFADDNONNORMALIZED = param;
    }

    public boolean isSECONDLINEOFADDNONNORMALIZEDSpecified() {
        return localSECONDLINEOFADDNONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECONDLINEOFADDNONNORMALIZED() {
        return localSECONDLINEOFADDNONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param SECONDLINEOFADDNONNORMALIZED
     */
    public void setSECONDLINEOFADDNONNORMALIZED(java.lang.String param) {
        localSECONDLINEOFADDNONNORMALIZEDTracker = param != null;

        this.localSECONDLINEOFADDNONNORMALIZED = param;
    }

    public boolean isTHIRDLINEOFADDNONNORMALIZEDSpecified() {
        return localTHIRDLINEOFADDNONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTHIRDLINEOFADDNONNORMALIZED() {
        return localTHIRDLINEOFADDNONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param THIRDLINEOFADDNONNORMALIZED
     */
    public void setTHIRDLINEOFADDNONNORMALIZED(java.lang.String param) {
        localTHIRDLINEOFADDNONNORMALIZEDTracker = param != null;

        this.localTHIRDLINEOFADDNONNORMALIZED = param;
    }

    public boolean isCITYNONNORMALIZEDSpecified() {
        return localCITYNONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCITYNONNORMALIZED() {
        return localCITYNONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param CITYNONNORMALIZED
     */
    public void setCITYNONNORMALIZED(java.lang.String param) {
        localCITYNONNORMALIZEDTracker = param != null;

        this.localCITYNONNORMALIZED = param;
    }

    public boolean isFIFTHLINEOFADDNONNORMALIZEDSpecified() {
        return localFIFTHLINEOFADDNONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFIFTHLINEOFADDNONNORMALIZED() {
        return localFIFTHLINEOFADDNONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param FIFTHLINEOFADDNONNORMALIZED
     */
    public void setFIFTHLINEOFADDNONNORMALIZED(java.lang.String param) {
        localFIFTHLINEOFADDNONNORMALIZEDTracker = param != null;

        this.localFIFTHLINEOFADDNONNORMALIZED = param;
    }

    public boolean isSTATECODENONNORMALIZEDSpecified() {
        return localSTATECODENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATECODENONNORMALIZED() {
        return localSTATECODENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param STATECODENONNORMALIZED
     */
    public void setSTATECODENONNORMALIZED(java.lang.String param) {
        localSTATECODENONNORMALIZEDTracker = param != null;

        this.localSTATECODENONNORMALIZED = param;
    }

    public boolean isSTATENONNORMALIZEDSpecified() {
        return localSTATENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATENONNORMALIZED() {
        return localSTATENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param STATENONNORMALIZED
     */
    public void setSTATENONNORMALIZED(java.lang.String param) {
        localSTATENONNORMALIZEDTracker = param != null;

        this.localSTATENONNORMALIZED = param;
    }

    public boolean isZIPPOSTALCODENONNORMALIZEDSpecified() {
        return localZIPPOSTALCODENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getZIPPOSTALCODENONNORMALIZED() {
        return localZIPPOSTALCODENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param ZIPPOSTALCODENONNORMALIZED
     */
    public void setZIPPOSTALCODENONNORMALIZED(java.lang.String param) {
        localZIPPOSTALCODENONNORMALIZEDTracker = param != null;

        this.localZIPPOSTALCODENONNORMALIZED = param;
    }

    public boolean isCOUNTRYCODENONNORMALIZEDSpecified() {
        return localCOUNTRYCODENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOUNTRYCODENONNORMALIZED() {
        return localCOUNTRYCODENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param COUNTRYCODENONNORMALIZED
     */
    public void setCOUNTRYCODENONNORMALIZED(java.lang.String param) {
        localCOUNTRYCODENONNORMALIZEDTracker = param != null;

        this.localCOUNTRYCODENONNORMALIZED = param;
    }

    public boolean isADDINDICATORNONNORMALIZEDSpecified() {
        return localADDINDICATORNONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDINDICATORNONNORMALIZED() {
        return localADDINDICATORNONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param ADDINDICATORNONNORMALIZED
     */
    public void setADDINDICATORNONNORMALIZED(java.lang.String param) {
        localADDINDICATORNONNORMALIZEDTracker = param != null;

        this.localADDINDICATORNONNORMALIZED = param;
    }

    public boolean isRESIDENCECODENONNORMALIZEDSpecified() {
        return localRESIDENCECODENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESIDENCECODENONNORMALIZED() {
        return localRESIDENCECODENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param RESIDENCECODENONNORMALIZED
     */
    public void setRESIDENCECODENONNORMALIZED(java.lang.String param) {
        localRESIDENCECODENONNORMALIZEDTracker = param != null;

        this.localRESIDENCECODENONNORMALIZED = param;
    }

    public boolean isRESIDENCENONNORMALIZEDSpecified() {
        return localRESIDENCENONNORMALIZEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESIDENCENONNORMALIZED() {
        return localRESIDENCENONNORMALIZED;
    }

    /**
     * Auto generated setter method
     * @param param RESIDENCENONNORMALIZED
     */
    public void setRESIDENCENONNORMALIZED(java.lang.String param) {
        localRESIDENCENONNORMALIZEDTracker = param != null;

        this.localRESIDENCENONNORMALIZED = param;
    }

    public boolean isTELEPHONENUMBERSpecified() {
        return localTELEPHONENUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEPHONENUMBER() {
        return localTELEPHONENUMBER;
    }

    /**
     * Auto generated setter method
     * @param param TELEPHONENUMBER
     */
    public void setTELEPHONENUMBER(java.lang.String param) {
        localTELEPHONENUMBERTracker = param != null;

        this.localTELEPHONENUMBER = param;
    }

    public boolean isTELETYPE_PHONESpecified() {
        return localTELETYPE_PHONETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELETYPE_PHONE() {
        return localTELETYPE_PHONE;
    }

    /**
     * Auto generated setter method
     * @param param TELETYPE_PHONE
     */
    public void setTELETYPE_PHONE(java.lang.String param) {
        localTELETYPE_PHONETracker = param != null;

        this.localTELETYPE_PHONE = param;
    }

    public boolean isTELEEXTENSION_PHONESpecified() {
        return localTELEEXTENSION_PHONETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEEXTENSION_PHONE() {
        return localTELEEXTENSION_PHONE;
    }

    /**
     * Auto generated setter method
     * @param param TELEEXTENSION_PHONE
     */
    public void setTELEEXTENSION_PHONE(java.lang.String param) {
        localTELEEXTENSION_PHONETracker = param != null;

        this.localTELEEXTENSION_PHONE = param;
    }

    public boolean isMOBILETELEPHONENUMBERSpecified() {
        return localMOBILETELEPHONENUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOBILETELEPHONENUMBER() {
        return localMOBILETELEPHONENUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MOBILETELEPHONENUMBER
     */
    public void setMOBILETELEPHONENUMBER(java.lang.String param) {
        localMOBILETELEPHONENUMBERTracker = param != null;

        this.localMOBILETELEPHONENUMBER = param;
    }

    public boolean isFAXNUMBERSpecified() {
        return localFAXNUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFAXNUMBER() {
        return localFAXNUMBER;
    }

    /**
     * Auto generated setter method
     * @param param FAXNUMBER
     */
    public void setFAXNUMBER(java.lang.String param) {
        localFAXNUMBERTracker = param != null;

        this.localFAXNUMBER = param;
    }

    public boolean isEMAILID_PHONESpecified() {
        return localEMAILID_PHONETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAILID_PHONE() {
        return localEMAILID_PHONE;
    }

    /**
     * Auto generated setter method
     * @param param EMAILID_PHONE
     */
    public void setEMAILID_PHONE(java.lang.String param) {
        localEMAILID_PHONETracker = param != null;

        this.localEMAILID_PHONE = param;
    }

    public boolean isINCOME_TAX_PAN_IDSpecified() {
        return localINCOME_TAX_PAN_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCOME_TAX_PAN_ID() {
        return localINCOME_TAX_PAN_ID;
    }

    /**
     * Auto generated setter method
     * @param param INCOME_TAX_PAN_ID
     */
    public void setINCOME_TAX_PAN_ID(java.lang.String param) {
        localINCOME_TAX_PAN_IDTracker = param != null;

        this.localINCOME_TAX_PAN_ID = param;
    }

    public boolean isPAN_ISSUE_DATE_IDSpecified() {
        return localPAN_ISSUE_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_ISSUE_DATE_ID() {
        return localPAN_ISSUE_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param PAN_ISSUE_DATE_ID
     */
    public void setPAN_ISSUE_DATE_ID(java.lang.String param) {
        localPAN_ISSUE_DATE_IDTracker = param != null;

        this.localPAN_ISSUE_DATE_ID = param;
    }

    public boolean isPAN_EXP_DATE_IDSpecified() {
        return localPAN_EXP_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_EXP_DATE_ID() {
        return localPAN_EXP_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param PAN_EXP_DATE_ID
     */
    public void setPAN_EXP_DATE_ID(java.lang.String param) {
        localPAN_EXP_DATE_IDTracker = param != null;

        this.localPAN_EXP_DATE_ID = param;
    }

    public boolean isPASSPORT_NUMBER_IDSpecified() {
        return localPASSPORT_NUMBER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_NUMBER_ID() {
        return localPASSPORT_NUMBER_ID;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_NUMBER_ID
     */
    public void setPASSPORT_NUMBER_ID(java.lang.String param) {
        localPASSPORT_NUMBER_IDTracker = param != null;

        this.localPASSPORT_NUMBER_ID = param;
    }

    public boolean isPASSPORT_ISSUE_DATE_IDSpecified() {
        return localPASSPORT_ISSUE_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_ISSUE_DATE_ID() {
        return localPASSPORT_ISSUE_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_ISSUE_DATE_ID
     */
    public void setPASSPORT_ISSUE_DATE_ID(java.lang.String param) {
        localPASSPORT_ISSUE_DATE_IDTracker = param != null;

        this.localPASSPORT_ISSUE_DATE_ID = param;
    }

    public boolean isPASSPORT_EXP_DATE_IDSpecified() {
        return localPASSPORT_EXP_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_EXP_DATE_ID() {
        return localPASSPORT_EXP_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_EXP_DATE_ID
     */
    public void setPASSPORT_EXP_DATE_ID(java.lang.String param) {
        localPASSPORT_EXP_DATE_IDTracker = param != null;

        this.localPASSPORT_EXP_DATE_ID = param;
    }

    public boolean isVOTER_IDENTITY_CARD_IDSpecified() {
        return localVOTER_IDENTITY_CARD_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_IDENTITY_CARD_ID() {
        return localVOTER_IDENTITY_CARD_ID;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_IDENTITY_CARD_ID
     */
    public void setVOTER_IDENTITY_CARD_ID(java.lang.String param) {
        localVOTER_IDENTITY_CARD_IDTracker = param != null;

        this.localVOTER_IDENTITY_CARD_ID = param;
    }

    public boolean isVOTER_ID_ISSUE_DATE_IDSpecified() {
        return localVOTER_ID_ISSUE_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID_ISSUE_DATE_ID() {
        return localVOTER_ID_ISSUE_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID_ISSUE_DATE_ID
     */
    public void setVOTER_ID_ISSUE_DATE_ID(java.lang.String param) {
        localVOTER_ID_ISSUE_DATE_IDTracker = param != null;

        this.localVOTER_ID_ISSUE_DATE_ID = param;
    }

    public boolean isVOTER_ID_EXP_DATE_IDSpecified() {
        return localVOTER_ID_EXP_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID_EXP_DATE_ID() {
        return localVOTER_ID_EXP_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID_EXP_DATE_ID
     */
    public void setVOTER_ID_EXP_DATE_ID(java.lang.String param) {
        localVOTER_ID_EXP_DATE_IDTracker = param != null;

        this.localVOTER_ID_EXP_DATE_ID = param;
    }

    public boolean isDRIVER_LICENSE_NUMBER_IDSpecified() {
        return localDRIVER_LICENSE_NUMBER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVER_LICENSE_NUMBER_ID() {
        return localDRIVER_LICENSE_NUMBER_ID;
    }

    /**
     * Auto generated setter method
     * @param param DRIVER_LICENSE_NUMBER_ID
     */
    public void setDRIVER_LICENSE_NUMBER_ID(java.lang.String param) {
        localDRIVER_LICENSE_NUMBER_IDTracker = param != null;

        this.localDRIVER_LICENSE_NUMBER_ID = param;
    }

    public boolean isDRIVER_LICENSE_ISSUE_DATE_IDSpecified() {
        return localDRIVER_LICENSE_ISSUE_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVER_LICENSE_ISSUE_DATE_ID() {
        return localDRIVER_LICENSE_ISSUE_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param DRIVER_LICENSE_ISSUE_DATE_ID
     */
    public void setDRIVER_LICENSE_ISSUE_DATE_ID(java.lang.String param) {
        localDRIVER_LICENSE_ISSUE_DATE_IDTracker = param != null;

        this.localDRIVER_LICENSE_ISSUE_DATE_ID = param;
    }

    public boolean isDRIVER_LICENSE_EXP_DATE_IDSpecified() {
        return localDRIVER_LICENSE_EXP_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVER_LICENSE_EXP_DATE_ID() {
        return localDRIVER_LICENSE_EXP_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param DRIVER_LICENSE_EXP_DATE_ID
     */
    public void setDRIVER_LICENSE_EXP_DATE_ID(java.lang.String param) {
        localDRIVER_LICENSE_EXP_DATE_IDTracker = param != null;

        this.localDRIVER_LICENSE_EXP_DATE_ID = param;
    }

    public boolean isRATION_CARD_NUMBER_IDSpecified() {
        return localRATION_CARD_NUMBER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_NUMBER_ID() {
        return localRATION_CARD_NUMBER_ID;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_NUMBER_ID
     */
    public void setRATION_CARD_NUMBER_ID(java.lang.String param) {
        localRATION_CARD_NUMBER_IDTracker = param != null;

        this.localRATION_CARD_NUMBER_ID = param;
    }

    public boolean isRATION_CARD_ISSUE_DATE_IDSpecified() {
        return localRATION_CARD_ISSUE_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_ISSUE_DATE_ID() {
        return localRATION_CARD_ISSUE_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_ISSUE_DATE_ID
     */
    public void setRATION_CARD_ISSUE_DATE_ID(java.lang.String param) {
        localRATION_CARD_ISSUE_DATE_IDTracker = param != null;

        this.localRATION_CARD_ISSUE_DATE_ID = param;
    }

    public boolean isRATION_CARD_EXP_DATE_IDSpecified() {
        return localRATION_CARD_EXP_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_EXP_DATE_ID() {
        return localRATION_CARD_EXP_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_EXP_DATE_ID
     */
    public void setRATION_CARD_EXP_DATE_ID(java.lang.String param) {
        localRATION_CARD_EXP_DATE_IDTracker = param != null;

        this.localRATION_CARD_EXP_DATE_ID = param;
    }

    public boolean isUNIVERSAL_ID_NUMBER_IDSpecified() {
        return localUNIVERSAL_ID_NUMBER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUNIVERSAL_ID_NUMBER_ID() {
        return localUNIVERSAL_ID_NUMBER_ID;
    }

    /**
     * Auto generated setter method
     * @param param UNIVERSAL_ID_NUMBER_ID
     */
    public void setUNIVERSAL_ID_NUMBER_ID(java.lang.String param) {
        localUNIVERSAL_ID_NUMBER_IDTracker = param != null;

        this.localUNIVERSAL_ID_NUMBER_ID = param;
    }

    public boolean isUNIVERSAL_ID_ISSUE_DATE_IDSpecified() {
        return localUNIVERSAL_ID_ISSUE_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUNIVERSAL_ID_ISSUE_DATE_ID() {
        return localUNIVERSAL_ID_ISSUE_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param UNIVERSAL_ID_ISSUE_DATE_ID
     */
    public void setUNIVERSAL_ID_ISSUE_DATE_ID(java.lang.String param) {
        localUNIVERSAL_ID_ISSUE_DATE_IDTracker = param != null;

        this.localUNIVERSAL_ID_ISSUE_DATE_ID = param;
    }

    public boolean isUNIVERSAL_ID_EXP_DATE_IDSpecified() {
        return localUNIVERSAL_ID_EXP_DATE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUNIVERSAL_ID_EXP_DATE_ID() {
        return localUNIVERSAL_ID_EXP_DATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param UNIVERSAL_ID_EXP_DATE_ID
     */
    public void setUNIVERSAL_ID_EXP_DATE_ID(java.lang.String param) {
        localUNIVERSAL_ID_EXP_DATE_IDTracker = param != null;

        this.localUNIVERSAL_ID_EXP_DATE_ID = param;
    }

    public boolean isEMAILID_IDSpecified() {
        return localEMAILID_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAILID_ID() {
        return localEMAILID_ID;
    }

    /**
     * Auto generated setter method
     * @param param EMAILID_ID
     */
    public void setEMAILID_ID(java.lang.String param) {
        localEMAILID_IDTracker = param != null;

        this.localEMAILID_ID = param;
    }

    public boolean isEXACTMATCHSpecified() {
        return localEXACTMATCHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXACTMATCH() {
        return localEXACTMATCH;
    }

    /**
     * Auto generated setter method
     * @param param EXACTMATCH
     */
    public void setEXACTMATCH(java.lang.String param) {
        localEXACTMATCHTracker = param != null;

        this.localEXACTMATCH = param;
    }

    public boolean isCAPSLAST7DAYSSpecified() {
        return localCAPSLAST7DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSLAST7DAYS() {
        return localCAPSLAST7DAYS;
    }

    /**
     * Auto generated setter method
     * @param param CAPSLAST7DAYS
     */
    public void setCAPSLAST7DAYS(java.lang.String param) {
        localCAPSLAST7DAYSTracker = param != null;

        this.localCAPSLAST7DAYS = param;
    }

    public boolean isCAPSLAST30DAYSSpecified() {
        return localCAPSLAST30DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSLAST30DAYS() {
        return localCAPSLAST30DAYS;
    }

    /**
     * Auto generated setter method
     * @param param CAPSLAST30DAYS
     */
    public void setCAPSLAST30DAYS(java.lang.String param) {
        localCAPSLAST30DAYSTracker = param != null;

        this.localCAPSLAST30DAYS = param;
    }

    public boolean isCAPSLAST90DAYSSpecified() {
        return localCAPSLAST90DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSLAST90DAYS() {
        return localCAPSLAST90DAYS;
    }

    /**
     * Auto generated setter method
     * @param param CAPSLAST90DAYS
     */
    public void setCAPSLAST90DAYS(java.lang.String param) {
        localCAPSLAST90DAYSTracker = param != null;

        this.localCAPSLAST90DAYS = param;
    }

    public boolean isCAPSLAST180DAYSSpecified() {
        return localCAPSLAST180DAYSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSLAST180DAYS() {
        return localCAPSLAST180DAYS;
    }

    /**
     * Auto generated setter method
     * @param param CAPSLAST180DAYS
     */
    public void setCAPSLAST180DAYS(java.lang.String param) {
        localCAPSLAST180DAYSTracker = param != null;

        this.localCAPSLAST180DAYS = param;
    }

    public boolean isCAPSSUBSCRIBERCODESpecified() {
        return localCAPSSUBSCRIBERCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSSUBSCRIBERCODE() {
        return localCAPSSUBSCRIBERCODE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSSUBSCRIBERCODE
     */
    public void setCAPSSUBSCRIBERCODE(java.lang.String param) {
        localCAPSSUBSCRIBERCODETracker = param != null;

        this.localCAPSSUBSCRIBERCODE = param;
    }

    public boolean isCAPSSUBSCRIBERNAMESpecified() {
        return localCAPSSUBSCRIBERNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSSUBSCRIBERNAME() {
        return localCAPSSUBSCRIBERNAME;
    }

    /**
     * Auto generated setter method
     * @param param CAPSSUBSCRIBERNAME
     */
    public void setCAPSSUBSCRIBERNAME(java.lang.String param) {
        localCAPSSUBSCRIBERNAMETracker = param != null;

        this.localCAPSSUBSCRIBERNAME = param;
    }

    public boolean isCAPSDATEOFREQUESTSpecified() {
        return localCAPSDATEOFREQUESTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSDATEOFREQUEST() {
        return localCAPSDATEOFREQUEST;
    }

    /**
     * Auto generated setter method
     * @param param CAPSDATEOFREQUEST
     */
    public void setCAPSDATEOFREQUEST(java.lang.String param) {
        localCAPSDATEOFREQUESTTracker = param != null;

        this.localCAPSDATEOFREQUEST = param;
    }

    public boolean isCAPSREPORTDATESpecified() {
        return localCAPSREPORTDATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAPSREPORTDATE() {
        return localCAPSREPORTDATE;
    }

    /**
     * Auto generated setter method
     * @param param CAPSREPORTDATE
     */
    public void setCAPSREPORTDATE(java.lang.String param) {
        localCAPSREPORTDATETracker = param != null;

        this.localCAPSREPORTDATE = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ExperianSRespType1", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ExperianSRespType1", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_DATE", xmlWriter);

            if (localENQUIRY_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSYSTEMCODE_HEADERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SYSTEMCODE_HEADER", xmlWriter);

            if (localSYSTEMCODE_HEADER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SYSTEMCODE_HEADER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSYSTEMCODE_HEADER);
            }

            xmlWriter.writeEndElement();
        }

        if (localMESSAGETEXT_HEADERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MESSAGETEXT_HEADER", xmlWriter);

            if (localMESSAGETEXT_HEADER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MESSAGETEXT_HEADER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMESSAGETEXT_HEADER);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTDATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTDATE", xmlWriter);

            if (localREPORTDATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTDATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTDATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTIME_HEADERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTIME_HEADER", xmlWriter);

            if (localREPORTIME_HEADER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTIME_HEADER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTIME_HEADER);
            }

            xmlWriter.writeEndElement();
        }

        if (localUSERMESSAGETEXTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "USERMESSAGETEXT", xmlWriter);

            if (localUSERMESSAGETEXT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "USERMESSAGETEXT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUSERMESSAGETEXT);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRYUSERNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRYUSERNAME", xmlWriter);

            if (localENQUIRYUSERNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRYUSERNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRYUSERNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_PROFILE_REPORT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_PROFILE_REPORT_DATE",
                xmlWriter);

            if (localCREDIT_PROFILE_REPORT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_PROFILE_REPORT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_PROFILE_REPORT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_PROFILE_REPORT_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_PROFILE_REPORT_TIME",
                xmlWriter);

            if (localCREDIT_PROFILE_REPORT_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_PROFILE_REPORT_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_PROFILE_REPORT_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localVERSIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VERSION", xmlWriter);

            if (localVERSION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VERSION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVERSION);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTNUMBER", xmlWriter);

            if (localREPORTNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUBSCRIBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUBSCRIBER", xmlWriter);

            if (localSUBSCRIBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBSCRIBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUBSCRIBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUBSCRIBERNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUBSCRIBERNAME", xmlWriter);

            if (localSUBSCRIBERNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBSCRIBERNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUBSCRIBERNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRYREASONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRYREASON", xmlWriter);

            if (localENQUIRYREASON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRYREASON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRYREASON);
            }

            xmlWriter.writeEndElement();
        }

        if (localFINANCEPURPOSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "FINANCEPURPOSE", xmlWriter);

            if (localFINANCEPURPOSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FINANCEPURPOSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFINANCEPURPOSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNTFINANCEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNTFINANCED", xmlWriter);

            if (localAMOUNTFINANCED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNTFINANCED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNTFINANCED);
            }

            xmlWriter.writeEndElement();
        }

        if (localDURATIONOFAGREEMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DURATIONOFAGREEMENT", xmlWriter);

            if (localDURATIONOFAGREEMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DURATIONOFAGREEMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDURATIONOFAGREEMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLASTNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LASTNAME", xmlWriter);

            if (localLASTNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LASTNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLASTNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localFIRSTNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "FIRSTNAME", xmlWriter);

            if (localFIRSTNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FIRSTNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFIRSTNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDDLENAME1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDDLENAME1", xmlWriter);

            if (localMIDDLENAME1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDDLENAME1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDDLENAME1);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDDLENAME2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDDLENAME2", xmlWriter);

            if (localMIDDLENAME2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDDLENAME2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDDLENAME2);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDDLENAME3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDDLENAME3", xmlWriter);

            if (localMIDDLENAME3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDDLENAME3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDDLENAME3);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDERCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDERCODE", xmlWriter);

            if (localGENDERCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDERCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDERCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDER", xmlWriter);

            if (localGENDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCOME_TAX_PAN_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INCOME_TAX_PAN_APP", xmlWriter);

            if (localINCOME_TAX_PAN_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCOME_TAX_PAN_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCOME_TAX_PAN_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_ISSUE_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_ISSUE_DATE_APP", xmlWriter);

            if (localPAN_ISSUE_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_ISSUE_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_ISSUE_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_EXP_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_EXP_DATE_APP", xmlWriter);

            if (localPAN_EXP_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_EXP_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_EXP_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_NUMBER_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_NUMBER_APP", xmlWriter);

            if (localPASSPORT_NUMBER_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_NUMBER_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_NUMBER_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_ISSUE_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_ISSUE_DATE_APP",
                xmlWriter);

            if (localPASSPORT_ISSUE_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_ISSUE_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_ISSUE_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_EXP_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_EXP_DATE_APP",
                xmlWriter);

            if (localPASSPORT_EXP_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_EXP_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_EXP_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_IDENTITY_CARD_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_IDENTITY_CARD_APP",
                xmlWriter);

            if (localVOTER_IDENTITY_CARD_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_IDENTITY_CARD_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_IDENTITY_CARD_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_ID_ISSUE_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID_ISSUE_DATE_APP",
                xmlWriter);

            if (localVOTER_ID_ISSUE_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID_ISSUE_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID_ISSUE_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_ID_EXP_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID_EXP_DATE_APP",
                xmlWriter);

            if (localVOTER_ID_EXP_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID_EXP_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID_EXP_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVER_LICENSE_NUMBER_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVER_LICENSE_NUMBER_APP",
                xmlWriter);

            if (localDRIVER_LICENSE_NUMBER_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVER_LICENSE_NUMBER_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVER_LICENSE_NUMBER_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVER_LICENSE_ISSUE_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVER_LICENSE_ISSUE_DATE_APP",
                xmlWriter);

            if (localDRIVER_LICENSE_ISSUE_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVER_LICENSE_ISSUE_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVER_LICENSE_ISSUE_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVER_LICENSE_EXP_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVER_LICENSE_EXP_DATE_APP",
                xmlWriter);

            if (localDRIVER_LICENSE_EXP_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVER_LICENSE_EXP_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVER_LICENSE_EXP_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_NUMBER_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_NUMBER_APP",
                xmlWriter);

            if (localRATION_CARD_NUMBER_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_NUMBER_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_NUMBER_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_ISSUE_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_ISSUE_DATE_APP",
                xmlWriter);

            if (localRATION_CARD_ISSUE_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_ISSUE_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_ISSUE_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_EXP_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_EXP_DATE_APP",
                xmlWriter);

            if (localRATION_CARD_EXP_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_EXP_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_EXP_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localUNIVERSAL_ID_NUMBER_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "UNIVERSAL_ID_NUMBER_APP",
                xmlWriter);

            if (localUNIVERSAL_ID_NUMBER_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UNIVERSAL_ID_NUMBER_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUNIVERSAL_ID_NUMBER_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localUNIVERSAL_ID_ISSUE_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "UNIVERSAL_ID_ISSUE_DATE_APP",
                xmlWriter);

            if (localUNIVERSAL_ID_ISSUE_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UNIVERSAL_ID_ISSUE_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUNIVERSAL_ID_ISSUE_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localUNIVERSAL_ID_EXP_DATE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "UNIVERSAL_ID_EXP_DATE_APP",
                xmlWriter);

            if (localUNIVERSAL_ID_EXP_DATE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UNIVERSAL_ID_EXP_DATE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUNIVERSAL_ID_EXP_DATE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOFBIRTHAPPLICANTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOFBIRTHAPPLICANT", xmlWriter);

            if (localDATEOFBIRTHAPPLICANT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOFBIRTHAPPLICANT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOFBIRTHAPPLICANT);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEPHONENUMBERAPPLICANT1STTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEPHONENUMBERAPPLICANT1ST",
                xmlWriter);

            if (localTELEPHONENUMBERAPPLICANT1ST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEPHONENUMBERAPPLICANT1ST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEPHONENUMBERAPPLICANT1ST);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEEXTENSION_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEEXTENSION_APP", xmlWriter);

            if (localTELEEXTENSION_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEEXTENSION_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEEXTENSION_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELETYPE_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELETYPE_APP", xmlWriter);

            if (localTELETYPE_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELETYPE_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELETYPE_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOBILEPHONENUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MOBILEPHONENUMBER", xmlWriter);

            if (localMOBILEPHONENUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOBILEPHONENUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOBILEPHONENUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAILID_APPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAILID_APP", xmlWriter);

            if (localEMAILID_APP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAILID_APP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAILID_APP);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCOMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "INCOME", xmlWriter);

            if (localINCOME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCOME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCOME);
            }

            xmlWriter.writeEndElement();
        }

        if (localMARITALSTATUSCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "MARITALSTATUSCODE", xmlWriter);

            if (localMARITALSTATUSCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MARITALSTATUSCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMARITALSTATUSCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localMARITALSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MARITALSTATUS", xmlWriter);

            if (localMARITALSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MARITALSTATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMARITALSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMPLOYMENTSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMPLOYMENTSTATUS", xmlWriter);

            if (localEMPLOYMENTSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMPLOYMENTSTATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMPLOYMENTSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEWITHEMPLOYERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEWITHEMPLOYER", xmlWriter);

            if (localDATEWITHEMPLOYER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEWITHEMPLOYER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEWITHEMPLOYER);
            }

            xmlWriter.writeEndElement();
        }

        if (localNUMBEROFMAJORCREDITCARDHELDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NUMBEROFMAJORCREDITCARDHELD",
                xmlWriter);

            if (localNUMBEROFMAJORCREDITCARDHELD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NUMBEROFMAJORCREDITCARDHELD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNUMBEROFMAJORCREDITCARDHELD);
            }

            xmlWriter.writeEndElement();
        }

        if (localFLATNOPLOTNOHOUSENOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FLATNOPLOTNOHOUSENO", xmlWriter);

            if (localFLATNOPLOTNOHOUSENO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FLATNOPLOTNOHOUSENO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFLATNOPLOTNOHOUSENO);
            }

            xmlWriter.writeEndElement();
        }

        if (localBLDGNOSOCIETYNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "BLDGNOSOCIETYNAME", xmlWriter);

            if (localBLDGNOSOCIETYNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BLDGNOSOCIETYNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBLDGNOSOCIETYNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localROADNONAMEAREALOCALITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ROADNONAMEAREALOCALITY",
                xmlWriter);

            if (localROADNONAMEAREALOCALITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ROADNONAMEAREALOCALITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localROADNONAMEAREALOCALITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localCITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CITY", xmlWriter);

            if (localCITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localLANDMARKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LANDMARK", xmlWriter);

            if (localLANDMARK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LANDMARK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLANDMARK);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATE", xmlWriter);

            if (localSTATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPINCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PINCODE", xmlWriter);

            if (localPINCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PINCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPINCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOUNTRY_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "COUNTRY_CODE", xmlWriter);

            if (localCOUNTRY_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COUNTRY_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOUNTRY_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_FLATNOPLOTNOHOUSENOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_FLATNOPLOTNOHOUSENO",
                xmlWriter);

            if (localADD_FLATNOPLOTNOHOUSENO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_FLATNOPLOTNOHOUSENO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_FLATNOPLOTNOHOUSENO);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_BLDGNOSOCIETYNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_BLDGNOSOCIETYNAME",
                xmlWriter);

            if (localADD_BLDGNOSOCIETYNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_BLDGNOSOCIETYNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_BLDGNOSOCIETYNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_ROADNONAMEAREALOCALITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_ROADNONAMEAREALOCALITY",
                xmlWriter);

            if (localADD_ROADNONAMEAREALOCALITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_ROADNONAMEAREALOCALITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_ROADNONAMEAREALOCALITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_CITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_CITY", xmlWriter);

            if (localADD_CITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_CITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_CITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_LANDMARKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_LANDMARK", xmlWriter);

            if (localADD_LANDMARK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_LANDMARK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_LANDMARK);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_STATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_STATE", xmlWriter);

            if (localADD_STATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_STATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_STATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_PINCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_PINCODE", xmlWriter);

            if (localADD_PINCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_PINCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_PINCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localADD_COUNTRYCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADD_COUNTRYCODE", xmlWriter);

            if (localADD_COUNTRYCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_COUNTRYCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_COUNTRYCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITACCOUNTTOTALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITACCOUNTTOTAL", xmlWriter);

            if (localCREDITACCOUNTTOTAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITACCOUNTTOTAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITACCOUNTTOTAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITACCOUNTACTIVETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITACCOUNTACTIVE", xmlWriter);

            if (localCREDITACCOUNTACTIVE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITACCOUNTACTIVE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITACCOUNTACTIVE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITACCOUNTDEFAULTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITACCOUNTDEFAULT", xmlWriter);

            if (localCREDITACCOUNTDEFAULT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITACCOUNTDEFAULT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITACCOUNTDEFAULT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITACCOUNTCLOSEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITACCOUNTCLOSED", xmlWriter);

            if (localCREDITACCOUNTCLOSED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITACCOUNTCLOSED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITACCOUNTCLOSED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCADSUITFILEDCURRENTBALANCETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CADSUITFILEDCURRENTBALANCE",
                xmlWriter);

            if (localCADSUITFILEDCURRENTBALANCE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CADSUITFILEDCURRENTBALANCE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCADSUITFILEDCURRENTBALANCE);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTSTANDINGBALANCESECUREDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTSTANDINGBALANCESECURED",
                xmlWriter);

            if (localOUTSTANDINGBALANCESECURED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTSTANDINGBALANCESECURED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTSTANDINGBALANCESECURED);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTSTANDINGBALSECUREDPERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTSTANDINGBALSECUREDPER",
                xmlWriter);

            if (localOUTSTANDINGBALSECUREDPER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTSTANDINGBALSECUREDPER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTSTANDINGBALSECUREDPER);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTSTANDINGBALANCEUNSECUREDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTSTANDINGBALANCEUNSECURED",
                xmlWriter);

            if (localOUTSTANDINGBALANCEUNSECURED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTSTANDINGBALANCEUNSECURED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTSTANDINGBALANCEUNSECURED);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTSTANDINGBALUNSECPERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTSTANDINGBALUNSECPER",
                xmlWriter);

            if (localOUTSTANDINGBALUNSECPER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTSTANDINGBALUNSECPER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTSTANDINGBALUNSECPER);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTSTANDINGBALANCEALLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTSTANDINGBALANCEALL",
                xmlWriter);

            if (localOUTSTANDINGBALANCEALL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTSTANDINGBALANCEALL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTSTANDINGBALANCEALL);
            }

            xmlWriter.writeEndElement();
        }

        if (localIDENTIFICATIONNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IDENTIFICATIONNUMBER", xmlWriter);

            if (localIDENTIFICATIONNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IDENTIFICATIONNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIDENTIFICATIONNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAISSUBSCRIBERNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAISSUBSCRIBERNAME", xmlWriter);

            if (localCAISSUBSCRIBERNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAISSUBSCRIBERNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAISSUBSCRIBERNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTNUMBER", xmlWriter);

            if (localACCOUNTNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localPORTFOLIOTYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PORTFOLIOTYPE", xmlWriter);

            if (localPORTFOLIOTYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PORTFOLIOTYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPORTFOLIOTYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTTYPE_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTTYPE_CODE", xmlWriter);

            if (localACCOUNTTYPE_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTTYPE_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTTYPE_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTTYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTTYPE", xmlWriter);

            if (localACCOUNTTYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTTYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTTYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPENDATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OPENDATE", xmlWriter);

            if (localOPENDATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OPENDATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOPENDATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localHIGHESTCREDITORORGNLOANAMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "HIGHESTCREDITORORGNLOANAMT",
                xmlWriter);

            if (localHIGHESTCREDITORORGNLOANAMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HIGHESTCREDITORORGNLOANAMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHIGHESTCREDITORORGNLOANAMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localTERMSDURATIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TERMSDURATION", xmlWriter);

            if (localTERMSDURATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TERMSDURATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTERMSDURATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localTERMSFREQUENCYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TERMSFREQUENCY", xmlWriter);

            if (localTERMSFREQUENCY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TERMSFREQUENCY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTERMSFREQUENCY);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCHEDULEDMONTHLYPAYAMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCHEDULEDMONTHLYPAYAMT",
                xmlWriter);

            if (localSCHEDULEDMONTHLYPAYAMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCHEDULEDMONTHLYPAYAMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCHEDULEDMONTHLYPAYAMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTSTATUS_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTSTATUS_CODE", xmlWriter);

            if (localACCOUNTSTATUS_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTSTATUS_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTSTATUS_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTSTATUS", xmlWriter);

            if (localACCOUNTSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTSTATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENTRATINGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENTRATING", xmlWriter);

            if (localPAYMENTRATING == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENTRATING cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENTRATING);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENTHISTORYPROFILETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENTHISTORYPROFILE",
                xmlWriter);

            if (localPAYMENTHISTORYPROFILE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENTHISTORYPROFILE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENTHISTORYPROFILE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSPECIALCOMMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SPECIALCOMMENT", xmlWriter);

            if (localSPECIALCOMMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SPECIALCOMMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSPECIALCOMMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENTBALANCE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENTBALANCE_TL", xmlWriter);

            if (localCURRENTBALANCE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENTBALANCE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENTBALANCE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNTPASTDUE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNTPASTDUE_TL", xmlWriter);

            if (localAMOUNTPASTDUE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNTPASTDUE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNTPASTDUE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localORIGINALCHARGEOFFAMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ORIGINALCHARGEOFFAMOUNT",
                xmlWriter);

            if (localORIGINALCHARGEOFFAMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ORIGINALCHARGEOFFAMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localORIGINALCHARGEOFFAMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEREPORTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEREPORTED", xmlWriter);

            if (localDATEREPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEREPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEREPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOFFIRSTDELINQUENCYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOFFIRSTDELINQUENCY",
                xmlWriter);

            if (localDATEOFFIRSTDELINQUENCY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOFFIRSTDELINQUENCY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOFFIRSTDELINQUENCY);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATECLOSEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATECLOSED", xmlWriter);

            if (localDATECLOSED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATECLOSED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATECLOSED);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOFLASTPAYMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOFLASTPAYMENT", xmlWriter);

            if (localDATEOFLASTPAYMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOFLASTPAYMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOFLASTPAYMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUITFWILFULDFTWRITTENOFFSTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUITFWILFULDFTWRITTENOFFSTS",
                xmlWriter);

            if (localSUITFWILFULDFTWRITTENOFFSTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUITFWILFULDFTWRITTENOFFSTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUITFWILFULDFTWRITTENOFFSTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUITFILEDWILFULDEFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUITFILEDWILFULDEF ", xmlWriter);

            if (localSUITFILEDWILFULDEF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUITFILEDWILFULDEF  cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUITFILEDWILFULDEF);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITTENOFFSETTLEDSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITTENOFFSETTLEDSTATUS",
                xmlWriter);

            if (localWRITTENOFFSETTLEDSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITTENOFFSETTLEDSTATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITTENOFFSETTLEDSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localVALUEOFCREDITSLASTMONTHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VALUEOFCREDITSLASTMONTH",
                xmlWriter);

            if (localVALUEOFCREDITSLASTMONTH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VALUEOFCREDITSLASTMONTH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVALUEOFCREDITSLASTMONTH);
            }

            xmlWriter.writeEndElement();
        }

        if (localOCCUPATIONCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OCCUPATIONCODE", xmlWriter);

            if (localOCCUPATIONCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OCCUPATIONCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOCCUPATIONCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSATTLEMENTAMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SATTLEMENTAMOUNT", xmlWriter);

            if (localSATTLEMENTAMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SATTLEMENTAMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSATTLEMENTAMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localVALUEOFCOLATERALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VALUEOFCOLATERAL", xmlWriter);

            if (localVALUEOFCOLATERAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VALUEOFCOLATERAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVALUEOFCOLATERAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTYPEOFCOLATERALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TYPEOFCOLATERAL", xmlWriter);

            if (localTYPEOFCOLATERAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TYPEOFCOLATERAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTYPEOFCOLATERAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITTENOFFAMTTOTALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITTENOFFAMTTOTAL", xmlWriter);

            if (localWRITTENOFFAMTTOTAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITTENOFFAMTTOTAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITTENOFFAMTTOTAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITTENOFFAMTPRINCIPALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITTENOFFAMTPRINCIPAL",
                xmlWriter);

            if (localWRITTENOFFAMTPRINCIPAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITTENOFFAMTPRINCIPAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITTENOFFAMTPRINCIPAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATEOFINTERESTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATEOFINTEREST", xmlWriter);

            if (localRATEOFINTEREST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATEOFINTEREST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATEOFINTEREST);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPAYMENTTENURETracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPAYMENTTENURE", xmlWriter);

            if (localREPAYMENTTENURE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPAYMENTTENURE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPAYMENTTENURE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPROMOTIONALRATEFLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PROMOTIONALRATEFLAG", xmlWriter);

            if (localPROMOTIONALRATEFLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PROMOTIONALRATEFLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPROMOTIONALRATEFLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAISINCOMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAISINCOME", xmlWriter);

            if (localCAISINCOME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAISINCOME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAISINCOME);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCOMEINDICATORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INCOMEINDICATOR", xmlWriter);

            if (localINCOMEINDICATOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCOMEINDICATOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCOMEINDICATOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCOMEFREQUENCYINDICATORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INCOMEFREQUENCYINDICATOR",
                xmlWriter);

            if (localINCOMEFREQUENCYINDICATOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCOMEFREQUENCYINDICATOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCOMEFREQUENCYINDICATOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localDEFAULTSTATUSDATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DEFAULTSTATUSDATE", xmlWriter);

            if (localDEFAULTSTATUSDATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DEFAULTSTATUSDATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDEFAULTSTATUSDATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLITIGATIONSTATUSDATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LITIGATIONSTATUSDATE", xmlWriter);

            if (localLITIGATIONSTATUSDATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LITIGATIONSTATUSDATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLITIGATIONSTATUSDATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITEOFFSTATUSDATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITEOFFSTATUSDATE", xmlWriter);

            if (localWRITEOFFSTATUSDATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITEOFFSTATUSDATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITEOFFSTATUSDATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENCYCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENCYCODE", xmlWriter);

            if (localCURRENCYCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENCYCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENCYCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUBSCRIBERCOMMENTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUBSCRIBERCOMMENTS", xmlWriter);

            if (localSUBSCRIBERCOMMENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBSCRIBERCOMMENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUBSCRIBERCOMMENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONSUMERCOMMENTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CONSUMERCOMMENTS", xmlWriter);

            if (localCONSUMERCOMMENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONSUMERCOMMENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONSUMERCOMMENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTHOLDERTYPECODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTHOLDERTYPECODE",
                xmlWriter);

            if (localACCOUNTHOLDERTYPECODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTHOLDERTYPECODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTHOLDERTYPECODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTHOLDER_TYPENAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTHOLDER_TYPENAME",
                xmlWriter);

            if (localACCOUNTHOLDER_TYPENAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTHOLDER_TYPENAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTHOLDER_TYPENAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localYEAR_HISTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "YEAR_HIST", xmlWriter);

            if (localYEAR_HIST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "YEAR_HIST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localYEAR_HIST);
            }

            xmlWriter.writeEndElement();
        }

        if (localMONTH_HISTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MONTH_HIST", xmlWriter);

            if (localMONTH_HIST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MONTH_HIST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMONTH_HIST);
            }

            xmlWriter.writeEndElement();
        }

        if (localDAYSPASTDUETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DAYSPASTDUE", xmlWriter);

            if (localDAYSPASTDUE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DAYSPASTDUE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDAYSPASTDUE);
            }

            xmlWriter.writeEndElement();
        }

        if (localASSETCLASSIFICATIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ASSETCLASSIFICATION", xmlWriter);

            if (localASSETCLASSIFICATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ASSETCLASSIFICATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localASSETCLASSIFICATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localYEAR_ADVHISTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "YEAR_ADVHIST", xmlWriter);

            if (localYEAR_ADVHIST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "YEAR_ADVHIST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localYEAR_ADVHIST);
            }

            xmlWriter.writeEndElement();
        }

        if (localMONTH_ADVHISTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MONTH_ADVHIST", xmlWriter);

            if (localMONTH_ADVHIST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MONTH_ADVHIST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMONTH_ADVHIST);
            }

            xmlWriter.writeEndElement();
        }

        if (localCASHLIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CASHLIMIT", xmlWriter);

            if (localCASHLIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CASHLIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCASHLIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITLIMITAMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITLIMITAMT", xmlWriter);

            if (localCREDITLIMITAMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITLIMITAMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITLIMITAMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localACTUALPAYMENTAMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACTUALPAYMENTAMT", xmlWriter);

            if (localACTUALPAYMENTAMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACTUALPAYMENTAMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACTUALPAYMENTAMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMIAMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMIAMT", xmlWriter);

            if (localEMIAMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMIAMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMIAMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENTBALANCE_ADVHISTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENTBALANCE_ADVHIST",
                xmlWriter);

            if (localCURRENTBALANCE_ADVHIST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENTBALANCE_ADVHIST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENTBALANCE_ADVHIST);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNTPASTDUE_ADVHISTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNTPASTDUE_ADVHIST",
                xmlWriter);

            if (localAMOUNTPASTDUE_ADVHIST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNTPASTDUE_ADVHIST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNTPASTDUE_ADVHIST);
            }

            xmlWriter.writeEndElement();
        }

        if (localSURNAMENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SURNAMENONNORMALIZED", xmlWriter);

            if (localSURNAMENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SURNAMENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSURNAMENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localFIRSTNAMENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FIRSTNAMENONNORMALIZED",
                xmlWriter);

            if (localFIRSTNAMENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FIRSTNAMENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFIRSTNAMENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDDLENAME1NONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDDLENAME1NONNORMALIZED",
                xmlWriter);

            if (localMIDDLENAME1NONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDDLENAME1NONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDDLENAME1NONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDDLENAME2NONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDDLENAME2NONNORMALIZED",
                xmlWriter);

            if (localMIDDLENAME2NONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDDLENAME2NONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDDLENAME2NONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDDLENAME3NONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDDLENAME3NONNORMALIZED",
                xmlWriter);

            if (localMIDDLENAME3NONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDDLENAME3NONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDDLENAME3NONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localALIASTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALIAS", xmlWriter);

            if (localALIAS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALIAS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALIAS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAISGENDERCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAISGENDERCODE", xmlWriter);

            if (localCAISGENDERCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAISGENDERCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAISGENDERCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAISGENDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAISGENDER", xmlWriter);

            if (localCAISGENDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAISGENDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAISGENDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAISINCOMETAXPANTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAISINCOMETAXPAN", xmlWriter);

            if (localCAISINCOMETAXPAN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAISINCOMETAXPAN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAISINCOMETAXPAN);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAISPASSPORTNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAISPASSPORTNUMBER", xmlWriter);

            if (localCAISPASSPORTNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAISPASSPORTNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAISPASSPORTNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTERIDNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTERIDNUMBER", xmlWriter);

            if (localVOTERIDNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTERIDNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTERIDNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOFBIRTHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOFBIRTH", xmlWriter);

            if (localDATEOFBIRTH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOFBIRTH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOFBIRTH);
            }

            xmlWriter.writeEndElement();
        }

        if (localFIRSTLINEOFADDNONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FIRSTLINEOFADDNONNORMALIZED",
                xmlWriter);

            if (localFIRSTLINEOFADDNONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FIRSTLINEOFADDNONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFIRSTLINEOFADDNONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECONDLINEOFADDNONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECONDLINEOFADDNONNORMALIZED",
                xmlWriter);

            if (localSECONDLINEOFADDNONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECONDLINEOFADDNONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECONDLINEOFADDNONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localTHIRDLINEOFADDNONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "THIRDLINEOFADDNONNORMALIZED",
                xmlWriter);

            if (localTHIRDLINEOFADDNONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "THIRDLINEOFADDNONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTHIRDLINEOFADDNONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCITYNONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CITYNONNORMALIZED", xmlWriter);

            if (localCITYNONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CITYNONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCITYNONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localFIFTHLINEOFADDNONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FIFTHLINEOFADDNONNORMALIZED",
                xmlWriter);

            if (localFIFTHLINEOFADDNONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FIFTHLINEOFADDNONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFIFTHLINEOFADDNONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATECODENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATECODENONNORMALIZED",
                xmlWriter);

            if (localSTATECODENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATECODENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATECODENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATENONNORMALIZED", xmlWriter);

            if (localSTATENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localZIPPOSTALCODENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ZIPPOSTALCODENONNORMALIZED",
                xmlWriter);

            if (localZIPPOSTALCODENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ZIPPOSTALCODENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localZIPPOSTALCODENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOUNTRYCODENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "COUNTRYCODENONNORMALIZED",
                xmlWriter);

            if (localCOUNTRYCODENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COUNTRYCODENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOUNTRYCODENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDINDICATORNONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDINDICATORNONNORMALIZED",
                xmlWriter);

            if (localADDINDICATORNONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDINDICATORNONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDINDICATORNONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESIDENCECODENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESIDENCECODENONNORMALIZED",
                xmlWriter);

            if (localRESIDENCECODENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESIDENCECODENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESIDENCECODENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESIDENCENONNORMALIZEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESIDENCENONNORMALIZED",
                xmlWriter);

            if (localRESIDENCENONNORMALIZED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESIDENCENONNORMALIZED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESIDENCENONNORMALIZED);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEPHONENUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEPHONENUMBER", xmlWriter);

            if (localTELEPHONENUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEPHONENUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEPHONENUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELETYPE_PHONETracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELETYPE_PHONE", xmlWriter);

            if (localTELETYPE_PHONE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELETYPE_PHONE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELETYPE_PHONE);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEEXTENSION_PHONETracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEEXTENSION_PHONE", xmlWriter);

            if (localTELEEXTENSION_PHONE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEEXTENSION_PHONE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEEXTENSION_PHONE);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOBILETELEPHONENUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MOBILETELEPHONENUMBER",
                xmlWriter);

            if (localMOBILETELEPHONENUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOBILETELEPHONENUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOBILETELEPHONENUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localFAXNUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FAXNUMBER", xmlWriter);

            if (localFAXNUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FAXNUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFAXNUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAILID_PHONETracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAILID_PHONE", xmlWriter);

            if (localEMAILID_PHONE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAILID_PHONE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAILID_PHONE);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCOME_TAX_PAN_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INCOME_TAX_PAN_ID", xmlWriter);

            if (localINCOME_TAX_PAN_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCOME_TAX_PAN_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCOME_TAX_PAN_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_ISSUE_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_ISSUE_DATE_ID", xmlWriter);

            if (localPAN_ISSUE_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_ISSUE_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_ISSUE_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_EXP_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_EXP_DATE_ID", xmlWriter);

            if (localPAN_EXP_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_EXP_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_EXP_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_NUMBER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_NUMBER_ID", xmlWriter);

            if (localPASSPORT_NUMBER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_NUMBER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_NUMBER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_ISSUE_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_ISSUE_DATE_ID",
                xmlWriter);

            if (localPASSPORT_ISSUE_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_ISSUE_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_ISSUE_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_EXP_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_EXP_DATE_ID", xmlWriter);

            if (localPASSPORT_EXP_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_EXP_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_EXP_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_IDENTITY_CARD_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_IDENTITY_CARD_ID",
                xmlWriter);

            if (localVOTER_IDENTITY_CARD_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_IDENTITY_CARD_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_IDENTITY_CARD_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_ID_ISSUE_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID_ISSUE_DATE_ID",
                xmlWriter);

            if (localVOTER_ID_ISSUE_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID_ISSUE_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID_ISSUE_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_ID_EXP_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID_EXP_DATE_ID", xmlWriter);

            if (localVOTER_ID_EXP_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID_EXP_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID_EXP_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVER_LICENSE_NUMBER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVER_LICENSE_NUMBER_ID",
                xmlWriter);

            if (localDRIVER_LICENSE_NUMBER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVER_LICENSE_NUMBER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVER_LICENSE_NUMBER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVER_LICENSE_ISSUE_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVER_LICENSE_ISSUE_DATE_ID",
                xmlWriter);

            if (localDRIVER_LICENSE_ISSUE_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVER_LICENSE_ISSUE_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVER_LICENSE_ISSUE_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVER_LICENSE_EXP_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVER_LICENSE_EXP_DATE_ID",
                xmlWriter);

            if (localDRIVER_LICENSE_EXP_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVER_LICENSE_EXP_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVER_LICENSE_EXP_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_NUMBER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_NUMBER_ID",
                xmlWriter);

            if (localRATION_CARD_NUMBER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_NUMBER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_NUMBER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_ISSUE_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_ISSUE_DATE_ID",
                xmlWriter);

            if (localRATION_CARD_ISSUE_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_ISSUE_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_ISSUE_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_EXP_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_EXP_DATE_ID",
                xmlWriter);

            if (localRATION_CARD_EXP_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_EXP_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_EXP_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localUNIVERSAL_ID_NUMBER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "UNIVERSAL_ID_NUMBER_ID",
                xmlWriter);

            if (localUNIVERSAL_ID_NUMBER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UNIVERSAL_ID_NUMBER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUNIVERSAL_ID_NUMBER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localUNIVERSAL_ID_ISSUE_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "UNIVERSAL_ID_ISSUE_DATE_ID",
                xmlWriter);

            if (localUNIVERSAL_ID_ISSUE_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UNIVERSAL_ID_ISSUE_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUNIVERSAL_ID_ISSUE_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localUNIVERSAL_ID_EXP_DATE_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "UNIVERSAL_ID_EXP_DATE_ID",
                xmlWriter);

            if (localUNIVERSAL_ID_EXP_DATE_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UNIVERSAL_ID_EXP_DATE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUNIVERSAL_ID_EXP_DATE_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAILID_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAILID_ID", xmlWriter);

            if (localEMAILID_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAILID_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAILID_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXACTMATCHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXACTMATCH", xmlWriter);

            if (localEXACTMATCH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXACTMATCH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXACTMATCH);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSLAST7DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSLAST7DAYS", xmlWriter);

            if (localCAPSLAST7DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSLAST7DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSLAST7DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSLAST30DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSLAST30DAYS", xmlWriter);

            if (localCAPSLAST30DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSLAST30DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSLAST30DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSLAST90DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSLAST90DAYS", xmlWriter);

            if (localCAPSLAST90DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSLAST90DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSLAST90DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSLAST180DAYSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSLAST180DAYS", xmlWriter);

            if (localCAPSLAST180DAYS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSLAST180DAYS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSLAST180DAYS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSSUBSCRIBERCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSSUBSCRIBERCODE", xmlWriter);

            if (localCAPSSUBSCRIBERCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSSUBSCRIBERCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSSUBSCRIBERCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSSUBSCRIBERNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSSUBSCRIBERNAME", xmlWriter);

            if (localCAPSSUBSCRIBERNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSSUBSCRIBERNAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSSUBSCRIBERNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSDATEOFREQUESTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSDATEOFREQUEST", xmlWriter);

            if (localCAPSDATEOFREQUEST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSDATEOFREQUEST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSDATEOFREQUEST);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAPSREPORTDATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CAPSREPORTDATE", xmlWriter);

            if (localCAPSREPORTDATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAPSREPORTDATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAPSREPORTDATE);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ExperianSRespType1 parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ExperianSRespType1 object = new ExperianSRespType1();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ExperianSRespType1".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ExperianSRespType1) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRY_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRY_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SYSTEMCODE_HEADER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SYSTEMCODE_HEADER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SYSTEMCODE_HEADER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSYSTEMCODE_HEADER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MESSAGETEXT_HEADER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MESSAGETEXT_HEADER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MESSAGETEXT_HEADER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMESSAGETEXT_HEADER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTDATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTDATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTDATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTDATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTIME_HEADER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTIME_HEADER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTIME_HEADER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTIME_HEADER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "USERMESSAGETEXT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "USERMESSAGETEXT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "USERMESSAGETEXT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUSERMESSAGETEXT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRYUSERNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRYUSERNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRYUSERNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRYUSERNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDIT_PROFILE_REPORT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDIT_PROFILE_REPORT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_PROFILE_REPORT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_PROFILE_REPORT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDIT_PROFILE_REPORT_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDIT_PROFILE_REPORT_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_PROFILE_REPORT_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_PROFILE_REPORT_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VERSION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VERSION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VERSION" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVERSION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTNUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUBSCRIBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBSCRIBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUBSCRIBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUBSCRIBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUBSCRIBERNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBSCRIBERNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUBSCRIBERNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUBSCRIBERNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRYREASON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRYREASON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRYREASON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRYREASON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FINANCEPURPOSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FINANCEPURPOSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FINANCEPURPOSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFINANCEPURPOSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AMOUNTFINANCED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AMOUNTFINANCED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNTFINANCED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNTFINANCED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DURATIONOFAGREEMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DURATIONOFAGREEMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DURATIONOFAGREEMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDURATIONOFAGREEMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LASTNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LASTNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LASTNAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLASTNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FIRSTNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FIRSTNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FIRSTNAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFIRSTNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MIDDLENAME1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MIDDLENAME1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDDLENAME1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDDLENAME1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MIDDLENAME2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MIDDLENAME2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDDLENAME2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDDLENAME2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MIDDLENAME3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MIDDLENAME3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDDLENAME3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDDLENAME3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDERCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDERCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDERCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDERCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INCOME_TAX_PAN_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCOME_TAX_PAN_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCOME_TAX_PAN_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCOME_TAX_PAN_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_ISSUE_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_ISSUE_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_ISSUE_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_ISSUE_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_EXP_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_EXP_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_EXP_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_EXP_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT_NUMBER_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT_NUMBER_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_NUMBER_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_NUMBER_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PASSPORT_ISSUE_DATE_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PASSPORT_ISSUE_DATE_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_ISSUE_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_ISSUE_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PASSPORT_EXP_DATE_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PASSPORT_EXP_DATE_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_EXP_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_EXP_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VOTER_IDENTITY_CARD_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VOTER_IDENTITY_CARD_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_IDENTITY_CARD_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_IDENTITY_CARD_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_ISSUE_DATE_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_ISSUE_DATE_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID_ISSUE_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID_ISSUE_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_EXP_DATE_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_EXP_DATE_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID_EXP_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID_EXP_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_NUMBER_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_NUMBER_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVER_LICENSE_NUMBER_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVER_LICENSE_NUMBER_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_ISSUE_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_ISSUE_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVER_LICENSE_ISSUE_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVER_LICENSE_ISSUE_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_EXP_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_EXP_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVER_LICENSE_EXP_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVER_LICENSE_EXP_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_NUMBER_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_NUMBER_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_NUMBER_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_NUMBER_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_ISSUE_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_ISSUE_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_ISSUE_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_ISSUE_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_EXP_DATE_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_EXP_DATE_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_EXP_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_EXP_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_NUMBER_APP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_NUMBER_APP").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UNIVERSAL_ID_NUMBER_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUNIVERSAL_ID_NUMBER_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_ISSUE_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_ISSUE_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UNIVERSAL_ID_ISSUE_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUNIVERSAL_ID_ISSUE_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_EXP_DATE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_EXP_DATE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UNIVERSAL_ID_EXP_DATE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUNIVERSAL_ID_EXP_DATE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEOFBIRTHAPPLICANT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEOFBIRTHAPPLICANT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOFBIRTHAPPLICANT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOFBIRTHAPPLICANT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TELEPHONENUMBERAPPLICANT1ST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TELEPHONENUMBERAPPLICANT1ST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEPHONENUMBERAPPLICANT1ST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEPHONENUMBERAPPLICANT1ST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELEEXTENSION_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELEEXTENSION_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEEXTENSION_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEEXTENSION_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELETYPE_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELETYPE_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELETYPE_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELETYPE_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MOBILEPHONENUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MOBILEPHONENUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOBILEPHONENUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOBILEPHONENUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAILID_APP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAILID_APP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAILID_APP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAILID_APP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INCOME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCOME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCOME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCOME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MARITALSTATUSCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MARITALSTATUSCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MARITALSTATUSCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMARITALSTATUSCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MARITALSTATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MARITALSTATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MARITALSTATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMARITALSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMPLOYMENTSTATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMPLOYMENTSTATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMPLOYMENTSTATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMPLOYMENTSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEWITHEMPLOYER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEWITHEMPLOYER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEWITHEMPLOYER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEWITHEMPLOYER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NUMBEROFMAJORCREDITCARDHELD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NUMBEROFMAJORCREDITCARDHELD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUMBEROFMAJORCREDITCARDHELD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUMBEROFMAJORCREDITCARDHELD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FLATNOPLOTNOHOUSENO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FLATNOPLOTNOHOUSENO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FLATNOPLOTNOHOUSENO" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFLATNOPLOTNOHOUSENO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BLDGNOSOCIETYNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BLDGNOSOCIETYNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BLDGNOSOCIETYNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBLDGNOSOCIETYNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ROADNONAMEAREALOCALITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ROADNONAMEAREALOCALITY").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ROADNONAMEAREALOCALITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setROADNONAMEAREALOCALITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CITY" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LANDMARK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LANDMARK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LANDMARK" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLANDMARK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PINCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PINCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PINCODE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPINCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "COUNTRY_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COUNTRY_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COUNTRY_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOUNTRY_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ADD_FLATNOPLOTNOHOUSENO").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ADD_FLATNOPLOTNOHOUSENO").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_FLATNOPLOTNOHOUSENO" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_FLATNOPLOTNOHOUSENO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ADD_BLDGNOSOCIETYNAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ADD_BLDGNOSOCIETYNAME").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_BLDGNOSOCIETYNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_BLDGNOSOCIETYNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ADD_ROADNONAMEAREALOCALITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ADD_ROADNONAMEAREALOCALITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_ROADNONAMEAREALOCALITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_ROADNONAMEAREALOCALITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADD_CITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADD_CITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_CITY" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_CITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADD_LANDMARK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADD_LANDMARK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_LANDMARK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_LANDMARK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADD_STATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADD_STATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_STATE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_STATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADD_PINCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADD_PINCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_PINCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_PINCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADD_COUNTRYCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADD_COUNTRYCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_COUNTRYCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_COUNTRYCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITACCOUNTTOTAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITACCOUNTTOTAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITACCOUNTTOTAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITACCOUNTTOTAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITACCOUNTACTIVE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITACCOUNTACTIVE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITACCOUNTACTIVE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITACCOUNTACTIVE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITACCOUNTDEFAULT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITACCOUNTDEFAULT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITACCOUNTDEFAULT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITACCOUNTDEFAULT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITACCOUNTCLOSED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITACCOUNTCLOSED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITACCOUNTCLOSED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITACCOUNTCLOSED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CADSUITFILEDCURRENTBALANCE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CADSUITFILEDCURRENTBALANCE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CADSUITFILEDCURRENTBALANCE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCADSUITFILEDCURRENTBALANCE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALANCESECURED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALANCESECURED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTSTANDINGBALANCESECURED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTSTANDINGBALANCESECURED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALSECUREDPER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALSECUREDPER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTSTANDINGBALSECUREDPER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTSTANDINGBALSECUREDPER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALANCEUNSECURED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALANCEUNSECURED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTSTANDINGBALANCEUNSECURED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTSTANDINGBALANCEUNSECURED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALUNSECPER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALUNSECPER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTSTANDINGBALUNSECPER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTSTANDINGBALUNSECPER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALANCEALL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OUTSTANDINGBALANCEALL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTSTANDINGBALANCEALL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTSTANDINGBALANCEALL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IDENTIFICATIONNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IDENTIFICATIONNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IDENTIFICATIONNUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIDENTIFICATIONNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAISSUBSCRIBERNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAISSUBSCRIBERNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAISSUBSCRIBERNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAISSUBSCRIBERNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTNUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PORTFOLIOTYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PORTFOLIOTYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PORTFOLIOTYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPORTFOLIOTYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTTYPE_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTTYPE_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTTYPE_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTTYPE_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTTYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTTYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTTYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTTYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OPENDATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPENDATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OPENDATE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOPENDATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "HIGHESTCREDITORORGNLOANAMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "HIGHESTCREDITORORGNLOANAMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HIGHESTCREDITORORGNLOANAMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHIGHESTCREDITORORGNLOANAMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TERMSDURATION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TERMSDURATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TERMSDURATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTERMSDURATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TERMSFREQUENCY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TERMSFREQUENCY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TERMSFREQUENCY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTERMSFREQUENCY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SCHEDULEDMONTHLYPAYAMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SCHEDULEDMONTHLYPAYAMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCHEDULEDMONTHLYPAYAMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCHEDULEDMONTHLYPAYAMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTSTATUS_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTSTATUS_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTSTATUS_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTSTATUS_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTSTATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTSTATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTSTATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAYMENTRATING").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAYMENTRATING").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENTRATING" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENTRATING(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAYMENTHISTORYPROFILE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAYMENTHISTORYPROFILE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENTHISTORYPROFILE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENTHISTORYPROFILE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SPECIALCOMMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SPECIALCOMMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SPECIALCOMMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSPECIALCOMMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CURRENTBALANCE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CURRENTBALANCE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENTBALANCE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENTBALANCE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AMOUNTPASTDUE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AMOUNTPASTDUE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNTPASTDUE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNTPASTDUE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ORIGINALCHARGEOFFAMOUNT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ORIGINALCHARGEOFFAMOUNT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ORIGINALCHARGEOFFAMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setORIGINALCHARGEOFFAMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEREPORTED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEREPORTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEREPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEREPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATEOFFIRSTDELINQUENCY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATEOFFIRSTDELINQUENCY").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOFFIRSTDELINQUENCY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOFFIRSTDELINQUENCY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATECLOSED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATECLOSED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATECLOSED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATECLOSED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEOFLASTPAYMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEOFLASTPAYMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOFLASTPAYMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOFLASTPAYMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SUITFWILFULDFTWRITTENOFFSTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SUITFWILFULDFTWRITTENOFFSTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUITFWILFULDFTWRITTENOFFSTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUITFWILFULDFTWRITTENOFFSTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUITFILEDWILFULDEF ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUITFILEDWILFULDEF ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUITFILEDWILFULDEF " +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUITFILEDWILFULDEF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "WRITTENOFFSETTLEDSTATUS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "WRITTENOFFSETTLEDSTATUS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITTENOFFSETTLEDSTATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITTENOFFSETTLEDSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VALUEOFCREDITSLASTMONTH").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VALUEOFCREDITSLASTMONTH").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VALUEOFCREDITSLASTMONTH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVALUEOFCREDITSLASTMONTH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OCCUPATIONCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OCCUPATIONCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OCCUPATIONCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOCCUPATIONCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SATTLEMENTAMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SATTLEMENTAMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SATTLEMENTAMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSATTLEMENTAMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VALUEOFCOLATERAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VALUEOFCOLATERAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VALUEOFCOLATERAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVALUEOFCOLATERAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TYPEOFCOLATERAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPEOFCOLATERAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TYPEOFCOLATERAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTYPEOFCOLATERAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WRITTENOFFAMTTOTAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WRITTENOFFAMTTOTAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITTENOFFAMTTOTAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITTENOFFAMTTOTAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "WRITTENOFFAMTPRINCIPAL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "WRITTENOFFAMTPRINCIPAL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITTENOFFAMTPRINCIPAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITTENOFFAMTPRINCIPAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATEOFINTEREST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATEOFINTEREST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATEOFINTEREST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATEOFINTEREST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPAYMENTTENURE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPAYMENTTENURE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPAYMENTTENURE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPAYMENTTENURE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PROMOTIONALRATEFLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PROMOTIONALRATEFLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PROMOTIONALRATEFLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPROMOTIONALRATEFLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAISINCOME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAISINCOME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAISINCOME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAISINCOME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INCOMEINDICATOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCOMEINDICATOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCOMEINDICATOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCOMEINDICATOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INCOMEFREQUENCYINDICATOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCOMEFREQUENCYINDICATOR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCOMEFREQUENCYINDICATOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCOMEFREQUENCYINDICATOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DEFAULTSTATUSDATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DEFAULTSTATUSDATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DEFAULTSTATUSDATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDEFAULTSTATUSDATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LITIGATIONSTATUSDATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LITIGATIONSTATUSDATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LITIGATIONSTATUSDATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLITIGATIONSTATUSDATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WRITEOFFSTATUSDATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WRITEOFFSTATUSDATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITEOFFSTATUSDATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITEOFFSTATUSDATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CURRENCYCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CURRENCYCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENCYCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENCYCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUBSCRIBERCOMMENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBSCRIBERCOMMENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUBSCRIBERCOMMENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUBSCRIBERCOMMENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CONSUMERCOMMENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONSUMERCOMMENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONSUMERCOMMENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONSUMERCOMMENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ACCOUNTHOLDERTYPECODE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ACCOUNTHOLDERTYPECODE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTHOLDERTYPECODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTHOLDERTYPECODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ACCOUNTHOLDER_TYPENAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ACCOUNTHOLDER_TYPENAME").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTHOLDER_TYPENAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTHOLDER_TYPENAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "YEAR_HIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "YEAR_HIST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "YEAR_HIST" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setYEAR_HIST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MONTH_HIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MONTH_HIST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MONTH_HIST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMONTH_HIST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DAYSPASTDUE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DAYSPASTDUE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DAYSPASTDUE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDAYSPASTDUE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ASSETCLASSIFICATION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ASSETCLASSIFICATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ASSETCLASSIFICATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setASSETCLASSIFICATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "YEAR_ADVHIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "YEAR_ADVHIST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "YEAR_ADVHIST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setYEAR_ADVHIST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MONTH_ADVHIST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MONTH_ADVHIST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MONTH_ADVHIST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMONTH_ADVHIST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CASHLIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CASHLIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CASHLIMIT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCASHLIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITLIMITAMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITLIMITAMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITLIMITAMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITLIMITAMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACTUALPAYMENTAMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACTUALPAYMENTAMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACTUALPAYMENTAMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACTUALPAYMENTAMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMIAMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMIAMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMIAMT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMIAMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CURRENTBALANCE_ADVHIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CURRENTBALANCE_ADVHIST").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENTBALANCE_ADVHIST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENTBALANCE_ADVHIST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "AMOUNTPASTDUE_ADVHIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "AMOUNTPASTDUE_ADVHIST").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNTPASTDUE_ADVHIST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNTPASTDUE_ADVHIST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SURNAMENONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SURNAMENONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SURNAMENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSURNAMENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "FIRSTNAMENONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "FIRSTNAMENONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FIRSTNAMENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFIRSTNAMENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MIDDLENAME1NONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MIDDLENAME1NONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDDLENAME1NONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDDLENAME1NONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MIDDLENAME2NONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MIDDLENAME2NONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDDLENAME2NONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDDLENAME2NONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MIDDLENAME3NONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MIDDLENAME3NONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDDLENAME3NONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDDLENAME3NONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ALIAS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALIAS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALIAS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALIAS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAISGENDERCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAISGENDERCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAISGENDERCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAISGENDERCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAISGENDER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAISGENDER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAISGENDER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAISGENDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAISINCOMETAXPAN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAISINCOMETAXPAN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAISINCOMETAXPAN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAISINCOMETAXPAN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAISPASSPORTNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAISPASSPORTNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAISPASSPORTNUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAISPASSPORTNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VOTERIDNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VOTERIDNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTERIDNUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTERIDNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEOFBIRTH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEOFBIRTH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOFBIRTH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOFBIRTH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "FIRSTLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "FIRSTLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FIRSTLINEOFADDNONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFIRSTLINEOFADDNONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SECONDLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SECONDLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECONDLINEOFADDNONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECONDLINEOFADDNONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "THIRDLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "THIRDLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "THIRDLINEOFADDNONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTHIRDLINEOFADDNONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CITYNONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CITYNONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CITYNONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCITYNONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "FIFTHLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "FIFTHLINEOFADDNONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FIFTHLINEOFADDNONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFIFTHLINEOFADDNONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "STATECODENONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "STATECODENONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATECODENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATECODENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATENONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATENONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ZIPPOSTALCODENONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ZIPPOSTALCODENONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ZIPPOSTALCODENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setZIPPOSTALCODENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "COUNTRYCODENONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "COUNTRYCODENONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COUNTRYCODENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOUNTRYCODENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ADDINDICATORNONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ADDINDICATORNONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDINDICATORNONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDINDICATORNONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RESIDENCECODENONNORMALIZED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RESIDENCECODENONNORMALIZED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESIDENCECODENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESIDENCECODENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RESIDENCENONNORMALIZED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RESIDENCENONNORMALIZED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESIDENCENONNORMALIZED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESIDENCENONNORMALIZED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELEPHONENUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELEPHONENUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEPHONENUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEPHONENUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELETYPE_PHONE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELETYPE_PHONE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELETYPE_PHONE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELETYPE_PHONE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELEEXTENSION_PHONE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELEEXTENSION_PHONE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEEXTENSION_PHONE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEEXTENSION_PHONE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MOBILETELEPHONENUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MOBILETELEPHONENUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOBILETELEPHONENUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOBILETELEPHONENUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FAXNUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FAXNUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FAXNUMBER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFAXNUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAILID_PHONE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAILID_PHONE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAILID_PHONE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAILID_PHONE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INCOME_TAX_PAN_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCOME_TAX_PAN_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCOME_TAX_PAN_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCOME_TAX_PAN_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_ISSUE_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_ISSUE_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_ISSUE_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_ISSUE_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_EXP_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_EXP_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_EXP_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_EXP_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT_NUMBER_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT_NUMBER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_NUMBER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_NUMBER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PASSPORT_ISSUE_DATE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PASSPORT_ISSUE_DATE_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_ISSUE_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_ISSUE_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT_EXP_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT_EXP_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_EXP_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_EXP_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VOTER_IDENTITY_CARD_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VOTER_IDENTITY_CARD_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_IDENTITY_CARD_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_IDENTITY_CARD_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_ISSUE_DATE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_ISSUE_DATE_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID_ISSUE_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID_ISSUE_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VOTER_ID_EXP_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VOTER_ID_EXP_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID_EXP_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID_EXP_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_NUMBER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_NUMBER_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVER_LICENSE_NUMBER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVER_LICENSE_NUMBER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_ISSUE_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_ISSUE_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVER_LICENSE_ISSUE_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVER_LICENSE_ISSUE_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_EXP_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVER_LICENSE_EXP_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVER_LICENSE_EXP_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVER_LICENSE_EXP_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_NUMBER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_NUMBER_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_NUMBER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_NUMBER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_ISSUE_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_ISSUE_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_ISSUE_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_ISSUE_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_EXP_DATE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RATION_CARD_EXP_DATE_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_EXP_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_EXP_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_NUMBER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_NUMBER_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UNIVERSAL_ID_NUMBER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUNIVERSAL_ID_NUMBER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_ISSUE_DATE_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_ISSUE_DATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UNIVERSAL_ID_ISSUE_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUNIVERSAL_ID_ISSUE_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_EXP_DATE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "UNIVERSAL_ID_EXP_DATE_ID").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UNIVERSAL_ID_EXP_DATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUNIVERSAL_ID_EXP_DATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAILID_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAILID_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAILID_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAILID_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EXACTMATCH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXACTMATCH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXACTMATCH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXACTMATCH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSLAST7DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSLAST7DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSLAST7DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSLAST7DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSLAST30DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSLAST30DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSLAST30DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSLAST30DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSLAST90DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSLAST90DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSLAST90DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSLAST90DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSLAST180DAYS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSLAST180DAYS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSLAST180DAYS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSLAST180DAYS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSSUBSCRIBERCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSSUBSCRIBERCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSSUBSCRIBERCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSSUBSCRIBERCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSSUBSCRIBERNAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSSUBSCRIBERNAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSSUBSCRIBERNAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSSUBSCRIBERNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSDATEOFREQUEST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSDATEOFREQUEST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSDATEOFREQUEST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSDATEOFREQUEST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CAPSREPORTDATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAPSREPORTDATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAPSREPORTDATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAPSREPORTDATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
