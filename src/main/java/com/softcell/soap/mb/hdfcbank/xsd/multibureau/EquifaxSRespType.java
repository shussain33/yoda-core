/**
 * EquifaxSRespType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  EquifaxSRespType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class EquifaxSRespType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = EquifaxSRespType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for ENQUIRY_DATE
     */
    protected java.lang.String localENQUIRY_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_DATETracker = false;

    /**
     * field for CUSTOMERCODE
     */
    protected java.lang.String localCUSTOMERCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCUSTOMERCODETracker = false;

    /**
     * field for FIRSTNAME_NM_TP
     */
    protected java.lang.String localFIRSTNAME_NM_TP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFIRSTNAME_NM_TPTracker = false;

    /**
     * field for MIDLLENAME_NM_TP
     */
    protected java.lang.String localMIDLLENAME_NM_TP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMIDLLENAME_NM_TPTracker = false;

    /**
     * field for LASTNAME_NM_TP
     */
    protected java.lang.String localLASTNAME_NM_TP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLASTNAME_NM_TPTracker = false;

    /**
     * field for ADDMIDLLENAME_NM_TP
     */
    protected java.lang.String localADDMIDLLENAME_NM_TP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDMIDLLENAME_NM_TPTracker = false;

    /**
     * field for SUFFIX_NM_TP
     */
    protected java.lang.String localSUFFIX_NM_TP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUFFIX_NM_TPTracker = false;

    /**
     * field for REPORTEDDATE_AG_IF
     */
    protected java.lang.String localREPORTEDDATE_AG_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_AG_IFTracker = false;

    /**
     * field for AGE_AG_IF
     */
    protected java.lang.String localAGE_AG_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGE_AG_IFTracker = false;

    /**
     * field for REPORTEDDATE_AL_NM
     */
    protected java.lang.String localREPORTEDDATE_AL_NM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_AL_NMTracker = false;

    /**
     * field for ALIASNAME_AL_NM
     */
    protected java.lang.String localALIASNAME_AL_NM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALIASNAME_AL_NMTracker = false;

    /**
     * field for ADDITIONALNAMEINFO_AD_NM_IF
     */
    protected java.lang.String localADDITIONALNAMEINFO_AD_NM_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDITIONALNAMEINFO_AD_NM_IFTracker = false;

    /**
     * field for NOOFDEPENDENTS_AD_NM_IF
     */
    protected java.lang.String localNOOFDEPENDENTS_AD_NM_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFDEPENDENTS_AD_NM_IFTracker = false;

    /**
     * field for REPORTEDDATE_RS_ADD
     */
    protected java.lang.String localREPORTEDDATE_RS_ADD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_RS_ADDTracker = false;

    /**
     * field for ADDRESS_RS_ADD
     */
    protected java.lang.String localADDRESS_RS_ADD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_RS_ADDTracker = false;

    /**
     * field for STATE_RS_ADD
     */
    protected java.lang.String localSTATE_RS_ADD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATE_RS_ADDTracker = false;

    /**
     * field for POSTAL_RS_ADD
     */
    protected java.lang.String localPOSTAL_RS_ADD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPOSTAL_RS_ADDTracker = false;

    /**
     * field for ADDTYPE_RS_ADD
     */
    protected java.lang.String localADDTYPE_RS_ADD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDTYPE_RS_ADDTracker = false;

    /**
     * field for IDTYPE_RS_ID
     */
    protected java.lang.String localIDTYPE_RS_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIDTYPE_RS_IDTracker = false;

    /**
     * field for REPORTEDDATE_RS_ID
     */
    protected java.lang.String localREPORTEDDATE_RS_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_RS_IDTracker = false;

    /**
     * field for IDNUMBER_RS_ID
     */
    protected java.lang.String localIDNUMBER_RS_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIDNUMBER_RS_IDTracker = false;

    /**
     * field for EMAILREPORTEDDATE_RS_ML
     */
    protected java.lang.String localEMAILREPORTEDDATE_RS_ML;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILREPORTEDDATE_RS_MLTracker = false;

    /**
     * field for EMAILADDRESS_RS_ML
     */
    protected java.lang.String localEMAILADDRESS_RS_ML;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILADDRESS_RS_MLTracker = false;

    /**
     * field for EMPNMREPDATE_RS_EMDL
     */
    protected java.lang.String localEMPNMREPDATE_RS_EMDL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMPNMREPDATE_RS_EMDLTracker = false;

    /**
     * field for EMPLOYERNAME_RS_EMDL
     */
    protected java.lang.String localEMPLOYERNAME_RS_EMDL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMPLOYERNAME_RS_EMDLTracker = false;

    /**
     * field for EMPPOSITION_RS_EMDL
     */
    protected java.lang.String localEMPPOSITION_RS_EMDL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMPPOSITION_RS_EMDLTracker = false;

    /**
     * field for EMPADDRESS_RS_EMDL
     */
    protected java.lang.String localEMPADDRESS_RS_EMDL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMPADDRESS_RS_EMDLTracker = false;

    /**
     * field for DOB_RS_PER_IF
     */
    protected java.lang.String localDOB_RS_PER_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOB_RS_PER_IFTracker = false;

    /**
     * field for GENDER_RS_PER_IF
     */
    protected java.lang.String localGENDER_RS_PER_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDER_RS_PER_IFTracker = false;

    /**
     * field for TOTALINCOME_RS_PER_IF
     */
    protected java.lang.String localTOTALINCOME_RS_PER_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALINCOME_RS_PER_IFTracker = false;

    /**
     * field for OCCUPATION_RS_PER_IF
     */
    protected java.lang.String localOCCUPATION_RS_PER_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOCCUPATION_RS_PER_IFTracker = false;

    /**
     * field for MARITALSTAUS_RS_PER_IF
     */
    protected java.lang.String localMARITALSTAUS_RS_PER_IF;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMARITALSTAUS_RS_PER_IFTracker = false;

    /**
     * field for OCCUPATION_IC_DL
     */
    protected java.lang.String localOCCUPATION_IC_DL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOCCUPATION_IC_DLTracker = false;

    /**
     * field for MONTHLYINCOME_IC_DL
     */
    protected java.lang.String localMONTHLYINCOME_IC_DL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMONTHLYINCOME_IC_DLTracker = false;

    /**
     * field for MONTHLYEXPENSE_IC_DL
     */
    protected java.lang.String localMONTHLYEXPENSE_IC_DL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMONTHLYEXPENSE_IC_DLTracker = false;

    /**
     * field for POVERTYINDEX_IC_DL
     */
    protected java.lang.String localPOVERTYINDEX_IC_DL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPOVERTYINDEX_IC_DLTracker = false;

    /**
     * field for ASSETOWNERSHIP_IC_DL
     */
    protected java.lang.String localASSETOWNERSHIP_IC_DL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localASSETOWNERSHIP_IC_DLTracker = false;

    /**
     * field for REPORTEDDATE_RS_PH
     */
    protected java.lang.String localREPORTEDDATE_RS_PH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_RS_PHTracker = false;

    /**
     * field for TYPECODE_RS_PH
     */
    protected java.lang.String localTYPECODE_RS_PH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTYPECODE_RS_PHTracker = false;

    /**
     * field for COUNTRYCODE_RS_PH
     */
    protected java.lang.String localCOUNTRYCODE_RS_PH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOUNTRYCODE_RS_PHTracker = false;

    /**
     * field for AREACODE_RS_PH
     */
    protected java.lang.String localAREACODE_RS_PH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAREACODE_RS_PHTracker = false;

    /**
     * field for PHNUMBER_RS_PH
     */
    protected java.lang.String localPHNUMBER_RS_PH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHNUMBER_RS_PHTracker = false;

    /**
     * field for PHNOEXTENTION_RS_PH
     */
    protected java.lang.String localPHNOEXTENTION_RS_PH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHNOEXTENTION_RS_PHTracker = false;

    /**
     * field for CUSTOMERCODE_RS_HD
     */
    protected java.lang.String localCUSTOMERCODE_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCUSTOMERCODE_RS_HDTracker = false;

    /**
     * field for CLIENTID_RS_HD
     */
    protected java.lang.String localCLIENTID_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCLIENTID_RS_HDTracker = false;

    /**
     * field for CUSTREFFIELD_RS_HD
     */
    protected java.lang.String localCUSTREFFIELD_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCUSTREFFIELD_RS_HDTracker = false;

    /**
     * field for REPORTORDERNO_RS_HD
     */
    protected java.lang.String localREPORTORDERNO_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTORDERNO_RS_HDTracker = false;

    /**
     * field for PRODUCTCODE_RS_HD
     */
    protected java.lang.String localPRODUCTCODE_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPRODUCTCODE_RS_HDTracker = false;

    /**
     * field for PRODUCTVERSION_RS_HD
     */
    protected java.lang.String localPRODUCTVERSION_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPRODUCTVERSION_RS_HDTracker = false;

    /**
     * field for SUCCESSCODE_RS_HD
     */
    protected java.lang.String localSUCCESSCODE_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUCCESSCODE_RS_HDTracker = false;

    /**
     * field for MATCHTYPE_RS_HD
     */
    protected java.lang.String localMATCHTYPE_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMATCHTYPE_RS_HDTracker = false;

    /**
     * field for RESDATE_RS_HD
     */
    protected java.lang.String localRESDATE_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESDATE_RS_HDTracker = false;

    /**
     * field for RESTIME_RS_HD
     */
    protected java.lang.String localRESTIME_RS_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESTIME_RS_HDTracker = false;

    /**
     * field for REPORTEDDATE_RS_AC
     */
    protected java.lang.String localREPORTEDDATE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_RS_ACTracker = false;

    /**
     * field for CLIENTNAME_RS_AC
     */
    protected java.lang.String localCLIENTNAME_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCLIENTNAME_RS_ACTracker = false;

    /**
     * field for ACCOUNTNUMBER_RS_AC
     */
    protected java.lang.String localACCOUNTNUMBER_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTNUMBER_RS_ACTracker = false;

    /**
     * field for CURRENTBALANCE_RS_AC
     */
    protected java.lang.String localCURRENTBALANCE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENTBALANCE_RS_ACTracker = false;

    /**
     * field for INSTITUTION_RS_AC
     */
    protected java.lang.String localINSTITUTION_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINSTITUTION_RS_ACTracker = false;

    /**
     * field for ACCOUNTTYPE_RS_AC
     */
    protected java.lang.String localACCOUNTTYPE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTTYPE_RS_ACTracker = false;

    /**
     * field for OWNERSHIPTYPE_RS_AC
     */
    protected java.lang.String localOWNERSHIPTYPE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNERSHIPTYPE_RS_ACTracker = false;

    /**
     * field for BALANCE_RS_AC
     */
    protected java.lang.String localBALANCE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBALANCE_RS_ACTracker = false;

    /**
     * field for PASTDUEAMT_RS_AC
     */
    protected java.lang.String localPASTDUEAMT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASTDUEAMT_RS_ACTracker = false;

    /**
     * field for DISBURSEDAMOUNT_RS_AC
     */
    protected java.lang.String localDISBURSEDAMOUNT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISBURSEDAMOUNT_RS_ACTracker = false;

    /**
     * field for LOANCATEGORY_RS_AC
     */
    protected java.lang.String localLOANCATEGORY_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOANCATEGORY_RS_ACTracker = false;

    /**
     * field for LOANPURPOSE_RS_AC
     */
    protected java.lang.String localLOANPURPOSE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOANPURPOSE_RS_ACTracker = false;

    /**
     * field for LASTPAYMENT_RS_AC
     */
    protected java.lang.String localLASTPAYMENT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLASTPAYMENT_RS_ACTracker = false;

    /**
     * field for WRITEOFFAMT_RS_AC
     */
    protected java.lang.String localWRITEOFFAMT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITEOFFAMT_RS_ACTracker = false;

    /**
     * field for ACCOPEN_RS_AC
     */
    protected java.lang.String localACCOPEN_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOPEN_RS_ACTracker = false;

    /**
     * field for SACTIONAMT_RS_AC
     */
    protected java.lang.String localSACTIONAMT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSACTIONAMT_RS_ACTracker = false;

    /**
     * field for LASTPAYMENTDATE_RS_AC
     */
    protected java.lang.String localLASTPAYMENTDATE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLASTPAYMENTDATE_RS_ACTracker = false;

    /**
     * field for HIGHCREDIT_RS_AC
     */
    protected java.lang.String localHIGHCREDIT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHIGHCREDIT_RS_ACTracker = false;

    /**
     * field for DATEREPORTED_RS_AC
     */
    protected java.lang.String localDATEREPORTED_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEREPORTED_RS_ACTracker = false;

    /**
     * field for DATEOPENED_RS_AC
     */
    protected java.lang.String localDATEOPENED_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOPENED_RS_ACTracker = false;

    /**
     * field for DATECLOSED_RS_AC
     */
    protected java.lang.String localDATECLOSED_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATECLOSED_RS_ACTracker = false;

    /**
     * field for REASON_RS_AC
     */
    protected java.lang.String localREASON_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_RS_ACTracker = false;

    /**
     * field for DATEWRITTENOFF_RS_AC
     */
    protected java.lang.String localDATEWRITTENOFF_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEWRITTENOFF_RS_ACTracker = false;

    /**
     * field for LOANCYCLEID_RS_AC
     */
    protected java.lang.String localLOANCYCLEID_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOANCYCLEID_RS_ACTracker = false;

    /**
     * field for DATESANCTIONED_RS_AC
     */
    protected java.lang.String localDATESANCTIONED_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATESANCTIONED_RS_ACTracker = false;

    /**
     * field for DATEAPPLIED_RS_AC
     */
    protected java.lang.String localDATEAPPLIED_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEAPPLIED_RS_ACTracker = false;

    /**
     * field for INTRESTRATE_RS_AC
     */
    protected java.lang.String localINTRESTRATE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINTRESTRATE_RS_ACTracker = false;

    /**
     * field for APPLIEDAMOUNT_RS_AC
     */
    protected java.lang.String localAPPLIEDAMOUNT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAPPLIEDAMOUNT_RS_ACTracker = false;

    /**
     * field for NOOFINSTALLMENTS_RS_AC
     */
    protected java.lang.String localNOOFINSTALLMENTS_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFINSTALLMENTS_RS_ACTracker = false;

    /**
     * field for REPAYMENTTENURE_RS_AC
     */
    protected java.lang.String localREPAYMENTTENURE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPAYMENTTENURE_RS_ACTracker = false;

    /**
     * field for DISPUTECODE_RS_AC
     */
    protected java.lang.String localDISPUTECODE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISPUTECODE_RS_ACTracker = false;

    /**
     * field for INSTALLMENTAMT_RS_AC
     */
    protected java.lang.String localINSTALLMENTAMT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINSTALLMENTAMT_RS_ACTracker = false;

    /**
     * field for KEYPERSON_RS_AC
     */
    protected java.lang.String localKEYPERSON_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localKEYPERSON_RS_ACTracker = false;

    /**
     * field for NOMINEE_RS_AC
     */
    protected java.lang.String localNOMINEE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOMINEE_RS_ACTracker = false;

    /**
     * field for TERMFREQUENCY_RS_AC
     */
    protected java.lang.String localTERMFREQUENCY_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTERMFREQUENCY_RS_ACTracker = false;

    /**
     * field for CREDITLIMIT_RS_AC
     */
    protected java.lang.String localCREDITLIMIT_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITLIMIT_RS_ACTracker = false;

    /**
     * field for COLLATERALVALUE_RS_AC
     */
    protected java.lang.String localCOLLATERALVALUE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLLATERALVALUE_RS_ACTracker = false;

    /**
     * field for COLLATERALTYPE_RS_AC
     */
    protected java.lang.String localCOLLATERALTYPE_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLLATERALTYPE_RS_ACTracker = false;

    /**
     * field for ACCOUNTSTATUS_RS_AC
     */
    protected java.lang.String localACCOUNTSTATUS_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTSTATUS_RS_ACTracker = false;

    /**
     * field for ASSETCLASSIFICATION_RS_AC
     */
    protected java.lang.String localASSETCLASSIFICATION_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localASSETCLASSIFICATION_RS_ACTracker = false;

    /**
     * field for SUITFILEDSTATUS_RS_AC
     */
    protected java.lang.String localSUITFILEDSTATUS_RS_AC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUITFILEDSTATUS_RS_ACTracker = false;

    /**
     * field for MONTHKEY_RS_AC_HIS24
     */
    protected java.lang.String localMONTHKEY_RS_AC_HIS24;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMONTHKEY_RS_AC_HIS24Tracker = false;

    /**
     * field for PAYMENTSTATUS_RS_AC_HIS24
     */
    protected java.lang.String localPAYMENTSTATUS_RS_AC_HIS24;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENTSTATUS_RS_AC_HIS24Tracker = false;

    /**
     * field for SUITFILEDSTATUS_RS_AC_HIS24
     */
    protected java.lang.String localSUITFILEDSTATUS_RS_AC_HIS24;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUITFILEDSTATUS_RS_AC_HIS24Tracker = false;

    /**
     * field for ASSETTCLSSTATUS_RS_AC_HIS24
     */
    protected java.lang.String localASSETTCLSSTATUS_RS_AC_HIS24;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localASSETTCLSSTATUS_RS_AC_HIS24Tracker = false;

    /**
     * field for MONTHKEY_RS_AC_HIS48
     */
    protected java.lang.String localMONTHKEY_RS_AC_HIS48;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMONTHKEY_RS_AC_HIS48Tracker = false;

    /**
     * field for PAYMENTSTATUS_RS_AC_HIS48
     */
    protected java.lang.String localPAYMENTSTATUS_RS_AC_HIS48;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENTSTATUS_RS_AC_HIS48Tracker = false;

    /**
     * field for SUITFILEDSTATUS_RS_AC_HIS48
     */
    protected java.lang.String localSUITFILEDSTATUS_RS_AC_HIS48;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUITFILEDSTATUS_RS_AC_HIS48Tracker = false;

    /**
     * field for ASSETCLSSTATUS_RS_AC_HIS48
     */
    protected java.lang.String localASSETCLSSTATUS_RS_AC_HIS48;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localASSETCLSSTATUS_RS_AC_HIS48Tracker = false;

    /**
     * field for NOOFACCOUNTS_RS_AC_SUM
     */
    protected java.lang.String localNOOFACCOUNTS_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFACCOUNTS_RS_AC_SUMTracker = false;

    /**
     * field for NOOFACTIVEACCOUNTS_RS_AC_SUM
     */
    protected java.lang.String localNOOFACTIVEACCOUNTS_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFACTIVEACCOUNTS_RS_AC_SUMTracker = false;

    /**
     * field for NOOFWRITEOFFS_RS_AC_SUM
     */
    protected java.lang.String localNOOFWRITEOFFS_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFWRITEOFFS_RS_AC_SUMTracker = false;

    /**
     * field for TOTALPASTDUE_RS_AC_SUM
     */
    protected java.lang.String localTOTALPASTDUE_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALPASTDUE_RS_AC_SUMTracker = false;

    /**
     * field for MOSTSRVRSTAWHIN24MON_RS_AC_SUM
     */
    protected java.lang.String localMOSTSRVRSTAWHIN24MON_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOSTSRVRSTAWHIN24MON_RS_AC_SUMTracker = false;

    /**
     * field for SINGLEHIGHESTCREDIT_RS_AC_SUM
     */
    protected java.lang.String localSINGLEHIGHESTCREDIT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSINGLEHIGHESTCREDIT_RS_AC_SUMTracker = false;

    /**
     * field for SINGLEHIGHSANCTAMT_RS_AC_SUM
     */
    protected java.lang.String localSINGLEHIGHSANCTAMT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSINGLEHIGHSANCTAMT_RS_AC_SUMTracker = false;

    /**
     * field for TOTALHIGHCREDIT_RS_AC_SUM
     */
    protected java.lang.String localTOTALHIGHCREDIT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALHIGHCREDIT_RS_AC_SUMTracker = false;

    /**
     * field for AVERAGEOPENBALANCE_RS_AC_SUM
     */
    protected java.lang.String localAVERAGEOPENBALANCE_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAVERAGEOPENBALANCE_RS_AC_SUMTracker = false;

    /**
     * field for SINGLEHIGHESTBALANCE_RS_AC_SUM
     */
    protected java.lang.String localSINGLEHIGHESTBALANCE_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSINGLEHIGHESTBALANCE_RS_AC_SUMTracker = false;

    /**
     * field for NOOFPASTDUEACCTS_RS_AC_SUM
     */
    protected java.lang.String localNOOFPASTDUEACCTS_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFPASTDUEACCTS_RS_AC_SUMTracker = false;

    /**
     * field for NOOFZEROBALACCTS_RS_AC_SUM
     */
    protected java.lang.String localNOOFZEROBALACCTS_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFZEROBALACCTS_RS_AC_SUMTracker = false;

    /**
     * field for RECENTACCOUNT_RS_AC_SUM
     */
    protected java.lang.String localRECENTACCOUNT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRECENTACCOUNT_RS_AC_SUMTracker = false;

    /**
     * field for OLDESTACCOUNT_RS_AC_SUM
     */
    protected java.lang.String localOLDESTACCOUNT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOLDESTACCOUNT_RS_AC_SUMTracker = false;

    /**
     * field for TOTALBALAMT_RS_AC_SUM
     */
    protected java.lang.String localTOTALBALAMT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALBALAMT_RS_AC_SUMTracker = false;

    /**
     * field for TOTALSANCTAMT_RS_AC_SUM
     */
    protected java.lang.String localTOTALSANCTAMT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALSANCTAMT_RS_AC_SUMTracker = false;

    /**
     * field for TOTALCRDTLIMIT_RS_AC_SUM
     */
    protected java.lang.String localTOTALCRDTLIMIT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALCRDTLIMIT_RS_AC_SUMTracker = false;

    /**
     * field for TOTALMONLYPYMNTAMT_RS_AC_SUM
     */
    protected java.lang.String localTOTALMONLYPYMNTAMT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALMONLYPYMNTAMT_RS_AC_SUMTracker = false;

    /**
     * field for TOTALWRITTENOFFAMNT_RS_AC_SUM
     */
    protected java.lang.String localTOTALWRITTENOFFAMNT_RS_AC_SUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALWRITTENOFFAMNT_RS_AC_SUMTracker = false;

    /**
     * field for AGEOFOLDESTTRADE_RS_OTK
     */
    protected java.lang.String localAGEOFOLDESTTRADE_RS_OTK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGEOFOLDESTTRADE_RS_OTKTracker = false;

    /**
     * field for NUMBEROFOPENTRADES_RS_OTK
     */
    protected java.lang.String localNUMBEROFOPENTRADES_RS_OTK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNUMBEROFOPENTRADES_RS_OTKTracker = false;

    /**
     * field for ALINESEVERWRITTEN_RS_OTK
     */
    protected java.lang.String localALINESEVERWRITTEN_RS_OTK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALINESEVERWRITTEN_RS_OTKTracker = false;

    /**
     * field for ALINESVWRTNIN9MNTHS_RS_OTK
     */
    protected java.lang.String localALINESVWRTNIN9MNTHS_RS_OTK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALINESVWRTNIN9MNTHS_RS_OTKTracker = false;

    /**
     * field for ALINESVRWRTNIN6MNTHS_RS_OTK
     */
    protected java.lang.String localALINESVRWRTNIN6MNTHS_RS_OTK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALINESVRWRTNIN6MNTHS_RS_OTKTracker = false;

    /**
     * field for REPORTEDDATE_RS_EQ_SM
     */
    protected java.lang.String localREPORTEDDATE_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_RS_EQ_SMTracker = false;

    /**
     * field for PURPOSE_RS_EQ_SM
     */
    protected java.lang.String localPURPOSE_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPURPOSE_RS_EQ_SMTracker = false;

    /**
     * field for TOTAL_RS_EQ_SM
     */
    protected java.lang.String localTOTAL_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTAL_RS_EQ_SMTracker = false;

    /**
     * field for PAST30DAYS_RS_EQ_SM
     */
    protected java.lang.String localPAST30DAYS_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAST30DAYS_RS_EQ_SMTracker = false;

    /**
     * field for PAST12MONTHS_RS_EQ_SM
     */
    protected java.lang.String localPAST12MONTHS_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAST12MONTHS_RS_EQ_SMTracker = false;

    /**
     * field for PAST24MONTHS_RS_EQ_SM
     */
    protected java.lang.String localPAST24MONTHS_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAST24MONTHS_RS_EQ_SMTracker = false;

    /**
     * field for RECENT_RS_EQ_SM
     */
    protected java.lang.String localRECENT_RS_EQ_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRECENT_RS_EQ_SMTracker = false;

    /**
     * field for REPORTEDDATE_RS_EQ
     */
    protected java.lang.String localREPORTEDDATE_RS_EQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORTEDDATE_RS_EQTracker = false;

    /**
     * field for ENQREFERENCE_RS_EQ
     */
    protected java.lang.String localENQREFERENCE_RS_EQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQREFERENCE_RS_EQTracker = false;

    /**
     * field for ENQDATE_RS_EQ
     */
    protected java.lang.String localENQDATE_RS_EQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQDATE_RS_EQTracker = false;

    /**
     * field for ENQTIME_RS_EQ
     */
    protected java.lang.String localENQTIME_RS_EQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQTIME_RS_EQTracker = false;

    /**
     * field for REQUESTPURPOSE_RS_EQ
     */
    protected java.lang.String localREQUESTPURPOSE_RS_EQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREQUESTPURPOSE_RS_EQTracker = false;

    /**
     * field for AMOUNT_RS_EQ
     */
    protected java.lang.String localAMOUNT_RS_EQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNT_RS_EQTracker = false;

    /**
     * field for ACCOUNTSDELIQUENT_RS_RE_ACT
     */
    protected java.lang.String localACCOUNTSDELIQUENT_RS_RE_ACT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTSDELIQUENT_RS_RE_ACTTracker = false;

    /**
     * field for ACCOUNTSOPENED_RS_RE_ACT
     */
    protected java.lang.String localACCOUNTSOPENED_RS_RE_ACT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTSOPENED_RS_RE_ACTTracker = false;

    /**
     * field for TOTALINQUIRIES_RS_RE_ACT
     */
    protected java.lang.String localTOTALINQUIRIES_RS_RE_ACT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTOTALINQUIRIES_RS_RE_ACTTracker = false;

    /**
     * field for ACCOUNTSUPDATED_RS_RE_ACT
     */
    protected java.lang.String localACCOUNTSUPDATED_RS_RE_ACT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTSUPDATED_RS_RE_ACTTracker = false;

    /**
     * field for INSTITUTION_GRP_SM
     */
    protected java.lang.String localINSTITUTION_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINSTITUTION_GRP_SMTracker = false;

    /**
     * field for CURRENTBALANCE_GRP_SM
     */
    protected java.lang.String localCURRENTBALANCE_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENTBALANCE_GRP_SMTracker = false;

    /**
     * field for STATUS_GRP_SM
     */
    protected java.lang.String localSTATUS_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUS_GRP_SMTracker = false;

    /**
     * field for DATEREPORTED_GRP_SM
     */
    protected java.lang.String localDATEREPORTED_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEREPORTED_GRP_SMTracker = false;

    /**
     * field for NOOFMEMBERS_GRP_SM
     */
    protected java.lang.String localNOOFMEMBERS_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNOOFMEMBERS_GRP_SMTracker = false;

    /**
     * field for PASTDUEAMOUNT_GRP_SM
     */
    protected java.lang.String localPASTDUEAMOUNT_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASTDUEAMOUNT_GRP_SMTracker = false;

    /**
     * field for SANCTIONAMOUNT_GRP_SM
     */
    protected java.lang.String localSANCTIONAMOUNT_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSANCTIONAMOUNT_GRP_SMTracker = false;

    /**
     * field for DATEOPENED_GRP_SM
     */
    protected java.lang.String localDATEOPENED_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATEOPENED_GRP_SMTracker = false;

    /**
     * field for ACCOUNTNO_GRP_SM
     */
    protected java.lang.String localACCOUNTNO_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNTNO_GRP_SMTracker = false;

    /**
     * field for MEMBERSPASTDUE_GRP_SM
     */
    protected java.lang.String localMEMBERSPASTDUE_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBERSPASTDUE_GRP_SMTracker = false;

    /**
     * field for WRITEOFFAMOUNT_GRP_SM
     */
    protected java.lang.String localWRITEOFFAMOUNT_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITEOFFAMOUNT_GRP_SMTracker = false;

    /**
     * field for WRITEOFFDATE_GRP_SM
     */
    protected java.lang.String localWRITEOFFDATE_GRP_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITEOFFDATE_GRP_SMTracker = false;

    /**
     * field for REASONCODE_SC
     */
    protected java.lang.String localREASONCODE_SC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASONCODE_SCTracker = false;

    /**
     * field for SCOREDESCRIPTION_SC
     */
    protected java.lang.String localSCOREDESCRIPTION_SC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCOREDESCRIPTION_SCTracker = false;

    /**
     * field for SCORENAME_SC
     */
    protected java.lang.String localSCORENAME_SC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORENAME_SCTracker = false;

    /**
     * field for SCOREVALUE_SC
     */
    protected java.lang.String localSCOREVALUE_SC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCOREVALUE_SCTracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_TIME
     */
    protected java.lang.String localOUTPUT_WRITE_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_TIMETracker = false;

    /**
     * field for OUTPUT_READ_TIME
     */
    protected java.lang.String localOUTPUT_READ_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_TIMETracker = false;

    /**
     * field for ACCOUNT_KEY
     */
    protected java.lang.String localACCOUNT_KEY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_KEYTracker = false;

    /**
     * field for ACCNT_NUM
     */
    protected java.lang.String localACCNT_NUM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCNT_NUMTracker = false;

    /**
     * field for DISPUTE_COMMENTS
     */
    protected java.lang.String localDISPUTE_COMMENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISPUTE_COMMENTSTracker = false;

    /**
     * field for STATUS
     */
    protected java.lang.String localSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUSTracker = false;

    /**
     * field for RESOLVED_DATE
     */
    protected java.lang.String localRESOLVED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESOLVED_DATETracker = false;

    /**
     * field for CODE
     */
    protected java.lang.String localCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCODETracker = false;

    /**
     * field for DESCRIPTION
     */
    protected java.lang.String localDESCRIPTION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDESCRIPTIONTracker = false;

    /**
     * field for HITCODE_HD
     */
    protected java.lang.String localHITCODE_HD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHITCODE_HDTracker = false;

    /**
     * field for FULLNAME_NM_TP
     */
    protected java.lang.String localFULLNAME_NM_TP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFULLNAME_NM_TPTracker = false;

    /**
     * field for EMPPHONE_RS_EMDL
     */
    protected java.lang.String localEMPPHONE_RS_EMDL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMPPHONE_RS_EMDLTracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isENQUIRY_DATESpecified() {
        return localENQUIRY_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_DATE() {
        return localENQUIRY_DATE;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_DATE
     */
    public void setENQUIRY_DATE(java.lang.String param) {
        localENQUIRY_DATETracker = param != null;

        this.localENQUIRY_DATE = param;
    }

    public boolean isCUSTOMERCODESpecified() {
        return localCUSTOMERCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCUSTOMERCODE() {
        return localCUSTOMERCODE;
    }

    /**
     * Auto generated setter method
     * @param param CUSTOMERCODE
     */
    public void setCUSTOMERCODE(java.lang.String param) {
        localCUSTOMERCODETracker = param != null;

        this.localCUSTOMERCODE = param;
    }

    public boolean isFIRSTNAME_NM_TPSpecified() {
        return localFIRSTNAME_NM_TPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFIRSTNAME_NM_TP() {
        return localFIRSTNAME_NM_TP;
    }

    /**
     * Auto generated setter method
     * @param param FIRSTNAME_NM_TP
     */
    public void setFIRSTNAME_NM_TP(java.lang.String param) {
        localFIRSTNAME_NM_TPTracker = param != null;

        this.localFIRSTNAME_NM_TP = param;
    }

    public boolean isMIDLLENAME_NM_TPSpecified() {
        return localMIDLLENAME_NM_TPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMIDLLENAME_NM_TP() {
        return localMIDLLENAME_NM_TP;
    }

    /**
     * Auto generated setter method
     * @param param MIDLLENAME_NM_TP
     */
    public void setMIDLLENAME_NM_TP(java.lang.String param) {
        localMIDLLENAME_NM_TPTracker = param != null;

        this.localMIDLLENAME_NM_TP = param;
    }

    public boolean isLASTNAME_NM_TPSpecified() {
        return localLASTNAME_NM_TPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLASTNAME_NM_TP() {
        return localLASTNAME_NM_TP;
    }

    /**
     * Auto generated setter method
     * @param param LASTNAME_NM_TP
     */
    public void setLASTNAME_NM_TP(java.lang.String param) {
        localLASTNAME_NM_TPTracker = param != null;

        this.localLASTNAME_NM_TP = param;
    }

    public boolean isADDMIDLLENAME_NM_TPSpecified() {
        return localADDMIDLLENAME_NM_TPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDMIDLLENAME_NM_TP() {
        return localADDMIDLLENAME_NM_TP;
    }

    /**
     * Auto generated setter method
     * @param param ADDMIDLLENAME_NM_TP
     */
    public void setADDMIDLLENAME_NM_TP(java.lang.String param) {
        localADDMIDLLENAME_NM_TPTracker = param != null;

        this.localADDMIDLLENAME_NM_TP = param;
    }

    public boolean isSUFFIX_NM_TPSpecified() {
        return localSUFFIX_NM_TPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUFFIX_NM_TP() {
        return localSUFFIX_NM_TP;
    }

    /**
     * Auto generated setter method
     * @param param SUFFIX_NM_TP
     */
    public void setSUFFIX_NM_TP(java.lang.String param) {
        localSUFFIX_NM_TPTracker = param != null;

        this.localSUFFIX_NM_TP = param;
    }

    public boolean isREPORTEDDATE_AG_IFSpecified() {
        return localREPORTEDDATE_AG_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_AG_IF() {
        return localREPORTEDDATE_AG_IF;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_AG_IF
     */
    public void setREPORTEDDATE_AG_IF(java.lang.String param) {
        localREPORTEDDATE_AG_IFTracker = param != null;

        this.localREPORTEDDATE_AG_IF = param;
    }

    public boolean isAGE_AG_IFSpecified() {
        return localAGE_AG_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE_AG_IF() {
        return localAGE_AG_IF;
    }

    /**
     * Auto generated setter method
     * @param param AGE_AG_IF
     */
    public void setAGE_AG_IF(java.lang.String param) {
        localAGE_AG_IFTracker = param != null;

        this.localAGE_AG_IF = param;
    }

    public boolean isREPORTEDDATE_AL_NMSpecified() {
        return localREPORTEDDATE_AL_NMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_AL_NM() {
        return localREPORTEDDATE_AL_NM;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_AL_NM
     */
    public void setREPORTEDDATE_AL_NM(java.lang.String param) {
        localREPORTEDDATE_AL_NMTracker = param != null;

        this.localREPORTEDDATE_AL_NM = param;
    }

    public boolean isALIASNAME_AL_NMSpecified() {
        return localALIASNAME_AL_NMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALIASNAME_AL_NM() {
        return localALIASNAME_AL_NM;
    }

    /**
     * Auto generated setter method
     * @param param ALIASNAME_AL_NM
     */
    public void setALIASNAME_AL_NM(java.lang.String param) {
        localALIASNAME_AL_NMTracker = param != null;

        this.localALIASNAME_AL_NM = param;
    }

    public boolean isADDITIONALNAMEINFO_AD_NM_IFSpecified() {
        return localADDITIONALNAMEINFO_AD_NM_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDITIONALNAMEINFO_AD_NM_IF() {
        return localADDITIONALNAMEINFO_AD_NM_IF;
    }

    /**
     * Auto generated setter method
     * @param param ADDITIONALNAMEINFO_AD_NM_IF
     */
    public void setADDITIONALNAMEINFO_AD_NM_IF(java.lang.String param) {
        localADDITIONALNAMEINFO_AD_NM_IFTracker = param != null;

        this.localADDITIONALNAMEINFO_AD_NM_IF = param;
    }

    public boolean isNOOFDEPENDENTS_AD_NM_IFSpecified() {
        return localNOOFDEPENDENTS_AD_NM_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFDEPENDENTS_AD_NM_IF() {
        return localNOOFDEPENDENTS_AD_NM_IF;
    }

    /**
     * Auto generated setter method
     * @param param NOOFDEPENDENTS_AD_NM_IF
     */
    public void setNOOFDEPENDENTS_AD_NM_IF(java.lang.String param) {
        localNOOFDEPENDENTS_AD_NM_IFTracker = param != null;

        this.localNOOFDEPENDENTS_AD_NM_IF = param;
    }

    public boolean isREPORTEDDATE_RS_ADDSpecified() {
        return localREPORTEDDATE_RS_ADDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_RS_ADD() {
        return localREPORTEDDATE_RS_ADD;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_RS_ADD
     */
    public void setREPORTEDDATE_RS_ADD(java.lang.String param) {
        localREPORTEDDATE_RS_ADDTracker = param != null;

        this.localREPORTEDDATE_RS_ADD = param;
    }

    public boolean isADDRESS_RS_ADDSpecified() {
        return localADDRESS_RS_ADDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_RS_ADD() {
        return localADDRESS_RS_ADD;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_RS_ADD
     */
    public void setADDRESS_RS_ADD(java.lang.String param) {
        localADDRESS_RS_ADDTracker = param != null;

        this.localADDRESS_RS_ADD = param;
    }

    public boolean isSTATE_RS_ADDSpecified() {
        return localSTATE_RS_ADDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATE_RS_ADD() {
        return localSTATE_RS_ADD;
    }

    /**
     * Auto generated setter method
     * @param param STATE_RS_ADD
     */
    public void setSTATE_RS_ADD(java.lang.String param) {
        localSTATE_RS_ADDTracker = param != null;

        this.localSTATE_RS_ADD = param;
    }

    public boolean isPOSTAL_RS_ADDSpecified() {
        return localPOSTAL_RS_ADDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPOSTAL_RS_ADD() {
        return localPOSTAL_RS_ADD;
    }

    /**
     * Auto generated setter method
     * @param param POSTAL_RS_ADD
     */
    public void setPOSTAL_RS_ADD(java.lang.String param) {
        localPOSTAL_RS_ADDTracker = param != null;

        this.localPOSTAL_RS_ADD = param;
    }

    public boolean isADDTYPE_RS_ADDSpecified() {
        return localADDTYPE_RS_ADDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDTYPE_RS_ADD() {
        return localADDTYPE_RS_ADD;
    }

    /**
     * Auto generated setter method
     * @param param ADDTYPE_RS_ADD
     */
    public void setADDTYPE_RS_ADD(java.lang.String param) {
        localADDTYPE_RS_ADDTracker = param != null;

        this.localADDTYPE_RS_ADD = param;
    }

    public boolean isIDTYPE_RS_IDSpecified() {
        return localIDTYPE_RS_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIDTYPE_RS_ID() {
        return localIDTYPE_RS_ID;
    }

    /**
     * Auto generated setter method
     * @param param IDTYPE_RS_ID
     */
    public void setIDTYPE_RS_ID(java.lang.String param) {
        localIDTYPE_RS_IDTracker = param != null;

        this.localIDTYPE_RS_ID = param;
    }

    public boolean isREPORTEDDATE_RS_IDSpecified() {
        return localREPORTEDDATE_RS_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_RS_ID() {
        return localREPORTEDDATE_RS_ID;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_RS_ID
     */
    public void setREPORTEDDATE_RS_ID(java.lang.String param) {
        localREPORTEDDATE_RS_IDTracker = param != null;

        this.localREPORTEDDATE_RS_ID = param;
    }

    public boolean isIDNUMBER_RS_IDSpecified() {
        return localIDNUMBER_RS_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIDNUMBER_RS_ID() {
        return localIDNUMBER_RS_ID;
    }

    /**
     * Auto generated setter method
     * @param param IDNUMBER_RS_ID
     */
    public void setIDNUMBER_RS_ID(java.lang.String param) {
        localIDNUMBER_RS_IDTracker = param != null;

        this.localIDNUMBER_RS_ID = param;
    }

    public boolean isEMAILREPORTEDDATE_RS_MLSpecified() {
        return localEMAILREPORTEDDATE_RS_MLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAILREPORTEDDATE_RS_ML() {
        return localEMAILREPORTEDDATE_RS_ML;
    }

    /**
     * Auto generated setter method
     * @param param EMAILREPORTEDDATE_RS_ML
     */
    public void setEMAILREPORTEDDATE_RS_ML(java.lang.String param) {
        localEMAILREPORTEDDATE_RS_MLTracker = param != null;

        this.localEMAILREPORTEDDATE_RS_ML = param;
    }

    public boolean isEMAILADDRESS_RS_MLSpecified() {
        return localEMAILADDRESS_RS_MLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAILADDRESS_RS_ML() {
        return localEMAILADDRESS_RS_ML;
    }

    /**
     * Auto generated setter method
     * @param param EMAILADDRESS_RS_ML
     */
    public void setEMAILADDRESS_RS_ML(java.lang.String param) {
        localEMAILADDRESS_RS_MLTracker = param != null;

        this.localEMAILADDRESS_RS_ML = param;
    }

    public boolean isEMPNMREPDATE_RS_EMDLSpecified() {
        return localEMPNMREPDATE_RS_EMDLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMPNMREPDATE_RS_EMDL() {
        return localEMPNMREPDATE_RS_EMDL;
    }

    /**
     * Auto generated setter method
     * @param param EMPNMREPDATE_RS_EMDL
     */
    public void setEMPNMREPDATE_RS_EMDL(java.lang.String param) {
        localEMPNMREPDATE_RS_EMDLTracker = param != null;

        this.localEMPNMREPDATE_RS_EMDL = param;
    }

    public boolean isEMPLOYERNAME_RS_EMDLSpecified() {
        return localEMPLOYERNAME_RS_EMDLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMPLOYERNAME_RS_EMDL() {
        return localEMPLOYERNAME_RS_EMDL;
    }

    /**
     * Auto generated setter method
     * @param param EMPLOYERNAME_RS_EMDL
     */
    public void setEMPLOYERNAME_RS_EMDL(java.lang.String param) {
        localEMPLOYERNAME_RS_EMDLTracker = param != null;

        this.localEMPLOYERNAME_RS_EMDL = param;
    }

    public boolean isEMPPOSITION_RS_EMDLSpecified() {
        return localEMPPOSITION_RS_EMDLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMPPOSITION_RS_EMDL() {
        return localEMPPOSITION_RS_EMDL;
    }

    /**
     * Auto generated setter method
     * @param param EMPPOSITION_RS_EMDL
     */
    public void setEMPPOSITION_RS_EMDL(java.lang.String param) {
        localEMPPOSITION_RS_EMDLTracker = param != null;

        this.localEMPPOSITION_RS_EMDL = param;
    }

    public boolean isEMPADDRESS_RS_EMDLSpecified() {
        return localEMPADDRESS_RS_EMDLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMPADDRESS_RS_EMDL() {
        return localEMPADDRESS_RS_EMDL;
    }

    /**
     * Auto generated setter method
     * @param param EMPADDRESS_RS_EMDL
     */
    public void setEMPADDRESS_RS_EMDL(java.lang.String param) {
        localEMPADDRESS_RS_EMDLTracker = param != null;

        this.localEMPADDRESS_RS_EMDL = param;
    }

    public boolean isDOB_RS_PER_IFSpecified() {
        return localDOB_RS_PER_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDOB_RS_PER_IF() {
        return localDOB_RS_PER_IF;
    }

    /**
     * Auto generated setter method
     * @param param DOB_RS_PER_IF
     */
    public void setDOB_RS_PER_IF(java.lang.String param) {
        localDOB_RS_PER_IFTracker = param != null;

        this.localDOB_RS_PER_IF = param;
    }

    public boolean isGENDER_RS_PER_IFSpecified() {
        return localGENDER_RS_PER_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDER_RS_PER_IF() {
        return localGENDER_RS_PER_IF;
    }

    /**
     * Auto generated setter method
     * @param param GENDER_RS_PER_IF
     */
    public void setGENDER_RS_PER_IF(java.lang.String param) {
        localGENDER_RS_PER_IFTracker = param != null;

        this.localGENDER_RS_PER_IF = param;
    }

    public boolean isTOTALINCOME_RS_PER_IFSpecified() {
        return localTOTALINCOME_RS_PER_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALINCOME_RS_PER_IF() {
        return localTOTALINCOME_RS_PER_IF;
    }

    /**
     * Auto generated setter method
     * @param param TOTALINCOME_RS_PER_IF
     */
    public void setTOTALINCOME_RS_PER_IF(java.lang.String param) {
        localTOTALINCOME_RS_PER_IFTracker = param != null;

        this.localTOTALINCOME_RS_PER_IF = param;
    }

    public boolean isOCCUPATION_RS_PER_IFSpecified() {
        return localOCCUPATION_RS_PER_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOCCUPATION_RS_PER_IF() {
        return localOCCUPATION_RS_PER_IF;
    }

    /**
     * Auto generated setter method
     * @param param OCCUPATION_RS_PER_IF
     */
    public void setOCCUPATION_RS_PER_IF(java.lang.String param) {
        localOCCUPATION_RS_PER_IFTracker = param != null;

        this.localOCCUPATION_RS_PER_IF = param;
    }

    public boolean isMARITALSTAUS_RS_PER_IFSpecified() {
        return localMARITALSTAUS_RS_PER_IFTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMARITALSTAUS_RS_PER_IF() {
        return localMARITALSTAUS_RS_PER_IF;
    }

    /**
     * Auto generated setter method
     * @param param MARITALSTAUS_RS_PER_IF
     */
    public void setMARITALSTAUS_RS_PER_IF(java.lang.String param) {
        localMARITALSTAUS_RS_PER_IFTracker = param != null;

        this.localMARITALSTAUS_RS_PER_IF = param;
    }

    public boolean isOCCUPATION_IC_DLSpecified() {
        return localOCCUPATION_IC_DLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOCCUPATION_IC_DL() {
        return localOCCUPATION_IC_DL;
    }

    /**
     * Auto generated setter method
     * @param param OCCUPATION_IC_DL
     */
    public void setOCCUPATION_IC_DL(java.lang.String param) {
        localOCCUPATION_IC_DLTracker = param != null;

        this.localOCCUPATION_IC_DL = param;
    }

    public boolean isMONTHLYINCOME_IC_DLSpecified() {
        return localMONTHLYINCOME_IC_DLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMONTHLYINCOME_IC_DL() {
        return localMONTHLYINCOME_IC_DL;
    }

    /**
     * Auto generated setter method
     * @param param MONTHLYINCOME_IC_DL
     */
    public void setMONTHLYINCOME_IC_DL(java.lang.String param) {
        localMONTHLYINCOME_IC_DLTracker = param != null;

        this.localMONTHLYINCOME_IC_DL = param;
    }

    public boolean isMONTHLYEXPENSE_IC_DLSpecified() {
        return localMONTHLYEXPENSE_IC_DLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMONTHLYEXPENSE_IC_DL() {
        return localMONTHLYEXPENSE_IC_DL;
    }

    /**
     * Auto generated setter method
     * @param param MONTHLYEXPENSE_IC_DL
     */
    public void setMONTHLYEXPENSE_IC_DL(java.lang.String param) {
        localMONTHLYEXPENSE_IC_DLTracker = param != null;

        this.localMONTHLYEXPENSE_IC_DL = param;
    }

    public boolean isPOVERTYINDEX_IC_DLSpecified() {
        return localPOVERTYINDEX_IC_DLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPOVERTYINDEX_IC_DL() {
        return localPOVERTYINDEX_IC_DL;
    }

    /**
     * Auto generated setter method
     * @param param POVERTYINDEX_IC_DL
     */
    public void setPOVERTYINDEX_IC_DL(java.lang.String param) {
        localPOVERTYINDEX_IC_DLTracker = param != null;

        this.localPOVERTYINDEX_IC_DL = param;
    }

    public boolean isASSETOWNERSHIP_IC_DLSpecified() {
        return localASSETOWNERSHIP_IC_DLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getASSETOWNERSHIP_IC_DL() {
        return localASSETOWNERSHIP_IC_DL;
    }

    /**
     * Auto generated setter method
     * @param param ASSETOWNERSHIP_IC_DL
     */
    public void setASSETOWNERSHIP_IC_DL(java.lang.String param) {
        localASSETOWNERSHIP_IC_DLTracker = param != null;

        this.localASSETOWNERSHIP_IC_DL = param;
    }

    public boolean isREPORTEDDATE_RS_PHSpecified() {
        return localREPORTEDDATE_RS_PHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_RS_PH() {
        return localREPORTEDDATE_RS_PH;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_RS_PH
     */
    public void setREPORTEDDATE_RS_PH(java.lang.String param) {
        localREPORTEDDATE_RS_PHTracker = param != null;

        this.localREPORTEDDATE_RS_PH = param;
    }

    public boolean isTYPECODE_RS_PHSpecified() {
        return localTYPECODE_RS_PHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTYPECODE_RS_PH() {
        return localTYPECODE_RS_PH;
    }

    /**
     * Auto generated setter method
     * @param param TYPECODE_RS_PH
     */
    public void setTYPECODE_RS_PH(java.lang.String param) {
        localTYPECODE_RS_PHTracker = param != null;

        this.localTYPECODE_RS_PH = param;
    }

    public boolean isCOUNTRYCODE_RS_PHSpecified() {
        return localCOUNTRYCODE_RS_PHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOUNTRYCODE_RS_PH() {
        return localCOUNTRYCODE_RS_PH;
    }

    /**
     * Auto generated setter method
     * @param param COUNTRYCODE_RS_PH
     */
    public void setCOUNTRYCODE_RS_PH(java.lang.String param) {
        localCOUNTRYCODE_RS_PHTracker = param != null;

        this.localCOUNTRYCODE_RS_PH = param;
    }

    public boolean isAREACODE_RS_PHSpecified() {
        return localAREACODE_RS_PHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAREACODE_RS_PH() {
        return localAREACODE_RS_PH;
    }

    /**
     * Auto generated setter method
     * @param param AREACODE_RS_PH
     */
    public void setAREACODE_RS_PH(java.lang.String param) {
        localAREACODE_RS_PHTracker = param != null;

        this.localAREACODE_RS_PH = param;
    }

    public boolean isPHNUMBER_RS_PHSpecified() {
        return localPHNUMBER_RS_PHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHNUMBER_RS_PH() {
        return localPHNUMBER_RS_PH;
    }

    /**
     * Auto generated setter method
     * @param param PHNUMBER_RS_PH
     */
    public void setPHNUMBER_RS_PH(java.lang.String param) {
        localPHNUMBER_RS_PHTracker = param != null;

        this.localPHNUMBER_RS_PH = param;
    }

    public boolean isPHNOEXTENTION_RS_PHSpecified() {
        return localPHNOEXTENTION_RS_PHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHNOEXTENTION_RS_PH() {
        return localPHNOEXTENTION_RS_PH;
    }

    /**
     * Auto generated setter method
     * @param param PHNOEXTENTION_RS_PH
     */
    public void setPHNOEXTENTION_RS_PH(java.lang.String param) {
        localPHNOEXTENTION_RS_PHTracker = param != null;

        this.localPHNOEXTENTION_RS_PH = param;
    }

    public boolean isCUSTOMERCODE_RS_HDSpecified() {
        return localCUSTOMERCODE_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCUSTOMERCODE_RS_HD() {
        return localCUSTOMERCODE_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param CUSTOMERCODE_RS_HD
     */
    public void setCUSTOMERCODE_RS_HD(java.lang.String param) {
        localCUSTOMERCODE_RS_HDTracker = param != null;

        this.localCUSTOMERCODE_RS_HD = param;
    }

    public boolean isCLIENTID_RS_HDSpecified() {
        return localCLIENTID_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCLIENTID_RS_HD() {
        return localCLIENTID_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param CLIENTID_RS_HD
     */
    public void setCLIENTID_RS_HD(java.lang.String param) {
        localCLIENTID_RS_HDTracker = param != null;

        this.localCLIENTID_RS_HD = param;
    }

    public boolean isCUSTREFFIELD_RS_HDSpecified() {
        return localCUSTREFFIELD_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCUSTREFFIELD_RS_HD() {
        return localCUSTREFFIELD_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param CUSTREFFIELD_RS_HD
     */
    public void setCUSTREFFIELD_RS_HD(java.lang.String param) {
        localCUSTREFFIELD_RS_HDTracker = param != null;

        this.localCUSTREFFIELD_RS_HD = param;
    }

    public boolean isREPORTORDERNO_RS_HDSpecified() {
        return localREPORTORDERNO_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTORDERNO_RS_HD() {
        return localREPORTORDERNO_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param REPORTORDERNO_RS_HD
     */
    public void setREPORTORDERNO_RS_HD(java.lang.String param) {
        localREPORTORDERNO_RS_HDTracker = param != null;

        this.localREPORTORDERNO_RS_HD = param;
    }

    public boolean isPRODUCTCODE_RS_HDSpecified() {
        return localPRODUCTCODE_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPRODUCTCODE_RS_HD() {
        return localPRODUCTCODE_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param PRODUCTCODE_RS_HD
     */
    public void setPRODUCTCODE_RS_HD(java.lang.String param) {
        localPRODUCTCODE_RS_HDTracker = param != null;

        this.localPRODUCTCODE_RS_HD = param;
    }

    public boolean isPRODUCTVERSION_RS_HDSpecified() {
        return localPRODUCTVERSION_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPRODUCTVERSION_RS_HD() {
        return localPRODUCTVERSION_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param PRODUCTVERSION_RS_HD
     */
    public void setPRODUCTVERSION_RS_HD(java.lang.String param) {
        localPRODUCTVERSION_RS_HDTracker = param != null;

        this.localPRODUCTVERSION_RS_HD = param;
    }

    public boolean isSUCCESSCODE_RS_HDSpecified() {
        return localSUCCESSCODE_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUCCESSCODE_RS_HD() {
        return localSUCCESSCODE_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param SUCCESSCODE_RS_HD
     */
    public void setSUCCESSCODE_RS_HD(java.lang.String param) {
        localSUCCESSCODE_RS_HDTracker = param != null;

        this.localSUCCESSCODE_RS_HD = param;
    }

    public boolean isMATCHTYPE_RS_HDSpecified() {
        return localMATCHTYPE_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMATCHTYPE_RS_HD() {
        return localMATCHTYPE_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param MATCHTYPE_RS_HD
     */
    public void setMATCHTYPE_RS_HD(java.lang.String param) {
        localMATCHTYPE_RS_HDTracker = param != null;

        this.localMATCHTYPE_RS_HD = param;
    }

    public boolean isRESDATE_RS_HDSpecified() {
        return localRESDATE_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESDATE_RS_HD() {
        return localRESDATE_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param RESDATE_RS_HD
     */
    public void setRESDATE_RS_HD(java.lang.String param) {
        localRESDATE_RS_HDTracker = param != null;

        this.localRESDATE_RS_HD = param;
    }

    public boolean isRESTIME_RS_HDSpecified() {
        return localRESTIME_RS_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESTIME_RS_HD() {
        return localRESTIME_RS_HD;
    }

    /**
     * Auto generated setter method
     * @param param RESTIME_RS_HD
     */
    public void setRESTIME_RS_HD(java.lang.String param) {
        localRESTIME_RS_HDTracker = param != null;

        this.localRESTIME_RS_HD = param;
    }

    public boolean isREPORTEDDATE_RS_ACSpecified() {
        return localREPORTEDDATE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_RS_AC() {
        return localREPORTEDDATE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_RS_AC
     */
    public void setREPORTEDDATE_RS_AC(java.lang.String param) {
        localREPORTEDDATE_RS_ACTracker = param != null;

        this.localREPORTEDDATE_RS_AC = param;
    }

    public boolean isCLIENTNAME_RS_ACSpecified() {
        return localCLIENTNAME_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCLIENTNAME_RS_AC() {
        return localCLIENTNAME_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param CLIENTNAME_RS_AC
     */
    public void setCLIENTNAME_RS_AC(java.lang.String param) {
        localCLIENTNAME_RS_ACTracker = param != null;

        this.localCLIENTNAME_RS_AC = param;
    }

    public boolean isACCOUNTNUMBER_RS_ACSpecified() {
        return localACCOUNTNUMBER_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTNUMBER_RS_AC() {
        return localACCOUNTNUMBER_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTNUMBER_RS_AC
     */
    public void setACCOUNTNUMBER_RS_AC(java.lang.String param) {
        localACCOUNTNUMBER_RS_ACTracker = param != null;

        this.localACCOUNTNUMBER_RS_AC = param;
    }

    public boolean isCURRENTBALANCE_RS_ACSpecified() {
        return localCURRENTBALANCE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENTBALANCE_RS_AC() {
        return localCURRENTBALANCE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param CURRENTBALANCE_RS_AC
     */
    public void setCURRENTBALANCE_RS_AC(java.lang.String param) {
        localCURRENTBALANCE_RS_ACTracker = param != null;

        this.localCURRENTBALANCE_RS_AC = param;
    }

    public boolean isINSTITUTION_RS_ACSpecified() {
        return localINSTITUTION_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINSTITUTION_RS_AC() {
        return localINSTITUTION_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param INSTITUTION_RS_AC
     */
    public void setINSTITUTION_RS_AC(java.lang.String param) {
        localINSTITUTION_RS_ACTracker = param != null;

        this.localINSTITUTION_RS_AC = param;
    }

    public boolean isACCOUNTTYPE_RS_ACSpecified() {
        return localACCOUNTTYPE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTTYPE_RS_AC() {
        return localACCOUNTTYPE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTTYPE_RS_AC
     */
    public void setACCOUNTTYPE_RS_AC(java.lang.String param) {
        localACCOUNTTYPE_RS_ACTracker = param != null;

        this.localACCOUNTTYPE_RS_AC = param;
    }

    public boolean isOWNERSHIPTYPE_RS_ACSpecified() {
        return localOWNERSHIPTYPE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNERSHIPTYPE_RS_AC() {
        return localOWNERSHIPTYPE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param OWNERSHIPTYPE_RS_AC
     */
    public void setOWNERSHIPTYPE_RS_AC(java.lang.String param) {
        localOWNERSHIPTYPE_RS_ACTracker = param != null;

        this.localOWNERSHIPTYPE_RS_AC = param;
    }

    public boolean isBALANCE_RS_ACSpecified() {
        return localBALANCE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBALANCE_RS_AC() {
        return localBALANCE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param BALANCE_RS_AC
     */
    public void setBALANCE_RS_AC(java.lang.String param) {
        localBALANCE_RS_ACTracker = param != null;

        this.localBALANCE_RS_AC = param;
    }

    public boolean isPASTDUEAMT_RS_ACSpecified() {
        return localPASTDUEAMT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASTDUEAMT_RS_AC() {
        return localPASTDUEAMT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param PASTDUEAMT_RS_AC
     */
    public void setPASTDUEAMT_RS_AC(java.lang.String param) {
        localPASTDUEAMT_RS_ACTracker = param != null;

        this.localPASTDUEAMT_RS_AC = param;
    }

    public boolean isDISBURSEDAMOUNT_RS_ACSpecified() {
        return localDISBURSEDAMOUNT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISBURSEDAMOUNT_RS_AC() {
        return localDISBURSEDAMOUNT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DISBURSEDAMOUNT_RS_AC
     */
    public void setDISBURSEDAMOUNT_RS_AC(java.lang.String param) {
        localDISBURSEDAMOUNT_RS_ACTracker = param != null;

        this.localDISBURSEDAMOUNT_RS_AC = param;
    }

    public boolean isLOANCATEGORY_RS_ACSpecified() {
        return localLOANCATEGORY_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOANCATEGORY_RS_AC() {
        return localLOANCATEGORY_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param LOANCATEGORY_RS_AC
     */
    public void setLOANCATEGORY_RS_AC(java.lang.String param) {
        localLOANCATEGORY_RS_ACTracker = param != null;

        this.localLOANCATEGORY_RS_AC = param;
    }

    public boolean isLOANPURPOSE_RS_ACSpecified() {
        return localLOANPURPOSE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOANPURPOSE_RS_AC() {
        return localLOANPURPOSE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param LOANPURPOSE_RS_AC
     */
    public void setLOANPURPOSE_RS_AC(java.lang.String param) {
        localLOANPURPOSE_RS_ACTracker = param != null;

        this.localLOANPURPOSE_RS_AC = param;
    }

    public boolean isLASTPAYMENT_RS_ACSpecified() {
        return localLASTPAYMENT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLASTPAYMENT_RS_AC() {
        return localLASTPAYMENT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param LASTPAYMENT_RS_AC
     */
    public void setLASTPAYMENT_RS_AC(java.lang.String param) {
        localLASTPAYMENT_RS_ACTracker = param != null;

        this.localLASTPAYMENT_RS_AC = param;
    }

    public boolean isWRITEOFFAMT_RS_ACSpecified() {
        return localWRITEOFFAMT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITEOFFAMT_RS_AC() {
        return localWRITEOFFAMT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param WRITEOFFAMT_RS_AC
     */
    public void setWRITEOFFAMT_RS_AC(java.lang.String param) {
        localWRITEOFFAMT_RS_ACTracker = param != null;

        this.localWRITEOFFAMT_RS_AC = param;
    }

    public boolean isACCOPEN_RS_ACSpecified() {
        return localACCOPEN_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOPEN_RS_AC() {
        return localACCOPEN_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param ACCOPEN_RS_AC
     */
    public void setACCOPEN_RS_AC(java.lang.String param) {
        localACCOPEN_RS_ACTracker = param != null;

        this.localACCOPEN_RS_AC = param;
    }

    public boolean isSACTIONAMT_RS_ACSpecified() {
        return localSACTIONAMT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSACTIONAMT_RS_AC() {
        return localSACTIONAMT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param SACTIONAMT_RS_AC
     */
    public void setSACTIONAMT_RS_AC(java.lang.String param) {
        localSACTIONAMT_RS_ACTracker = param != null;

        this.localSACTIONAMT_RS_AC = param;
    }

    public boolean isLASTPAYMENTDATE_RS_ACSpecified() {
        return localLASTPAYMENTDATE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLASTPAYMENTDATE_RS_AC() {
        return localLASTPAYMENTDATE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param LASTPAYMENTDATE_RS_AC
     */
    public void setLASTPAYMENTDATE_RS_AC(java.lang.String param) {
        localLASTPAYMENTDATE_RS_ACTracker = param != null;

        this.localLASTPAYMENTDATE_RS_AC = param;
    }

    public boolean isHIGHCREDIT_RS_ACSpecified() {
        return localHIGHCREDIT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHIGHCREDIT_RS_AC() {
        return localHIGHCREDIT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param HIGHCREDIT_RS_AC
     */
    public void setHIGHCREDIT_RS_AC(java.lang.String param) {
        localHIGHCREDIT_RS_ACTracker = param != null;

        this.localHIGHCREDIT_RS_AC = param;
    }

    public boolean isDATEREPORTED_RS_ACSpecified() {
        return localDATEREPORTED_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEREPORTED_RS_AC() {
        return localDATEREPORTED_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DATEREPORTED_RS_AC
     */
    public void setDATEREPORTED_RS_AC(java.lang.String param) {
        localDATEREPORTED_RS_ACTracker = param != null;

        this.localDATEREPORTED_RS_AC = param;
    }

    public boolean isDATEOPENED_RS_ACSpecified() {
        return localDATEOPENED_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOPENED_RS_AC() {
        return localDATEOPENED_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DATEOPENED_RS_AC
     */
    public void setDATEOPENED_RS_AC(java.lang.String param) {
        localDATEOPENED_RS_ACTracker = param != null;

        this.localDATEOPENED_RS_AC = param;
    }

    public boolean isDATECLOSED_RS_ACSpecified() {
        return localDATECLOSED_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATECLOSED_RS_AC() {
        return localDATECLOSED_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DATECLOSED_RS_AC
     */
    public void setDATECLOSED_RS_AC(java.lang.String param) {
        localDATECLOSED_RS_ACTracker = param != null;

        this.localDATECLOSED_RS_AC = param;
    }

    public boolean isREASON_RS_ACSpecified() {
        return localREASON_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_RS_AC() {
        return localREASON_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param REASON_RS_AC
     */
    public void setREASON_RS_AC(java.lang.String param) {
        localREASON_RS_ACTracker = param != null;

        this.localREASON_RS_AC = param;
    }

    public boolean isDATEWRITTENOFF_RS_ACSpecified() {
        return localDATEWRITTENOFF_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEWRITTENOFF_RS_AC() {
        return localDATEWRITTENOFF_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DATEWRITTENOFF_RS_AC
     */
    public void setDATEWRITTENOFF_RS_AC(java.lang.String param) {
        localDATEWRITTENOFF_RS_ACTracker = param != null;

        this.localDATEWRITTENOFF_RS_AC = param;
    }

    public boolean isLOANCYCLEID_RS_ACSpecified() {
        return localLOANCYCLEID_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOANCYCLEID_RS_AC() {
        return localLOANCYCLEID_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param LOANCYCLEID_RS_AC
     */
    public void setLOANCYCLEID_RS_AC(java.lang.String param) {
        localLOANCYCLEID_RS_ACTracker = param != null;

        this.localLOANCYCLEID_RS_AC = param;
    }

    public boolean isDATESANCTIONED_RS_ACSpecified() {
        return localDATESANCTIONED_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATESANCTIONED_RS_AC() {
        return localDATESANCTIONED_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DATESANCTIONED_RS_AC
     */
    public void setDATESANCTIONED_RS_AC(java.lang.String param) {
        localDATESANCTIONED_RS_ACTracker = param != null;

        this.localDATESANCTIONED_RS_AC = param;
    }

    public boolean isDATEAPPLIED_RS_ACSpecified() {
        return localDATEAPPLIED_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEAPPLIED_RS_AC() {
        return localDATEAPPLIED_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DATEAPPLIED_RS_AC
     */
    public void setDATEAPPLIED_RS_AC(java.lang.String param) {
        localDATEAPPLIED_RS_ACTracker = param != null;

        this.localDATEAPPLIED_RS_AC = param;
    }

    public boolean isINTRESTRATE_RS_ACSpecified() {
        return localINTRESTRATE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINTRESTRATE_RS_AC() {
        return localINTRESTRATE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param INTRESTRATE_RS_AC
     */
    public void setINTRESTRATE_RS_AC(java.lang.String param) {
        localINTRESTRATE_RS_ACTracker = param != null;

        this.localINTRESTRATE_RS_AC = param;
    }

    public boolean isAPPLIEDAMOUNT_RS_ACSpecified() {
        return localAPPLIEDAMOUNT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAPPLIEDAMOUNT_RS_AC() {
        return localAPPLIEDAMOUNT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param APPLIEDAMOUNT_RS_AC
     */
    public void setAPPLIEDAMOUNT_RS_AC(java.lang.String param) {
        localAPPLIEDAMOUNT_RS_ACTracker = param != null;

        this.localAPPLIEDAMOUNT_RS_AC = param;
    }

    public boolean isNOOFINSTALLMENTS_RS_ACSpecified() {
        return localNOOFINSTALLMENTS_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFINSTALLMENTS_RS_AC() {
        return localNOOFINSTALLMENTS_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param NOOFINSTALLMENTS_RS_AC
     */
    public void setNOOFINSTALLMENTS_RS_AC(java.lang.String param) {
        localNOOFINSTALLMENTS_RS_ACTracker = param != null;

        this.localNOOFINSTALLMENTS_RS_AC = param;
    }

    public boolean isREPAYMENTTENURE_RS_ACSpecified() {
        return localREPAYMENTTENURE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPAYMENTTENURE_RS_AC() {
        return localREPAYMENTTENURE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param REPAYMENTTENURE_RS_AC
     */
    public void setREPAYMENTTENURE_RS_AC(java.lang.String param) {
        localREPAYMENTTENURE_RS_ACTracker = param != null;

        this.localREPAYMENTTENURE_RS_AC = param;
    }

    public boolean isDISPUTECODE_RS_ACSpecified() {
        return localDISPUTECODE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISPUTECODE_RS_AC() {
        return localDISPUTECODE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param DISPUTECODE_RS_AC
     */
    public void setDISPUTECODE_RS_AC(java.lang.String param) {
        localDISPUTECODE_RS_ACTracker = param != null;

        this.localDISPUTECODE_RS_AC = param;
    }

    public boolean isINSTALLMENTAMT_RS_ACSpecified() {
        return localINSTALLMENTAMT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINSTALLMENTAMT_RS_AC() {
        return localINSTALLMENTAMT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param INSTALLMENTAMT_RS_AC
     */
    public void setINSTALLMENTAMT_RS_AC(java.lang.String param) {
        localINSTALLMENTAMT_RS_ACTracker = param != null;

        this.localINSTALLMENTAMT_RS_AC = param;
    }

    public boolean isKEYPERSON_RS_ACSpecified() {
        return localKEYPERSON_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getKEYPERSON_RS_AC() {
        return localKEYPERSON_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param KEYPERSON_RS_AC
     */
    public void setKEYPERSON_RS_AC(java.lang.String param) {
        localKEYPERSON_RS_ACTracker = param != null;

        this.localKEYPERSON_RS_AC = param;
    }

    public boolean isNOMINEE_RS_ACSpecified() {
        return localNOMINEE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOMINEE_RS_AC() {
        return localNOMINEE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param NOMINEE_RS_AC
     */
    public void setNOMINEE_RS_AC(java.lang.String param) {
        localNOMINEE_RS_ACTracker = param != null;

        this.localNOMINEE_RS_AC = param;
    }

    public boolean isTERMFREQUENCY_RS_ACSpecified() {
        return localTERMFREQUENCY_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTERMFREQUENCY_RS_AC() {
        return localTERMFREQUENCY_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param TERMFREQUENCY_RS_AC
     */
    public void setTERMFREQUENCY_RS_AC(java.lang.String param) {
        localTERMFREQUENCY_RS_ACTracker = param != null;

        this.localTERMFREQUENCY_RS_AC = param;
    }

    public boolean isCREDITLIMIT_RS_ACSpecified() {
        return localCREDITLIMIT_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITLIMIT_RS_AC() {
        return localCREDITLIMIT_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param CREDITLIMIT_RS_AC
     */
    public void setCREDITLIMIT_RS_AC(java.lang.String param) {
        localCREDITLIMIT_RS_ACTracker = param != null;

        this.localCREDITLIMIT_RS_AC = param;
    }

    public boolean isCOLLATERALVALUE_RS_ACSpecified() {
        return localCOLLATERALVALUE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOLLATERALVALUE_RS_AC() {
        return localCOLLATERALVALUE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param COLLATERALVALUE_RS_AC
     */
    public void setCOLLATERALVALUE_RS_AC(java.lang.String param) {
        localCOLLATERALVALUE_RS_ACTracker = param != null;

        this.localCOLLATERALVALUE_RS_AC = param;
    }

    public boolean isCOLLATERALTYPE_RS_ACSpecified() {
        return localCOLLATERALTYPE_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOLLATERALTYPE_RS_AC() {
        return localCOLLATERALTYPE_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param COLLATERALTYPE_RS_AC
     */
    public void setCOLLATERALTYPE_RS_AC(java.lang.String param) {
        localCOLLATERALTYPE_RS_ACTracker = param != null;

        this.localCOLLATERALTYPE_RS_AC = param;
    }

    public boolean isACCOUNTSTATUS_RS_ACSpecified() {
        return localACCOUNTSTATUS_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTSTATUS_RS_AC() {
        return localACCOUNTSTATUS_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTSTATUS_RS_AC
     */
    public void setACCOUNTSTATUS_RS_AC(java.lang.String param) {
        localACCOUNTSTATUS_RS_ACTracker = param != null;

        this.localACCOUNTSTATUS_RS_AC = param;
    }

    public boolean isASSETCLASSIFICATION_RS_ACSpecified() {
        return localASSETCLASSIFICATION_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getASSETCLASSIFICATION_RS_AC() {
        return localASSETCLASSIFICATION_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param ASSETCLASSIFICATION_RS_AC
     */
    public void setASSETCLASSIFICATION_RS_AC(java.lang.String param) {
        localASSETCLASSIFICATION_RS_ACTracker = param != null;

        this.localASSETCLASSIFICATION_RS_AC = param;
    }

    public boolean isSUITFILEDSTATUS_RS_ACSpecified() {
        return localSUITFILEDSTATUS_RS_ACTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUITFILEDSTATUS_RS_AC() {
        return localSUITFILEDSTATUS_RS_AC;
    }

    /**
     * Auto generated setter method
     * @param param SUITFILEDSTATUS_RS_AC
     */
    public void setSUITFILEDSTATUS_RS_AC(java.lang.String param) {
        localSUITFILEDSTATUS_RS_ACTracker = param != null;

        this.localSUITFILEDSTATUS_RS_AC = param;
    }

    public boolean isMONTHKEY_RS_AC_HIS24Specified() {
        return localMONTHKEY_RS_AC_HIS24Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMONTHKEY_RS_AC_HIS24() {
        return localMONTHKEY_RS_AC_HIS24;
    }

    /**
     * Auto generated setter method
     * @param param MONTHKEY_RS_AC_HIS24
     */
    public void setMONTHKEY_RS_AC_HIS24(java.lang.String param) {
        localMONTHKEY_RS_AC_HIS24Tracker = param != null;

        this.localMONTHKEY_RS_AC_HIS24 = param;
    }

    public boolean isPAYMENTSTATUS_RS_AC_HIS24Specified() {
        return localPAYMENTSTATUS_RS_AC_HIS24Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENTSTATUS_RS_AC_HIS24() {
        return localPAYMENTSTATUS_RS_AC_HIS24;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENTSTATUS_RS_AC_HIS24
     */
    public void setPAYMENTSTATUS_RS_AC_HIS24(java.lang.String param) {
        localPAYMENTSTATUS_RS_AC_HIS24Tracker = param != null;

        this.localPAYMENTSTATUS_RS_AC_HIS24 = param;
    }

    public boolean isSUITFILEDSTATUS_RS_AC_HIS24Specified() {
        return localSUITFILEDSTATUS_RS_AC_HIS24Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUITFILEDSTATUS_RS_AC_HIS24() {
        return localSUITFILEDSTATUS_RS_AC_HIS24;
    }

    /**
     * Auto generated setter method
     * @param param SUITFILEDSTATUS_RS_AC_HIS24
     */
    public void setSUITFILEDSTATUS_RS_AC_HIS24(java.lang.String param) {
        localSUITFILEDSTATUS_RS_AC_HIS24Tracker = param != null;

        this.localSUITFILEDSTATUS_RS_AC_HIS24 = param;
    }

    public boolean isASSETTCLSSTATUS_RS_AC_HIS24Specified() {
        return localASSETTCLSSTATUS_RS_AC_HIS24Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getASSETTCLSSTATUS_RS_AC_HIS24() {
        return localASSETTCLSSTATUS_RS_AC_HIS24;
    }

    /**
     * Auto generated setter method
     * @param param ASSETTCLSSTATUS_RS_AC_HIS24
     */
    public void setASSETTCLSSTATUS_RS_AC_HIS24(java.lang.String param) {
        localASSETTCLSSTATUS_RS_AC_HIS24Tracker = param != null;

        this.localASSETTCLSSTATUS_RS_AC_HIS24 = param;
    }

    public boolean isMONTHKEY_RS_AC_HIS48Specified() {
        return localMONTHKEY_RS_AC_HIS48Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMONTHKEY_RS_AC_HIS48() {
        return localMONTHKEY_RS_AC_HIS48;
    }

    /**
     * Auto generated setter method
     * @param param MONTHKEY_RS_AC_HIS48
     */
    public void setMONTHKEY_RS_AC_HIS48(java.lang.String param) {
        localMONTHKEY_RS_AC_HIS48Tracker = param != null;

        this.localMONTHKEY_RS_AC_HIS48 = param;
    }

    public boolean isPAYMENTSTATUS_RS_AC_HIS48Specified() {
        return localPAYMENTSTATUS_RS_AC_HIS48Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENTSTATUS_RS_AC_HIS48() {
        return localPAYMENTSTATUS_RS_AC_HIS48;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENTSTATUS_RS_AC_HIS48
     */
    public void setPAYMENTSTATUS_RS_AC_HIS48(java.lang.String param) {
        localPAYMENTSTATUS_RS_AC_HIS48Tracker = param != null;

        this.localPAYMENTSTATUS_RS_AC_HIS48 = param;
    }

    public boolean isSUITFILEDSTATUS_RS_AC_HIS48Specified() {
        return localSUITFILEDSTATUS_RS_AC_HIS48Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUITFILEDSTATUS_RS_AC_HIS48() {
        return localSUITFILEDSTATUS_RS_AC_HIS48;
    }

    /**
     * Auto generated setter method
     * @param param SUITFILEDSTATUS_RS_AC_HIS48
     */
    public void setSUITFILEDSTATUS_RS_AC_HIS48(java.lang.String param) {
        localSUITFILEDSTATUS_RS_AC_HIS48Tracker = param != null;

        this.localSUITFILEDSTATUS_RS_AC_HIS48 = param;
    }

    public boolean isASSETCLSSTATUS_RS_AC_HIS48Specified() {
        return localASSETCLSSTATUS_RS_AC_HIS48Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getASSETCLSSTATUS_RS_AC_HIS48() {
        return localASSETCLSSTATUS_RS_AC_HIS48;
    }

    /**
     * Auto generated setter method
     * @param param ASSETCLSSTATUS_RS_AC_HIS48
     */
    public void setASSETCLSSTATUS_RS_AC_HIS48(java.lang.String param) {
        localASSETCLSSTATUS_RS_AC_HIS48Tracker = param != null;

        this.localASSETCLSSTATUS_RS_AC_HIS48 = param;
    }

    public boolean isNOOFACCOUNTS_RS_AC_SUMSpecified() {
        return localNOOFACCOUNTS_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFACCOUNTS_RS_AC_SUM() {
        return localNOOFACCOUNTS_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param NOOFACCOUNTS_RS_AC_SUM
     */
    public void setNOOFACCOUNTS_RS_AC_SUM(java.lang.String param) {
        localNOOFACCOUNTS_RS_AC_SUMTracker = param != null;

        this.localNOOFACCOUNTS_RS_AC_SUM = param;
    }

    public boolean isNOOFACTIVEACCOUNTS_RS_AC_SUMSpecified() {
        return localNOOFACTIVEACCOUNTS_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFACTIVEACCOUNTS_RS_AC_SUM() {
        return localNOOFACTIVEACCOUNTS_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param NOOFACTIVEACCOUNTS_RS_AC_SUM
     */
    public void setNOOFACTIVEACCOUNTS_RS_AC_SUM(java.lang.String param) {
        localNOOFACTIVEACCOUNTS_RS_AC_SUMTracker = param != null;

        this.localNOOFACTIVEACCOUNTS_RS_AC_SUM = param;
    }

    public boolean isNOOFWRITEOFFS_RS_AC_SUMSpecified() {
        return localNOOFWRITEOFFS_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFWRITEOFFS_RS_AC_SUM() {
        return localNOOFWRITEOFFS_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param NOOFWRITEOFFS_RS_AC_SUM
     */
    public void setNOOFWRITEOFFS_RS_AC_SUM(java.lang.String param) {
        localNOOFWRITEOFFS_RS_AC_SUMTracker = param != null;

        this.localNOOFWRITEOFFS_RS_AC_SUM = param;
    }

    public boolean isTOTALPASTDUE_RS_AC_SUMSpecified() {
        return localTOTALPASTDUE_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALPASTDUE_RS_AC_SUM() {
        return localTOTALPASTDUE_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALPASTDUE_RS_AC_SUM
     */
    public void setTOTALPASTDUE_RS_AC_SUM(java.lang.String param) {
        localTOTALPASTDUE_RS_AC_SUMTracker = param != null;

        this.localTOTALPASTDUE_RS_AC_SUM = param;
    }

    public boolean isMOSTSRVRSTAWHIN24MON_RS_AC_SUMSpecified() {
        return localMOSTSRVRSTAWHIN24MON_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOSTSRVRSTAWHIN24MON_RS_AC_SUM() {
        return localMOSTSRVRSTAWHIN24MON_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param MOSTSRVRSTAWHIN24MON_RS_AC_SUM
     */
    public void setMOSTSRVRSTAWHIN24MON_RS_AC_SUM(java.lang.String param) {
        localMOSTSRVRSTAWHIN24MON_RS_AC_SUMTracker = param != null;

        this.localMOSTSRVRSTAWHIN24MON_RS_AC_SUM = param;
    }

    public boolean isSINGLEHIGHESTCREDIT_RS_AC_SUMSpecified() {
        return localSINGLEHIGHESTCREDIT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSINGLEHIGHESTCREDIT_RS_AC_SUM() {
        return localSINGLEHIGHESTCREDIT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param SINGLEHIGHESTCREDIT_RS_AC_SUM
     */
    public void setSINGLEHIGHESTCREDIT_RS_AC_SUM(java.lang.String param) {
        localSINGLEHIGHESTCREDIT_RS_AC_SUMTracker = param != null;

        this.localSINGLEHIGHESTCREDIT_RS_AC_SUM = param;
    }

    public boolean isSINGLEHIGHSANCTAMT_RS_AC_SUMSpecified() {
        return localSINGLEHIGHSANCTAMT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSINGLEHIGHSANCTAMT_RS_AC_SUM() {
        return localSINGLEHIGHSANCTAMT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param SINGLEHIGHSANCTAMT_RS_AC_SUM
     */
    public void setSINGLEHIGHSANCTAMT_RS_AC_SUM(java.lang.String param) {
        localSINGLEHIGHSANCTAMT_RS_AC_SUMTracker = param != null;

        this.localSINGLEHIGHSANCTAMT_RS_AC_SUM = param;
    }

    public boolean isTOTALHIGHCREDIT_RS_AC_SUMSpecified() {
        return localTOTALHIGHCREDIT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALHIGHCREDIT_RS_AC_SUM() {
        return localTOTALHIGHCREDIT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALHIGHCREDIT_RS_AC_SUM
     */
    public void setTOTALHIGHCREDIT_RS_AC_SUM(java.lang.String param) {
        localTOTALHIGHCREDIT_RS_AC_SUMTracker = param != null;

        this.localTOTALHIGHCREDIT_RS_AC_SUM = param;
    }

    public boolean isAVERAGEOPENBALANCE_RS_AC_SUMSpecified() {
        return localAVERAGEOPENBALANCE_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAVERAGEOPENBALANCE_RS_AC_SUM() {
        return localAVERAGEOPENBALANCE_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param AVERAGEOPENBALANCE_RS_AC_SUM
     */
    public void setAVERAGEOPENBALANCE_RS_AC_SUM(java.lang.String param) {
        localAVERAGEOPENBALANCE_RS_AC_SUMTracker = param != null;

        this.localAVERAGEOPENBALANCE_RS_AC_SUM = param;
    }

    public boolean isSINGLEHIGHESTBALANCE_RS_AC_SUMSpecified() {
        return localSINGLEHIGHESTBALANCE_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSINGLEHIGHESTBALANCE_RS_AC_SUM() {
        return localSINGLEHIGHESTBALANCE_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param SINGLEHIGHESTBALANCE_RS_AC_SUM
     */
    public void setSINGLEHIGHESTBALANCE_RS_AC_SUM(java.lang.String param) {
        localSINGLEHIGHESTBALANCE_RS_AC_SUMTracker = param != null;

        this.localSINGLEHIGHESTBALANCE_RS_AC_SUM = param;
    }

    public boolean isNOOFPASTDUEACCTS_RS_AC_SUMSpecified() {
        return localNOOFPASTDUEACCTS_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFPASTDUEACCTS_RS_AC_SUM() {
        return localNOOFPASTDUEACCTS_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param NOOFPASTDUEACCTS_RS_AC_SUM
     */
    public void setNOOFPASTDUEACCTS_RS_AC_SUM(java.lang.String param) {
        localNOOFPASTDUEACCTS_RS_AC_SUMTracker = param != null;

        this.localNOOFPASTDUEACCTS_RS_AC_SUM = param;
    }

    public boolean isNOOFZEROBALACCTS_RS_AC_SUMSpecified() {
        return localNOOFZEROBALACCTS_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFZEROBALACCTS_RS_AC_SUM() {
        return localNOOFZEROBALACCTS_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param NOOFZEROBALACCTS_RS_AC_SUM
     */
    public void setNOOFZEROBALACCTS_RS_AC_SUM(java.lang.String param) {
        localNOOFZEROBALACCTS_RS_AC_SUMTracker = param != null;

        this.localNOOFZEROBALACCTS_RS_AC_SUM = param;
    }

    public boolean isRECENTACCOUNT_RS_AC_SUMSpecified() {
        return localRECENTACCOUNT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRECENTACCOUNT_RS_AC_SUM() {
        return localRECENTACCOUNT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param RECENTACCOUNT_RS_AC_SUM
     */
    public void setRECENTACCOUNT_RS_AC_SUM(java.lang.String param) {
        localRECENTACCOUNT_RS_AC_SUMTracker = param != null;

        this.localRECENTACCOUNT_RS_AC_SUM = param;
    }

    public boolean isOLDESTACCOUNT_RS_AC_SUMSpecified() {
        return localOLDESTACCOUNT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOLDESTACCOUNT_RS_AC_SUM() {
        return localOLDESTACCOUNT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param OLDESTACCOUNT_RS_AC_SUM
     */
    public void setOLDESTACCOUNT_RS_AC_SUM(java.lang.String param) {
        localOLDESTACCOUNT_RS_AC_SUMTracker = param != null;

        this.localOLDESTACCOUNT_RS_AC_SUM = param;
    }

    public boolean isTOTALBALAMT_RS_AC_SUMSpecified() {
        return localTOTALBALAMT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALBALAMT_RS_AC_SUM() {
        return localTOTALBALAMT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALBALAMT_RS_AC_SUM
     */
    public void setTOTALBALAMT_RS_AC_SUM(java.lang.String param) {
        localTOTALBALAMT_RS_AC_SUMTracker = param != null;

        this.localTOTALBALAMT_RS_AC_SUM = param;
    }

    public boolean isTOTALSANCTAMT_RS_AC_SUMSpecified() {
        return localTOTALSANCTAMT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALSANCTAMT_RS_AC_SUM() {
        return localTOTALSANCTAMT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALSANCTAMT_RS_AC_SUM
     */
    public void setTOTALSANCTAMT_RS_AC_SUM(java.lang.String param) {
        localTOTALSANCTAMT_RS_AC_SUMTracker = param != null;

        this.localTOTALSANCTAMT_RS_AC_SUM = param;
    }

    public boolean isTOTALCRDTLIMIT_RS_AC_SUMSpecified() {
        return localTOTALCRDTLIMIT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALCRDTLIMIT_RS_AC_SUM() {
        return localTOTALCRDTLIMIT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALCRDTLIMIT_RS_AC_SUM
     */
    public void setTOTALCRDTLIMIT_RS_AC_SUM(java.lang.String param) {
        localTOTALCRDTLIMIT_RS_AC_SUMTracker = param != null;

        this.localTOTALCRDTLIMIT_RS_AC_SUM = param;
    }

    public boolean isTOTALMONLYPYMNTAMT_RS_AC_SUMSpecified() {
        return localTOTALMONLYPYMNTAMT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALMONLYPYMNTAMT_RS_AC_SUM() {
        return localTOTALMONLYPYMNTAMT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALMONLYPYMNTAMT_RS_AC_SUM
     */
    public void setTOTALMONLYPYMNTAMT_RS_AC_SUM(java.lang.String param) {
        localTOTALMONLYPYMNTAMT_RS_AC_SUMTracker = param != null;

        this.localTOTALMONLYPYMNTAMT_RS_AC_SUM = param;
    }

    public boolean isTOTALWRITTENOFFAMNT_RS_AC_SUMSpecified() {
        return localTOTALWRITTENOFFAMNT_RS_AC_SUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALWRITTENOFFAMNT_RS_AC_SUM() {
        return localTOTALWRITTENOFFAMNT_RS_AC_SUM;
    }

    /**
     * Auto generated setter method
     * @param param TOTALWRITTENOFFAMNT_RS_AC_SUM
     */
    public void setTOTALWRITTENOFFAMNT_RS_AC_SUM(java.lang.String param) {
        localTOTALWRITTENOFFAMNT_RS_AC_SUMTracker = param != null;

        this.localTOTALWRITTENOFFAMNT_RS_AC_SUM = param;
    }

    public boolean isAGEOFOLDESTTRADE_RS_OTKSpecified() {
        return localAGEOFOLDESTTRADE_RS_OTKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGEOFOLDESTTRADE_RS_OTK() {
        return localAGEOFOLDESTTRADE_RS_OTK;
    }

    /**
     * Auto generated setter method
     * @param param AGEOFOLDESTTRADE_RS_OTK
     */
    public void setAGEOFOLDESTTRADE_RS_OTK(java.lang.String param) {
        localAGEOFOLDESTTRADE_RS_OTKTracker = param != null;

        this.localAGEOFOLDESTTRADE_RS_OTK = param;
    }

    public boolean isNUMBEROFOPENTRADES_RS_OTKSpecified() {
        return localNUMBEROFOPENTRADES_RS_OTKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNUMBEROFOPENTRADES_RS_OTK() {
        return localNUMBEROFOPENTRADES_RS_OTK;
    }

    /**
     * Auto generated setter method
     * @param param NUMBEROFOPENTRADES_RS_OTK
     */
    public void setNUMBEROFOPENTRADES_RS_OTK(java.lang.String param) {
        localNUMBEROFOPENTRADES_RS_OTKTracker = param != null;

        this.localNUMBEROFOPENTRADES_RS_OTK = param;
    }

    public boolean isALINESEVERWRITTEN_RS_OTKSpecified() {
        return localALINESEVERWRITTEN_RS_OTKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALINESEVERWRITTEN_RS_OTK() {
        return localALINESEVERWRITTEN_RS_OTK;
    }

    /**
     * Auto generated setter method
     * @param param ALINESEVERWRITTEN_RS_OTK
     */
    public void setALINESEVERWRITTEN_RS_OTK(java.lang.String param) {
        localALINESEVERWRITTEN_RS_OTKTracker = param != null;

        this.localALINESEVERWRITTEN_RS_OTK = param;
    }

    public boolean isALINESVWRTNIN9MNTHS_RS_OTKSpecified() {
        return localALINESVWRTNIN9MNTHS_RS_OTKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALINESVWRTNIN9MNTHS_RS_OTK() {
        return localALINESVWRTNIN9MNTHS_RS_OTK;
    }

    /**
     * Auto generated setter method
     * @param param ALINESVWRTNIN9MNTHS_RS_OTK
     */
    public void setALINESVWRTNIN9MNTHS_RS_OTK(java.lang.String param) {
        localALINESVWRTNIN9MNTHS_RS_OTKTracker = param != null;

        this.localALINESVWRTNIN9MNTHS_RS_OTK = param;
    }

    public boolean isALINESVRWRTNIN6MNTHS_RS_OTKSpecified() {
        return localALINESVRWRTNIN6MNTHS_RS_OTKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALINESVRWRTNIN6MNTHS_RS_OTK() {
        return localALINESVRWRTNIN6MNTHS_RS_OTK;
    }

    /**
     * Auto generated setter method
     * @param param ALINESVRWRTNIN6MNTHS_RS_OTK
     */
    public void setALINESVRWRTNIN6MNTHS_RS_OTK(java.lang.String param) {
        localALINESVRWRTNIN6MNTHS_RS_OTKTracker = param != null;

        this.localALINESVRWRTNIN6MNTHS_RS_OTK = param;
    }

    public boolean isREPORTEDDATE_RS_EQ_SMSpecified() {
        return localREPORTEDDATE_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_RS_EQ_SM() {
        return localREPORTEDDATE_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_RS_EQ_SM
     */
    public void setREPORTEDDATE_RS_EQ_SM(java.lang.String param) {
        localREPORTEDDATE_RS_EQ_SMTracker = param != null;

        this.localREPORTEDDATE_RS_EQ_SM = param;
    }

    public boolean isPURPOSE_RS_EQ_SMSpecified() {
        return localPURPOSE_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPURPOSE_RS_EQ_SM() {
        return localPURPOSE_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param PURPOSE_RS_EQ_SM
     */
    public void setPURPOSE_RS_EQ_SM(java.lang.String param) {
        localPURPOSE_RS_EQ_SMTracker = param != null;

        this.localPURPOSE_RS_EQ_SM = param;
    }

    public boolean isTOTAL_RS_EQ_SMSpecified() {
        return localTOTAL_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTAL_RS_EQ_SM() {
        return localTOTAL_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param TOTAL_RS_EQ_SM
     */
    public void setTOTAL_RS_EQ_SM(java.lang.String param) {
        localTOTAL_RS_EQ_SMTracker = param != null;

        this.localTOTAL_RS_EQ_SM = param;
    }

    public boolean isPAST30DAYS_RS_EQ_SMSpecified() {
        return localPAST30DAYS_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAST30DAYS_RS_EQ_SM() {
        return localPAST30DAYS_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param PAST30DAYS_RS_EQ_SM
     */
    public void setPAST30DAYS_RS_EQ_SM(java.lang.String param) {
        localPAST30DAYS_RS_EQ_SMTracker = param != null;

        this.localPAST30DAYS_RS_EQ_SM = param;
    }

    public boolean isPAST12MONTHS_RS_EQ_SMSpecified() {
        return localPAST12MONTHS_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAST12MONTHS_RS_EQ_SM() {
        return localPAST12MONTHS_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param PAST12MONTHS_RS_EQ_SM
     */
    public void setPAST12MONTHS_RS_EQ_SM(java.lang.String param) {
        localPAST12MONTHS_RS_EQ_SMTracker = param != null;

        this.localPAST12MONTHS_RS_EQ_SM = param;
    }

    public boolean isPAST24MONTHS_RS_EQ_SMSpecified() {
        return localPAST24MONTHS_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAST24MONTHS_RS_EQ_SM() {
        return localPAST24MONTHS_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param PAST24MONTHS_RS_EQ_SM
     */
    public void setPAST24MONTHS_RS_EQ_SM(java.lang.String param) {
        localPAST24MONTHS_RS_EQ_SMTracker = param != null;

        this.localPAST24MONTHS_RS_EQ_SM = param;
    }

    public boolean isRECENT_RS_EQ_SMSpecified() {
        return localRECENT_RS_EQ_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRECENT_RS_EQ_SM() {
        return localRECENT_RS_EQ_SM;
    }

    /**
     * Auto generated setter method
     * @param param RECENT_RS_EQ_SM
     */
    public void setRECENT_RS_EQ_SM(java.lang.String param) {
        localRECENT_RS_EQ_SMTracker = param != null;

        this.localRECENT_RS_EQ_SM = param;
    }

    public boolean isREPORTEDDATE_RS_EQSpecified() {
        return localREPORTEDDATE_RS_EQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORTEDDATE_RS_EQ() {
        return localREPORTEDDATE_RS_EQ;
    }

    /**
     * Auto generated setter method
     * @param param REPORTEDDATE_RS_EQ
     */
    public void setREPORTEDDATE_RS_EQ(java.lang.String param) {
        localREPORTEDDATE_RS_EQTracker = param != null;

        this.localREPORTEDDATE_RS_EQ = param;
    }

    public boolean isENQREFERENCE_RS_EQSpecified() {
        return localENQREFERENCE_RS_EQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQREFERENCE_RS_EQ() {
        return localENQREFERENCE_RS_EQ;
    }

    /**
     * Auto generated setter method
     * @param param ENQREFERENCE_RS_EQ
     */
    public void setENQREFERENCE_RS_EQ(java.lang.String param) {
        localENQREFERENCE_RS_EQTracker = param != null;

        this.localENQREFERENCE_RS_EQ = param;
    }

    public boolean isENQDATE_RS_EQSpecified() {
        return localENQDATE_RS_EQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQDATE_RS_EQ() {
        return localENQDATE_RS_EQ;
    }

    /**
     * Auto generated setter method
     * @param param ENQDATE_RS_EQ
     */
    public void setENQDATE_RS_EQ(java.lang.String param) {
        localENQDATE_RS_EQTracker = param != null;

        this.localENQDATE_RS_EQ = param;
    }

    public boolean isENQTIME_RS_EQSpecified() {
        return localENQTIME_RS_EQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQTIME_RS_EQ() {
        return localENQTIME_RS_EQ;
    }

    /**
     * Auto generated setter method
     * @param param ENQTIME_RS_EQ
     */
    public void setENQTIME_RS_EQ(java.lang.String param) {
        localENQTIME_RS_EQTracker = param != null;

        this.localENQTIME_RS_EQ = param;
    }

    public boolean isREQUESTPURPOSE_RS_EQSpecified() {
        return localREQUESTPURPOSE_RS_EQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREQUESTPURPOSE_RS_EQ() {
        return localREQUESTPURPOSE_RS_EQ;
    }

    /**
     * Auto generated setter method
     * @param param REQUESTPURPOSE_RS_EQ
     */
    public void setREQUESTPURPOSE_RS_EQ(java.lang.String param) {
        localREQUESTPURPOSE_RS_EQTracker = param != null;

        this.localREQUESTPURPOSE_RS_EQ = param;
    }

    public boolean isAMOUNT_RS_EQSpecified() {
        return localAMOUNT_RS_EQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNT_RS_EQ() {
        return localAMOUNT_RS_EQ;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNT_RS_EQ
     */
    public void setAMOUNT_RS_EQ(java.lang.String param) {
        localAMOUNT_RS_EQTracker = param != null;

        this.localAMOUNT_RS_EQ = param;
    }

    public boolean isACCOUNTSDELIQUENT_RS_RE_ACTSpecified() {
        return localACCOUNTSDELIQUENT_RS_RE_ACTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTSDELIQUENT_RS_RE_ACT() {
        return localACCOUNTSDELIQUENT_RS_RE_ACT;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTSDELIQUENT_RS_RE_ACT
     */
    public void setACCOUNTSDELIQUENT_RS_RE_ACT(java.lang.String param) {
        localACCOUNTSDELIQUENT_RS_RE_ACTTracker = param != null;

        this.localACCOUNTSDELIQUENT_RS_RE_ACT = param;
    }

    public boolean isACCOUNTSOPENED_RS_RE_ACTSpecified() {
        return localACCOUNTSOPENED_RS_RE_ACTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTSOPENED_RS_RE_ACT() {
        return localACCOUNTSOPENED_RS_RE_ACT;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTSOPENED_RS_RE_ACT
     */
    public void setACCOUNTSOPENED_RS_RE_ACT(java.lang.String param) {
        localACCOUNTSOPENED_RS_RE_ACTTracker = param != null;

        this.localACCOUNTSOPENED_RS_RE_ACT = param;
    }

    public boolean isTOTALINQUIRIES_RS_RE_ACTSpecified() {
        return localTOTALINQUIRIES_RS_RE_ACTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTOTALINQUIRIES_RS_RE_ACT() {
        return localTOTALINQUIRIES_RS_RE_ACT;
    }

    /**
     * Auto generated setter method
     * @param param TOTALINQUIRIES_RS_RE_ACT
     */
    public void setTOTALINQUIRIES_RS_RE_ACT(java.lang.String param) {
        localTOTALINQUIRIES_RS_RE_ACTTracker = param != null;

        this.localTOTALINQUIRIES_RS_RE_ACT = param;
    }

    public boolean isACCOUNTSUPDATED_RS_RE_ACTSpecified() {
        return localACCOUNTSUPDATED_RS_RE_ACTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTSUPDATED_RS_RE_ACT() {
        return localACCOUNTSUPDATED_RS_RE_ACT;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTSUPDATED_RS_RE_ACT
     */
    public void setACCOUNTSUPDATED_RS_RE_ACT(java.lang.String param) {
        localACCOUNTSUPDATED_RS_RE_ACTTracker = param != null;

        this.localACCOUNTSUPDATED_RS_RE_ACT = param;
    }

    public boolean isINSTITUTION_GRP_SMSpecified() {
        return localINSTITUTION_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINSTITUTION_GRP_SM() {
        return localINSTITUTION_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param INSTITUTION_GRP_SM
     */
    public void setINSTITUTION_GRP_SM(java.lang.String param) {
        localINSTITUTION_GRP_SMTracker = param != null;

        this.localINSTITUTION_GRP_SM = param;
    }

    public boolean isCURRENTBALANCE_GRP_SMSpecified() {
        return localCURRENTBALANCE_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENTBALANCE_GRP_SM() {
        return localCURRENTBALANCE_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param CURRENTBALANCE_GRP_SM
     */
    public void setCURRENTBALANCE_GRP_SM(java.lang.String param) {
        localCURRENTBALANCE_GRP_SMTracker = param != null;

        this.localCURRENTBALANCE_GRP_SM = param;
    }

    public boolean isSTATUS_GRP_SMSpecified() {
        return localSTATUS_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS_GRP_SM() {
        return localSTATUS_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param STATUS_GRP_SM
     */
    public void setSTATUS_GRP_SM(java.lang.String param) {
        localSTATUS_GRP_SMTracker = param != null;

        this.localSTATUS_GRP_SM = param;
    }

    public boolean isDATEREPORTED_GRP_SMSpecified() {
        return localDATEREPORTED_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEREPORTED_GRP_SM() {
        return localDATEREPORTED_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param DATEREPORTED_GRP_SM
     */
    public void setDATEREPORTED_GRP_SM(java.lang.String param) {
        localDATEREPORTED_GRP_SMTracker = param != null;

        this.localDATEREPORTED_GRP_SM = param;
    }

    public boolean isNOOFMEMBERS_GRP_SMSpecified() {
        return localNOOFMEMBERS_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNOOFMEMBERS_GRP_SM() {
        return localNOOFMEMBERS_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param NOOFMEMBERS_GRP_SM
     */
    public void setNOOFMEMBERS_GRP_SM(java.lang.String param) {
        localNOOFMEMBERS_GRP_SMTracker = param != null;

        this.localNOOFMEMBERS_GRP_SM = param;
    }

    public boolean isPASTDUEAMOUNT_GRP_SMSpecified() {
        return localPASTDUEAMOUNT_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASTDUEAMOUNT_GRP_SM() {
        return localPASTDUEAMOUNT_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param PASTDUEAMOUNT_GRP_SM
     */
    public void setPASTDUEAMOUNT_GRP_SM(java.lang.String param) {
        localPASTDUEAMOUNT_GRP_SMTracker = param != null;

        this.localPASTDUEAMOUNT_GRP_SM = param;
    }

    public boolean isSANCTIONAMOUNT_GRP_SMSpecified() {
        return localSANCTIONAMOUNT_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSANCTIONAMOUNT_GRP_SM() {
        return localSANCTIONAMOUNT_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param SANCTIONAMOUNT_GRP_SM
     */
    public void setSANCTIONAMOUNT_GRP_SM(java.lang.String param) {
        localSANCTIONAMOUNT_GRP_SMTracker = param != null;

        this.localSANCTIONAMOUNT_GRP_SM = param;
    }

    public boolean isDATEOPENED_GRP_SMSpecified() {
        return localDATEOPENED_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATEOPENED_GRP_SM() {
        return localDATEOPENED_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param DATEOPENED_GRP_SM
     */
    public void setDATEOPENED_GRP_SM(java.lang.String param) {
        localDATEOPENED_GRP_SMTracker = param != null;

        this.localDATEOPENED_GRP_SM = param;
    }

    public boolean isACCOUNTNO_GRP_SMSpecified() {
        return localACCOUNTNO_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNTNO_GRP_SM() {
        return localACCOUNTNO_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNTNO_GRP_SM
     */
    public void setACCOUNTNO_GRP_SM(java.lang.String param) {
        localACCOUNTNO_GRP_SMTracker = param != null;

        this.localACCOUNTNO_GRP_SM = param;
    }

    public boolean isMEMBERSPASTDUE_GRP_SMSpecified() {
        return localMEMBERSPASTDUE_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBERSPASTDUE_GRP_SM() {
        return localMEMBERSPASTDUE_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param MEMBERSPASTDUE_GRP_SM
     */
    public void setMEMBERSPASTDUE_GRP_SM(java.lang.String param) {
        localMEMBERSPASTDUE_GRP_SMTracker = param != null;

        this.localMEMBERSPASTDUE_GRP_SM = param;
    }

    public boolean isWRITEOFFAMOUNT_GRP_SMSpecified() {
        return localWRITEOFFAMOUNT_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITEOFFAMOUNT_GRP_SM() {
        return localWRITEOFFAMOUNT_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param WRITEOFFAMOUNT_GRP_SM
     */
    public void setWRITEOFFAMOUNT_GRP_SM(java.lang.String param) {
        localWRITEOFFAMOUNT_GRP_SMTracker = param != null;

        this.localWRITEOFFAMOUNT_GRP_SM = param;
    }

    public boolean isWRITEOFFDATE_GRP_SMSpecified() {
        return localWRITEOFFDATE_GRP_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITEOFFDATE_GRP_SM() {
        return localWRITEOFFDATE_GRP_SM;
    }

    /**
     * Auto generated setter method
     * @param param WRITEOFFDATE_GRP_SM
     */
    public void setWRITEOFFDATE_GRP_SM(java.lang.String param) {
        localWRITEOFFDATE_GRP_SMTracker = param != null;

        this.localWRITEOFFDATE_GRP_SM = param;
    }

    public boolean isREASONCODE_SCSpecified() {
        return localREASONCODE_SCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASONCODE_SC() {
        return localREASONCODE_SC;
    }

    /**
     * Auto generated setter method
     * @param param REASONCODE_SC
     */
    public void setREASONCODE_SC(java.lang.String param) {
        localREASONCODE_SCTracker = param != null;

        this.localREASONCODE_SC = param;
    }

    public boolean isSCOREDESCRIPTION_SCSpecified() {
        return localSCOREDESCRIPTION_SCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCOREDESCRIPTION_SC() {
        return localSCOREDESCRIPTION_SC;
    }

    /**
     * Auto generated setter method
     * @param param SCOREDESCRIPTION_SC
     */
    public void setSCOREDESCRIPTION_SC(java.lang.String param) {
        localSCOREDESCRIPTION_SCTracker = param != null;

        this.localSCOREDESCRIPTION_SC = param;
    }

    public boolean isSCORENAME_SCSpecified() {
        return localSCORENAME_SCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORENAME_SC() {
        return localSCORENAME_SC;
    }

    /**
     * Auto generated setter method
     * @param param SCORENAME_SC
     */
    public void setSCORENAME_SC(java.lang.String param) {
        localSCORENAME_SCTracker = param != null;

        this.localSCORENAME_SC = param;
    }

    public boolean isSCOREVALUE_SCSpecified() {
        return localSCOREVALUE_SCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCOREVALUE_SC() {
        return localSCOREVALUE_SC;
    }

    /**
     * Auto generated setter method
     * @param param SCOREVALUE_SC
     */
    public void setSCOREVALUE_SC(java.lang.String param) {
        localSCOREVALUE_SCTracker = param != null;

        this.localSCOREVALUE_SC = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_TIMESpecified() {
        return localOUTPUT_WRITE_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_TIME() {
        return localOUTPUT_WRITE_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_TIME
     */
    public void setOUTPUT_WRITE_TIME(java.lang.String param) {
        localOUTPUT_WRITE_TIMETracker = param != null;

        this.localOUTPUT_WRITE_TIME = param;
    }

    public boolean isOUTPUT_READ_TIMESpecified() {
        return localOUTPUT_READ_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_TIME() {
        return localOUTPUT_READ_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_TIME
     */
    public void setOUTPUT_READ_TIME(java.lang.String param) {
        localOUTPUT_READ_TIMETracker = param != null;

        this.localOUTPUT_READ_TIME = param;
    }

    public boolean isACCOUNT_KEYSpecified() {
        return localACCOUNT_KEYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_KEY() {
        return localACCOUNT_KEY;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_KEY
     */
    public void setACCOUNT_KEY(java.lang.String param) {
        localACCOUNT_KEYTracker = param != null;

        this.localACCOUNT_KEY = param;
    }

    public boolean isACCNT_NUMSpecified() {
        return localACCNT_NUMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCNT_NUM() {
        return localACCNT_NUM;
    }

    /**
     * Auto generated setter method
     * @param param ACCNT_NUM
     */
    public void setACCNT_NUM(java.lang.String param) {
        localACCNT_NUMTracker = param != null;

        this.localACCNT_NUM = param;
    }

    public boolean isDISPUTE_COMMENTSSpecified() {
        return localDISPUTE_COMMENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISPUTE_COMMENTS() {
        return localDISPUTE_COMMENTS;
    }

    /**
     * Auto generated setter method
     * @param param DISPUTE_COMMENTS
     */
    public void setDISPUTE_COMMENTS(java.lang.String param) {
        localDISPUTE_COMMENTSTracker = param != null;

        this.localDISPUTE_COMMENTS = param;
    }

    public boolean isSTATUSSpecified() {
        return localSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS() {
        return localSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param STATUS
     */
    public void setSTATUS(java.lang.String param) {
        localSTATUSTracker = param != null;

        this.localSTATUS = param;
    }

    public boolean isRESOLVED_DATESpecified() {
        return localRESOLVED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESOLVED_DATE() {
        return localRESOLVED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param RESOLVED_DATE
     */
    public void setRESOLVED_DATE(java.lang.String param) {
        localRESOLVED_DATETracker = param != null;

        this.localRESOLVED_DATE = param;
    }

    public boolean isCODESpecified() {
        return localCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCODE() {
        return localCODE;
    }

    /**
     * Auto generated setter method
     * @param param CODE
     */
    public void setCODE(java.lang.String param) {
        localCODETracker = param != null;

        this.localCODE = param;
    }

    public boolean isDESCRIPTIONSpecified() {
        return localDESCRIPTIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDESCRIPTION() {
        return localDESCRIPTION;
    }

    /**
     * Auto generated setter method
     * @param param DESCRIPTION
     */
    public void setDESCRIPTION(java.lang.String param) {
        localDESCRIPTIONTracker = param != null;

        this.localDESCRIPTION = param;
    }

    public boolean isHITCODE_HDSpecified() {
        return localHITCODE_HDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHITCODE_HD() {
        return localHITCODE_HD;
    }

    /**
     * Auto generated setter method
     * @param param HITCODE_HD
     */
    public void setHITCODE_HD(java.lang.String param) {
        localHITCODE_HDTracker = param != null;

        this.localHITCODE_HD = param;
    }

    public boolean isFULLNAME_NM_TPSpecified() {
        return localFULLNAME_NM_TPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFULLNAME_NM_TP() {
        return localFULLNAME_NM_TP;
    }

    /**
     * Auto generated setter method
     * @param param FULLNAME_NM_TP
     */
    public void setFULLNAME_NM_TP(java.lang.String param) {
        localFULLNAME_NM_TPTracker = param != null;

        this.localFULLNAME_NM_TP = param;
    }

    public boolean isEMPPHONE_RS_EMDLSpecified() {
        return localEMPPHONE_RS_EMDLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMPPHONE_RS_EMDL() {
        return localEMPPHONE_RS_EMDL;
    }

    /**
     * Auto generated setter method
     * @param param EMPPHONE_RS_EMDL
     */
    public void setEMPPHONE_RS_EMDL(java.lang.String param) {
        localEMPPHONE_RS_EMDLTracker = param != null;

        this.localEMPPHONE_RS_EMDL = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":EquifaxSRespType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "EquifaxSRespType", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_DATE", xmlWriter);

            if (localENQUIRY_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCUSTOMERCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CUSTOMERCODE", xmlWriter);

            if (localCUSTOMERCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CUSTOMERCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCUSTOMERCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localFIRSTNAME_NM_TPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FIRSTNAME_NM_TP", xmlWriter);

            if (localFIRSTNAME_NM_TP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FIRSTNAME_NM_TP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFIRSTNAME_NM_TP);
            }

            xmlWriter.writeEndElement();
        }

        if (localMIDLLENAME_NM_TPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MIDLLENAME_NM_TP", xmlWriter);

            if (localMIDLLENAME_NM_TP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MIDLLENAME_NM_TP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMIDLLENAME_NM_TP);
            }

            xmlWriter.writeEndElement();
        }

        if (localLASTNAME_NM_TPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LASTNAME_NM_TP", xmlWriter);

            if (localLASTNAME_NM_TP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LASTNAME_NM_TP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLASTNAME_NM_TP);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDMIDLLENAME_NM_TPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDMIDLLENAME_NM_TP", xmlWriter);

            if (localADDMIDLLENAME_NM_TP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDMIDLLENAME_NM_TP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDMIDLLENAME_NM_TP);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUFFIX_NM_TPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUFFIX_NM_TP", xmlWriter);

            if (localSUFFIX_NM_TP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUFFIX_NM_TP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUFFIX_NM_TP);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_AG_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_AG_IF", xmlWriter);

            if (localREPORTEDDATE_AG_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_AG_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_AG_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGE_AG_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE_AG_IF", xmlWriter);

            if (localAGE_AG_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE_AG_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE_AG_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_AL_NMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_AL_NM", xmlWriter);

            if (localREPORTEDDATE_AL_NM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_AL_NM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_AL_NM);
            }

            xmlWriter.writeEndElement();
        }

        if (localALIASNAME_AL_NMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALIASNAME_AL_NM", xmlWriter);

            if (localALIASNAME_AL_NM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALIASNAME_AL_NM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALIASNAME_AL_NM);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDITIONALNAMEINFO_AD_NM_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDITIONALNAMEINFO_AD_NM_IF",
                xmlWriter);

            if (localADDITIONALNAMEINFO_AD_NM_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDITIONALNAMEINFO_AD_NM_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDITIONALNAMEINFO_AD_NM_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFDEPENDENTS_AD_NM_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFDEPENDENTS_AD_NM_IF",
                xmlWriter);

            if (localNOOFDEPENDENTS_AD_NM_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFDEPENDENTS_AD_NM_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFDEPENDENTS_AD_NM_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_RS_ADDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_RS_ADD", xmlWriter);

            if (localREPORTEDDATE_RS_ADD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_RS_ADD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_RS_ADD);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_RS_ADDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_RS_ADD", xmlWriter);

            if (localADDRESS_RS_ADD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_RS_ADD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_RS_ADD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATE_RS_ADDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATE_RS_ADD", xmlWriter);

            if (localSTATE_RS_ADD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATE_RS_ADD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATE_RS_ADD);
            }

            xmlWriter.writeEndElement();
        }

        if (localPOSTAL_RS_ADDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "POSTAL_RS_ADD", xmlWriter);

            if (localPOSTAL_RS_ADD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "POSTAL_RS_ADD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPOSTAL_RS_ADD);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDTYPE_RS_ADDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDTYPE_RS_ADD", xmlWriter);

            if (localADDTYPE_RS_ADD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDTYPE_RS_ADD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDTYPE_RS_ADD);
            }

            xmlWriter.writeEndElement();
        }

        if (localIDTYPE_RS_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IDTYPE_RS_ID", xmlWriter);

            if (localIDTYPE_RS_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IDTYPE_RS_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIDTYPE_RS_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_RS_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_RS_ID", xmlWriter);

            if (localREPORTEDDATE_RS_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_RS_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_RS_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localIDNUMBER_RS_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IDNUMBER_RS_ID", xmlWriter);

            if (localIDNUMBER_RS_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IDNUMBER_RS_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIDNUMBER_RS_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAILREPORTEDDATE_RS_MLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAILREPORTEDDATE_RS_ML",
                xmlWriter);

            if (localEMAILREPORTEDDATE_RS_ML == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAILREPORTEDDATE_RS_ML cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAILREPORTEDDATE_RS_ML);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAILADDRESS_RS_MLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAILADDRESS_RS_ML", xmlWriter);

            if (localEMAILADDRESS_RS_ML == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAILADDRESS_RS_ML cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAILADDRESS_RS_ML);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMPNMREPDATE_RS_EMDLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMPNMREPDATE_RS_EMDL", xmlWriter);

            if (localEMPNMREPDATE_RS_EMDL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMPNMREPDATE_RS_EMDL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMPNMREPDATE_RS_EMDL);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMPLOYERNAME_RS_EMDLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMPLOYERNAME_RS_EMDL", xmlWriter);

            if (localEMPLOYERNAME_RS_EMDL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMPLOYERNAME_RS_EMDL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMPLOYERNAME_RS_EMDL);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMPPOSITION_RS_EMDLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMPPOSITION_RS_EMDL", xmlWriter);

            if (localEMPPOSITION_RS_EMDL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMPPOSITION_RS_EMDL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMPPOSITION_RS_EMDL);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMPADDRESS_RS_EMDLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMPADDRESS_RS_EMDL", xmlWriter);

            if (localEMPADDRESS_RS_EMDL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMPADDRESS_RS_EMDL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMPADDRESS_RS_EMDL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDOB_RS_PER_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DOB_RS_PER_IF", xmlWriter);

            if (localDOB_RS_PER_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DOB_RS_PER_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDOB_RS_PER_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDER_RS_PER_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDER_RS_PER_IF", xmlWriter);

            if (localGENDER_RS_PER_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDER_RS_PER_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDER_RS_PER_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALINCOME_RS_PER_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALINCOME_RS_PER_IF",
                xmlWriter);

            if (localTOTALINCOME_RS_PER_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALINCOME_RS_PER_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALINCOME_RS_PER_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localOCCUPATION_RS_PER_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OCCUPATION_RS_PER_IF", xmlWriter);

            if (localOCCUPATION_RS_PER_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OCCUPATION_RS_PER_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOCCUPATION_RS_PER_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localMARITALSTAUS_RS_PER_IFTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MARITALSTAUS_RS_PER_IF",
                xmlWriter);

            if (localMARITALSTAUS_RS_PER_IF == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MARITALSTAUS_RS_PER_IF cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMARITALSTAUS_RS_PER_IF);
            }

            xmlWriter.writeEndElement();
        }

        if (localOCCUPATION_IC_DLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OCCUPATION_IC_DL", xmlWriter);

            if (localOCCUPATION_IC_DL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OCCUPATION_IC_DL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOCCUPATION_IC_DL);
            }

            xmlWriter.writeEndElement();
        }

        if (localMONTHLYINCOME_IC_DLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MONTHLYINCOME_IC_DL", xmlWriter);

            if (localMONTHLYINCOME_IC_DL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MONTHLYINCOME_IC_DL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMONTHLYINCOME_IC_DL);
            }

            xmlWriter.writeEndElement();
        }

        if (localMONTHLYEXPENSE_IC_DLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MONTHLYEXPENSE_IC_DL", xmlWriter);

            if (localMONTHLYEXPENSE_IC_DL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MONTHLYEXPENSE_IC_DL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMONTHLYEXPENSE_IC_DL);
            }

            xmlWriter.writeEndElement();
        }

        if (localPOVERTYINDEX_IC_DLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "POVERTYINDEX_IC_DL", xmlWriter);

            if (localPOVERTYINDEX_IC_DL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "POVERTYINDEX_IC_DL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPOVERTYINDEX_IC_DL);
            }

            xmlWriter.writeEndElement();
        }

        if (localASSETOWNERSHIP_IC_DLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ASSETOWNERSHIP_IC_DL", xmlWriter);

            if (localASSETOWNERSHIP_IC_DL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ASSETOWNERSHIP_IC_DL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localASSETOWNERSHIP_IC_DL);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_RS_PHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_RS_PH", xmlWriter);

            if (localREPORTEDDATE_RS_PH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_RS_PH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_RS_PH);
            }

            xmlWriter.writeEndElement();
        }

        if (localTYPECODE_RS_PHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TYPECODE_RS_PH", xmlWriter);

            if (localTYPECODE_RS_PH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TYPECODE_RS_PH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTYPECODE_RS_PH);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOUNTRYCODE_RS_PHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "COUNTRYCODE_RS_PH", xmlWriter);

            if (localCOUNTRYCODE_RS_PH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COUNTRYCODE_RS_PH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOUNTRYCODE_RS_PH);
            }

            xmlWriter.writeEndElement();
        }

        if (localAREACODE_RS_PHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AREACODE_RS_PH", xmlWriter);

            if (localAREACODE_RS_PH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AREACODE_RS_PH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAREACODE_RS_PH);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHNUMBER_RS_PHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHNUMBER_RS_PH", xmlWriter);

            if (localPHNUMBER_RS_PH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHNUMBER_RS_PH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHNUMBER_RS_PH);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHNOEXTENTION_RS_PHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHNOEXTENTION_RS_PH", xmlWriter);

            if (localPHNOEXTENTION_RS_PH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHNOEXTENTION_RS_PH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHNOEXTENTION_RS_PH);
            }

            xmlWriter.writeEndElement();
        }

        if (localCUSTOMERCODE_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CUSTOMERCODE_RS_HD", xmlWriter);

            if (localCUSTOMERCODE_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CUSTOMERCODE_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCUSTOMERCODE_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localCLIENTID_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CLIENTID_RS_HD", xmlWriter);

            if (localCLIENTID_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CLIENTID_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCLIENTID_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localCUSTREFFIELD_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CUSTREFFIELD_RS_HD", xmlWriter);

            if (localCUSTREFFIELD_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CUSTREFFIELD_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCUSTREFFIELD_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTORDERNO_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTORDERNO_RS_HD", xmlWriter);

            if (localREPORTORDERNO_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTORDERNO_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTORDERNO_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localPRODUCTCODE_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PRODUCTCODE_RS_HD", xmlWriter);

            if (localPRODUCTCODE_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PRODUCTCODE_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPRODUCTCODE_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localPRODUCTVERSION_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PRODUCTVERSION_RS_HD", xmlWriter);

            if (localPRODUCTVERSION_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PRODUCTVERSION_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPRODUCTVERSION_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUCCESSCODE_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUCCESSCODE_RS_HD", xmlWriter);

            if (localSUCCESSCODE_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUCCESSCODE_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUCCESSCODE_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localMATCHTYPE_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MATCHTYPE_RS_HD", xmlWriter);

            if (localMATCHTYPE_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MATCHTYPE_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMATCHTYPE_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESDATE_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESDATE_RS_HD", xmlWriter);

            if (localRESDATE_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESDATE_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESDATE_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESTIME_RS_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESTIME_RS_HD", xmlWriter);

            if (localRESTIME_RS_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESTIME_RS_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESTIME_RS_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_RS_AC", xmlWriter);

            if (localREPORTEDDATE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCLIENTNAME_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CLIENTNAME_RS_AC", xmlWriter);

            if (localCLIENTNAME_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CLIENTNAME_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCLIENTNAME_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTNUMBER_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTNUMBER_RS_AC", xmlWriter);

            if (localACCOUNTNUMBER_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTNUMBER_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTNUMBER_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENTBALANCE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENTBALANCE_RS_AC", xmlWriter);

            if (localCURRENTBALANCE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENTBALANCE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENTBALANCE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localINSTITUTION_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INSTITUTION_RS_AC", xmlWriter);

            if (localINSTITUTION_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INSTITUTION_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINSTITUTION_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTTYPE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTTYPE_RS_AC", xmlWriter);

            if (localACCOUNTTYPE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTTYPE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTTYPE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNERSHIPTYPE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNERSHIPTYPE_RS_AC", xmlWriter);

            if (localOWNERSHIPTYPE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNERSHIPTYPE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNERSHIPTYPE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localBALANCE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BALANCE_RS_AC", xmlWriter);

            if (localBALANCE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BALANCE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBALANCE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASTDUEAMT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASTDUEAMT_RS_AC", xmlWriter);

            if (localPASTDUEAMT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASTDUEAMT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASTDUEAMT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISBURSEDAMOUNT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISBURSEDAMOUNT_RS_AC",
                xmlWriter);

            if (localDISBURSEDAMOUNT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISBURSEDAMOUNT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISBURSEDAMOUNT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOANCATEGORY_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOANCATEGORY_RS_AC", xmlWriter);

            if (localLOANCATEGORY_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOANCATEGORY_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOANCATEGORY_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOANPURPOSE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOANPURPOSE_RS_AC", xmlWriter);

            if (localLOANPURPOSE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOANPURPOSE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOANPURPOSE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localLASTPAYMENT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LASTPAYMENT_RS_AC", xmlWriter);

            if (localLASTPAYMENT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LASTPAYMENT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLASTPAYMENT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITEOFFAMT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITEOFFAMT_RS_AC", xmlWriter);

            if (localWRITEOFFAMT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITEOFFAMT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITEOFFAMT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOPEN_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOPEN_RS_AC", xmlWriter);

            if (localACCOPEN_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOPEN_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOPEN_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localSACTIONAMT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SACTIONAMT_RS_AC", xmlWriter);

            if (localSACTIONAMT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SACTIONAMT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSACTIONAMT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localLASTPAYMENTDATE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LASTPAYMENTDATE_RS_AC",
                xmlWriter);

            if (localLASTPAYMENTDATE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LASTPAYMENTDATE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLASTPAYMENTDATE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localHIGHCREDIT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "HIGHCREDIT_RS_AC", xmlWriter);

            if (localHIGHCREDIT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HIGHCREDIT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHIGHCREDIT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEREPORTED_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEREPORTED_RS_AC", xmlWriter);

            if (localDATEREPORTED_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEREPORTED_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEREPORTED_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOPENED_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOPENED_RS_AC", xmlWriter);

            if (localDATEOPENED_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOPENED_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOPENED_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATECLOSED_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATECLOSED_RS_AC", xmlWriter);

            if (localDATECLOSED_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATECLOSED_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATECLOSED_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_RS_AC", xmlWriter);

            if (localREASON_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEWRITTENOFF_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEWRITTENOFF_RS_AC", xmlWriter);

            if (localDATEWRITTENOFF_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEWRITTENOFF_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEWRITTENOFF_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOANCYCLEID_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOANCYCLEID_RS_AC", xmlWriter);

            if (localLOANCYCLEID_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOANCYCLEID_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOANCYCLEID_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATESANCTIONED_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATESANCTIONED_RS_AC", xmlWriter);

            if (localDATESANCTIONED_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATESANCTIONED_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATESANCTIONED_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEAPPLIED_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEAPPLIED_RS_AC", xmlWriter);

            if (localDATEAPPLIED_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEAPPLIED_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEAPPLIED_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localINTRESTRATE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INTRESTRATE_RS_AC", xmlWriter);

            if (localINTRESTRATE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INTRESTRATE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINTRESTRATE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localAPPLIEDAMOUNT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "APPLIEDAMOUNT_RS_AC", xmlWriter);

            if (localAPPLIEDAMOUNT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "APPLIEDAMOUNT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAPPLIEDAMOUNT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFINSTALLMENTS_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFINSTALLMENTS_RS_AC",
                xmlWriter);

            if (localNOOFINSTALLMENTS_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFINSTALLMENTS_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFINSTALLMENTS_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPAYMENTTENURE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPAYMENTTENURE_RS_AC",
                xmlWriter);

            if (localREPAYMENTTENURE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPAYMENTTENURE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPAYMENTTENURE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISPUTECODE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISPUTECODE_RS_AC", xmlWriter);

            if (localDISPUTECODE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISPUTECODE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISPUTECODE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localINSTALLMENTAMT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INSTALLMENTAMT_RS_AC", xmlWriter);

            if (localINSTALLMENTAMT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INSTALLMENTAMT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINSTALLMENTAMT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localKEYPERSON_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "KEYPERSON_RS_AC", xmlWriter);

            if (localKEYPERSON_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "KEYPERSON_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localKEYPERSON_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOMINEE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOMINEE_RS_AC", xmlWriter);

            if (localNOMINEE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOMINEE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOMINEE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localTERMFREQUENCY_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TERMFREQUENCY_RS_AC", xmlWriter);

            if (localTERMFREQUENCY_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TERMFREQUENCY_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTERMFREQUENCY_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITLIMIT_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITLIMIT_RS_AC", xmlWriter);

            if (localCREDITLIMIT_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITLIMIT_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITLIMIT_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOLLATERALVALUE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "COLLATERALVALUE_RS_AC",
                xmlWriter);

            if (localCOLLATERALVALUE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COLLATERALVALUE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOLLATERALVALUE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOLLATERALTYPE_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "COLLATERALTYPE_RS_AC", xmlWriter);

            if (localCOLLATERALTYPE_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COLLATERALTYPE_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOLLATERALTYPE_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTSTATUS_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTSTATUS_RS_AC", xmlWriter);

            if (localACCOUNTSTATUS_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTSTATUS_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTSTATUS_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localASSETCLASSIFICATION_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ASSETCLASSIFICATION_RS_AC",
                xmlWriter);

            if (localASSETCLASSIFICATION_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ASSETCLASSIFICATION_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localASSETCLASSIFICATION_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUITFILEDSTATUS_RS_ACTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUITFILEDSTATUS_RS_AC",
                xmlWriter);

            if (localSUITFILEDSTATUS_RS_AC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUITFILEDSTATUS_RS_AC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUITFILEDSTATUS_RS_AC);
            }

            xmlWriter.writeEndElement();
        }

        if (localMONTHKEY_RS_AC_HIS24Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "MONTHKEY_RS_AC_HIS24", xmlWriter);

            if (localMONTHKEY_RS_AC_HIS24 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MONTHKEY_RS_AC_HIS24 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMONTHKEY_RS_AC_HIS24);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENTSTATUS_RS_AC_HIS24Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENTSTATUS_RS_AC_HIS24",
                xmlWriter);

            if (localPAYMENTSTATUS_RS_AC_HIS24 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENTSTATUS_RS_AC_HIS24 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENTSTATUS_RS_AC_HIS24);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUITFILEDSTATUS_RS_AC_HIS24Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUITFILEDSTATUS_RS_AC_HIS24",
                xmlWriter);

            if (localSUITFILEDSTATUS_RS_AC_HIS24 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUITFILEDSTATUS_RS_AC_HIS24 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUITFILEDSTATUS_RS_AC_HIS24);
            }

            xmlWriter.writeEndElement();
        }

        if (localASSETTCLSSTATUS_RS_AC_HIS24Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ASSETTCLSSTATUS_RS_AC_HIS24",
                xmlWriter);

            if (localASSETTCLSSTATUS_RS_AC_HIS24 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ASSETTCLSSTATUS_RS_AC_HIS24 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localASSETTCLSSTATUS_RS_AC_HIS24);
            }

            xmlWriter.writeEndElement();
        }

        if (localMONTHKEY_RS_AC_HIS48Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "MONTHKEY_RS_AC_HIS48", xmlWriter);

            if (localMONTHKEY_RS_AC_HIS48 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MONTHKEY_RS_AC_HIS48 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMONTHKEY_RS_AC_HIS48);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENTSTATUS_RS_AC_HIS48Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENTSTATUS_RS_AC_HIS48",
                xmlWriter);

            if (localPAYMENTSTATUS_RS_AC_HIS48 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENTSTATUS_RS_AC_HIS48 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENTSTATUS_RS_AC_HIS48);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUITFILEDSTATUS_RS_AC_HIS48Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUITFILEDSTATUS_RS_AC_HIS48",
                xmlWriter);

            if (localSUITFILEDSTATUS_RS_AC_HIS48 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUITFILEDSTATUS_RS_AC_HIS48 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUITFILEDSTATUS_RS_AC_HIS48);
            }

            xmlWriter.writeEndElement();
        }

        if (localASSETCLSSTATUS_RS_AC_HIS48Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ASSETCLSSTATUS_RS_AC_HIS48",
                xmlWriter);

            if (localASSETCLSSTATUS_RS_AC_HIS48 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ASSETCLSSTATUS_RS_AC_HIS48 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localASSETCLSSTATUS_RS_AC_HIS48);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFACCOUNTS_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFACCOUNTS_RS_AC_SUM",
                xmlWriter);

            if (localNOOFACCOUNTS_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFACCOUNTS_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFACCOUNTS_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFACTIVEACCOUNTS_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFACTIVEACCOUNTS_RS_AC_SUM",
                xmlWriter);

            if (localNOOFACTIVEACCOUNTS_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFACTIVEACCOUNTS_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFACTIVEACCOUNTS_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFWRITEOFFS_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFWRITEOFFS_RS_AC_SUM",
                xmlWriter);

            if (localNOOFWRITEOFFS_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFWRITEOFFS_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFWRITEOFFS_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALPASTDUE_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALPASTDUE_RS_AC_SUM",
                xmlWriter);

            if (localTOTALPASTDUE_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALPASTDUE_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALPASTDUE_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOSTSRVRSTAWHIN24MON_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "MOSTSRVRSTAWHIN24MON_RS_AC_SUM", xmlWriter);

            if (localMOSTSRVRSTAWHIN24MON_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOSTSRVRSTAWHIN24MON_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOSTSRVRSTAWHIN24MON_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSINGLEHIGHESTCREDIT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SINGLEHIGHESTCREDIT_RS_AC_SUM",
                xmlWriter);

            if (localSINGLEHIGHESTCREDIT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SINGLEHIGHESTCREDIT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSINGLEHIGHESTCREDIT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSINGLEHIGHSANCTAMT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SINGLEHIGHSANCTAMT_RS_AC_SUM",
                xmlWriter);

            if (localSINGLEHIGHSANCTAMT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SINGLEHIGHSANCTAMT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSINGLEHIGHSANCTAMT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALHIGHCREDIT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALHIGHCREDIT_RS_AC_SUM",
                xmlWriter);

            if (localTOTALHIGHCREDIT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALHIGHCREDIT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALHIGHCREDIT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localAVERAGEOPENBALANCE_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AVERAGEOPENBALANCE_RS_AC_SUM",
                xmlWriter);

            if (localAVERAGEOPENBALANCE_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AVERAGEOPENBALANCE_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAVERAGEOPENBALANCE_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSINGLEHIGHESTBALANCE_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "SINGLEHIGHESTBALANCE_RS_AC_SUM", xmlWriter);

            if (localSINGLEHIGHESTBALANCE_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SINGLEHIGHESTBALANCE_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSINGLEHIGHESTBALANCE_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFPASTDUEACCTS_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFPASTDUEACCTS_RS_AC_SUM",
                xmlWriter);

            if (localNOOFPASTDUEACCTS_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFPASTDUEACCTS_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFPASTDUEACCTS_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFZEROBALACCTS_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFZEROBALACCTS_RS_AC_SUM",
                xmlWriter);

            if (localNOOFZEROBALACCTS_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFZEROBALACCTS_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFZEROBALACCTS_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localRECENTACCOUNT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RECENTACCOUNT_RS_AC_SUM",
                xmlWriter);

            if (localRECENTACCOUNT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RECENTACCOUNT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRECENTACCOUNT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localOLDESTACCOUNT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OLDESTACCOUNT_RS_AC_SUM",
                xmlWriter);

            if (localOLDESTACCOUNT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OLDESTACCOUNT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOLDESTACCOUNT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALBALAMT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALBALAMT_RS_AC_SUM",
                xmlWriter);

            if (localTOTALBALAMT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALBALAMT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALBALAMT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALSANCTAMT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALSANCTAMT_RS_AC_SUM",
                xmlWriter);

            if (localTOTALSANCTAMT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALSANCTAMT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALSANCTAMT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALCRDTLIMIT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALCRDTLIMIT_RS_AC_SUM",
                xmlWriter);

            if (localTOTALCRDTLIMIT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALCRDTLIMIT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALCRDTLIMIT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALMONLYPYMNTAMT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALMONLYPYMNTAMT_RS_AC_SUM",
                xmlWriter);

            if (localTOTALMONLYPYMNTAMT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALMONLYPYMNTAMT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALMONLYPYMNTAMT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALWRITTENOFFAMNT_RS_AC_SUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALWRITTENOFFAMNT_RS_AC_SUM",
                xmlWriter);

            if (localTOTALWRITTENOFFAMNT_RS_AC_SUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALWRITTENOFFAMNT_RS_AC_SUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALWRITTENOFFAMNT_RS_AC_SUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGEOFOLDESTTRADE_RS_OTKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGEOFOLDESTTRADE_RS_OTK",
                xmlWriter);

            if (localAGEOFOLDESTTRADE_RS_OTK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGEOFOLDESTTRADE_RS_OTK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGEOFOLDESTTRADE_RS_OTK);
            }

            xmlWriter.writeEndElement();
        }

        if (localNUMBEROFOPENTRADES_RS_OTKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NUMBEROFOPENTRADES_RS_OTK",
                xmlWriter);

            if (localNUMBEROFOPENTRADES_RS_OTK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NUMBEROFOPENTRADES_RS_OTK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNUMBEROFOPENTRADES_RS_OTK);
            }

            xmlWriter.writeEndElement();
        }

        if (localALINESEVERWRITTEN_RS_OTKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALINESEVERWRITTEN_RS_OTK",
                xmlWriter);

            if (localALINESEVERWRITTEN_RS_OTK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALINESEVERWRITTEN_RS_OTK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALINESEVERWRITTEN_RS_OTK);
            }

            xmlWriter.writeEndElement();
        }

        if (localALINESVWRTNIN9MNTHS_RS_OTKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALINESVWRTNIN9MNTHS_RS_OTK",
                xmlWriter);

            if (localALINESVWRTNIN9MNTHS_RS_OTK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALINESVWRTNIN9MNTHS_RS_OTK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALINESVWRTNIN9MNTHS_RS_OTK);
            }

            xmlWriter.writeEndElement();
        }

        if (localALINESVRWRTNIN6MNTHS_RS_OTKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALINESVRWRTNIN6MNTHS_RS_OTK",
                xmlWriter);

            if (localALINESVRWRTNIN6MNTHS_RS_OTK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALINESVRWRTNIN6MNTHS_RS_OTK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALINESVRWRTNIN6MNTHS_RS_OTK);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_RS_EQ_SM",
                xmlWriter);

            if (localREPORTEDDATE_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localPURPOSE_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PURPOSE_RS_EQ_SM", xmlWriter);

            if (localPURPOSE_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PURPOSE_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPURPOSE_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTAL_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTAL_RS_EQ_SM", xmlWriter);

            if (localTOTAL_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTAL_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTAL_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAST30DAYS_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAST30DAYS_RS_EQ_SM", xmlWriter);

            if (localPAST30DAYS_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAST30DAYS_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAST30DAYS_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAST12MONTHS_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAST12MONTHS_RS_EQ_SM",
                xmlWriter);

            if (localPAST12MONTHS_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAST12MONTHS_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAST12MONTHS_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAST24MONTHS_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAST24MONTHS_RS_EQ_SM",
                xmlWriter);

            if (localPAST24MONTHS_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAST24MONTHS_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAST24MONTHS_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localRECENT_RS_EQ_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RECENT_RS_EQ_SM", xmlWriter);

            if (localRECENT_RS_EQ_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RECENT_RS_EQ_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRECENT_RS_EQ_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORTEDDATE_RS_EQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORTEDDATE_RS_EQ", xmlWriter);

            if (localREPORTEDDATE_RS_EQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORTEDDATE_RS_EQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORTEDDATE_RS_EQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQREFERENCE_RS_EQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQREFERENCE_RS_EQ", xmlWriter);

            if (localENQREFERENCE_RS_EQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQREFERENCE_RS_EQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQREFERENCE_RS_EQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQDATE_RS_EQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQDATE_RS_EQ", xmlWriter);

            if (localENQDATE_RS_EQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQDATE_RS_EQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQDATE_RS_EQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQTIME_RS_EQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQTIME_RS_EQ", xmlWriter);

            if (localENQTIME_RS_EQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQTIME_RS_EQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQTIME_RS_EQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREQUESTPURPOSE_RS_EQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REQUESTPURPOSE_RS_EQ", xmlWriter);

            if (localREQUESTPURPOSE_RS_EQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REQUESTPURPOSE_RS_EQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREQUESTPURPOSE_RS_EQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNT_RS_EQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNT_RS_EQ", xmlWriter);

            if (localAMOUNT_RS_EQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNT_RS_EQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNT_RS_EQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTSDELIQUENT_RS_RE_ACTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTSDELIQUENT_RS_RE_ACT",
                xmlWriter);

            if (localACCOUNTSDELIQUENT_RS_RE_ACT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTSDELIQUENT_RS_RE_ACT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTSDELIQUENT_RS_RE_ACT);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTSOPENED_RS_RE_ACTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTSOPENED_RS_RE_ACT",
                xmlWriter);

            if (localACCOUNTSOPENED_RS_RE_ACT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTSOPENED_RS_RE_ACT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTSOPENED_RS_RE_ACT);
            }

            xmlWriter.writeEndElement();
        }

        if (localTOTALINQUIRIES_RS_RE_ACTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TOTALINQUIRIES_RS_RE_ACT",
                xmlWriter);

            if (localTOTALINQUIRIES_RS_RE_ACT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TOTALINQUIRIES_RS_RE_ACT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTOTALINQUIRIES_RS_RE_ACT);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTSUPDATED_RS_RE_ACTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTSUPDATED_RS_RE_ACT",
                xmlWriter);

            if (localACCOUNTSUPDATED_RS_RE_ACT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTSUPDATED_RS_RE_ACT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTSUPDATED_RS_RE_ACT);
            }

            xmlWriter.writeEndElement();
        }

        if (localINSTITUTION_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INSTITUTION_GRP_SM", xmlWriter);

            if (localINSTITUTION_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INSTITUTION_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINSTITUTION_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENTBALANCE_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENTBALANCE_GRP_SM",
                xmlWriter);

            if (localCURRENTBALANCE_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENTBALANCE_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENTBALANCE_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATUS_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS_GRP_SM", xmlWriter);

            if (localSTATUS_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEREPORTED_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEREPORTED_GRP_SM", xmlWriter);

            if (localDATEREPORTED_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEREPORTED_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEREPORTED_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localNOOFMEMBERS_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NOOFMEMBERS_GRP_SM", xmlWriter);

            if (localNOOFMEMBERS_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NOOFMEMBERS_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNOOFMEMBERS_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASTDUEAMOUNT_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASTDUEAMOUNT_GRP_SM", xmlWriter);

            if (localPASTDUEAMOUNT_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASTDUEAMOUNT_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASTDUEAMOUNT_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSANCTIONAMOUNT_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SANCTIONAMOUNT_GRP_SM",
                xmlWriter);

            if (localSANCTIONAMOUNT_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SANCTIONAMOUNT_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSANCTIONAMOUNT_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATEOPENED_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATEOPENED_GRP_SM", xmlWriter);

            if (localDATEOPENED_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATEOPENED_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATEOPENED_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNTNO_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNTNO_GRP_SM", xmlWriter);

            if (localACCOUNTNO_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNTNO_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNTNO_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBERSPASTDUE_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBERSPASTDUE_GRP_SM",
                xmlWriter);

            if (localMEMBERSPASTDUE_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBERSPASTDUE_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBERSPASTDUE_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITEOFFAMOUNT_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITEOFFAMOUNT_GRP_SM",
                xmlWriter);

            if (localWRITEOFFAMOUNT_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITEOFFAMOUNT_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITEOFFAMOUNT_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITEOFFDATE_GRP_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITEOFFDATE_GRP_SM", xmlWriter);

            if (localWRITEOFFDATE_GRP_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITEOFFDATE_GRP_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITEOFFDATE_GRP_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASONCODE_SCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASONCODE_SC", xmlWriter);

            if (localREASONCODE_SC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASONCODE_SC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASONCODE_SC);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCOREDESCRIPTION_SCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCOREDESCRIPTION_SC", xmlWriter);

            if (localSCOREDESCRIPTION_SC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCOREDESCRIPTION_SC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCOREDESCRIPTION_SC);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORENAME_SCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORENAME_SC", xmlWriter);

            if (localSCORENAME_SC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORENAME_SC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORENAME_SC);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCOREVALUE_SCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCOREVALUE_SC", xmlWriter);

            if (localSCOREVALUE_SC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCOREVALUE_SC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCOREVALUE_SC);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_TIME", xmlWriter);

            if (localOUTPUT_WRITE_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_TIME", xmlWriter);

            if (localOUTPUT_READ_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_KEYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_KEY", xmlWriter);

            if (localACCOUNT_KEY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_KEY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_KEY);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCNT_NUMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCNT_NUM", xmlWriter);

            if (localACCNT_NUM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCNT_NUM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCNT_NUM);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISPUTE_COMMENTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISPUTE_COMMENTS", xmlWriter);

            if (localDISPUTE_COMMENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISPUTE_COMMENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISPUTE_COMMENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS", xmlWriter);

            if (localSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESOLVED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESOLVED_DATE", xmlWriter);

            if (localRESOLVED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESOLVED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESOLVED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CODE", xmlWriter);

            if (localCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDESCRIPTIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DESCRIPTION", xmlWriter);

            if (localDESCRIPTION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DESCRIPTION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDESCRIPTION);
            }

            xmlWriter.writeEndElement();
        }

        if (localHITCODE_HDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "HITCODE_HD", xmlWriter);

            if (localHITCODE_HD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HITCODE_HD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHITCODE_HD);
            }

            xmlWriter.writeEndElement();
        }

        if (localFULLNAME_NM_TPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FULLNAME_NM_TP", xmlWriter);

            if (localFULLNAME_NM_TP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FULLNAME_NM_TP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFULLNAME_NM_TP);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMPPHONE_RS_EMDLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMPPHONE_RS_EMDL", xmlWriter);

            if (localEMPPHONE_RS_EMDL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMPPHONE_RS_EMDL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMPPHONE_RS_EMDL);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static EquifaxSRespType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            EquifaxSRespType object = new EquifaxSRespType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"EquifaxSRespType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (EquifaxSRespType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRY_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRY_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CUSTOMERCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CUSTOMERCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CUSTOMERCODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCUSTOMERCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FIRSTNAME_NM_TP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FIRSTNAME_NM_TP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FIRSTNAME_NM_TP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFIRSTNAME_NM_TP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MIDLLENAME_NM_TP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MIDLLENAME_NM_TP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MIDLLENAME_NM_TP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMIDLLENAME_NM_TP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LASTNAME_NM_TP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LASTNAME_NM_TP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LASTNAME_NM_TP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLASTNAME_NM_TP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDMIDLLENAME_NM_TP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDMIDLLENAME_NM_TP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDMIDLLENAME_NM_TP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDMIDLLENAME_NM_TP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUFFIX_NM_TP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUFFIX_NM_TP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUFFIX_NM_TP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUFFIX_NM_TP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_AG_IF").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_AG_IF").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_AG_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_AG_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE_AG_IF").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE_AG_IF").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE_AG_IF" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE_AG_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_AL_NM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_AL_NM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_AL_NM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_AL_NM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ALIASNAME_AL_NM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALIASNAME_AL_NM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALIASNAME_AL_NM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALIASNAME_AL_NM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ADDITIONALNAMEINFO_AD_NM_IF").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ADDITIONALNAMEINFO_AD_NM_IF").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDITIONALNAMEINFO_AD_NM_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDITIONALNAMEINFO_AD_NM_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFDEPENDENTS_AD_NM_IF").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFDEPENDENTS_AD_NM_IF").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFDEPENDENTS_AD_NM_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFDEPENDENTS_AD_NM_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_ADD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_ADD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_RS_ADD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_RS_ADD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_RS_ADD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_RS_ADD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_RS_ADD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_RS_ADD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATE_RS_ADD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATE_RS_ADD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATE_RS_ADD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATE_RS_ADD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "POSTAL_RS_ADD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "POSTAL_RS_ADD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "POSTAL_RS_ADD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPOSTAL_RS_ADD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDTYPE_RS_ADD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDTYPE_RS_ADD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDTYPE_RS_ADD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDTYPE_RS_ADD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IDTYPE_RS_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IDTYPE_RS_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IDTYPE_RS_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIDTYPE_RS_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_RS_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_RS_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IDNUMBER_RS_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IDNUMBER_RS_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IDNUMBER_RS_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIDNUMBER_RS_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EMAILREPORTEDDATE_RS_ML").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EMAILREPORTEDDATE_RS_ML").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAILREPORTEDDATE_RS_ML" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAILREPORTEDDATE_RS_ML(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAILADDRESS_RS_ML").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAILADDRESS_RS_ML").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAILADDRESS_RS_ML" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAILADDRESS_RS_ML(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMPNMREPDATE_RS_EMDL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMPNMREPDATE_RS_EMDL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMPNMREPDATE_RS_EMDL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMPNMREPDATE_RS_EMDL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMPLOYERNAME_RS_EMDL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMPLOYERNAME_RS_EMDL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMPLOYERNAME_RS_EMDL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMPLOYERNAME_RS_EMDL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMPPOSITION_RS_EMDL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMPPOSITION_RS_EMDL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMPPOSITION_RS_EMDL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMPPOSITION_RS_EMDL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMPADDRESS_RS_EMDL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMPADDRESS_RS_EMDL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMPADDRESS_RS_EMDL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMPADDRESS_RS_EMDL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DOB_RS_PER_IF").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOB_RS_PER_IF").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DOB_RS_PER_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDOB_RS_PER_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDER_RS_PER_IF").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDER_RS_PER_IF").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDER_RS_PER_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDER_RS_PER_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALINCOME_RS_PER_IF").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALINCOME_RS_PER_IF").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALINCOME_RS_PER_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALINCOME_RS_PER_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OCCUPATION_RS_PER_IF").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OCCUPATION_RS_PER_IF").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OCCUPATION_RS_PER_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOCCUPATION_RS_PER_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MARITALSTAUS_RS_PER_IF").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MARITALSTAUS_RS_PER_IF").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MARITALSTAUS_RS_PER_IF" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMARITALSTAUS_RS_PER_IF(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OCCUPATION_IC_DL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OCCUPATION_IC_DL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OCCUPATION_IC_DL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOCCUPATION_IC_DL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MONTHLYINCOME_IC_DL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MONTHLYINCOME_IC_DL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MONTHLYINCOME_IC_DL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMONTHLYINCOME_IC_DL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MONTHLYEXPENSE_IC_DL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MONTHLYEXPENSE_IC_DL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MONTHLYEXPENSE_IC_DL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMONTHLYEXPENSE_IC_DL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "POVERTYINDEX_IC_DL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "POVERTYINDEX_IC_DL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "POVERTYINDEX_IC_DL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPOVERTYINDEX_IC_DL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ASSETOWNERSHIP_IC_DL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ASSETOWNERSHIP_IC_DL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ASSETOWNERSHIP_IC_DL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setASSETOWNERSHIP_IC_DL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_PH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_PH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_RS_PH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_RS_PH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TYPECODE_RS_PH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPECODE_RS_PH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TYPECODE_RS_PH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTYPECODE_RS_PH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "COUNTRYCODE_RS_PH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COUNTRYCODE_RS_PH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COUNTRYCODE_RS_PH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOUNTRYCODE_RS_PH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AREACODE_RS_PH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AREACODE_RS_PH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AREACODE_RS_PH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAREACODE_RS_PH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHNUMBER_RS_PH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHNUMBER_RS_PH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHNUMBER_RS_PH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHNUMBER_RS_PH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHNOEXTENTION_RS_PH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHNOEXTENTION_RS_PH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHNOEXTENTION_RS_PH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHNOEXTENTION_RS_PH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CUSTOMERCODE_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CUSTOMERCODE_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CUSTOMERCODE_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCUSTOMERCODE_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CLIENTID_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CLIENTID_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CLIENTID_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCLIENTID_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CUSTREFFIELD_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CUSTREFFIELD_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CUSTREFFIELD_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCUSTREFFIELD_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTORDERNO_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTORDERNO_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTORDERNO_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTORDERNO_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PRODUCTCODE_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PRODUCTCODE_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PRODUCTCODE_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPRODUCTCODE_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PRODUCTVERSION_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PRODUCTVERSION_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PRODUCTVERSION_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPRODUCTVERSION_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUCCESSCODE_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUCCESSCODE_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUCCESSCODE_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUCCESSCODE_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MATCHTYPE_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MATCHTYPE_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MATCHTYPE_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMATCHTYPE_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RESDATE_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RESDATE_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESDATE_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESDATE_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RESTIME_RS_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RESTIME_RS_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESTIME_RS_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESTIME_RS_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CLIENTNAME_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CLIENTNAME_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CLIENTNAME_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCLIENTNAME_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTNUMBER_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTNUMBER_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTNUMBER_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTNUMBER_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CURRENTBALANCE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CURRENTBALANCE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENTBALANCE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENTBALANCE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INSTITUTION_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INSTITUTION_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INSTITUTION_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINSTITUTION_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTTYPE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTTYPE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTTYPE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTTYPE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNERSHIPTYPE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNERSHIPTYPE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNERSHIPTYPE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNERSHIPTYPE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BALANCE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BALANCE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BALANCE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBALANCE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASTDUEAMT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASTDUEAMT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASTDUEAMT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASTDUEAMT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DISBURSEDAMOUNT_RS_AC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DISBURSEDAMOUNT_RS_AC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISBURSEDAMOUNT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISBURSEDAMOUNT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOANCATEGORY_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOANCATEGORY_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOANCATEGORY_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOANCATEGORY_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOANPURPOSE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOANPURPOSE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOANPURPOSE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOANPURPOSE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LASTPAYMENT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LASTPAYMENT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LASTPAYMENT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLASTPAYMENT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WRITEOFFAMT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WRITEOFFAMT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITEOFFAMT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITEOFFAMT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOPEN_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOPEN_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOPEN_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOPEN_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SACTIONAMT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SACTIONAMT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SACTIONAMT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSACTIONAMT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LASTPAYMENTDATE_RS_AC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LASTPAYMENTDATE_RS_AC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LASTPAYMENTDATE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLASTPAYMENTDATE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HIGHCREDIT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "HIGHCREDIT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HIGHCREDIT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHIGHCREDIT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEREPORTED_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEREPORTED_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEREPORTED_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEREPORTED_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEOPENED_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEOPENED_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOPENED_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOPENED_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATECLOSED_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATECLOSED_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATECLOSED_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATECLOSED_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REASON_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REASON_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEWRITTENOFF_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEWRITTENOFF_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEWRITTENOFF_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEWRITTENOFF_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOANCYCLEID_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOANCYCLEID_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOANCYCLEID_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOANCYCLEID_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATESANCTIONED_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATESANCTIONED_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATESANCTIONED_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATESANCTIONED_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEAPPLIED_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEAPPLIED_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEAPPLIED_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEAPPLIED_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INTRESTRATE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INTRESTRATE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INTRESTRATE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINTRESTRATE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "APPLIEDAMOUNT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "APPLIEDAMOUNT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "APPLIEDAMOUNT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAPPLIEDAMOUNT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFINSTALLMENTS_RS_AC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFINSTALLMENTS_RS_AC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFINSTALLMENTS_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFINSTALLMENTS_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REPAYMENTTENURE_RS_AC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REPAYMENTTENURE_RS_AC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPAYMENTTENURE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPAYMENTTENURE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISPUTECODE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISPUTECODE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISPUTECODE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISPUTECODE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INSTALLMENTAMT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INSTALLMENTAMT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INSTALLMENTAMT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINSTALLMENTAMT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "KEYPERSON_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "KEYPERSON_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "KEYPERSON_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setKEYPERSON_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NOMINEE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NOMINEE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOMINEE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOMINEE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TERMFREQUENCY_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TERMFREQUENCY_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TERMFREQUENCY_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTERMFREQUENCY_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITLIMIT_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITLIMIT_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITLIMIT_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITLIMIT_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "COLLATERALVALUE_RS_AC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "COLLATERALVALUE_RS_AC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COLLATERALVALUE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOLLATERALVALUE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "COLLATERALTYPE_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLLATERALTYPE_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COLLATERALTYPE_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOLLATERALTYPE_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTSTATUS_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTSTATUS_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTSTATUS_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTSTATUS_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ASSETCLASSIFICATION_RS_AC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ASSETCLASSIFICATION_RS_AC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ASSETCLASSIFICATION_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setASSETCLASSIFICATION_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SUITFILEDSTATUS_RS_AC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SUITFILEDSTATUS_RS_AC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUITFILEDSTATUS_RS_AC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUITFILEDSTATUS_RS_AC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MONTHKEY_RS_AC_HIS24").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MONTHKEY_RS_AC_HIS24").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MONTHKEY_RS_AC_HIS24" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMONTHKEY_RS_AC_HIS24(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAYMENTSTATUS_RS_AC_HIS24").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAYMENTSTATUS_RS_AC_HIS24").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENTSTATUS_RS_AC_HIS24" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENTSTATUS_RS_AC_HIS24(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SUITFILEDSTATUS_RS_AC_HIS24").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SUITFILEDSTATUS_RS_AC_HIS24").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUITFILEDSTATUS_RS_AC_HIS24" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUITFILEDSTATUS_RS_AC_HIS24(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ASSETTCLSSTATUS_RS_AC_HIS24").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ASSETTCLSSTATUS_RS_AC_HIS24").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ASSETTCLSSTATUS_RS_AC_HIS24" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setASSETTCLSSTATUS_RS_AC_HIS24(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MONTHKEY_RS_AC_HIS48").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MONTHKEY_RS_AC_HIS48").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MONTHKEY_RS_AC_HIS48" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMONTHKEY_RS_AC_HIS48(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAYMENTSTATUS_RS_AC_HIS48").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAYMENTSTATUS_RS_AC_HIS48").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENTSTATUS_RS_AC_HIS48" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENTSTATUS_RS_AC_HIS48(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SUITFILEDSTATUS_RS_AC_HIS48").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SUITFILEDSTATUS_RS_AC_HIS48").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUITFILEDSTATUS_RS_AC_HIS48" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUITFILEDSTATUS_RS_AC_HIS48(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ASSETCLSSTATUS_RS_AC_HIS48").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ASSETCLSSTATUS_RS_AC_HIS48").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ASSETCLSSTATUS_RS_AC_HIS48" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setASSETCLSSTATUS_RS_AC_HIS48(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFACCOUNTS_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFACCOUNTS_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFACCOUNTS_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFACCOUNTS_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFACTIVEACCOUNTS_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFACTIVEACCOUNTS_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFACTIVEACCOUNTS_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFACTIVEACCOUNTS_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFWRITEOFFS_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFWRITEOFFS_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFWRITEOFFS_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFWRITEOFFS_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALPASTDUE_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALPASTDUE_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALPASTDUE_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALPASTDUE_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MOSTSRVRSTAWHIN24MON_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MOSTSRVRSTAWHIN24MON_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOSTSRVRSTAWHIN24MON_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOSTSRVRSTAWHIN24MON_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SINGLEHIGHESTCREDIT_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SINGLEHIGHESTCREDIT_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SINGLEHIGHESTCREDIT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSINGLEHIGHESTCREDIT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SINGLEHIGHSANCTAMT_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SINGLEHIGHSANCTAMT_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SINGLEHIGHSANCTAMT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSINGLEHIGHSANCTAMT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALHIGHCREDIT_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALHIGHCREDIT_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALHIGHCREDIT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALHIGHCREDIT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "AVERAGEOPENBALANCE_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "AVERAGEOPENBALANCE_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AVERAGEOPENBALANCE_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAVERAGEOPENBALANCE_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SINGLEHIGHESTBALANCE_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SINGLEHIGHESTBALANCE_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SINGLEHIGHESTBALANCE_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSINGLEHIGHESTBALANCE_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFPASTDUEACCTS_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFPASTDUEACCTS_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFPASTDUEACCTS_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFPASTDUEACCTS_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NOOFZEROBALACCTS_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NOOFZEROBALACCTS_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFZEROBALACCTS_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFZEROBALACCTS_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "RECENTACCOUNT_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "RECENTACCOUNT_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RECENTACCOUNT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRECENTACCOUNT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OLDESTACCOUNT_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OLDESTACCOUNT_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OLDESTACCOUNT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOLDESTACCOUNT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALBALAMT_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALBALAMT_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALBALAMT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALBALAMT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALSANCTAMT_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALSANCTAMT_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALSANCTAMT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALSANCTAMT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALCRDTLIMIT_RS_AC_SUM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALCRDTLIMIT_RS_AC_SUM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALCRDTLIMIT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALCRDTLIMIT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALMONLYPYMNTAMT_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALMONLYPYMNTAMT_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALMONLYPYMNTAMT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALMONLYPYMNTAMT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALWRITTENOFFAMNT_RS_AC_SUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALWRITTENOFFAMNT_RS_AC_SUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALWRITTENOFFAMNT_RS_AC_SUM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALWRITTENOFFAMNT_RS_AC_SUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "AGEOFOLDESTTRADE_RS_OTK").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "AGEOFOLDESTTRADE_RS_OTK").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGEOFOLDESTTRADE_RS_OTK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGEOFOLDESTTRADE_RS_OTK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NUMBEROFOPENTRADES_RS_OTK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NUMBEROFOPENTRADES_RS_OTK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUMBEROFOPENTRADES_RS_OTK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUMBEROFOPENTRADES_RS_OTK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ALINESEVERWRITTEN_RS_OTK").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ALINESEVERWRITTEN_RS_OTK").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALINESEVERWRITTEN_RS_OTK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALINESEVERWRITTEN_RS_OTK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ALINESVWRTNIN9MNTHS_RS_OTK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ALINESVWRTNIN9MNTHS_RS_OTK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALINESVWRTNIN9MNTHS_RS_OTK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALINESVWRTNIN9MNTHS_RS_OTK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ALINESVRWRTNIN6MNTHS_RS_OTK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ALINESVRWRTNIN6MNTHS_RS_OTK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALINESVRWRTNIN6MNTHS_RS_OTK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALINESVRWRTNIN6MNTHS_RS_OTK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REPORTEDDATE_RS_EQ_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REPORTEDDATE_RS_EQ_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PURPOSE_RS_EQ_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PURPOSE_RS_EQ_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PURPOSE_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPURPOSE_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TOTAL_RS_EQ_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TOTAL_RS_EQ_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTAL_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTAL_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAST30DAYS_RS_EQ_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAST30DAYS_RS_EQ_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAST30DAYS_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAST30DAYS_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAST12MONTHS_RS_EQ_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAST12MONTHS_RS_EQ_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAST12MONTHS_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAST12MONTHS_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAST24MONTHS_RS_EQ_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAST24MONTHS_RS_EQ_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAST24MONTHS_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAST24MONTHS_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RECENT_RS_EQ_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RECENT_RS_EQ_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RECENT_RS_EQ_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRECENT_RS_EQ_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_EQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORTEDDATE_RS_EQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORTEDDATE_RS_EQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORTEDDATE_RS_EQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQREFERENCE_RS_EQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQREFERENCE_RS_EQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQREFERENCE_RS_EQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQREFERENCE_RS_EQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQDATE_RS_EQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQDATE_RS_EQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQDATE_RS_EQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQDATE_RS_EQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQTIME_RS_EQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQTIME_RS_EQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQTIME_RS_EQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQTIME_RS_EQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REQUESTPURPOSE_RS_EQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REQUESTPURPOSE_RS_EQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REQUESTPURPOSE_RS_EQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREQUESTPURPOSE_RS_EQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AMOUNT_RS_EQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AMOUNT_RS_EQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNT_RS_EQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNT_RS_EQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ACCOUNTSDELIQUENT_RS_RE_ACT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ACCOUNTSDELIQUENT_RS_RE_ACT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTSDELIQUENT_RS_RE_ACT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTSDELIQUENT_RS_RE_ACT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ACCOUNTSOPENED_RS_RE_ACT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ACCOUNTSOPENED_RS_RE_ACT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTSOPENED_RS_RE_ACT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTSOPENED_RS_RE_ACT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "TOTALINQUIRIES_RS_RE_ACT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "TOTALINQUIRIES_RS_RE_ACT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TOTALINQUIRIES_RS_RE_ACT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTOTALINQUIRIES_RS_RE_ACT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ACCOUNTSUPDATED_RS_RE_ACT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ACCOUNTSUPDATED_RS_RE_ACT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTSUPDATED_RS_RE_ACT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTSUPDATED_RS_RE_ACT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INSTITUTION_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INSTITUTION_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INSTITUTION_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINSTITUTION_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CURRENTBALANCE_GRP_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CURRENTBALANCE_GRP_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENTBALANCE_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENTBALANCE_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEREPORTED_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEREPORTED_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEREPORTED_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEREPORTED_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NOOFMEMBERS_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NOOFMEMBERS_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NOOFMEMBERS_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNOOFMEMBERS_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASTDUEAMOUNT_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASTDUEAMOUNT_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASTDUEAMOUNT_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASTDUEAMOUNT_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SANCTIONAMOUNT_GRP_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SANCTIONAMOUNT_GRP_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SANCTIONAMOUNT_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSANCTIONAMOUNT_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATEOPENED_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATEOPENED_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATEOPENED_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATEOPENED_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNTNO_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNTNO_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNTNO_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNTNO_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBERSPASTDUE_GRP_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBERSPASTDUE_GRP_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBERSPASTDUE_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBERSPASTDUE_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "WRITEOFFAMOUNT_GRP_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "WRITEOFFAMOUNT_GRP_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITEOFFAMOUNT_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITEOFFAMOUNT_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WRITEOFFDATE_GRP_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WRITEOFFDATE_GRP_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITEOFFDATE_GRP_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITEOFFDATE_GRP_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REASONCODE_SC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REASONCODE_SC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASONCODE_SC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASONCODE_SC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCOREDESCRIPTION_SC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCOREDESCRIPTION_SC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCOREDESCRIPTION_SC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCOREDESCRIPTION_SC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORENAME_SC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORENAME_SC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORENAME_SC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORENAME_SC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCOREVALUE_SC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCOREVALUE_SC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCOREVALUE_SC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCOREVALUE_SC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_KEY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_KEY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_KEY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_KEY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCNT_NUM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCNT_NUM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCNT_NUM" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCNT_NUM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISPUTE_COMMENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISPUTE_COMMENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISPUTE_COMMENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISPUTE_COMMENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RESOLVED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RESOLVED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESOLVED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESOLVED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CODE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DESCRIPTION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DESCRIPTION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DESCRIPTION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDESCRIPTION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HITCODE_HD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "HITCODE_HD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HITCODE_HD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHITCODE_HD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FULLNAME_NM_TP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FULLNAME_NM_TP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FULLNAME_NM_TP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFULLNAME_NM_TP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMPPHONE_RS_EMDL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMPPHONE_RS_EMDL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMPPHONE_RS_EMDL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMPPHONE_RS_EMDL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
