/**
 * ChmbaseSRespType2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ChmbaseSRespType2 bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ChmbaseSRespType2 implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ChmbaseSRespType2
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SM_SECURITY_STATUS
     */
    protected java.lang.String localSM_SECURITY_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_STATUSTracker = false;

    /**
     * field for SM_ORIGINAL_TERM
     */
    protected java.lang.String localSM_ORIGINAL_TERM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ORIGINAL_TERMTracker = false;

    /**
     * field for SM_TERM_TO_MATURITY
     */
    protected java.lang.String localSM_TERM_TO_MATURITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_TERM_TO_MATURITYTracker = false;

    /**
     * field for SM_ACCT_IN_DISPUTE
     */
    protected java.lang.String localSM_ACCT_IN_DISPUTE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ACCT_IN_DISPUTETracker = false;

    /**
     * field for SM_SETTLEMENT_AMT
     */
    protected java.lang.String localSM_SETTLEMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SETTLEMENT_AMTTracker = false;

    /**
     * field for SM_PRINCIPAL_WRITE_OFF_AMT
     */
    protected java.lang.String localSM_PRINCIPAL_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_PRINCIPAL_WRITE_OFF_AMTTracker = false;

    /**
     * field for SM_COMBINED_PAYMENT_HISTORY
     */
    protected java.lang.String localSM_COMBINED_PAYMENT_HISTORY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_COMBINED_PAYMENT_HISTORYTracker = false;

    /**
     * field for SM_REPAYMENT_TENURE
     */
    protected java.lang.String localSM_REPAYMENT_TENURE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_REPAYMENT_TENURETracker = false;

    /**
     * field for SM_INTEREST_RATE
     */
    protected java.lang.String localSM_INTEREST_RATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_INTEREST_RATETracker = false;

    /**
     * field for SM_SUIT_FILED_WILFUL_DEFAULT
     */
    protected java.lang.String localSM_SUIT_FILED_WILFUL_DEFAULT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SUIT_FILED_WILFUL_DEFAULTTracker = false;

    /**
     * field for SM_WRITTEN_OFF_SETTLED_STATUS
     */
    protected java.lang.String localSM_WRITTEN_OFF_SETTLED_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_WRITTEN_OFF_SETTLED_STATUSTracker = false;

    /**
     * field for SM_CASH_LIMIT
     */
    protected java.lang.String localSM_CASH_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CASH_LIMITTracker = false;

    /**
     * field for SM_ACTUAL_PAYMENT
     */
    protected java.lang.String localSM_ACTUAL_PAYMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ACTUAL_PAYMENTTracker = false;

    /**
     * field for SM_SECURITY_TYPE_LD
     */
    protected java.lang.String localSM_SECURITY_TYPE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_TYPE_LDTracker = false;

    /**
     * field for SM_OWNER_NAME_LD
     */
    protected java.lang.String localSM_OWNER_NAME_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_OWNER_NAME_LDTracker = false;

    /**
     * field for SM_SECURITY_VALUE_LD
     */
    protected java.lang.String localSM_SECURITY_VALUE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_VALUE_LDTracker = false;

    /**
     * field for SM_DATE_OF_VALUE_LD
     */
    protected java.lang.String localSM_DATE_OF_VALUE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DATE_OF_VALUE_LDTracker = false;

    /**
     * field for SM_SECURITY_CHARGE_LD
     */
    protected java.lang.String localSM_SECURITY_CHARGE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_CHARGE_LDTracker = false;

    /**
     * field for SM_PROPERTY_ADDRESS_LD
     */
    protected java.lang.String localSM_PROPERTY_ADDRESS_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_PROPERTY_ADDRESS_LDTracker = false;

    /**
     * field for SM_AUTOMOBILE_TYPE_LD
     */
    protected java.lang.String localSM_AUTOMOBILE_TYPE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_AUTOMOBILE_TYPE_LDTracker = false;

    /**
     * field for SM_YEAR_OF_MANUFACTURE_LD
     */
    protected java.lang.String localSM_YEAR_OF_MANUFACTURE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_YEAR_OF_MANUFACTURE_LDTracker = false;

    /**
     * field for SM_REGISTRATION_NUMBER_LD
     */
    protected java.lang.String localSM_REGISTRATION_NUMBER_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_REGISTRATION_NUMBER_LDTracker = false;

    /**
     * field for SM_ENGINE_NUMBER_LD
     */
    protected java.lang.String localSM_ENGINE_NUMBER_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ENGINE_NUMBER_LDTracker = false;

    /**
     * field for SM_CHASIS_NUMBER_LD
     */
    protected java.lang.String localSM_CHASIS_NUMBER_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CHASIS_NUMBER_LDTracker = false;

    /**
     * field for SM_LINKED_ACCT_NUMBER
     */
    protected java.lang.String localSM_LINKED_ACCT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ACCT_NUMBERTracker = false;

    /**
     * field for SM_LINKED_CREDIT_GUARANTOR
     */
    protected java.lang.String localSM_LINKED_CREDIT_GUARANTOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_CREDIT_GUARANTORTracker = false;

    /**
     * field for SM_LINKED_ACCT_TYPE
     */
    protected java.lang.String localSM_LINKED_ACCT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ACCT_TYPETracker = false;

    /**
     * field for SM_LINKED_DATE_REPORTED
     */
    protected java.lang.String localSM_LINKED_DATE_REPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_DATE_REPORTEDTracker = false;

    /**
     * field for SM_LINKED_OWNERSHIP_IND
     */
    protected java.lang.String localSM_LINKED_OWNERSHIP_IND;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_OWNERSHIP_INDTracker = false;

    /**
     * field for SM_LINKED_ACCOUNT_STATUS
     */
    protected java.lang.String localSM_LINKED_ACCOUNT_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ACCOUNT_STATUSTracker = false;

    /**
     * field for SM_LINKED_DISBURSED_AMT
     */
    protected java.lang.String localSM_LINKED_DISBURSED_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_DISBURSED_AMTTracker = false;

    /**
     * field for SM_LINKED_DISBURSED_DT
     */
    protected java.lang.String localSM_LINKED_DISBURSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_DISBURSED_DTTracker = false;

    /**
     * field for SM_LINKED_LAST_PAYMENT_DATE
     */
    protected java.lang.String localSM_LINKED_LAST_PAYMENT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_LAST_PAYMENT_DATETracker = false;

    /**
     * field for SM_LINKED_CLOSED_DATE
     */
    protected java.lang.String localSM_LINKED_CLOSED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_CLOSED_DATETracker = false;

    /**
     * field for SM_LINKED_INSTALLMENT_AMT
     */
    protected java.lang.String localSM_LINKED_INSTALLMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_INSTALLMENT_AMTTracker = false;

    /**
     * field for SM_LINKED_OVERDUE_AMT
     */
    protected java.lang.String localSM_LINKED_OVERDUE_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_OVERDUE_AMTTracker = false;

    /**
     * field for SM_LINKED_WRITE_OFF_AMT
     */
    protected java.lang.String localSM_LINKED_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_WRITE_OFF_AMTTracker = false;

    /**
     * field for SM_LINKED_CURRENT_BAL
     */
    protected java.lang.String localSM_LINKED_CURRENT_BAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_CURRENT_BALTracker = false;

    /**
     * field for SM_LINKED_CREDIT_LIMIT
     */
    protected java.lang.String localSM_LINKED_CREDIT_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_CREDIT_LIMITTracker = false;

    /**
     * field for SM_LINKED_ACCOUNT_REMARKS
     */
    protected java.lang.String localSM_LINKED_ACCOUNT_REMARKS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ACCOUNT_REMARKSTracker = false;

    /**
     * field for SM_LINKED_FREQUENCY
     */
    protected java.lang.String localSM_LINKED_FREQUENCY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_FREQUENCYTracker = false;

    /**
     * field for SM_LINKED_SECURITY_STATUS
     */
    protected java.lang.String localSM_LINKED_SECURITY_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_SECURITY_STATUSTracker = false;

    /**
     * field for SM_LINKED_ORIGINAL_TERM
     */
    protected java.lang.String localSM_LINKED_ORIGINAL_TERM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ORIGINAL_TERMTracker = false;

    /**
     * field for SM_LINKED_TERM_TO_MATURITY
     */
    protected java.lang.String localSM_LINKED_TERM_TO_MATURITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_TERM_TO_MATURITYTracker = false;

    /**
     * field for SM_LINKED_ACCT_IN_DISPUTE
     */
    protected java.lang.String localSM_LINKED_ACCT_IN_DISPUTE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ACCT_IN_DISPUTETracker = false;

    /**
     * field for SM_LINKED_SETTLEMENT_AMT
     */
    protected java.lang.String localSM_LINKED_SETTLEMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_SETTLEMENT_AMTTracker = false;

    /**
     * field for SM_LINKED_PRNPAL_WRITE_OFF_AMT
     */
    protected java.lang.String localSM_LINKED_PRNPAL_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_PRNPAL_WRITE_OFF_AMTTracker = false;

    /**
     * field for SM_LINKED_REPAYMENT_TENURE
     */
    protected java.lang.String localSM_LINKED_REPAYMENT_TENURE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_REPAYMENT_TENURETracker = false;

    /**
     * field for SM_LINKED_INTEREST_RATE
     */
    protected java.lang.String localSM_LINKED_INTEREST_RATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_INTEREST_RATETracker = false;

    /**
     * field for SM_LINKED_SUIT_F_WILFUL_DFLT
     */
    protected java.lang.String localSM_LINKED_SUIT_F_WILFUL_DFLT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_SUIT_F_WILFUL_DFLTTracker = false;

    /**
     * field for SM_LINKED_WRTN_OFF_SETTLD_STAT
     */
    protected java.lang.String localSM_LINKED_WRTN_OFF_SETTLD_STAT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_WRTN_OFF_SETTLD_STATTracker = false;

    /**
     * field for SM_LINKED_CASH_LIMIT
     */
    protected java.lang.String localSM_LINKED_CASH_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_CASH_LIMITTracker = false;

    /**
     * field for SM_LINKED_ACTUAL_PAYMENT
     */
    protected java.lang.String localSM_LINKED_ACTUAL_PAYMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LINKED_ACTUAL_PAYMENTTracker = false;

    /**
     * field for SM_SECURITY_TYPE_LA
     */
    protected java.lang.String localSM_SECURITY_TYPE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_TYPE_LATracker = false;

    /**
     * field for SM_OWNER_NAME_LA
     */
    protected java.lang.String localSM_OWNER_NAME_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_OWNER_NAME_LATracker = false;

    /**
     * field for SM_SECURITY_VALUE_LA
     */
    protected java.lang.String localSM_SECURITY_VALUE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_VALUE_LATracker = false;

    /**
     * field for SM_DATE_OF_VALUE_LA
     */
    protected java.lang.String localSM_DATE_OF_VALUE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DATE_OF_VALUE_LATracker = false;

    /**
     * field for SM_SECURITY_CHARGE_LA
     */
    protected java.lang.String localSM_SECURITY_CHARGE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_SECURITY_CHARGE_LATracker = false;

    /**
     * field for SM_PROPERTY_ADDRESS_LA
     */
    protected java.lang.String localSM_PROPERTY_ADDRESS_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_PROPERTY_ADDRESS_LATracker = false;

    /**
     * field for SM_AUTOMOBILE_TYPE_LA
     */
    protected java.lang.String localSM_AUTOMOBILE_TYPE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_AUTOMOBILE_TYPE_LATracker = false;

    /**
     * field for SM_YEAR_OF_MANUFACTURE_LA
     */
    protected java.lang.String localSM_YEAR_OF_MANUFACTURE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_YEAR_OF_MANUFACTURE_LATracker = false;

    /**
     * field for SM_REGISTRATION_NUMBER_LA
     */
    protected java.lang.String localSM_REGISTRATION_NUMBER_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_REGISTRATION_NUMBER_LATracker = false;

    /**
     * field for SM_ENGINE_NUMBER_LA
     */
    protected java.lang.String localSM_ENGINE_NUMBER_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ENGINE_NUMBER_LATracker = false;

    /**
     * field for SM_CHASIS_NUMBER_LA
     */
    protected java.lang.String localSM_CHASIS_NUMBER_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CHASIS_NUMBER_LATracker = false;

    /**
     * field for SCORE_TYPE
     */
    protected java.lang.String localSCORE_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_TYPETracker = false;

    /**
     * field for SCORE_VERSION
     */
    protected java.lang.String localSCORE_VERSION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_VERSIONTracker = false;

    /**
     * field for SCORE_VALUE
     */
    protected java.lang.String localSCORE_VALUE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_VALUETracker = false;

    /**
     * field for SCORE_FACTORS
     */
    protected java.lang.String localSCORE_FACTORS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_FACTORSTracker = false;

    /**
     * field for SCORE_COMMENTS
     */
    protected java.lang.String localSCORE_COMMENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_COMMENTSTracker = false;

    /**
     * field for MEMBER_NAME
     */
    protected java.lang.String localMEMBER_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_NAMETracker = false;

    /**
     * field for INQUIRY_DATE
     */
    protected java.lang.String localINQUIRY_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINQUIRY_DATETracker = false;

    /**
     * field for PURPOSE
     */
    protected java.lang.String localPURPOSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPURPOSETracker = false;

    /**
     * field for OWNERSHIP_TYPE
     */
    protected java.lang.String localOWNERSHIP_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNERSHIP_TYPETracker = false;

    /**
     * field for AMOUNT
     */
    protected java.lang.String localAMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNTTracker = false;

    /**
     * field for REMARK
     */
    protected java.lang.String localREMARK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREMARKTracker = false;

    /**
     * field for COMMENT_TEXT
     */
    protected java.lang.String localCOMMENT_TEXT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOMMENT_TEXTTracker = false;

    /**
     * field for COMMENT_DATE
     */
    protected java.lang.String localCOMMENT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOMMENT_DATETracker = false;

    /**
     * field for BUREAU_COMMENT
     */
    protected java.lang.String localBUREAU_COMMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBUREAU_COMMENTTracker = false;

    /**
     * field for ALERT_TYPE
     */
    protected java.lang.String localALERT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALERT_TYPETracker = false;

    /**
     * field for ALERT_DESC
     */
    protected java.lang.String localALERT_DESC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALERT_DESCTracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_TIME
     */
    protected java.lang.String localOUTPUT_WRITE_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_TIMETracker = false;

    /**
     * field for OUTPUT_READ_TIME
     */
    protected java.lang.String localOUTPUT_READ_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_TIMETracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isSM_SECURITY_STATUSSpecified() {
        return localSM_SECURITY_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_STATUS() {
        return localSM_SECURITY_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_STATUS
     */
    public void setSM_SECURITY_STATUS(java.lang.String param) {
        localSM_SECURITY_STATUSTracker = param != null;

        this.localSM_SECURITY_STATUS = param;
    }

    public boolean isSM_ORIGINAL_TERMSpecified() {
        return localSM_ORIGINAL_TERMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ORIGINAL_TERM() {
        return localSM_ORIGINAL_TERM;
    }

    /**
     * Auto generated setter method
     * @param param SM_ORIGINAL_TERM
     */
    public void setSM_ORIGINAL_TERM(java.lang.String param) {
        localSM_ORIGINAL_TERMTracker = param != null;

        this.localSM_ORIGINAL_TERM = param;
    }

    public boolean isSM_TERM_TO_MATURITYSpecified() {
        return localSM_TERM_TO_MATURITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_TERM_TO_MATURITY() {
        return localSM_TERM_TO_MATURITY;
    }

    /**
     * Auto generated setter method
     * @param param SM_TERM_TO_MATURITY
     */
    public void setSM_TERM_TO_MATURITY(java.lang.String param) {
        localSM_TERM_TO_MATURITYTracker = param != null;

        this.localSM_TERM_TO_MATURITY = param;
    }

    public boolean isSM_ACCT_IN_DISPUTESpecified() {
        return localSM_ACCT_IN_DISPUTETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ACCT_IN_DISPUTE() {
        return localSM_ACCT_IN_DISPUTE;
    }

    /**
     * Auto generated setter method
     * @param param SM_ACCT_IN_DISPUTE
     */
    public void setSM_ACCT_IN_DISPUTE(java.lang.String param) {
        localSM_ACCT_IN_DISPUTETracker = param != null;

        this.localSM_ACCT_IN_DISPUTE = param;
    }

    public boolean isSM_SETTLEMENT_AMTSpecified() {
        return localSM_SETTLEMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SETTLEMENT_AMT() {
        return localSM_SETTLEMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_SETTLEMENT_AMT
     */
    public void setSM_SETTLEMENT_AMT(java.lang.String param) {
        localSM_SETTLEMENT_AMTTracker = param != null;

        this.localSM_SETTLEMENT_AMT = param;
    }

    public boolean isSM_PRINCIPAL_WRITE_OFF_AMTSpecified() {
        return localSM_PRINCIPAL_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_PRINCIPAL_WRITE_OFF_AMT() {
        return localSM_PRINCIPAL_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_PRINCIPAL_WRITE_OFF_AMT
     */
    public void setSM_PRINCIPAL_WRITE_OFF_AMT(java.lang.String param) {
        localSM_PRINCIPAL_WRITE_OFF_AMTTracker = param != null;

        this.localSM_PRINCIPAL_WRITE_OFF_AMT = param;
    }

    public boolean isSM_COMBINED_PAYMENT_HISTORYSpecified() {
        return localSM_COMBINED_PAYMENT_HISTORYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_COMBINED_PAYMENT_HISTORY() {
        return localSM_COMBINED_PAYMENT_HISTORY;
    }

    /**
     * Auto generated setter method
     * @param param SM_COMBINED_PAYMENT_HISTORY
     */
    public void setSM_COMBINED_PAYMENT_HISTORY(java.lang.String param) {
        localSM_COMBINED_PAYMENT_HISTORYTracker = param != null;

        this.localSM_COMBINED_PAYMENT_HISTORY = param;
    }

    public boolean isSM_REPAYMENT_TENURESpecified() {
        return localSM_REPAYMENT_TENURETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_REPAYMENT_TENURE() {
        return localSM_REPAYMENT_TENURE;
    }

    /**
     * Auto generated setter method
     * @param param SM_REPAYMENT_TENURE
     */
    public void setSM_REPAYMENT_TENURE(java.lang.String param) {
        localSM_REPAYMENT_TENURETracker = param != null;

        this.localSM_REPAYMENT_TENURE = param;
    }

    public boolean isSM_INTEREST_RATESpecified() {
        return localSM_INTEREST_RATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_INTEREST_RATE() {
        return localSM_INTEREST_RATE;
    }

    /**
     * Auto generated setter method
     * @param param SM_INTEREST_RATE
     */
    public void setSM_INTEREST_RATE(java.lang.String param) {
        localSM_INTEREST_RATETracker = param != null;

        this.localSM_INTEREST_RATE = param;
    }

    public boolean isSM_SUIT_FILED_WILFUL_DEFAULTSpecified() {
        return localSM_SUIT_FILED_WILFUL_DEFAULTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SUIT_FILED_WILFUL_DEFAULT() {
        return localSM_SUIT_FILED_WILFUL_DEFAULT;
    }

    /**
     * Auto generated setter method
     * @param param SM_SUIT_FILED_WILFUL_DEFAULT
     */
    public void setSM_SUIT_FILED_WILFUL_DEFAULT(java.lang.String param) {
        localSM_SUIT_FILED_WILFUL_DEFAULTTracker = param != null;

        this.localSM_SUIT_FILED_WILFUL_DEFAULT = param;
    }

    public boolean isSM_WRITTEN_OFF_SETTLED_STATUSSpecified() {
        return localSM_WRITTEN_OFF_SETTLED_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_WRITTEN_OFF_SETTLED_STATUS() {
        return localSM_WRITTEN_OFF_SETTLED_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param SM_WRITTEN_OFF_SETTLED_STATUS
     */
    public void setSM_WRITTEN_OFF_SETTLED_STATUS(java.lang.String param) {
        localSM_WRITTEN_OFF_SETTLED_STATUSTracker = param != null;

        this.localSM_WRITTEN_OFF_SETTLED_STATUS = param;
    }

    public boolean isSM_CASH_LIMITSpecified() {
        return localSM_CASH_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CASH_LIMIT() {
        return localSM_CASH_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param SM_CASH_LIMIT
     */
    public void setSM_CASH_LIMIT(java.lang.String param) {
        localSM_CASH_LIMITTracker = param != null;

        this.localSM_CASH_LIMIT = param;
    }

    public boolean isSM_ACTUAL_PAYMENTSpecified() {
        return localSM_ACTUAL_PAYMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ACTUAL_PAYMENT() {
        return localSM_ACTUAL_PAYMENT;
    }

    /**
     * Auto generated setter method
     * @param param SM_ACTUAL_PAYMENT
     */
    public void setSM_ACTUAL_PAYMENT(java.lang.String param) {
        localSM_ACTUAL_PAYMENTTracker = param != null;

        this.localSM_ACTUAL_PAYMENT = param;
    }

    public boolean isSM_SECURITY_TYPE_LDSpecified() {
        return localSM_SECURITY_TYPE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_TYPE_LD() {
        return localSM_SECURITY_TYPE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_TYPE_LD
     */
    public void setSM_SECURITY_TYPE_LD(java.lang.String param) {
        localSM_SECURITY_TYPE_LDTracker = param != null;

        this.localSM_SECURITY_TYPE_LD = param;
    }

    public boolean isSM_OWNER_NAME_LDSpecified() {
        return localSM_OWNER_NAME_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_OWNER_NAME_LD() {
        return localSM_OWNER_NAME_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_OWNER_NAME_LD
     */
    public void setSM_OWNER_NAME_LD(java.lang.String param) {
        localSM_OWNER_NAME_LDTracker = param != null;

        this.localSM_OWNER_NAME_LD = param;
    }

    public boolean isSM_SECURITY_VALUE_LDSpecified() {
        return localSM_SECURITY_VALUE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_VALUE_LD() {
        return localSM_SECURITY_VALUE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_VALUE_LD
     */
    public void setSM_SECURITY_VALUE_LD(java.lang.String param) {
        localSM_SECURITY_VALUE_LDTracker = param != null;

        this.localSM_SECURITY_VALUE_LD = param;
    }

    public boolean isSM_DATE_OF_VALUE_LDSpecified() {
        return localSM_DATE_OF_VALUE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DATE_OF_VALUE_LD() {
        return localSM_DATE_OF_VALUE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_DATE_OF_VALUE_LD
     */
    public void setSM_DATE_OF_VALUE_LD(java.lang.String param) {
        localSM_DATE_OF_VALUE_LDTracker = param != null;

        this.localSM_DATE_OF_VALUE_LD = param;
    }

    public boolean isSM_SECURITY_CHARGE_LDSpecified() {
        return localSM_SECURITY_CHARGE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_CHARGE_LD() {
        return localSM_SECURITY_CHARGE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_CHARGE_LD
     */
    public void setSM_SECURITY_CHARGE_LD(java.lang.String param) {
        localSM_SECURITY_CHARGE_LDTracker = param != null;

        this.localSM_SECURITY_CHARGE_LD = param;
    }

    public boolean isSM_PROPERTY_ADDRESS_LDSpecified() {
        return localSM_PROPERTY_ADDRESS_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_PROPERTY_ADDRESS_LD() {
        return localSM_PROPERTY_ADDRESS_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_PROPERTY_ADDRESS_LD
     */
    public void setSM_PROPERTY_ADDRESS_LD(java.lang.String param) {
        localSM_PROPERTY_ADDRESS_LDTracker = param != null;

        this.localSM_PROPERTY_ADDRESS_LD = param;
    }

    public boolean isSM_AUTOMOBILE_TYPE_LDSpecified() {
        return localSM_AUTOMOBILE_TYPE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_AUTOMOBILE_TYPE_LD() {
        return localSM_AUTOMOBILE_TYPE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_AUTOMOBILE_TYPE_LD
     */
    public void setSM_AUTOMOBILE_TYPE_LD(java.lang.String param) {
        localSM_AUTOMOBILE_TYPE_LDTracker = param != null;

        this.localSM_AUTOMOBILE_TYPE_LD = param;
    }

    public boolean isSM_YEAR_OF_MANUFACTURE_LDSpecified() {
        return localSM_YEAR_OF_MANUFACTURE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_YEAR_OF_MANUFACTURE_LD() {
        return localSM_YEAR_OF_MANUFACTURE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_YEAR_OF_MANUFACTURE_LD
     */
    public void setSM_YEAR_OF_MANUFACTURE_LD(java.lang.String param) {
        localSM_YEAR_OF_MANUFACTURE_LDTracker = param != null;

        this.localSM_YEAR_OF_MANUFACTURE_LD = param;
    }

    public boolean isSM_REGISTRATION_NUMBER_LDSpecified() {
        return localSM_REGISTRATION_NUMBER_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_REGISTRATION_NUMBER_LD() {
        return localSM_REGISTRATION_NUMBER_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_REGISTRATION_NUMBER_LD
     */
    public void setSM_REGISTRATION_NUMBER_LD(java.lang.String param) {
        localSM_REGISTRATION_NUMBER_LDTracker = param != null;

        this.localSM_REGISTRATION_NUMBER_LD = param;
    }

    public boolean isSM_ENGINE_NUMBER_LDSpecified() {
        return localSM_ENGINE_NUMBER_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ENGINE_NUMBER_LD() {
        return localSM_ENGINE_NUMBER_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_ENGINE_NUMBER_LD
     */
    public void setSM_ENGINE_NUMBER_LD(java.lang.String param) {
        localSM_ENGINE_NUMBER_LDTracker = param != null;

        this.localSM_ENGINE_NUMBER_LD = param;
    }

    public boolean isSM_CHASIS_NUMBER_LDSpecified() {
        return localSM_CHASIS_NUMBER_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CHASIS_NUMBER_LD() {
        return localSM_CHASIS_NUMBER_LD;
    }

    /**
     * Auto generated setter method
     * @param param SM_CHASIS_NUMBER_LD
     */
    public void setSM_CHASIS_NUMBER_LD(java.lang.String param) {
        localSM_CHASIS_NUMBER_LDTracker = param != null;

        this.localSM_CHASIS_NUMBER_LD = param;
    }

    public boolean isSM_LINKED_ACCT_NUMBERSpecified() {
        return localSM_LINKED_ACCT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ACCT_NUMBER() {
        return localSM_LINKED_ACCT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ACCT_NUMBER
     */
    public void setSM_LINKED_ACCT_NUMBER(java.lang.String param) {
        localSM_LINKED_ACCT_NUMBERTracker = param != null;

        this.localSM_LINKED_ACCT_NUMBER = param;
    }

    public boolean isSM_LINKED_CREDIT_GUARANTORSpecified() {
        return localSM_LINKED_CREDIT_GUARANTORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_CREDIT_GUARANTOR() {
        return localSM_LINKED_CREDIT_GUARANTOR;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_CREDIT_GUARANTOR
     */
    public void setSM_LINKED_CREDIT_GUARANTOR(java.lang.String param) {
        localSM_LINKED_CREDIT_GUARANTORTracker = param != null;

        this.localSM_LINKED_CREDIT_GUARANTOR = param;
    }

    public boolean isSM_LINKED_ACCT_TYPESpecified() {
        return localSM_LINKED_ACCT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ACCT_TYPE() {
        return localSM_LINKED_ACCT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ACCT_TYPE
     */
    public void setSM_LINKED_ACCT_TYPE(java.lang.String param) {
        localSM_LINKED_ACCT_TYPETracker = param != null;

        this.localSM_LINKED_ACCT_TYPE = param;
    }

    public boolean isSM_LINKED_DATE_REPORTEDSpecified() {
        return localSM_LINKED_DATE_REPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_DATE_REPORTED() {
        return localSM_LINKED_DATE_REPORTED;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_DATE_REPORTED
     */
    public void setSM_LINKED_DATE_REPORTED(java.lang.String param) {
        localSM_LINKED_DATE_REPORTEDTracker = param != null;

        this.localSM_LINKED_DATE_REPORTED = param;
    }

    public boolean isSM_LINKED_OWNERSHIP_INDSpecified() {
        return localSM_LINKED_OWNERSHIP_INDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_OWNERSHIP_IND() {
        return localSM_LINKED_OWNERSHIP_IND;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_OWNERSHIP_IND
     */
    public void setSM_LINKED_OWNERSHIP_IND(java.lang.String param) {
        localSM_LINKED_OWNERSHIP_INDTracker = param != null;

        this.localSM_LINKED_OWNERSHIP_IND = param;
    }

    public boolean isSM_LINKED_ACCOUNT_STATUSSpecified() {
        return localSM_LINKED_ACCOUNT_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ACCOUNT_STATUS() {
        return localSM_LINKED_ACCOUNT_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ACCOUNT_STATUS
     */
    public void setSM_LINKED_ACCOUNT_STATUS(java.lang.String param) {
        localSM_LINKED_ACCOUNT_STATUSTracker = param != null;

        this.localSM_LINKED_ACCOUNT_STATUS = param;
    }

    public boolean isSM_LINKED_DISBURSED_AMTSpecified() {
        return localSM_LINKED_DISBURSED_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_DISBURSED_AMT() {
        return localSM_LINKED_DISBURSED_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_DISBURSED_AMT
     */
    public void setSM_LINKED_DISBURSED_AMT(java.lang.String param) {
        localSM_LINKED_DISBURSED_AMTTracker = param != null;

        this.localSM_LINKED_DISBURSED_AMT = param;
    }

    public boolean isSM_LINKED_DISBURSED_DTSpecified() {
        return localSM_LINKED_DISBURSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_DISBURSED_DT() {
        return localSM_LINKED_DISBURSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_DISBURSED_DT
     */
    public void setSM_LINKED_DISBURSED_DT(java.lang.String param) {
        localSM_LINKED_DISBURSED_DTTracker = param != null;

        this.localSM_LINKED_DISBURSED_DT = param;
    }

    public boolean isSM_LINKED_LAST_PAYMENT_DATESpecified() {
        return localSM_LINKED_LAST_PAYMENT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_LAST_PAYMENT_DATE() {
        return localSM_LINKED_LAST_PAYMENT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_LAST_PAYMENT_DATE
     */
    public void setSM_LINKED_LAST_PAYMENT_DATE(java.lang.String param) {
        localSM_LINKED_LAST_PAYMENT_DATETracker = param != null;

        this.localSM_LINKED_LAST_PAYMENT_DATE = param;
    }

    public boolean isSM_LINKED_CLOSED_DATESpecified() {
        return localSM_LINKED_CLOSED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_CLOSED_DATE() {
        return localSM_LINKED_CLOSED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_CLOSED_DATE
     */
    public void setSM_LINKED_CLOSED_DATE(java.lang.String param) {
        localSM_LINKED_CLOSED_DATETracker = param != null;

        this.localSM_LINKED_CLOSED_DATE = param;
    }

    public boolean isSM_LINKED_INSTALLMENT_AMTSpecified() {
        return localSM_LINKED_INSTALLMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_INSTALLMENT_AMT() {
        return localSM_LINKED_INSTALLMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_INSTALLMENT_AMT
     */
    public void setSM_LINKED_INSTALLMENT_AMT(java.lang.String param) {
        localSM_LINKED_INSTALLMENT_AMTTracker = param != null;

        this.localSM_LINKED_INSTALLMENT_AMT = param;
    }

    public boolean isSM_LINKED_OVERDUE_AMTSpecified() {
        return localSM_LINKED_OVERDUE_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_OVERDUE_AMT() {
        return localSM_LINKED_OVERDUE_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_OVERDUE_AMT
     */
    public void setSM_LINKED_OVERDUE_AMT(java.lang.String param) {
        localSM_LINKED_OVERDUE_AMTTracker = param != null;

        this.localSM_LINKED_OVERDUE_AMT = param;
    }

    public boolean isSM_LINKED_WRITE_OFF_AMTSpecified() {
        return localSM_LINKED_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_WRITE_OFF_AMT() {
        return localSM_LINKED_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_WRITE_OFF_AMT
     */
    public void setSM_LINKED_WRITE_OFF_AMT(java.lang.String param) {
        localSM_LINKED_WRITE_OFF_AMTTracker = param != null;

        this.localSM_LINKED_WRITE_OFF_AMT = param;
    }

    public boolean isSM_LINKED_CURRENT_BALSpecified() {
        return localSM_LINKED_CURRENT_BALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_CURRENT_BAL() {
        return localSM_LINKED_CURRENT_BAL;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_CURRENT_BAL
     */
    public void setSM_LINKED_CURRENT_BAL(java.lang.String param) {
        localSM_LINKED_CURRENT_BALTracker = param != null;

        this.localSM_LINKED_CURRENT_BAL = param;
    }

    public boolean isSM_LINKED_CREDIT_LIMITSpecified() {
        return localSM_LINKED_CREDIT_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_CREDIT_LIMIT() {
        return localSM_LINKED_CREDIT_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_CREDIT_LIMIT
     */
    public void setSM_LINKED_CREDIT_LIMIT(java.lang.String param) {
        localSM_LINKED_CREDIT_LIMITTracker = param != null;

        this.localSM_LINKED_CREDIT_LIMIT = param;
    }

    public boolean isSM_LINKED_ACCOUNT_REMARKSSpecified() {
        return localSM_LINKED_ACCOUNT_REMARKSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ACCOUNT_REMARKS() {
        return localSM_LINKED_ACCOUNT_REMARKS;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ACCOUNT_REMARKS
     */
    public void setSM_LINKED_ACCOUNT_REMARKS(java.lang.String param) {
        localSM_LINKED_ACCOUNT_REMARKSTracker = param != null;

        this.localSM_LINKED_ACCOUNT_REMARKS = param;
    }

    public boolean isSM_LINKED_FREQUENCYSpecified() {
        return localSM_LINKED_FREQUENCYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_FREQUENCY() {
        return localSM_LINKED_FREQUENCY;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_FREQUENCY
     */
    public void setSM_LINKED_FREQUENCY(java.lang.String param) {
        localSM_LINKED_FREQUENCYTracker = param != null;

        this.localSM_LINKED_FREQUENCY = param;
    }

    public boolean isSM_LINKED_SECURITY_STATUSSpecified() {
        return localSM_LINKED_SECURITY_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_SECURITY_STATUS() {
        return localSM_LINKED_SECURITY_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_SECURITY_STATUS
     */
    public void setSM_LINKED_SECURITY_STATUS(java.lang.String param) {
        localSM_LINKED_SECURITY_STATUSTracker = param != null;

        this.localSM_LINKED_SECURITY_STATUS = param;
    }

    public boolean isSM_LINKED_ORIGINAL_TERMSpecified() {
        return localSM_LINKED_ORIGINAL_TERMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ORIGINAL_TERM() {
        return localSM_LINKED_ORIGINAL_TERM;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ORIGINAL_TERM
     */
    public void setSM_LINKED_ORIGINAL_TERM(java.lang.String param) {
        localSM_LINKED_ORIGINAL_TERMTracker = param != null;

        this.localSM_LINKED_ORIGINAL_TERM = param;
    }

    public boolean isSM_LINKED_TERM_TO_MATURITYSpecified() {
        return localSM_LINKED_TERM_TO_MATURITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_TERM_TO_MATURITY() {
        return localSM_LINKED_TERM_TO_MATURITY;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_TERM_TO_MATURITY
     */
    public void setSM_LINKED_TERM_TO_MATURITY(java.lang.String param) {
        localSM_LINKED_TERM_TO_MATURITYTracker = param != null;

        this.localSM_LINKED_TERM_TO_MATURITY = param;
    }

    public boolean isSM_LINKED_ACCT_IN_DISPUTESpecified() {
        return localSM_LINKED_ACCT_IN_DISPUTETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ACCT_IN_DISPUTE() {
        return localSM_LINKED_ACCT_IN_DISPUTE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ACCT_IN_DISPUTE
     */
    public void setSM_LINKED_ACCT_IN_DISPUTE(java.lang.String param) {
        localSM_LINKED_ACCT_IN_DISPUTETracker = param != null;

        this.localSM_LINKED_ACCT_IN_DISPUTE = param;
    }

    public boolean isSM_LINKED_SETTLEMENT_AMTSpecified() {
        return localSM_LINKED_SETTLEMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_SETTLEMENT_AMT() {
        return localSM_LINKED_SETTLEMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_SETTLEMENT_AMT
     */
    public void setSM_LINKED_SETTLEMENT_AMT(java.lang.String param) {
        localSM_LINKED_SETTLEMENT_AMTTracker = param != null;

        this.localSM_LINKED_SETTLEMENT_AMT = param;
    }

    public boolean isSM_LINKED_PRNPAL_WRITE_OFF_AMTSpecified() {
        return localSM_LINKED_PRNPAL_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_PRNPAL_WRITE_OFF_AMT() {
        return localSM_LINKED_PRNPAL_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_PRNPAL_WRITE_OFF_AMT
     */
    public void setSM_LINKED_PRNPAL_WRITE_OFF_AMT(java.lang.String param) {
        localSM_LINKED_PRNPAL_WRITE_OFF_AMTTracker = param != null;

        this.localSM_LINKED_PRNPAL_WRITE_OFF_AMT = param;
    }

    public boolean isSM_LINKED_REPAYMENT_TENURESpecified() {
        return localSM_LINKED_REPAYMENT_TENURETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_REPAYMENT_TENURE() {
        return localSM_LINKED_REPAYMENT_TENURE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_REPAYMENT_TENURE
     */
    public void setSM_LINKED_REPAYMENT_TENURE(java.lang.String param) {
        localSM_LINKED_REPAYMENT_TENURETracker = param != null;

        this.localSM_LINKED_REPAYMENT_TENURE = param;
    }

    public boolean isSM_LINKED_INTEREST_RATESpecified() {
        return localSM_LINKED_INTEREST_RATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_INTEREST_RATE() {
        return localSM_LINKED_INTEREST_RATE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_INTEREST_RATE
     */
    public void setSM_LINKED_INTEREST_RATE(java.lang.String param) {
        localSM_LINKED_INTEREST_RATETracker = param != null;

        this.localSM_LINKED_INTEREST_RATE = param;
    }

    public boolean isSM_LINKED_SUIT_F_WILFUL_DFLTSpecified() {
        return localSM_LINKED_SUIT_F_WILFUL_DFLTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_SUIT_F_WILFUL_DFLT() {
        return localSM_LINKED_SUIT_F_WILFUL_DFLT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_SUIT_F_WILFUL_DFLT
     */
    public void setSM_LINKED_SUIT_F_WILFUL_DFLT(java.lang.String param) {
        localSM_LINKED_SUIT_F_WILFUL_DFLTTracker = param != null;

        this.localSM_LINKED_SUIT_F_WILFUL_DFLT = param;
    }

    public boolean isSM_LINKED_WRTN_OFF_SETTLD_STATSpecified() {
        return localSM_LINKED_WRTN_OFF_SETTLD_STATTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_WRTN_OFF_SETTLD_STAT() {
        return localSM_LINKED_WRTN_OFF_SETTLD_STAT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_WRTN_OFF_SETTLD_STAT
     */
    public void setSM_LINKED_WRTN_OFF_SETTLD_STAT(java.lang.String param) {
        localSM_LINKED_WRTN_OFF_SETTLD_STATTracker = param != null;

        this.localSM_LINKED_WRTN_OFF_SETTLD_STAT = param;
    }

    public boolean isSM_LINKED_CASH_LIMITSpecified() {
        return localSM_LINKED_CASH_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_CASH_LIMIT() {
        return localSM_LINKED_CASH_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_CASH_LIMIT
     */
    public void setSM_LINKED_CASH_LIMIT(java.lang.String param) {
        localSM_LINKED_CASH_LIMITTracker = param != null;

        this.localSM_LINKED_CASH_LIMIT = param;
    }

    public boolean isSM_LINKED_ACTUAL_PAYMENTSpecified() {
        return localSM_LINKED_ACTUAL_PAYMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LINKED_ACTUAL_PAYMENT() {
        return localSM_LINKED_ACTUAL_PAYMENT;
    }

    /**
     * Auto generated setter method
     * @param param SM_LINKED_ACTUAL_PAYMENT
     */
    public void setSM_LINKED_ACTUAL_PAYMENT(java.lang.String param) {
        localSM_LINKED_ACTUAL_PAYMENTTracker = param != null;

        this.localSM_LINKED_ACTUAL_PAYMENT = param;
    }

    public boolean isSM_SECURITY_TYPE_LASpecified() {
        return localSM_SECURITY_TYPE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_TYPE_LA() {
        return localSM_SECURITY_TYPE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_TYPE_LA
     */
    public void setSM_SECURITY_TYPE_LA(java.lang.String param) {
        localSM_SECURITY_TYPE_LATracker = param != null;

        this.localSM_SECURITY_TYPE_LA = param;
    }

    public boolean isSM_OWNER_NAME_LASpecified() {
        return localSM_OWNER_NAME_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_OWNER_NAME_LA() {
        return localSM_OWNER_NAME_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_OWNER_NAME_LA
     */
    public void setSM_OWNER_NAME_LA(java.lang.String param) {
        localSM_OWNER_NAME_LATracker = param != null;

        this.localSM_OWNER_NAME_LA = param;
    }

    public boolean isSM_SECURITY_VALUE_LASpecified() {
        return localSM_SECURITY_VALUE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_VALUE_LA() {
        return localSM_SECURITY_VALUE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_VALUE_LA
     */
    public void setSM_SECURITY_VALUE_LA(java.lang.String param) {
        localSM_SECURITY_VALUE_LATracker = param != null;

        this.localSM_SECURITY_VALUE_LA = param;
    }

    public boolean isSM_DATE_OF_VALUE_LASpecified() {
        return localSM_DATE_OF_VALUE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DATE_OF_VALUE_LA() {
        return localSM_DATE_OF_VALUE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_DATE_OF_VALUE_LA
     */
    public void setSM_DATE_OF_VALUE_LA(java.lang.String param) {
        localSM_DATE_OF_VALUE_LATracker = param != null;

        this.localSM_DATE_OF_VALUE_LA = param;
    }

    public boolean isSM_SECURITY_CHARGE_LASpecified() {
        return localSM_SECURITY_CHARGE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_SECURITY_CHARGE_LA() {
        return localSM_SECURITY_CHARGE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_SECURITY_CHARGE_LA
     */
    public void setSM_SECURITY_CHARGE_LA(java.lang.String param) {
        localSM_SECURITY_CHARGE_LATracker = param != null;

        this.localSM_SECURITY_CHARGE_LA = param;
    }

    public boolean isSM_PROPERTY_ADDRESS_LASpecified() {
        return localSM_PROPERTY_ADDRESS_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_PROPERTY_ADDRESS_LA() {
        return localSM_PROPERTY_ADDRESS_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_PROPERTY_ADDRESS_LA
     */
    public void setSM_PROPERTY_ADDRESS_LA(java.lang.String param) {
        localSM_PROPERTY_ADDRESS_LATracker = param != null;

        this.localSM_PROPERTY_ADDRESS_LA = param;
    }

    public boolean isSM_AUTOMOBILE_TYPE_LASpecified() {
        return localSM_AUTOMOBILE_TYPE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_AUTOMOBILE_TYPE_LA() {
        return localSM_AUTOMOBILE_TYPE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_AUTOMOBILE_TYPE_LA
     */
    public void setSM_AUTOMOBILE_TYPE_LA(java.lang.String param) {
        localSM_AUTOMOBILE_TYPE_LATracker = param != null;

        this.localSM_AUTOMOBILE_TYPE_LA = param;
    }

    public boolean isSM_YEAR_OF_MANUFACTURE_LASpecified() {
        return localSM_YEAR_OF_MANUFACTURE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_YEAR_OF_MANUFACTURE_LA() {
        return localSM_YEAR_OF_MANUFACTURE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_YEAR_OF_MANUFACTURE_LA
     */
    public void setSM_YEAR_OF_MANUFACTURE_LA(java.lang.String param) {
        localSM_YEAR_OF_MANUFACTURE_LATracker = param != null;

        this.localSM_YEAR_OF_MANUFACTURE_LA = param;
    }

    public boolean isSM_REGISTRATION_NUMBER_LASpecified() {
        return localSM_REGISTRATION_NUMBER_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_REGISTRATION_NUMBER_LA() {
        return localSM_REGISTRATION_NUMBER_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_REGISTRATION_NUMBER_LA
     */
    public void setSM_REGISTRATION_NUMBER_LA(java.lang.String param) {
        localSM_REGISTRATION_NUMBER_LATracker = param != null;

        this.localSM_REGISTRATION_NUMBER_LA = param;
    }

    public boolean isSM_ENGINE_NUMBER_LASpecified() {
        return localSM_ENGINE_NUMBER_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ENGINE_NUMBER_LA() {
        return localSM_ENGINE_NUMBER_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_ENGINE_NUMBER_LA
     */
    public void setSM_ENGINE_NUMBER_LA(java.lang.String param) {
        localSM_ENGINE_NUMBER_LATracker = param != null;

        this.localSM_ENGINE_NUMBER_LA = param;
    }

    public boolean isSM_CHASIS_NUMBER_LASpecified() {
        return localSM_CHASIS_NUMBER_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CHASIS_NUMBER_LA() {
        return localSM_CHASIS_NUMBER_LA;
    }

    /**
     * Auto generated setter method
     * @param param SM_CHASIS_NUMBER_LA
     */
    public void setSM_CHASIS_NUMBER_LA(java.lang.String param) {
        localSM_CHASIS_NUMBER_LATracker = param != null;

        this.localSM_CHASIS_NUMBER_LA = param;
    }

    public boolean isSCORE_TYPESpecified() {
        return localSCORE_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_TYPE() {
        return localSCORE_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_TYPE
     */
    public void setSCORE_TYPE(java.lang.String param) {
        localSCORE_TYPETracker = param != null;

        this.localSCORE_TYPE = param;
    }

    public boolean isSCORE_VERSIONSpecified() {
        return localSCORE_VERSIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_VERSION() {
        return localSCORE_VERSION;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_VERSION
     */
    public void setSCORE_VERSION(java.lang.String param) {
        localSCORE_VERSIONTracker = param != null;

        this.localSCORE_VERSION = param;
    }

    public boolean isSCORE_VALUESpecified() {
        return localSCORE_VALUETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_VALUE() {
        return localSCORE_VALUE;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_VALUE
     */
    public void setSCORE_VALUE(java.lang.String param) {
        localSCORE_VALUETracker = param != null;

        this.localSCORE_VALUE = param;
    }

    public boolean isSCORE_FACTORSSpecified() {
        return localSCORE_FACTORSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_FACTORS() {
        return localSCORE_FACTORS;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_FACTORS
     */
    public void setSCORE_FACTORS(java.lang.String param) {
        localSCORE_FACTORSTracker = param != null;

        this.localSCORE_FACTORS = param;
    }

    public boolean isSCORE_COMMENTSSpecified() {
        return localSCORE_COMMENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_COMMENTS() {
        return localSCORE_COMMENTS;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_COMMENTS
     */
    public void setSCORE_COMMENTS(java.lang.String param) {
        localSCORE_COMMENTSTracker = param != null;

        this.localSCORE_COMMENTS = param;
    }

    public boolean isMEMBER_NAMESpecified() {
        return localMEMBER_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_NAME() {
        return localMEMBER_NAME;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_NAME
     */
    public void setMEMBER_NAME(java.lang.String param) {
        localMEMBER_NAMETracker = param != null;

        this.localMEMBER_NAME = param;
    }

    public boolean isINQUIRY_DATESpecified() {
        return localINQUIRY_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINQUIRY_DATE() {
        return localINQUIRY_DATE;
    }

    /**
     * Auto generated setter method
     * @param param INQUIRY_DATE
     */
    public void setINQUIRY_DATE(java.lang.String param) {
        localINQUIRY_DATETracker = param != null;

        this.localINQUIRY_DATE = param;
    }

    public boolean isPURPOSESpecified() {
        return localPURPOSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPURPOSE() {
        return localPURPOSE;
    }

    /**
     * Auto generated setter method
     * @param param PURPOSE
     */
    public void setPURPOSE(java.lang.String param) {
        localPURPOSETracker = param != null;

        this.localPURPOSE = param;
    }

    public boolean isOWNERSHIP_TYPESpecified() {
        return localOWNERSHIP_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNERSHIP_TYPE() {
        return localOWNERSHIP_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param OWNERSHIP_TYPE
     */
    public void setOWNERSHIP_TYPE(java.lang.String param) {
        localOWNERSHIP_TYPETracker = param != null;

        this.localOWNERSHIP_TYPE = param;
    }

    public boolean isAMOUNTSpecified() {
        return localAMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNT() {
        return localAMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNT
     */
    public void setAMOUNT(java.lang.String param) {
        localAMOUNTTracker = param != null;

        this.localAMOUNT = param;
    }

    public boolean isREMARKSpecified() {
        return localREMARKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREMARK() {
        return localREMARK;
    }

    /**
     * Auto generated setter method
     * @param param REMARK
     */
    public void setREMARK(java.lang.String param) {
        localREMARKTracker = param != null;

        this.localREMARK = param;
    }

    public boolean isCOMMENT_TEXTSpecified() {
        return localCOMMENT_TEXTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOMMENT_TEXT() {
        return localCOMMENT_TEXT;
    }

    /**
     * Auto generated setter method
     * @param param COMMENT_TEXT
     */
    public void setCOMMENT_TEXT(java.lang.String param) {
        localCOMMENT_TEXTTracker = param != null;

        this.localCOMMENT_TEXT = param;
    }

    public boolean isCOMMENT_DATESpecified() {
        return localCOMMENT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOMMENT_DATE() {
        return localCOMMENT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param COMMENT_DATE
     */
    public void setCOMMENT_DATE(java.lang.String param) {
        localCOMMENT_DATETracker = param != null;

        this.localCOMMENT_DATE = param;
    }

    public boolean isBUREAU_COMMENTSpecified() {
        return localBUREAU_COMMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBUREAU_COMMENT() {
        return localBUREAU_COMMENT;
    }

    /**
     * Auto generated setter method
     * @param param BUREAU_COMMENT
     */
    public void setBUREAU_COMMENT(java.lang.String param) {
        localBUREAU_COMMENTTracker = param != null;

        this.localBUREAU_COMMENT = param;
    }

    public boolean isALERT_TYPESpecified() {
        return localALERT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALERT_TYPE() {
        return localALERT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param ALERT_TYPE
     */
    public void setALERT_TYPE(java.lang.String param) {
        localALERT_TYPETracker = param != null;

        this.localALERT_TYPE = param;
    }

    public boolean isALERT_DESCSpecified() {
        return localALERT_DESCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALERT_DESC() {
        return localALERT_DESC;
    }

    /**
     * Auto generated setter method
     * @param param ALERT_DESC
     */
    public void setALERT_DESC(java.lang.String param) {
        localALERT_DESCTracker = param != null;

        this.localALERT_DESC = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_TIMESpecified() {
        return localOUTPUT_WRITE_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_TIME() {
        return localOUTPUT_WRITE_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_TIME
     */
    public void setOUTPUT_WRITE_TIME(java.lang.String param) {
        localOUTPUT_WRITE_TIMETracker = param != null;

        this.localOUTPUT_WRITE_TIME = param;
    }

    public boolean isOUTPUT_READ_TIMESpecified() {
        return localOUTPUT_READ_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_TIME() {
        return localOUTPUT_READ_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_TIME
     */
    public void setOUTPUT_READ_TIME(java.lang.String param) {
        localOUTPUT_READ_TIMETracker = param != null;

        this.localOUTPUT_READ_TIME = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ChmbaseSRespType2", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ChmbaseSRespType2", xmlWriter);
            }
        }

        if (localSM_SECURITY_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_STATUS", xmlWriter);

            if (localSM_SECURITY_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ORIGINAL_TERMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ORIGINAL_TERM", xmlWriter);

            if (localSM_ORIGINAL_TERM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ORIGINAL_TERM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ORIGINAL_TERM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_TERM_TO_MATURITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_TERM_TO_MATURITY", xmlWriter);

            if (localSM_TERM_TO_MATURITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_TERM_TO_MATURITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_TERM_TO_MATURITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ACCT_IN_DISPUTETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ACCT_IN_DISPUTE", xmlWriter);

            if (localSM_ACCT_IN_DISPUTE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ACCT_IN_DISPUTE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ACCT_IN_DISPUTE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SETTLEMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SETTLEMENT_AMT", xmlWriter);

            if (localSM_SETTLEMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SETTLEMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SETTLEMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_PRINCIPAL_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_PRINCIPAL_WRITE_OFF_AMT",
                xmlWriter);

            if (localSM_PRINCIPAL_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_PRINCIPAL_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_PRINCIPAL_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_COMBINED_PAYMENT_HISTORYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_COMBINED_PAYMENT_HISTORY",
                xmlWriter);

            if (localSM_COMBINED_PAYMENT_HISTORY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_COMBINED_PAYMENT_HISTORY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_COMBINED_PAYMENT_HISTORY);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_REPAYMENT_TENURETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_REPAYMENT_TENURE", xmlWriter);

            if (localSM_REPAYMENT_TENURE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_REPAYMENT_TENURE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_REPAYMENT_TENURE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_INTEREST_RATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_INTEREST_RATE", xmlWriter);

            if (localSM_INTEREST_RATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_INTEREST_RATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_INTEREST_RATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SUIT_FILED_WILFUL_DEFAULTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SUIT_FILED_WILFUL_DEFAULT",
                xmlWriter);

            if (localSM_SUIT_FILED_WILFUL_DEFAULT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SUIT_FILED_WILFUL_DEFAULT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SUIT_FILED_WILFUL_DEFAULT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_WRITTEN_OFF_SETTLED_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_WRITTEN_OFF_SETTLED_STATUS",
                xmlWriter);

            if (localSM_WRITTEN_OFF_SETTLED_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_WRITTEN_OFF_SETTLED_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_WRITTEN_OFF_SETTLED_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CASH_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CASH_LIMIT", xmlWriter);

            if (localSM_CASH_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CASH_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CASH_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ACTUAL_PAYMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ACTUAL_PAYMENT", xmlWriter);

            if (localSM_ACTUAL_PAYMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ACTUAL_PAYMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ACTUAL_PAYMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SECURITY_TYPE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_TYPE_LD", xmlWriter);

            if (localSM_SECURITY_TYPE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_TYPE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_TYPE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_OWNER_NAME_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_OWNER_NAME_LD", xmlWriter);

            if (localSM_OWNER_NAME_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_OWNER_NAME_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_OWNER_NAME_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SECURITY_VALUE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_VALUE_LD", xmlWriter);

            if (localSM_SECURITY_VALUE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_VALUE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_VALUE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DATE_OF_VALUE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DATE_OF_VALUE_LD", xmlWriter);

            if (localSM_DATE_OF_VALUE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DATE_OF_VALUE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DATE_OF_VALUE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SECURITY_CHARGE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_CHARGE_LD",
                xmlWriter);

            if (localSM_SECURITY_CHARGE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_CHARGE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_CHARGE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_PROPERTY_ADDRESS_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_PROPERTY_ADDRESS_LD",
                xmlWriter);

            if (localSM_PROPERTY_ADDRESS_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_PROPERTY_ADDRESS_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_PROPERTY_ADDRESS_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_AUTOMOBILE_TYPE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_AUTOMOBILE_TYPE_LD",
                xmlWriter);

            if (localSM_AUTOMOBILE_TYPE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_AUTOMOBILE_TYPE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_AUTOMOBILE_TYPE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_YEAR_OF_MANUFACTURE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_YEAR_OF_MANUFACTURE_LD",
                xmlWriter);

            if (localSM_YEAR_OF_MANUFACTURE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_YEAR_OF_MANUFACTURE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_YEAR_OF_MANUFACTURE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_REGISTRATION_NUMBER_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_REGISTRATION_NUMBER_LD",
                xmlWriter);

            if (localSM_REGISTRATION_NUMBER_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_REGISTRATION_NUMBER_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_REGISTRATION_NUMBER_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ENGINE_NUMBER_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ENGINE_NUMBER_LD", xmlWriter);

            if (localSM_ENGINE_NUMBER_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ENGINE_NUMBER_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ENGINE_NUMBER_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CHASIS_NUMBER_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CHASIS_NUMBER_LD", xmlWriter);

            if (localSM_CHASIS_NUMBER_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CHASIS_NUMBER_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CHASIS_NUMBER_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ACCT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ACCT_NUMBER",
                xmlWriter);

            if (localSM_LINKED_ACCT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ACCT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ACCT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_CREDIT_GUARANTORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_CREDIT_GUARANTOR",
                xmlWriter);

            if (localSM_LINKED_CREDIT_GUARANTOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_CREDIT_GUARANTOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_CREDIT_GUARANTOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ACCT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ACCT_TYPE", xmlWriter);

            if (localSM_LINKED_ACCT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ACCT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ACCT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_DATE_REPORTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_DATE_REPORTED",
                xmlWriter);

            if (localSM_LINKED_DATE_REPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_DATE_REPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_DATE_REPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_OWNERSHIP_INDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_OWNERSHIP_IND",
                xmlWriter);

            if (localSM_LINKED_OWNERSHIP_IND == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_OWNERSHIP_IND cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_OWNERSHIP_IND);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ACCOUNT_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ACCOUNT_STATUS",
                xmlWriter);

            if (localSM_LINKED_ACCOUNT_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ACCOUNT_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ACCOUNT_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_DISBURSED_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_DISBURSED_AMT",
                xmlWriter);

            if (localSM_LINKED_DISBURSED_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_DISBURSED_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_DISBURSED_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_DISBURSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_DISBURSED_DT",
                xmlWriter);

            if (localSM_LINKED_DISBURSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_DISBURSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_DISBURSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_LAST_PAYMENT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_LAST_PAYMENT_DATE",
                xmlWriter);

            if (localSM_LINKED_LAST_PAYMENT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_LAST_PAYMENT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_LAST_PAYMENT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_CLOSED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_CLOSED_DATE",
                xmlWriter);

            if (localSM_LINKED_CLOSED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_CLOSED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_CLOSED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_INSTALLMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_INSTALLMENT_AMT",
                xmlWriter);

            if (localSM_LINKED_INSTALLMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_INSTALLMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_INSTALLMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_OVERDUE_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_OVERDUE_AMT",
                xmlWriter);

            if (localSM_LINKED_OVERDUE_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_OVERDUE_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_OVERDUE_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_WRITE_OFF_AMT",
                xmlWriter);

            if (localSM_LINKED_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_CURRENT_BALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_CURRENT_BAL",
                xmlWriter);

            if (localSM_LINKED_CURRENT_BAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_CURRENT_BAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_CURRENT_BAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_CREDIT_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_CREDIT_LIMIT",
                xmlWriter);

            if (localSM_LINKED_CREDIT_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_CREDIT_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_CREDIT_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ACCOUNT_REMARKSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ACCOUNT_REMARKS",
                xmlWriter);

            if (localSM_LINKED_ACCOUNT_REMARKS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ACCOUNT_REMARKS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ACCOUNT_REMARKS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_FREQUENCYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_FREQUENCY", xmlWriter);

            if (localSM_LINKED_FREQUENCY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_FREQUENCY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_FREQUENCY);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_SECURITY_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_SECURITY_STATUS",
                xmlWriter);

            if (localSM_LINKED_SECURITY_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_SECURITY_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_SECURITY_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ORIGINAL_TERMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ORIGINAL_TERM",
                xmlWriter);

            if (localSM_LINKED_ORIGINAL_TERM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ORIGINAL_TERM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ORIGINAL_TERM);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_TERM_TO_MATURITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_TERM_TO_MATURITY",
                xmlWriter);

            if (localSM_LINKED_TERM_TO_MATURITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_TERM_TO_MATURITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_TERM_TO_MATURITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ACCT_IN_DISPUTETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ACCT_IN_DISPUTE",
                xmlWriter);

            if (localSM_LINKED_ACCT_IN_DISPUTE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ACCT_IN_DISPUTE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ACCT_IN_DISPUTE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_SETTLEMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_SETTLEMENT_AMT",
                xmlWriter);

            if (localSM_LINKED_SETTLEMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_SETTLEMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_SETTLEMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_PRNPAL_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "SM_LINKED_PRNPAL_WRITE_OFF_AMT", xmlWriter);

            if (localSM_LINKED_PRNPAL_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_PRNPAL_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_PRNPAL_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_REPAYMENT_TENURETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_REPAYMENT_TENURE",
                xmlWriter);

            if (localSM_LINKED_REPAYMENT_TENURE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_REPAYMENT_TENURE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_REPAYMENT_TENURE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_INTEREST_RATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_INTEREST_RATE",
                xmlWriter);

            if (localSM_LINKED_INTEREST_RATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_INTEREST_RATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_INTEREST_RATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_SUIT_F_WILFUL_DFLTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_SUIT_F_WILFUL_DFLT",
                xmlWriter);

            if (localSM_LINKED_SUIT_F_WILFUL_DFLT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_SUIT_F_WILFUL_DFLT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_SUIT_F_WILFUL_DFLT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_WRTN_OFF_SETTLD_STATTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "SM_LINKED_WRTN_OFF_SETTLD_STAT", xmlWriter);

            if (localSM_LINKED_WRTN_OFF_SETTLD_STAT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_WRTN_OFF_SETTLD_STAT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_WRTN_OFF_SETTLD_STAT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_CASH_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_CASH_LIMIT", xmlWriter);

            if (localSM_LINKED_CASH_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_CASH_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_CASH_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LINKED_ACTUAL_PAYMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LINKED_ACTUAL_PAYMENT",
                xmlWriter);

            if (localSM_LINKED_ACTUAL_PAYMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LINKED_ACTUAL_PAYMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LINKED_ACTUAL_PAYMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SECURITY_TYPE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_TYPE_LA", xmlWriter);

            if (localSM_SECURITY_TYPE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_TYPE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_TYPE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_OWNER_NAME_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_OWNER_NAME_LA", xmlWriter);

            if (localSM_OWNER_NAME_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_OWNER_NAME_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_OWNER_NAME_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SECURITY_VALUE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_VALUE_LA", xmlWriter);

            if (localSM_SECURITY_VALUE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_VALUE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_VALUE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DATE_OF_VALUE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DATE_OF_VALUE_LA", xmlWriter);

            if (localSM_DATE_OF_VALUE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DATE_OF_VALUE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DATE_OF_VALUE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_SECURITY_CHARGE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_SECURITY_CHARGE_LA",
                xmlWriter);

            if (localSM_SECURITY_CHARGE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_SECURITY_CHARGE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_SECURITY_CHARGE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_PROPERTY_ADDRESS_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_PROPERTY_ADDRESS_LA",
                xmlWriter);

            if (localSM_PROPERTY_ADDRESS_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_PROPERTY_ADDRESS_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_PROPERTY_ADDRESS_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_AUTOMOBILE_TYPE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_AUTOMOBILE_TYPE_LA",
                xmlWriter);

            if (localSM_AUTOMOBILE_TYPE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_AUTOMOBILE_TYPE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_AUTOMOBILE_TYPE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_YEAR_OF_MANUFACTURE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_YEAR_OF_MANUFACTURE_LA",
                xmlWriter);

            if (localSM_YEAR_OF_MANUFACTURE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_YEAR_OF_MANUFACTURE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_YEAR_OF_MANUFACTURE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_REGISTRATION_NUMBER_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_REGISTRATION_NUMBER_LA",
                xmlWriter);

            if (localSM_REGISTRATION_NUMBER_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_REGISTRATION_NUMBER_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_REGISTRATION_NUMBER_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ENGINE_NUMBER_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ENGINE_NUMBER_LA", xmlWriter);

            if (localSM_ENGINE_NUMBER_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ENGINE_NUMBER_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ENGINE_NUMBER_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CHASIS_NUMBER_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CHASIS_NUMBER_LA", xmlWriter);

            if (localSM_CHASIS_NUMBER_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CHASIS_NUMBER_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CHASIS_NUMBER_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_TYPE", xmlWriter);

            if (localSCORE_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_VERSIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_VERSION", xmlWriter);

            if (localSCORE_VERSION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_VERSION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_VERSION);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_VALUETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_VALUE", xmlWriter);

            if (localSCORE_VALUE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_VALUE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_VALUE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_FACTORSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_FACTORS", xmlWriter);

            if (localSCORE_FACTORS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_FACTORS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_FACTORS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_COMMENTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_COMMENTS", xmlWriter);

            if (localSCORE_COMMENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_COMMENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_COMMENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_NAME", xmlWriter);

            if (localMEMBER_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localINQUIRY_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "INQUIRY_DATE", xmlWriter);

            if (localINQUIRY_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INQUIRY_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINQUIRY_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPURPOSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PURPOSE", xmlWriter);

            if (localPURPOSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PURPOSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPURPOSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNERSHIP_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNERSHIP_TYPE", xmlWriter);

            if (localOWNERSHIP_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNERSHIP_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNERSHIP_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNT", xmlWriter);

            if (localAMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localREMARKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REMARK", xmlWriter);

            if (localREMARK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REMARK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREMARK);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOMMENT_TEXTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "COMMENT_TEXT", xmlWriter);

            if (localCOMMENT_TEXT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COMMENT_TEXT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOMMENT_TEXT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOMMENT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "COMMENT_DATE", xmlWriter);

            if (localCOMMENT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COMMENT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOMMENT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localBUREAU_COMMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BUREAU_COMMENT", xmlWriter);

            if (localBUREAU_COMMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BUREAU_COMMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBUREAU_COMMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localALERT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALERT_TYPE", xmlWriter);

            if (localALERT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALERT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALERT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localALERT_DESCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ALERT_DESC", xmlWriter);

            if (localALERT_DESC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALERT_DESC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALERT_DESC);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_TIME", xmlWriter);

            if (localOUTPUT_WRITE_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_TIME", xmlWriter);

            if (localOUTPUT_READ_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ChmbaseSRespType2 parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ChmbaseSRespType2 object = new ChmbaseSRespType2();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ChmbaseSRespType2".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ChmbaseSRespType2) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_SECURITY_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_SECURITY_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ORIGINAL_TERM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ORIGINAL_TERM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ORIGINAL_TERM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ORIGINAL_TERM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_TERM_TO_MATURITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_TERM_TO_MATURITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_TERM_TO_MATURITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_TERM_TO_MATURITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ACCT_IN_DISPUTE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ACCT_IN_DISPUTE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ACCT_IN_DISPUTE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ACCT_IN_DISPUTE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_SETTLEMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_SETTLEMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SETTLEMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SETTLEMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_PRINCIPAL_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_PRINCIPAL_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_PRINCIPAL_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_PRINCIPAL_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_COMBINED_PAYMENT_HISTORY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_COMBINED_PAYMENT_HISTORY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_COMBINED_PAYMENT_HISTORY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_COMBINED_PAYMENT_HISTORY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_REPAYMENT_TENURE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_REPAYMENT_TENURE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_REPAYMENT_TENURE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_REPAYMENT_TENURE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_INTEREST_RATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_INTEREST_RATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_INTEREST_RATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_INTEREST_RATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_SUIT_FILED_WILFUL_DEFAULT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_SUIT_FILED_WILFUL_DEFAULT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SUIT_FILED_WILFUL_DEFAULT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SUIT_FILED_WILFUL_DEFAULT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_WRITTEN_OFF_SETTLED_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_WRITTEN_OFF_SETTLED_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_WRITTEN_OFF_SETTLED_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_WRITTEN_OFF_SETTLED_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CASH_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CASH_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CASH_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CASH_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ACTUAL_PAYMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ACTUAL_PAYMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ACTUAL_PAYMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ACTUAL_PAYMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_SECURITY_TYPE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_SECURITY_TYPE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_TYPE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_TYPE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_OWNER_NAME_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_OWNER_NAME_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_OWNER_NAME_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_OWNER_NAME_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_SECURITY_VALUE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_SECURITY_VALUE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_VALUE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_VALUE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DATE_OF_VALUE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DATE_OF_VALUE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DATE_OF_VALUE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DATE_OF_VALUE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_SECURITY_CHARGE_LD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_SECURITY_CHARGE_LD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_CHARGE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_CHARGE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_PROPERTY_ADDRESS_LD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_PROPERTY_ADDRESS_LD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_PROPERTY_ADDRESS_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_PROPERTY_ADDRESS_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_AUTOMOBILE_TYPE_LD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_AUTOMOBILE_TYPE_LD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_AUTOMOBILE_TYPE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_AUTOMOBILE_TYPE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_YEAR_OF_MANUFACTURE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_YEAR_OF_MANUFACTURE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_YEAR_OF_MANUFACTURE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_YEAR_OF_MANUFACTURE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_REGISTRATION_NUMBER_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_REGISTRATION_NUMBER_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_REGISTRATION_NUMBER_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_REGISTRATION_NUMBER_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ENGINE_NUMBER_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ENGINE_NUMBER_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ENGINE_NUMBER_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ENGINE_NUMBER_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CHASIS_NUMBER_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CHASIS_NUMBER_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CHASIS_NUMBER_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CHASIS_NUMBER_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCT_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCT_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ACCT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ACCT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CREDIT_GUARANTOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CREDIT_GUARANTOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_CREDIT_GUARANTOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_CREDIT_GUARANTOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_LINKED_ACCT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_LINKED_ACCT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ACCT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ACCT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_DATE_REPORTED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_DATE_REPORTED").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_DATE_REPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_DATE_REPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_OWNERSHIP_IND").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_OWNERSHIP_IND").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_OWNERSHIP_IND" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_OWNERSHIP_IND(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCOUNT_STATUS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCOUNT_STATUS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ACCOUNT_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ACCOUNT_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_DISBURSED_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_DISBURSED_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_DISBURSED_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_DISBURSED_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_DISBURSED_DT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_DISBURSED_DT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_DISBURSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_DISBURSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_LAST_PAYMENT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_LAST_PAYMENT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_LAST_PAYMENT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_LAST_PAYMENT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CLOSED_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CLOSED_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_CLOSED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_CLOSED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_INSTALLMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_INSTALLMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_INSTALLMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_INSTALLMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_OVERDUE_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_OVERDUE_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_OVERDUE_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_OVERDUE_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_WRITE_OFF_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_WRITE_OFF_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CURRENT_BAL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CURRENT_BAL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_CURRENT_BAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_CURRENT_BAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CREDIT_LIMIT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_CREDIT_LIMIT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_CREDIT_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_CREDIT_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCOUNT_REMARKS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCOUNT_REMARKS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ACCOUNT_REMARKS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ACCOUNT_REMARKS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_LINKED_FREQUENCY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_LINKED_FREQUENCY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_FREQUENCY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_FREQUENCY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_SECURITY_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_SECURITY_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_SECURITY_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_SECURITY_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ORIGINAL_TERM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ORIGINAL_TERM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ORIGINAL_TERM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ORIGINAL_TERM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_TERM_TO_MATURITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_TERM_TO_MATURITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_TERM_TO_MATURITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_TERM_TO_MATURITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCT_IN_DISPUTE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACCT_IN_DISPUTE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ACCT_IN_DISPUTE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ACCT_IN_DISPUTE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_SETTLEMENT_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_SETTLEMENT_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_SETTLEMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_SETTLEMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_PRNPAL_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_PRNPAL_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_PRNPAL_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_PRNPAL_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_REPAYMENT_TENURE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_REPAYMENT_TENURE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_REPAYMENT_TENURE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_REPAYMENT_TENURE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_INTEREST_RATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_INTEREST_RATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_INTEREST_RATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_INTEREST_RATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_SUIT_F_WILFUL_DFLT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_SUIT_F_WILFUL_DFLT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_SUIT_F_WILFUL_DFLT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_SUIT_F_WILFUL_DFLT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_WRTN_OFF_SETTLD_STAT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_WRTN_OFF_SETTLD_STAT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_WRTN_OFF_SETTLD_STAT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_WRTN_OFF_SETTLD_STAT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_LINKED_CASH_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_LINKED_CASH_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_CASH_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_CASH_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACTUAL_PAYMENT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_LINKED_ACTUAL_PAYMENT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LINKED_ACTUAL_PAYMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LINKED_ACTUAL_PAYMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_SECURITY_TYPE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_SECURITY_TYPE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_TYPE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_TYPE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_OWNER_NAME_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_OWNER_NAME_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_OWNER_NAME_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_OWNER_NAME_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_SECURITY_VALUE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_SECURITY_VALUE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_VALUE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_VALUE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DATE_OF_VALUE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DATE_OF_VALUE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DATE_OF_VALUE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DATE_OF_VALUE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_SECURITY_CHARGE_LA").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_SECURITY_CHARGE_LA").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_SECURITY_CHARGE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_SECURITY_CHARGE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_PROPERTY_ADDRESS_LA").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_PROPERTY_ADDRESS_LA").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_PROPERTY_ADDRESS_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_PROPERTY_ADDRESS_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_AUTOMOBILE_TYPE_LA").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_AUTOMOBILE_TYPE_LA").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_AUTOMOBILE_TYPE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_AUTOMOBILE_TYPE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_YEAR_OF_MANUFACTURE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_YEAR_OF_MANUFACTURE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_YEAR_OF_MANUFACTURE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_YEAR_OF_MANUFACTURE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SM_REGISTRATION_NUMBER_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SM_REGISTRATION_NUMBER_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_REGISTRATION_NUMBER_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_REGISTRATION_NUMBER_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ENGINE_NUMBER_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ENGINE_NUMBER_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ENGINE_NUMBER_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ENGINE_NUMBER_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CHASIS_NUMBER_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CHASIS_NUMBER_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CHASIS_NUMBER_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CHASIS_NUMBER_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_VERSION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_VERSION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_VERSION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_VERSION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_VALUE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_VALUE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_VALUE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_VALUE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_FACTORS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_FACTORS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_FACTORS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_FACTORS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_COMMENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_COMMENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_COMMENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_COMMENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MEMBER_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MEMBER_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INQUIRY_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INQUIRY_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INQUIRY_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINQUIRY_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PURPOSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PURPOSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PURPOSE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPURPOSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNERSHIP_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNERSHIP_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNERSHIP_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNERSHIP_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REMARK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REMARK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REMARK" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREMARK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "COMMENT_TEXT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COMMENT_TEXT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COMMENT_TEXT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOMMENT_TEXT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "COMMENT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COMMENT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COMMENT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOMMENT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BUREAU_COMMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BUREAU_COMMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BUREAU_COMMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBUREAU_COMMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ALERT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALERT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALERT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALERT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ALERT_DESC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALERT_DESC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALERT_DESC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALERT_DESC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
