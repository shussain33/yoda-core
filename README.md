                        GoNoGo Work-flow Server

  What is it?
  -----------

  The Softcell GoNoGo work-flow manager is a powerful and flexible GoNoGo work-flow manager/0.1 compliant
  work -flow engine.  

  The Latest Version
  ------------------

 

  Documentation
  -------------

  The documentation available as of the date of this release is
  included in HTML format in the docs/manual/ directory.  The most
  up-to-date documentation can be found at
   http://gonogo.softcell.in/.

  Installation
  ------------

 

  Licensing
  ---------

  Please see the file called LICENSE.

  Cryptographic Software Notice
  -----------------------------

 
  Contacts
  --------
softcell Pvt Ltd. 

Author: Pakrhe Kishor, Yogesh Bombe. 

email: kparkhe@softcell.com 
